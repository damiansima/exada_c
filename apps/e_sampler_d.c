
#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <string.h>
#include <unistd.h>


// device | description | driver | address | regstart | regend | step
typedef struct
{
    char device[128];
    char driver[128];
    int  addres;
    int  reg_start;
    int  reg_end;
    int  reg_step;
}e_scan_modbus_rtu;

typedef struct{
    char scan[128];
    int  num_block;
    e_scan_modbus_rtu block[256];
    void *next; 
}e_scan;

e_scan *SCANS=NULL;

#define TEST(A) {printf("%-40s",#A);printf("%3i\n",A);}
#define TESTS(A,fmt) {printf("%-40s",#A);printf(fmt,A);printf("\n");}

void print_scan(e_scan *scan)
{
    int i;
    TESTS(scan->scan,"%s");
    TESTS(scan->num_block,"%i");
    for(i=0; i<scan->num_block;i++)
    {
        TESTS(scan->block[i].device,"%s");
        TESTS(scan->block[i].driver,"%s");
        TEST(scan->block[i].addres);
        TEST(scan->block[i].reg_start);
        TEST(scan->block[i].reg_end);
        TEST(scan->block[i].reg_step);
    }
}

void print_scans(void)
{
    e_scan *escan=SCANS;
    while(escan)
    {
        puts(" ");
        print_scan(escan);
        escan = (e_scan *)escan->next;
    }
}

void free_scans(void)
{
    e_scan *escan=SCANS, *next;
    
    while(escan)
    {
        next = (e_scan *)escan->next;
        printf("free %s\n",escan->scan);
        usleep(100);
        free(escan);
        escan = next;
    }
}

void scan_append(char *scan, char *device, char *driver, int addres, int reg_start, int reg_end, int reg_step)
{
    e_scan *pescan, *escan=SCANS;
    e_scan_modbus_rtu *block;
    if( !escan )
    {
        printf("-----primer scan a guardar\n");
        escan = SCANS = (e_scan *)malloc(sizeof(e_scan));
        strcpy(escan->scan, scan);
        escan->num_block=0;
    }
    else
    {
        do{
            if( strcmp(escan->scan, scan)==0 )
            {
                printf("-----añade block en scan\n");
                break;
            }
            pescan = escan;
            escan = (e_scan *)escan->next;
        }while( escan );
    } 
    if( !escan )
    {
        printf("-----agrrega un nuevo scan\n");
        pescan->next=(void *)malloc(sizeof(e_scan));
        escan=pescan->next;
        strcpy(escan->scan, scan);
        escan->num_block=0; 
        escan->next=NULL;
    }
    block = &escan->block[escan->num_block];
    strcpy(block->device,device);
    strcpy(block->driver,driver);
    block->addres=addres;
    block->reg_start=reg_start;
    block->reg_end=reg_end;
    block->reg_step=reg_step;
    escan->num_block++;
//    print_scan(escan);
    return;
}

static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    e_scan_modbus_rtu block;
    char scan[128];
    
    int i;
    for (i = 0; i < argc; i++)
    {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    
    scan_append(argv[0], argv[2], argv[3], atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), atoi(argv[7]));
    printf("\n");
    return 0;
}

int main(int    argc,
         char * argv[])
{
    sqlite3 * db;
    char *    zErrMsg = 0;
    int       rc;
    char *    sql;

    rc = sqlite3_open("../cfg/DBConfig.db", &db);
    if (rc)
    {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        exit(0);
    }

    fprintf(stdout, "Opened database successfully\n");
    
    sql = " select s.scan,s.host,s.device,d.driver,d.address,d.regstart,d.regend,d.step from scans as s join devices as d where d.device=s.device and s.host='192.168.0.2'";
    puts(sql);
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    
//    sql = " select s.scan,s.host,s.device,d.driver,d.address from scans as s join devices as d where d.device=s.device and s.host='192.168.0.5';";
//    puts(sql);
//    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
//    
//    sql = " select s.scan,s.host,s.device,d.driver,d.address from scans as s join devices as d where d.device=s.device and s.host='192.168.0.8';";
//    puts(sql);
//    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, ":( SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        fprintf(stdout, ":) Operation done successfully\n");
    }

    sqlite3_close(db);
    
    print_scans();
    free_scans();
    
    return 0;
}

