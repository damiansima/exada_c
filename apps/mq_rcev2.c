#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<errno.h>
#include<unistd.h>

#include<sys/msg.h>
#include "../include/ipc_types.h"
#include "../include/utils.h"

struct my_msg_st
{
    long int my_msg_type;
    char some_text[BUFSIZ];
};

int main()

{
    int running = 1;
    int msgid;
    struct my_msg_st some_data;
    eipc_mq msg;
    long int msg_to_receive = 0;

//    msgid = msgget( (key_t)1234,0666 | IPC_CREAT);
    msgid = mq_open("eipc_mq_server","rw");
    printf("msgid:%i\n",msgid);
    
    if (msgid == -1)
    {
        fprintf(stderr, "failed to get:\n");
        exit(EXIT_FAILURE);
    }

    while (running)
    {
        if(msgrcv(msgid, (void *)&some_data,3333,msg_to_receive,0)  == -1)
        {
            fprintf(stderr, "failedto receive: \n");
            exit(EXIT_FAILURE);
        }

        printf("You Wrote: %s", some_data.some_text);
        
        if(strncmp(some_data.some_text, "end", 3)== 0)
        {
            running = 0;
        }
        
        if(msgrcv(msgid, (void *)&msg,BUFSIZ,0,0)  == -1)
        {
            fprintf(stderr, "failedto receive: \n");
            exit(EXIT_FAILURE);
        }
        
        printf("recibe msg: %s\n",msg.msg);
        printf("recibe msg.control: %i\n",msg.control );
    }
    
    if (msgctl(msgid, IPC_RMID, 0) == -1)
    {
        fprintf(stderr, "failed to delete\n");
        exit(EXIT_FAILURE);
    }
    exit(EXIT_SUCCESS);
}