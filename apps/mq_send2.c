#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>
#define  MAX_TEXT 512

#include "../include/ipc_types.h"
#include "../include/utils.h"

struct my_msg_st
{
    long int my_msg_type;
    char some_text[MAX_TEXT];
};

int main()
{
    int running = 1;
    struct my_msg_st some_data;
    
    eipc_mq msg;
    int msgid;
    char buffer[BUFSIZ];

//    msgid = msgget( (key_t)1234,0666 | IPC_CREAT);
    msgid = mq_open("eipc_mq_server","rw");
    
    printf("msgid:%i\n",msgid);
    
    if (msgid == -1)
    {
        fprintf(stderr, "failed to create:\n");
        exit(EXIT_FAILURE);
    }
    while(running)
    {
        printf("Enter Some Text: ");
        fgets(buffer, BUFSIZ, stdin);
        
//        some_data.my_msg_type = 1;
//        strcpy(some_data.some_text, buffer);
//        if(msgsnd(msgid, (void *)&some_data, MAX_TEXT, 0) == -1)
//        {
//            fprintf(stderr, "msgsnd failed\n");
//            exit(EXIT_FAILURE);
//        }
        
        
        msg.type = 1;
        msg.control = (int) buffer[0];
        strcpy(msg.msg, buffer);

        if(msgsnd(msgid, (void *)&msg, MQ_FIXE_LENGTH + strlen(buffer) , 0) == -1)
        {
            fprintf(stderr, "msgsnd failed\n");
            exit(EXIT_FAILURE);
        }
        
        if(strncmp(buffer, "end", 3) == 0)
        {
            running = 0;
        }
    }
    exit(EXIT_SUCCESS);
}
