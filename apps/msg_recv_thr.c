#include <stdio.h>
#include <sys/msg.h>
#include <pthread.h>	

#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>

typedef struct
{
    long type;
    int  reply;
    int reply_return;
}eipc_mq_reply;
#define MQ_REPLY_LENGTH  (sizeof(eipc_mq_reply) - sizeof(long))



#define TCOUNT 50	
#define WATCH_COUNT 15

int count = 0;
pthread_mutex_t count_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t count_threshold_cv = PTHREAD_COND_INITIALIZER;
int thread_ids[3] = {0, 1, 2};
pthread_t threads[3];
int WAIT_MSEG=1000;
int Tiempo=1;



typedef int (*hand_thr) (void *);
typedef struct
{
    pthread_t thr_function;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    int timer;
    hand_thr function;
    void *param;
    int retur;
}ipc_thr;


int inc_count(void *idp) 
{
    printf("------ inc_count()\n");
    
    int msqid = 32769;
    struct message {
        long type;
        char text[20];
    }msg;
    
//    msgrcv(msqid, (void *) &msg, sizeof(msg.text),0,0);
//    printf("------ %s \n", msg.text);
    
    printf("------ inc_count() msgsnd !\n");
    msqid = 98307;
    eipc_mq_reply msg_reply;
    msg_reply.type=333;
    if(msgsnd( msqid , (void *)&msg_reply, MQ_REPLY_LENGTH, IPC_NOWAIT) == -1)
    {
        perror("reply msgsnd failed\n");
    }
    
    return 1;
}

void *thr_handler(void *param) 
{
    ipc_thr *thr = (ipc_thr *)param;
    printf("--- thr_handler()\n");
    thr->retur = thr->function( thr->param );
    pthread_cond_signal( &thr->cond );
    pthread_mutex_unlock( &thr->mutex );
    printf("--- thr_handler() exit !\n");
    pthread_detach(pthread_self());
    return NULL;
}


int thread_create_timeout(hand_thr function, int timer, void *param)
{
    int rc;
    struct timespec ts;
    struct timeval tp;
    ipc_thr thr;
    memset( &thr, 0, sizeof(ipc_thr));
    
    thr.function = function;
    thr.timer = timer;
    thr.param = param;
    
    printf("- thread_create_timeout()\n");
    pthread_create( &thr.thr_function , NULL, thr_handler , &thr);
    
    rc = gettimeofday(&tp, NULL);
    tp.tv_sec = tp.tv_sec + thr.timer/1000;
    tp.tv_usec = tp.tv_usec + 1000*(thr.timer%1000);
    if ( tp.tv_usec >= 1000000 )
    {
        ++tp.tv_sec;
        tp.tv_usec -= 1000000;
    }
    ts.tv_sec = tp.tv_sec;
    ts.tv_nsec = tp.tv_usec*1000;
    
    printf("- Thread blocked\n");
    rc = pthread_cond_timedwait(&thr.cond, &thr.mutex, &ts);
    if (rc == ETIMEDOUT) {
        printf("- Timed Out!\n");
        pthread_cancel( thr.thr_function );
        return 0;
    }
    else
        return thr.retur;
}

int main(int argc, char **argv) 
{
    
    
    if(argc>1)
        WAIT_MSEG=atoi(argv[1]);
    
    int ret = thread_create_timeout( inc_count, WAIT_MSEG, NULL );
    
    printf("main() exit! %i\n",ret);
    return 0;


//  int msqid = 32769;
//  struct message {
//    long type;
//    char text[20];
//  } msg;
//  long msgtyp = 0;
//  msgrcv(msqid, (void *) &msg, sizeof(msg.text), msgtyp, MSG_NOERROR | IPC_NOWAIT);
//  printf("%s \n", msg.text);

//  return 0;
}




