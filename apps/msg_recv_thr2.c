#include <stdio.h>
#include <sys/msg.h>
#include <pthread.h>	

#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>


#define TCOUNT 50	
#define WATCH_COUNT 15

int count = 0;
pthread_mutex_t count_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t count_threshold_cv = PTHREAD_COND_INITIALIZER;
int thread_ids[3] = {0, 1, 2};
pthread_t threads[3];


#define checkResults(x,y) printf("%i %s",y,x)
int WAIT_MSEG=1000;
int Tiempo=1;

//void watch_count(void *idp) {
//    pthread_mutex_lock(&count_mutex);
//    while (count <= WATCH_COUNT) 
//    {
//        pthread_cond_wait(&count_threshold_cv, &count_mutex);
//        printf("watch_count(): Thread %d,Count is %d\n",*(int *)idp, count);
//    }
//    pthread_mutex_unlock(&count_mutex);
//}

//void *inc_count(void *idp) {
//    printf("--- inc_count()\n");
//    printf("--- inc_count() sleep %i\n",Tiempo);
//    sleep(Tiempo);
//    pthread_cond_signal(&count_threshold_cv);
//    pthread_mutex_unlock(&count_mutex);
//    printf("--- inc_count() exit !\n");
//    return NULL;
//}

void *inc_count(void *idp) {
    printf("--- inc_count()\n");
    
    int msqid = 32769;
    struct message {
        long type;
        char text[20];
    } msg;
    
    msgrcv(msqid, (void *) &msg, sizeof(msg.text),0,0);
    printf("%s \n", msg.text);
    
    pthread_cond_signal(&count_threshold_cv);
    pthread_mutex_unlock(&count_mutex);
    printf("--- inc_count() exit !\n");
    return NULL;
}


void *threadfunc(void *parm) {
    int rc;
    struct timespec ts;
    struct timeval tp;

    printf("- threadfunc()\n");
    pthread_create(&threads[1], NULL, inc_count, &thread_ids[1]);
    
    // espera un tiempo
    rc = gettimeofday(&tp, NULL);
    checkResults("gettimeofday()\n", rc);   
    tp.tv_usec += 1000*(WAIT_MSEG%1000);
    ts.tv_sec = tp.tv_sec + (WAIT_MSEG/1000);
    ts.tv_nsec = tp.tv_usec*1000;

    printf("- Thread blocked\n");
    rc = pthread_cond_timedwait(&count_threshold_cv, &count_mutex, &ts);
    if (rc == ETIMEDOUT) {
        printf("- Timed Out!\n");
        int x= pthread_cancel(threads[1]);
        printf("--- mata inc_count() %i!\n",x);
//        pthread_exit(NULL);
    }
    checkResults("pthread_cond_timedwait()\n", rc);
    printf("- threadfunc() exit !\n");
    return NULL;
}


//recv_thr(void *dat) {
//    int i;
//    pthread_t threads[3];
//    pthread_create(&threads[0], NULL, inc_count, &thread_ids[0]);
//    pthread_create(&threads[1], NULL, inc_count, &thread_ids[1]);
//    pthread_create(&threads[2], NULL, watch_count, &thread_ids[2]);
//    for (i = 0; i < 3; i++) {
//        pthread_join(threads[i], NULL);
//    }
//    return 0;
//}


int main(int argc, char **argv) 
{
    if(argc>1)
//        Tiempo=atoi(argv[1]);
        WAIT_MSEG=atoi(argv[1]);
    pthread_create(&threads[0], NULL, threadfunc, &thread_ids[0]);
    
    printf("join th 0 ! \n");
    pthread_join(threads[0], NULL);
    printf("join th 1 ! \n");
    pthread_join(threads[1], NULL);
    printf("main() exit! \n");
    return 0;


//  int msqid = 32769;
//  struct message {
//    long type;
//    char text[20];
//  } msg;
//  long msgtyp = 0;
//  msgrcv(msqid, (void *) &msg, sizeof(msg.text), msgtyp, MSG_NOERROR | IPC_NOWAIT);
//  printf("%s \n", msg.text);

//  return 0;
}




