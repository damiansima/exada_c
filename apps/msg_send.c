#include <string.h>
#include <stdio.h>
#include <sys/msg.h> 
#include <errno.h>


typedef struct
{
    long type;
    int  reply;
    int reply_return;
}eipc_mq_reply;
#define MQ_REPLY_LENGTH  (sizeof(eipc_mq_reply) - sizeof(long))


int main() {
    /* crear la queue con 
     *     $ ipcmk -Q 32769
     */ 
    int msqid = 98307;
    eipc_mq_reply msg_reply;
    msg_reply.type=333;
    if(msgsnd( msqid , (void *)&msg_reply, MQ_REPLY_LENGTH, IPC_NOWAIT) == -1)
    {
        perror("reply msgsnd failed\n");
    }
    return 0;
     
     
  
//  int msqid = 32769;
  struct message {
    long type;
    char text[20];
  } msg;

  msg.type = 1;
  strcpy(msg.text, "This is message 1");
  msgsnd(msqid, (void *) &msg, sizeof(msg.text), IPC_NOWAIT);

  return 0;
}