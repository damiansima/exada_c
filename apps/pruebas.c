#include <sys/msg.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <sys/times.h>

// pid
#include <sys/stat.h>
#include <errno.h>

#include "../include/ipc_types.h"
#include "../include/utils.h"
#include "../include/eipcc.h"
#include "../include/params.h"

//
//int pid_alive(int pid)
//{
//    struct stat sts;
//    char prcs[100];
//    sprintf( prcs, "/proc/%i", pid);
//    if (stat( prcs, &sts) == -1 && errno == ENOENT) {
//      // process doesn't exist
//       printf("El proceso NO EXISTE\n");
//       return 0;
//    }
//    else
//    {
//        printf("El proceso EXISTE\n");
//        return 1;
//    }    
//}

long long timestamp() {
    struct timeval te; 
    gettimeofday(&te, NULL);
    long long milliseconds = te.tv_sec*1000LL;
    milliseconds += te.tv_usec/1000LL;
    printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}

void print(char *text, time_t t)
{
    char bufti[200];                     
    strftime(bufti,80,"%d/%m/%Y %H:%M:%S", localtime(&t));
    printf("%s: %s\n",text,bufti);
}

int mq_void(int mqid, int timesleep)
{
    int i;
    struct msqid_ds buf;
    timesleep = timesleep>10 ? timesleep/10 : 1;
    for( i=0 ; i<= timesleep ; i++ )
    {
        msgctl(mqid, IPC_STAT, &buf);
        if(buf.msg_qnum==0)
            return 1;
        usleep(10000);
    }
    return 0;
}


    
    
#define TEST(A) {printf("%-40s%3i\n",#A,A);}

int main(void) {
//    int msqid = 688128;
    int msqid = 720897;
    struct msqid_ds buf;
    printf("%-40s%s\n","type","size");
    printf("%-40s%s\n","----","----");
    TEST(sizeof(char));
    TEST(sizeof(short));
    TEST(sizeof(int));
    TEST(sizeof(long));
    TEST(sizeof(float));
    TEST(sizeof(double));
    TEST(sizeof(long long));
    TEST(sizeof(unsigned long long));
    timestamp();

//    sleep(1);
    struct tms t;
    times(&t);
    printf("times.tms.stime:%u\n",(unsigned int)t.tms_cutime);
    printf("times.tms.stime:%u\n",(unsigned int)t.tms_cstime);
    printf("times.tms.stime:%u\n",(unsigned int)t.tms_utime);
    printf("times.tms.stime:%u\n",(unsigned int)t.tms_stime);

    print("hola", time(NULL));
    printf("hola : %i\n", (int) time(NULL));

//    msqid = mq_open("eipcd_shm","rw");
    msqid = 131076;
    (void)msgctl(msqid, IPC_STAT, &buf);


    printf("*****  msg void: %i\n",mq_void(msqid,3000));
    
    printf("msg_lrpid %i \n",buf.msg_lrpid);
    printf("msg_lspid %i \n",buf.msg_lspid);
    printf("msg_perm mode %o \n",buf.msg_perm.mode);
    printf("msg_perm key %i \n",buf.msg_perm.__key);

    printf("Num MSG msg_qnum              %7i \n",(int)buf.msg_qnum);
    printf("max bytes msg_qbytes          %7i \n",(int)buf.msg_qbytes);
    printf("current bytes msg_cbytes      %7i \n",(int)buf.msg_cbytes);

    print("creacion  msg_ctime ",buf.msg_ctime);
    print("lectura   msg_rtime ",buf.msg_rtime);
    print("escritura msg_stime ",buf.msg_stime);

//    pid_alive(11095);

  return 0;
}
