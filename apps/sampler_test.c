#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/eipcc.h"




#define MAX_NUM_BLOCK 128
typedef short e_scan_t; // registros de 16 bits
typedef struct
{
    char name[96];
    int  addres;
    int  start;
    int  end;
    int  len_block;
    int  offset_block;
}Block;

typedef struct
{
    int num_block;
    enum  {busy, ready, writing }state;
}E_SHM_SCAN_H;

typedef Block E_SHM_SCAN_B;

eipc_t *eipc;

int main(int argc, char* argv[])
{
    int i;
    

    
    /* 
     * con estos datos se procede: 
     *          1. chequea el driver (debe existir ModubsRTU)
     *          2. atacha con el nombre de sensor (el eipc solo permite un name atach)
     *          3. abre el puerto serie
     *          4. si pudo atachar crea un shm
     *          4. abre 
     */
    
    log_set_file("/tmp/log.log");
    log_debug_level(255);
    log_logger_level(0);
  
    eipc = eipc_attach("e_sampler");
    if((int)eipc == -1)
    {
        log_error(0,"attach() error !");
        return -1;
    }
   
    E_SHM_SCAN_H *shmctr= (E_SHM_SCAN_H *)map_shm("Balanza_XX", "rw", 0);   
    E_SHM_SCAN_B *shmblock = (E_SHM_SCAN_B *)(shmctr+1);
    e_scan_t *pshm;
            
    pshm = (e_scan_t *)shmblock;
    
    int j;
    Block *pblock;
    timems_t t_end, t_now;
    int t_interval = 500; // ms
    t_end = timems() + t_interval;
    fprintf(stderr,"scaning...\n");
    while(1)
    {
//        print_progress_bar(0);
        pblock = shmblock;
        for (i=0; i<shmctr->num_block; i++)
        {
            printf("\n%04i val[%02i-%02i]:", pblock->addres, pblock->start,pblock->end);
            for (j=0; j<pblock->len_block; j++)
                printf(" %04i",pshm[pblock->offset_block+j]);
            fflush(stdout);
            pblock++;  
        }
        t_now = timems();
        if( t_now < t_end )
            msleep( t_end-t_now );
        
        t_end = timems() + t_interval;        
        
    }
       
    sleep(333);
    eipc_deattach(eipc);
    return 0;
}

