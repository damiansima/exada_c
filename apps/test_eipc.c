#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>
#define  MAX_TEXT 512

#include "../include/ipc_types.h"
#include "../include/utils.h"
#include "../include/eipcc.h"
#include "../include/params.h"

eipc_hand function;
eipc_t *eipc;

eipc_mq msgrv;
//char msgrv[BUFSIZ];

int funcion_read(eipc_mq *msgrv) 
{
    log_msg("funcion_read() type:%i\nmsg:<<%s>>",msgrv->type,msgrv->msg);
    log_msg("esperando");
//    msleep(1000);
    return 234;
}

int funcion_read5(eipc_mq *msgrv) 
{
    log_msg("funcion_read5() *** type:%i\nmsg:<<%s>>",msgrv->type,msgrv->msg);
    
//    eipc_send_msg(eipc, "", "test_mq2", 45, "nada",4, 0);
    return 5;
}

int funcion_eventos(eipc_mq *msgrv) 
{
    printf("funcion_eventos() evento:%i length:%i  OK\n",msgrv->event, msgrv->length);
    return 0;
}

int main()
{   
    
//    char msg[200];
    
    eipc = eipc_attach("test_eipc");
    
    log_set_file("/tmp/test_eipc.log");
    log_debug_level(255);
    log_logger_level(0);
    
    
    pth_recv *pthr0 = eipc_pth_msg_recv( eipc, funcion_read, 0);
//    pth_recv *pthr5 = eipc_pth_msg_recv( eipc, funcion_read5, 5);
    pth_recv *pthre = eipc_pth_event_recv( eipc, funcion_eventos);
    
//    sprintf(msg,"ḧola mundo como msg 5 1");
//    eipc_send_msg(eipc, "", "test_eipc", 5, msg, strlen(msg), 5000 , ACK_PROCESS);
    
//    sprintf(msg,"ḧola mundo como msg 6 ");
//    eipc_send_msg(eipc, "", "test_mq4",6, msg,strlen(msg));
//    sprintf(msg,"ḧola mundo como msg 8  2");
//    eipc_send_msg(eipc, "", "test_mq4",8, msg,strlen(msg));
//    
//    sleep(1);
//    eipc_send_event(eipc, "", "test_mq4", 9);
    
    
//    sleep(10);    
//    exit(EXIT_SUCCESS);
    pthread_exit(NULL);
    
    
    pth_recv_delete(pthr0);
//    pth_recv_delete(pthr5);
    pth_recv_delete(pthre);
    
    eipc_deattach(eipc);
    exit(EXIT_SUCCESS);
}
