#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>
#define  MAX_TEXT 512

#include "../include/ipc_types.h"
#include "../include/utils.h"
#include "../include/eipcc.h"
#include "../include/params.h"

void launch_callback(void *e) 
{
    eipc_mq msg;
    eipc_t *eipc = (eipc_t *)e;
    
    if(msgrcv( eipc->shm->mqid, (void *)&msg, BUFSIZ, 0, 0 )  == -1)
    {
        perror("failedto receive: \n");
        return 0;
    }    
    logger("launch_callback ok");
    eipc->function_event( msg.info );
    return 0;
}


pthread_t eipc_event(eipc_t eipc, eipc_hand funtion) 
{
    int e;
    pthread_t pth;    
    eipc.function_event = funtion;    
    e = pthread_create (&pth, NULL, (void *)&launch_callback, (void *)&eipc);
//    launch_callback(&eipc);
    
    return pth;
}

void funcion_eventos(int event) 
{
    printf("funcion_eventos(%i) OK\n",event);
    return;
}

int main()
{
    int running = 1;
    
    eipc_mq msg;
    int msgid;
    char buffer[BUFSIZ];

    eipc_t eipc = eipc_attach("/tmp/hola");
//    eipc_event( eipc, funcion_eventos );
    
    sleep(1);
    eipc_send_event(eipc, "", "/tmp/hola", 9);

    sleep(1);
    eipc_event( eipc, funcion_eventos );
    
    
    sleep(1);
    eipc_deattach(eipc);
    
    exit(EXIT_SUCCESS);
    
//    msgid = mq_open("eipc_mq_server","rw");
    msgid = 98305;
    
    if (msgid == -1)
    {
        fprintf(stderr, "failed to create:\n");
        exit(EXIT_FAILURE);
    }
    while(running)
    {
        printf("Enter Some Text: ");
        fgets(buffer, BUFSIZ, stdin);
              
        msg.type = 1;
        msg.control = (int) buffer[0];
        strcpy(msg.msg, buffer);

        if(msgsnd(msgid, (void *)&msg, MQ_FIXE_LENGTH + strlen(buffer) , 0) == -1)
        {
            fprintf(stderr, "msgsnd failed\n");
            exit(EXIT_FAILURE);
        }
        
        if(strncmp(buffer, "end", 3) == 0)
        {
            running = 0;
        }
    }
//    eipc_deattach(eipc);
    exit(EXIT_SUCCESS);
}
