#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>
#define  MAX_TEXT 512

#include "../include/ipc_types.h"
#include "../include/utils.h"
#include "../include/eipcc.h"
#include "../include/params.h"

eipc_hand function;

eipc_mq msgrv;
//char msgrv[BUFSIZ];

void funcion_read(eipc_mq *msgrv) 
{
    logger("type:%i\nmsg:<<%s>>",msgrv->type,msgrv->msg);
    return;
}

void funcion_eventos(eipc_mq *msgrv) 
{
    printf("funcion_eventos() evento:%i length:%i  OK\n",msgrv->event, msgrv->length);
    return;
}

int main()
{   
    eipc_t *eipc = eipc_attach("test_mq4");
    
    pht_recv *pthr = eipc_pht_msg_recv( eipc, funcion_read, 0);
//    pht_recv *pthr2 = eipc_pht_msg_recv( eipc, funcion_eventos, TYPE_EVENT);
    pht_recv *pthr2 = eipc_pth_event_recv( eipc, funcion_eventos);
//    int pide = eipc_pth_event_recv( eipc, funcion_eventos );
    
//    sprintf(msg,"ḧola mundo como msg 5 1");
//    eipc_send_msg(eipc, "", "test_mq",5, msg,strlen(msg));
//    sprintf(msg,"ḧola mundo como msg 6 ");
//    eipc_send_msg(eipc, "", "test_mq4",6, msg,strlen(msg));
//    sprintf(msg,"ḧola mundo como msg 8  2");
//    eipc_send_msg(eipc, "", "test_mq4",8, msg,strlen(msg));
//    
//    sleep(1);
//    eipc_send_event(eipc, "", "test_mq4", 9);
    
//    sleep(1);
    
    pthread_exit(NULL);
    pht_recv_delete(pthr);
    pht_recv_delete(pthr2);
//    pth_event_delete(pide);
    eipc_deattach(eipc);
    exit(EXIT_SUCCESS);
}
