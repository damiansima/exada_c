#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>
#include <signal.h>
#define  MAX_TEXT 512

#include "../include/ipc_types.h"
#include "../include/utils.h"
#include "../include/eipcc.h"
#include "../include/params.h"

eipc_hand function;

eipc_mq msgrv;
//char msgrv[BUFSIZ];

#define test(fmt, args...) printf(fmt,##args)

void funcion_read(eipc_mq *msgrv) 
{
    log_msg("type:%i\nmsg:<<%s>>",msgrv->type,msgrv->msg);
    
    return;
}

void funcion_eventos(eipc_mq *msgrv) 
{
    printf("funcion_eventos() evento:%i length:%i  OK\n",msgrv->event, msgrv->length);
    return;
}

int main()
{  
    int i;
    char msg[2048];
    
    test("hola mundo %i",667);
    
    log_set_file("/tmp/log.log");
    log_debug_level(255);
    log_logger_level(255);
    
    eipc_t *eipc = eipc_attach("test_mq2");
    
    eipc_t *eipc_to = eipc_locate("test_eipc");
    if( eipc_to == (eipc_t *)-1)
        log_error("test_eipc is not running !");
    while( eipc_to == (eipc_t *)-1)
    {
        msleep(200);
        eipc_to = eipc_locate("test_eipc");
    }
    log_info("test_eipc running !");
    
    pth_recv *pthr = eipc_pth_msg_recv( eipc, funcion_read, 0);
//    int pide = eipc_pth_event_recv( eipc, funcion_eventos );
    
    sprintf(msg,"ḧola mundo como msg 5 1");
    TEST(eipc_send_msg(eipc, "", "test_eipc", 5, msg, strlen(msg), 1200 , ACK_PROCESS));
    TEST(eipc_send_msg(eipc, "", "test_eipc", 5, msg, strlen(msg), 1200 , ACK_PROCESS));
    TEST(eipc_send_msg(eipc, "", "test_eipc", 5, msg, strlen(msg), 1200 , ACK_PROCESS));
    TEST(eipc_send_msg(eipc, "", "Balanza_XX", 5, msg, strlen(msg), 1200 , ACK_PROCESS));
    
    for(i=0;i<10000;i++)
    {
//        if(eipc_send_msg(eipc, "", "test_eipc", 5, msg, strlen(msg), 1500 , ACK_PROCESS)==0)
        if(eipc_send_msg(eipc, "sima-ub", "test_eipc", 5, msg, strlen(msg), 1500 , ACK_PROCESS)==0)
            exit (-1);
//        msleep(2);
        log_msg("i:%i",i);
    }
    
//    sprintf(msg,"ḧola mundo como msg 6 ");
//    TEST(ACK_NONE);
//    eipc_send_msg(eipc, "", "test_eipc",6, msg,strlen(msg), 0 , ACK_NONE );
//    sprintf(msg,"ḧola mundo como msg 8  2");
//    TEST(ACK_TIMEOUT);
//    eipc_send_msg(eipc, "", "test_eipc",8, msg,strlen(msg), 1000, ACK_TIMEOUT );
//    TEST(ACK_ALIVE);
//    eipc_send_msg(eipc, "", "test_eipc",8, msg,strlen(msg), 1000, ACK_ALIVE );
    
//    eipc_send_signal(eipc, "", "test_eipc",SIGQUIT);
//    eipc_send_event(eipc, "", "test_eipc", 9, 200 );
    
//    sleep(1);
    
    pth_recv_delete(pthr);
    eipc_deattach(eipc);
    exit(EXIT_SUCCESS);
}
