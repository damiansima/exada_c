/*
 * File:   thread_cpp.cpp
 * Author: sima
 *
 * Created on 29 de marzo de 2014, 10:30
 */

#include <cstdlib>

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

 
using namespace std;


//class Devemu
//{
//    int       VarInc;
//    pthread_t thread_id;
//
//    void Increm()
//    {
//        for (;;)
//        {
//            if (VarInc > 532)
//            {
//                VarInc = 0;
//            }
//            else
//            {
//                VarInc++;
//            }
//        }
//    }
//
//    public:
//        static void * IncWrapper(void * thisPtr)
//        {
//            printf("IncWrapper()");     
//            for (;;)
//            {
//                if (VarInc > 532)
//                {
//                    VarInc = 0;
//                }
//                else
//                {
//                    VarInc++;
//                }
//            }
//            
//            return NULL;
//        }
//
//        Devemu()
//        {
//            VarInc = 0;
//
//            pthread_create(&thread_id, NULL, IncWrapper, NULL);
//        }
//
//        int Var()
//        {
//            return VarInc;
//        }
//};
class Pareja
{
    // atributos
    double a, b;
    pthread_t thread_id;
 
public:
    // métodos
    double getA();
    double getB();    
    void   loop(void* p);    
    void   setA(double n);
    void   setB(double n);
};
 
// implementación de los métodos de la clase Pareja
//
double Pareja::getA() { return a; }
double Pareja::getB() { return b; }
void Pareja::loop(void* p) 
{
    for (;;)
    {
        if (a > 532)
        {
            a = 0;
        }
        else
        {
            a++;
        }
        usleep(300000);
    }
}

void Pareja::setA(double n) 
{ 
    a = n;
    printf("seta()::getA():%f\n",getA());
    pthread_create(&thread_id, NULL, (void *)(&loop)(void *), NULL);
}
void Pareja::setB(double n) { b = n; }

int main(int argc, char **argv)
{
//    Devemu m;
//    Devemu m1;
//    printf("var:%i",m.Var());
    Pareja a;
    
    a.setA(3);
    printf("getA():%f\n",a.getA());
    
    sleep(1);
    return 0;
}

