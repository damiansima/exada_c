
#ifndef __ECOMMON_
    #define __ECOMMON_

#include <math.h>
#include <sys/types.h>

#include "utils.h"



#define UDP_MAX_TAGS        1000
#define UDP_MAX_BYTES_LENGTH    60000
#define UDP_MIN_BYTES_LENGTH    1000

// este valor es para definir variables en C
#define E_MAX_NUM_TAG       200

#define EXADA_MAX_TAG       2000
#define EXADA_PORT_ASI_SEND 50000
#define EXADA_PORT_ASI_RCV  50000
#define EXADA_PORT_PUB      50001
#define EXADA_PORT_SUS      50001

// server: refresco para tag rayado y envio
#define TIME_SEND       (1000)   // milisegundos
#define TIME_STREAKY    (5000)  // milisegundos
#define SHM_SERVER  "/shm_server_tags"
#define SHM_CLIENT  "/shm_client_tags"

#define E_SHM_SERVER_NAME  "shm_server_tags"
#define E_SHM_CLIENT_NAME  "SHM_TAGS"
#define E_SHM_SERVER_I_NAME  "shm_server_tags_index"


#define SCAN_NAME_FMT       "scan_%s"
#define SAMPLER_NAME_FMT    "sampler_%s"


// / WARNING: para el raspi se necesita que las struct tengan sizeof
// / divisibles por 8 :(


typedef struct{
    union {
        struct {
            unsigned n1:1;
            unsigned n2:1;
            unsigned n3:1;
            unsigned fill:5;
        }flag;
        char val;         
    }streaky;                 // tag rayado 1: nivel 1 2:nivel 2: 4:cliente
    char     hb;              // nivel del heart beat
    unsigned alarm_n1 :1;     // alarma nivel 1
    unsigned alarm_n2 :1;     // alarma de nivel 2, por limites, o por rayado o por usuario
    unsigned nivel2   :1;     // indica si es de nivel 2 o de campo
    unsigned fill     :5;     // relleno
    char     hb_client;       // heart beat en cliente
    int      index;           // indices del tag, en variables ID de (db)
    double   value;           // valor del tag
    double   value_max;       // maximo (db) o seteado por applicacion
    double   value_min;       // minimo (db) o seteado por applicacion
    char     tag[96];         // nombre del tag (db)
}e_shm_tags_client;
#define EXADA_SHM_TAGS  (E_MAX_NUM_TAG*sizeof(e_shm_tags_client))
#define E_SHM_TAGS_LEN  (E_MAX_NUM_TAG*sizeof(e_shm_tags_client))


typedef struct{
    union {
        struct {
            unsigned n1:1;
            unsigned n2:1;
            unsigned n3:1;
            unsigned fill:5;
        }flag;
        char val;         
    }streaky;                 // tag rayado 1: nivel 1 2:nivel 2: 4:cliente
    char     hb;              // nivel del heart beat
    unsigned alarm_n1 :1;     // alarma nivel 1
    unsigned alarm_n2 :1;     // alarma de nivel 2, por limites, o por rayado o por usuario
    unsigned nivel2   :1;     // indica si es de nivel 2 o de campo
    unsigned fill     :5;     // relleno
    char     hb_client;       // heart beat en cliente
    int      index;           // indices del tag, en variables ID de (db)
    double   value;           // valor del tag
    double   value_max;       // maximo (db) o seteado por applicacion
    double   value_min;       // minimo (db) o seteado por applicacion
    char     tag[96];         // nombre del tag (db)
}e_shm_flags;


typedef struct
{
    union {
        struct {
            unsigned n1:1;
            unsigned n2:1;
            unsigned n3:1;
            unsigned fill:5;
        }flag;
        char val;         
    }streaky;                 // tag rayado 1: nivel 1 2:nivel 2: 4:cliente
    char     hb;              // nivel del heart beat
    unsigned alarm_n1 :1;     // alarma nivel 1
    unsigned alarm_n2 :1;     // alarma de nivel 2, por limites, o por rayado o por usuario
    unsigned fill     :14;    // relleno
    int      index;           // indices del tag, en variables ID de (db)
    double   value;           // valor del tag
    double   value_max;       // maximo (db) o seteado por applicacion
    double   value_min;       // minimo (db) o seteado por applicacion
}e_shm_tags_server;


typedef struct
{
    char     streaky;    // tag rayado 1: nivel 1 2:nivel 2:
    unsigned alarm_n1 :1;
    unsigned alarm_n2 :1;
    unsigned fill     :22;
    int      index;      // indices del tag
    double   value;      // valor del tag
}e_shm_tags_scan;


typedef struct
{
    double val_min;
    double val_max;
    int    index;
    int    fill;
    char   tag[96];
}e_dat_tag; 


struct shm_control
{
    unsigned sending   :1;
    unsigned writing   :1;
    unsigned recv_exit :1;
    unsigned send_exit :1;
    unsigned fill      :28;
    int      actual_len;
};


typedef struct
{
    int fill;
    int length;
    e_shm_tags_scan tag[UDP_MAX_TAGS];
}e_shm_scan;


typedef struct
{
    struct shm_control control;
    e_shm_tags_server tags[UDP_MAX_TAGS];
}e_shm_server;


typedef struct
{
    int fill;
    int length;
    e_shm_tags_server tag[UDP_MAX_TAGS];
}rx_client_server;




/*     ------    N U E V O S   -------
 * 
 *      ex_ipc 
 *      ex_scan 
 *      ex_sampler 
 *      ex_rcvtags
 * 
 */ 

typedef union{
    double value;
    char string[8];
}values_t;
typedef short e_scan_t; // registros de 16 bits
typedef char e_alarm_t; // alarmas x 8  

// SHM de los SCAN  (escan)
typedef struct
{
    char name[96];
    int  addres;
    int  reg_start;
    int  reg_end;
    int  reg_length;
    int  alarm_start;
    int  alarm_end;
    int  alarm_length;
    timems_t update_time;
    e_scan_t data[1];
}SCAN_BLOCK;

typedef struct
{
    timems_t now;
    enum  {busy, ready, writing }state;
}SCAN_CTR;



// estructura a enviar por udp a ercvtags
typedef enum{type_int16=1, type_int32, typte_float, typte_double,type_string }e_TAG_TYPE;
typedef struct 
{
    int     id;              // indices del tag, en variables ID de (db)
    int     heartbeat;       // nivel del heart beat
    e_TAG_TYPE  type;
    int         len_type;
    union{
        double  value;
        char string[8];
    };                 // valor del tag
}SAMPLER_TAGS;

typedef enum{update_tags, update_limit, update_tags_n2 }e_CTR_TYPE;
typedef struct 
{
    e_CTR_TYPE      type;
    int             heartbeat; 
    int             length;
    timems_t        time_sampler;
}SAMPLER_CTR;




typedef struct
{
    int id; // indices del tag, en variables ID de (db)
    union {
        struct {
            unsigned n1:1;
            unsigned n2:1;
            unsigned n3:1;
            unsigned fill:5;
        };
        char val;         
    }streaky;                 // tag rayado 1: nivel 1 2:nivel 2:
    char        heartbeat;       // nivel del heart beat
    char        heartbeat_sampler;
    char        rwa;
    char        tag[96];         // nombre del tag (db)
    e_TAG_TYPE  type;
    int         len_type;
    double      value_max;       // maximo (db) o seteado por applicacion
    double      value_min;       // minimo (db) o seteado por applicacio
    union{
        double  value;
        char string[8]; // el string puede ser mayor para eso se usa la libreria lista_contina.h
    }; 
}SHM_TAGS;

typedef struct 
{
    int             num_tags;
    timems_t        time_sampler;
}SHM_TAGS_CTR;

typedef struct
{
    int         id;
    int         _addres;
    int         _register;
    e_TAG_TYPE  type;
    int         len_type;
    timems_t    *update_time;
    void        *ptr;
}s_TAG;




//typedef struct
//{
//    int         id;              // indices del tag, en variables ID de (db)
//    char        rwa;             // permisos
//    char        tag[96];         // nombre del tag (db)
//    e_TAG_TYPE  type;            // tipo: string int, etc.   
//    int         len_type;        // longitud: minimo 8: sizeof(double)   
//    double      value_max;       // maximo (db) o seteado por applicacion
//    double      value_min;       // minimo (db) o seteado por applicacio
//    union{
//        double  value;
//        char string[8]; // el string puede ser mayor para eso se usa la libreria lista_contina.h
//    };
//}ex_write_tag_t;

typedef struct
{
    int         id;              // indices del tag, en variables ID de (db)
    int         _addres;
    int         _register;
    char        _rwa[4];         // permisos
    char        tag[96];         // nombre del tag (db)
    double      value_max;       // maximo (db) o seteado por applicacion
    double      value_min;       // minimo (db) o seteado por applicacio
    union{
        double  value;
        char string[8]; // el string puede ser mayor para eso se usa la libreria lista_contina.h
    };
}write_tag_t;

#endif
