#ifndef __EIPCC_H
    #define __EIPCC_H

#include <pthread.h>
//#include "params.h"
#include <sys/msg.h> 

#include "../include/ipc_types.h"
#include "../include/utils.h"
#include "../include/ipc_common.h"
#include "e_common.h"

#include <string.h>
#include <time.h>

#include <fcntl.h>
//#include <sys/stat.h>
#include <errno.h>

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define eipc_lock(eipc) { pthread_mutex_lock(&((eipc_t *)eipc)->mutex);}
#define eipc_unlock(eipc) { pthread_mutex_unlock(&((eipc_t *)eipc)->mutex);}
#define LOGD(a, fmt, args...) log_debug("[%u]: "fmt,(unsigned int)a,##args)


typedef int (*eipc_hand) (eipc_mq *);


typedef struct 
{
    eipc_shm *shm;
    eipc_shm_control *shmctr;
    eipc_hand function_event;
    pthread_mutex_t event_mutex;
    pthread_mutex_t mutex;
    pthread_mutex_t shm_mutex;
}eipc_t;



// private
static eipc_t *_ceipc = NULL;


eipc_t *eipc_attach(char *name)
{
    static eipc_t eipc;
    log_info("eipc_attach(%s)",name);
    
    if( _ceipc != NULL )  
    {
        log_error("eipc_attach()::previo attach con nombre <%s>",_ceipc->shm->process);
        eipc = *_ceipc;
        return &eipc;
    }
    
    eipc.shmctr = (eipc_shm_control *)map_shm(EIPC_SHM_NAME, "rw",0);   
    if ((int)eipc.shmctr == -1)
    {
        log_error("eipc_attach()::map_shm()");
        return (eipc_t *)-1;
    }
    eipc.shm = get_process(name,eipc.shmctr);            
    if(chek_process_alive(eipc.shm->pid))
    {
        log_error("eipc_attach()::get_process() process is running ! ");
        return (eipc_t *)-1;
    }
    eipc.shm->mqid = mq_open(name,"rw");
    eipc.shm->pid = getpid();
    pthread_mutex_unlock(&eipc.shm->mutex);
    pthread_mutex_unlock(&eipc.shm->mutex_send);
    strcpy(eipc.shm->process,name);
    eipc_unlock(&eipc);
    _ceipc=&eipc;
    return &eipc;
}


// solo localhost
eipc_t *eipc_locate(char *name)
{
//    static eipc_t eipc;
    eipc_t *eipc=malloc(sizeof(eipc_t));
    log_debug("eipc_locate(%s)",name);
   
    if(_ceipc->shm->pid != getpid())
    {
        log_error("eipc_locate():: getpid() not eipc_attach() !");
        return (eipc_t *)-1;
    }
    eipc->shm = get_process(name,_ceipc->shmctr);            
    if(chek_process_alive(eipc->shm->pid))
    {
        log_debug("eipc_locate(): process is running ! ");
        return eipc;
    }
    log_debug("eipc_locate(): process is not running ! ");
    free(eipc);
    return (eipc_t *)-1;
}


int eipc_deattach(eipc_t *eipc)
{
    if((int)eipc ==-1)
        return -1;
    
    if( eipc == _ceipc && eipc != NULL )
    {
        eipc->shm->pid = 0;
        _ceipc = NULL;
        return 0;
    }
    if(!chek_process_alive(eipc->shm->pid))
        eipc->shm->pid = 0;
    
    free(eipc);
    return 0;
}

void logger_msg(eipc_t *eipc, eipc_mq *msg)
{
//    log_msg(0,"msg.type:   %10i",msg->type );
//    log_msg(0,"msg.control:%10i",msg->control );
//    log_msg(0,"msg.time:   %10i",msg->time );
//    log_msg(0,"msg.length: %10i",msg->length );
//    log_msg(0,"msg.to_h:     %s",msg->to_h );
//    log_msg(0,"msg.to_p:     %s",msg->to_p );
//    log_msg(0,"msg.from_h:   %s",msg->from_h );
//    log_msg(0,"msg.from_p:   %s",msg->from_p );
//    log_msg(0,"msg.info:   %10i",msg->info );
//    log_msg(0,"msg.fill:   %10i",msg->fill );
//    log_msg(0,"SHM_CTR->mq_id:%i",eipc->shmctr->mq_id);
//    log_msg(0,"process->mqid: %i",eipc->shm->mqid);
}



typedef struct{
    eipc_t *eipc; 
    pthread_t pth;
    int pid;
    eipc_reply reply;
}pth_wreply;


void wait_reply(eipc_shm *shm, int timesleep)
{
    int i;
    timesleep = timesleep>10 ? timesleep/10 : 1;
    for(i=0; i<=timesleep; i++)
    {
        if(shm->reply_ack.flag.ready)
            return;
        usleep(10000);
    }
    return;
}


int eipc_send_msg(eipc_t *eipc, char *to_host, char *process, long type, char *msgsd, 
        int len, int timedout, int ack )
{
    eipc_mq msg;
    
    assert_return( type!=TYPE_EVENT && type!=TYPE_REPLY );
    
    msg.type = type;
    msg.control = CTR_MSG | ack;
    msg.info = 0;
    msg.length = len;
    msg.timedout = timedout>0 ? timedout : 0 ;
//    if (timedout>0)
//        msg.time = timems() + timedout;
//    else
//        msg.time = 0;
    strcpy(msg.to_h,to_host);
    strcpy(msg.to_p,process);
    strcpy(msg.from_p,eipc->shm->process);
    strcpy(msg.from_h,"");    
    memcpy(msg.msg, msgsd, len);
    
    logger_msg(eipc, &msg);
    
    if(strlen(to_host)>0)
        msg.control |= CTR_TCP;
    else 
        msg.control |= CTR_LOCAL;

    eipc->shm->reply_ack.value = 0;
//    eipc_lock( eipc );
    
    if(msgsnd(eipc->shmctr->query_mq_id, (void *)&msg, MQ_FIXE_LENGTH + msg.length , MQFLAG) == -1)
    {
        perror("msgsnd failed\n");
        return -1;
    }
    
    if( ack & CTR_ACK )
    {
        wait_reply(eipc->shm,timedout);

//        eipc_unlock( eipc );
        
        switch( ack )
        {
            case ACK_PROCESS_RETURN :
                return eipc->shm->reply_return;
            case ACK_PROCESS :
                return eipc->shm->reply_ack.flag.ack_process;
            case ACK_HOST :
                return eipc->shm->reply_ack.flag.ack_host;
            case ACK_TIMEOUT :  
                return eipc->shm->reply_ack.flag.timeout;
            case ACK_ALIVE :
                return eipc->shm->reply_ack.flag.alive;
        }
    }
    return 0;
}


int eipc_send_event(eipc_t *eipc,char *to_host, char *process, int event, int timeout)
{
    eipc_mq msg;
    
    msg.type = TYPE_EVENT;
    msg.control = CTR_EVENT | ACK_PROCESS;
    msg.event = event;
    msg.length = 0;
     if (timeout>0)
        msg.time = timems() + timeout;
    else
        msg.time = 0;
    strcpy(msg.to_h,to_host);
    strcpy(msg.to_p,process);
    strcpy(msg.from_p,eipc->shm->process);
    strcpy(msg.from_h,"");
    
    if(strlen(to_host)>0)
        msg.control |= CTR_TCP;
    else 
        msg.control |= CTR_LOCAL;

    logger_msg(eipc, &msg);
    
    if(msgsnd(eipc->shmctr->query_mq_id, (void *)&msg, MQ_FIXE_LENGTH , MQFLAG) == -1)
    {
        perror("msgsnd failed\n");
        return -1;
    }
    
    wait_reply(eipc->shm,timeout);
    return eipc->shm->reply_ack.flag.ack_process;
}


int eipc_send_signal(eipc_t *eipc,char *to_host, char *process, int signal)
{
    eipc_mq msg;
    
    msg.type = TYPE_SIGNAL;
    msg.control = CTR_SIGNAL;
    msg.info = signal;
    msg.length = 0;
    msg.time = 0;
    strcpy(msg.to_h,to_host);
    strcpy(msg.to_p,process);
    strcpy(msg.from_p,eipc->shm->process);
    strcpy(msg.from_h,"");
    
    logger_msg(eipc, &msg);
     
    if(strlen(to_host)>0)
        msg.control |= CTR_TCP;
    else 
        msg.control |= CTR_LOCAL;

    if(msgsnd(eipc->shmctr->query_mq_id, (void *)&msg, MQ_FIXE_LENGTH , MQFLAG) == -1)
    {
        perror("msgsnd failed\n");
        return -1;
    }
    return 0;
}


int ex_write_tag( eipc_t *eipc, char *tag, int id, double value, int timedout )
{
    eipc_mq msg;
    write_tag_t *wt = (write_tag_t *)msg.msg;
            
    assert_return( tag!=NULL || id!=0 );
    
    strcpy(wt->tag,tag);
    wt->id=id;
    wt->value=value;
            
    msg.type = 1;
    msg.control = EX_WRT_TAG_VAL + ACK_PROCESS_RETURN;
    msg.info = 0;
    msg.length = sizeof(write_tag_t);
    msg.timedout = timedout>0 ? timedout : 0 ;

    strcpy(msg.from_p,eipc->shm->process);
    strcpy(msg.from_h,"");    

    eipc->shm->reply_ack.value = 0;
//    eipc_lock( eipc );
    
    if(msgsnd(eipc->shmctr->query_mq_id, (void *)&msg, MQ_FIXE_LENGTH + msg.length , MQFLAG) == -1)
    {
        perror("msgsnd failed\n");
        return -1;
    }
    
    wait_reply(eipc->shm,msg.timedout);
    
    return eipc->shm->reply_return;
}

//eipc_reply parser_reply(eipc_mq *buf, int mode, int process ) 
//{
//    eipc_reply reply;
//    reply.type = 0;
//    
//    // responde con queue
//    if (buf->control & CTR_MSG || buf->control & CTR_EVENT) 
//    {
//        if (mode == MODE_DAEMON) 
//        {
//            if( buf->control & CTR_ACK_HOST )
//            {
//                reply.type |= TYPE_REPLY;
//                reply.control |= CTR_ACK_HOST;
//                if (process)
//                    reply.control |= CTR_ALIVE;
//                else
//                    reply.control |= CTR_TIMEOUT;
//            }
//        } 
//        else if( buf->control & CTR_ACK_PROCESS )
//        {
//            reply.control |= CTR_ALIVE;
//            reply.control |= CTR_ACK_PROCESS;
//        }
//    }
//    if( buf->control & CTR_SIGNAL ) 
//    {
//        if (mode == MODE_PROGRAM) 
//        {
//            reply.type |= TYPE_REPLY;
//            reply.control |= CTR_ACK_HOST;
//            reply.control |= CTR_ACK_PROCESS;
//        }     
//    }
//    return reply;
//}


int eipc_recv_msg(eipc_t *eipc, eipc_mq *emsg, int type)
{
    log_debug("eipc_recv_msg()");
    if(msgrcv( eipc->shm->mqid, (void *)emsg, BUFSIZ, type, 0 )  == -1)
    {
        perror("failedto receive: \n");
        return 0;
    }    
    if( emsg->time > 0 && emsg->time < timems() )
    {
        log_debug("Cancela por timedout");
        return 0;
    } 
    logger_msg(eipc , emsg);
    return emsg->type;
}


typedef struct{
    eipc_hand function;
    eipc_t *eipc;
    pthread_t pth;
    int pid;
    int type;
}pth_recv;

typedef struct{
    pth_recv *pthrcv;
    eipc_mq *msg;
}pth_parser_msg;



void parser_msg(void *m)
{
    pth_parser_msg *pm=(pth_parser_msg *)m;
//    eipc_mq *msg=(eipc_mq *)m;
    eipc_mq_reply msg_reply;
    int msqid=pm->pthrcv->eipc->shmctr->reply_mq_id;
    
    TESTS(pm->msg->from_p,"%s");
    TESTS(pm->msg->to_p,"%s");
    TESTS(pm->msg->control,"%i");
    TESTS(pm->msg->control & ACK_PROCESS,"%i");
    TESTS(pm->msg->control & ACK_PROCESS_RETURN,"%i");

    if( pm->msg->control & ACK_PROCESS_RETURN )
    {
//        eipc_lock(p->eipc);
        msg_reply.type=pm->msg->type_reply;
        printf("reply: mq:%i type:%ld\n",msqid,msg_reply.type);
        msg_reply.reply_return = pm->pthrcv->function( pm->msg );
        msg_reply.reply.flag.alive=1;
        msg_reply.reply.flag.ack_host=1;
        msg_reply.reply.flag.ack_process=1;
        msg_reply.reply.flag.process_return_true=1;
        if(msgsnd(msqid, (void *)&msg_reply, MQ_REPLY_LENGTH, MQFLAG) == -1)
        {
            log_error("reply msgsnd failed %s",strerror(errno));
        }
//        eipc_unlock(p->eipc);
    }
    else
    {
//        eipc_lock(p->eipc);
        if( pm->msg->control & ACK_PROCESS )
        {
            msg_reply.type=pm->msg->type_reply;
            printf("reply: mq:%i type:%ld\n",msqid,msg_reply.type);
            msg_reply.reply.flag.alive=1;
            msg_reply.reply.flag.ack_host=1;
            msg_reply.reply.flag.ack_process=1;
            msg_reply.reply.flag.process_return_true=0;
            msg_reply.reply_return=0;
            if(msgsnd(msqid, (void *)&msg_reply, MQ_REPLY_LENGTH, MQFLAG) == -1)
            {
                log_error("reply msgsnd failed %s",strerror(errno));
            }
        }
        pm->pthrcv->function( pm->msg );
//        eipc_unlock(pm->pthrcv->eipc);
    }
    free(pm->msg);
    free(pm);
}


void eipc_pth_recv_callback(void *ep) 
{
    pth_recv *p = (pth_recv *)ep;
    int e;
    eipc_mq msg;
    int err;
    
    while (1)
    {
        e=eipc_recv_msg(p->eipc,&msg, p->type);
        if(e)
        {
            pthread_t pth;
            pth_parser_msg *pm=malloc(sizeof(pth_parser_msg));
            pm->pthrcv=p;
            pm->msg = malloc(sizeof(eipc_mq));
            memcpy(pm->msg ,&msg,sizeof(eipc_mq));
            err = pthread_create( &pth, NULL, (void *)&parser_msg, pm );
            assert_msg(err==0);
        }
    }
    return;
}



pth_recv *eipc_pth_msg_recv( eipc_t *eipc, eipc_hand callback_funtion, int type) 
{
    pth_recv *p = malloc(sizeof(pth_recv));
    p->function = callback_funtion;
    p->type = type;    
    p->eipc = eipc;    
    p->pid = pthread_create( &p->pth, NULL, (void *)&eipc_pth_recv_callback, (void *)p);
    return p;
}


void pth_recv_delete(pth_recv *p) 
{
    pthread_cancel(p->pth);
    free(p);
}

void pth_event_delete (int pid)
{
    return ;
}

pth_recv *eipc_pth_event_recv(eipc_t *eipc, eipc_hand callback_funtion); 
#define eipc_pth_event_recv(eipc, function)  eipc_pth_msg_recv( eipc, function, TYPE_EVENT) 


//
//void eipc_pth_event_callback(void *e) 
//{
//    eipc_mq msg;
//    eipc_t *eipc = (eipc_t *)e;
//    int mqid = eipc->shm->mqid;
//    
//    while (1)
//    {
//        if(msgrcv( mqid, (void *)&msg, 1024, TYPE_EVENT, 0 )  == -1)
//        {
//            perror("failedto receive: \n");
//            return;
//        }    
//        log_msg(0," ------- launch_callback()");
//        eipc->function_event( &msg );
//    }
//    return;
//}
//
//
//int eipc_pth_event_recv(eipc_t *eipc, eipc_hand callback_funtion) 
//{
//    int pid;    
//    eipc->function_event = callback_funtion;    
//    pid = pthread_create( &eipc->pth_event, NULL, (void *)&eipc_pth_event_callback, (void *)eipc);
//    return pid;
//}

#define write_tag( eipc, tag, value) ex_write_tag( eipc, tag, 0, value, 1500 )

#endif
