#ifndef __EIPC_H
    #define __EIPC_H


#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>     

#include "utils.h"

typedef struct _hosts_conn{
    void *zmq_context;
    void *zmq_socket;
    int  state;
    char *hostname;
}hosts_conn;


typedef struct _e_ipc_control{
    int ipc_msgflag;
    int time_alive;
    char from_host[256];
    char from_process[256];
    char to_host[256];
    char to_process[256];
}e_ipc_control;


typedef struct _e_ipc{
    int *zmq_context;
    int *zmq_socket;
    char *name_attach;
}e_ipc;


typedef struct _eipc_shm{
    int *zmq_context;
    int *zmq_socket;
    int pid;
    int signal;
    
    char name_attach[256];
}eipc_shm;


void eipc_dettach(void *em)
{
    e_ipc *eipc=(e_ipc *)em;
    logger("eipc_dettach: %i",(int)em);
    zmq_close(eipc->zmq_socket);
    zmq_ctx_destroy(eipc->zmq_context);
    free(&(eipc->name_attach[0]));
    free(em);    
}


void *eipc_attach(char *name)
{    
    e_ipc *em;
    char _name[300];
    
    em = (e_ipc *) malloc(sizeof(e_ipc));
    logger("eipc_attach: %i",(int)em);
    memcpy(_name,name,256);
    _name[255]=0;
    em->name_attach=strdup(_name);
    em->zmq_context = zmq_ctx_new ();
    em->zmq_socket = zmq_socket (em->zmq_context, ZMQ_REP);

    sprintf(_name,"ipc:///tmp%s",em->name_attach);
    //zmq_bind (sock, "tcp://*:5555");
    if(zmq_bind(em->zmq_socket, _name)!=0)
    {
        eipc_dettach((void *)em);
        return (void *)-1;
    }
    return ((void *)em);
}


void eipc_send(void *em,char *from_host, char *from_process, void *msg, int length )
{
    zmq_send (((e_ipc *)em)->zmq_socket, msg, length, 0);
}


// void *em = eipc_server_new("tcp://*:5555");
void *eipc_new(char *name)
{    
    e_ipc *em;
    char _name[300];
    
    em = (e_ipc *) malloc(sizeof(e_ipc));
    logger("eipc_attach: %i",(int)em);
    memcpy(_name,name,256);
    _name[255]=0;
    em->name_attach=strdup(_name);
    em->zmq_context = zmq_ctx_new ();
    em->zmq_socket = zmq_socket (em->zmq_context, ZMQ_REP);
    //zmq_bind (sock, "tcp://*:5555");
    if(zmq_bind(em->zmq_socket, em->name_attach)!=0)
    {
        eipc_dettach((void *)em);
        return (void *)-1;
    }
    return ((void *)em);
}

void zmq_tcp_disconnect(void *em)
{
    hosts_conn *eipc=(hosts_conn *)em;
    zmq_close(eipc->zmq_socket);
    zmq_ctx_destroy(eipc->zmq_context);
    free(eipc->hostname);
    free(eipc);    
}


void *zmq_tcp_connect(char *server,int port)
{    
    hosts_conn *em;
    int rc;
    char _name[300];
    
    sprintf(_name,"tcp://%s:%i",server,port);
    em = (hosts_conn *) malloc(sizeof(hosts_conn));
    em->hostname=strdup(server);
    em->zmq_context = zmq_ctx_new ();
    em->zmq_socket = zmq_socket (em->zmq_context, ZMQ_REP);
    em->state = zmq_connect(zmq_sock, "tcp://sima-nb:5656");
    return ((void *)em);
}

#endif

