#ifndef __IPC_COMOMN_H
    #define __IPC_COMOMN_H

#include <pthread.h>
#include <string.h>
#include <time.h>

#include "../include/ipc_types.h"

#include <fcntl.h>
//#include <sys/stat.h>
#include <errno.h>

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>



eipc_shm *get_process(char *process, eipc_shm_control *shmctr)
{
    int i=0;
    eipc_shm *shm = (eipc_shm *)(shmctr +1);
    for(i=0; i<shmctr->max_process; i++)
    {
        if(!strcmp(shm->process,process))
            return shm;
        shm++;
    }
    // si no encuentra registrado este proceso agrega donde pueda 
    shm = (eipc_shm *)(shmctr +1);
    ipc_shm_lock(shmctr);
    for(i=0; i<shmctr->max_process; i++)
    {
        if( shm->pid == 0 )
        {
            strcpy(shm->process,process);
            ipc_shm_unlock(shmctr);
            return shm;
        }
        shm++;
    }
    ipc_shm_unlock(shmctr);
    perror("get_process() no puede añadir mas procesos");
    return (eipc_shm *)-1;
}

int chek_process_alive(int pid)
{
    int fd;
    char prcs[100];
    sprintf( prcs, "/proc/%i/stat", pid);
     
    fd = open(prcs,O_RDONLY,0777);
    if ( fd <= 0 )
        pid = 0;
    close(fd);
    return pid;
    
//    struct stat sts;
//    char prcs[100];
//    sprintf( prcs, "/proc/%i", pid);
//    if (stat( prcs, &sts) == -1 && errno == ENOENT)
//        return 0;
//    else
//        return pid;
}



typedef int (*hand_thr) (void *);
typedef struct
{
    pthread_t thr_function;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    int timer;
    hand_thr function;
    void *param;
    int retur;
}ipc_thr;

void *thr_handler(void *param) 
{
    ipc_thr *thr = (ipc_thr *)param;
//    printf("--- thr_handler()\n");
    thr->retur = thr->function( thr->param );
    pthread_cond_signal( &thr->cond );
    pthread_mutex_unlock( &thr->mutex );
//    printf("--- thr_handler() exit !\n");
    pthread_detach(pthread_self());
    return NULL;
}

int thread_create_timeout(hand_thr function, int timer, void *param)
{
    int rc;
    struct timespec ts;
    struct timeval tp;
    ipc_thr thr;
    memset( &thr, 0, sizeof(ipc_thr));
    
    thr.function = function;
    thr.timer = timer;
    thr.param = param;
    
//    printf("- thread_create_timeout()\n");
    pthread_create( &thr.thr_function , NULL, thr_handler , &thr);
    
    rc = gettimeofday(&tp, NULL);
    tp.tv_sec = tp.tv_sec + thr.timer/1000;
    tp.tv_usec = tp.tv_usec + 1000*(thr.timer%1000);
    if ( tp.tv_usec >= 1000000 )
    {
        ++tp.tv_sec;
        tp.tv_usec -= 1000000;
    }
    ts.tv_sec = tp.tv_sec;
    ts.tv_nsec = tp.tv_usec*1000;
    
//    printf("- Thread blocked\n");
    rc = pthread_cond_timedwait(&thr.cond, &thr.mutex, &ts);
    if (rc == ETIMEDOUT) {
        printf("- Timed Out!\n");
        pthread_cancel( thr.thr_function );
        return -1;
    }
    else
        return thr.retur;
}


#endif
