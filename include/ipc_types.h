#ifndef __IPC_TYPES_H
    #define __IPC_TYPES_H


#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>     

#include "utils.h"

#define MQ_SIGNAL   5
#define MQ_EVENT    6

#define MQ_HP       10
#define MQ_ACK      11
#define MQ_NOWAIT   12
#define MQ_NORMAL   13


typedef struct _e_ipc_control{
    int ipc_msgflag;
    int time_alive;
    char from_host[256];
    char from_process[256];
    char to_host[256];
    char to_process[256];
}e_ipc_control;


typedef struct _e_ipc{
    int *zmq_context;
    int *zmq_socket;
    char *name_attach;
}e_ipc;


// typedef struct _eipc_shm{
    // int *zmq_context;
    // int *zmq_socket;
    // int pid;
    // int signal;
    // 
    // char name_attach[256];
// }eipc_shm;




typedef struct _msgbuf {
    long type;          /* tipo de msj., debe ser > 0 */
    char data[BUFSIZ];  /* datos del mensaje */
}msg_t;

struct msg_signal{
    int  control;
    int  signal;     
    char to_host[256];
    char to_process[256];     
};

struct msg_event{
    int  control;
    int  event;
    char to_host[256];
    char to_process[256];
    char from_host[256];
    char from_process[256]; 
};

struct msg_tcp_mq{
    long type;
    int  length;
    int  control;
    char to_host[256];
    char to_process[256];
    char from_host[256];
    char from_process[256];
    char data[BUFSIZ-8];
};





/// nuevos

#define     TYPE_EVENT      0xffffff0
#define     TYPE_REPLY      0xffffff1
#define     TYPE_SIGNAL     0xffffff2
#define     SHM_KEY         345345
#define     EIPC_SHM_NAME        "eipcd_shm"

#define MQ_MSG_LEN      (BUFSIZ-1024)

//#define CTR_LOCAL           1
//#define CTR_TCP             2 
//#define CTR_ACK_NONE        0
//#define CTR_ACK_HOST        4
//#define CTR_ACK_PROCESS     8
//#define CTR_TIMEOUT         16
//#define CTR_ALIVE           32
//
//#define CTR_EVENT           64
//#define CTR_MSG             128
//#define CTR_SIGNAL          256

union flags{ 
    int value;
    struct{
        unsigned busy:1;
        unsigned ready:1;
        unsigned local:1;
        unsigned send_tcp:1;
        unsigned ack_host:1;
        unsigned ack_process:1;
        unsigned ack_process_return:1;
        unsigned process_return_true:1;
        unsigned timeout:1;
        unsigned alive:1;
        unsigned msg:1;
        unsigned event:1;
        unsigned signal:1;
        unsigned fill:19;
    }flag;
};

typedef struct _eipc_shm
{
    int pid;
    char process[128];
    union flags reply_ack;
    union flags query_ack;
    int reply_return;
    int query_return;
    int mqid;
    union{
        int info;
        int event;
    };
    pthread_mutex_t mutex;
    pthread_mutex_t mutex_send;
}eipc_shm;

typedef struct _eipc_shm_control
{
    unsigned busy:1;
    unsigned writing:1;
    unsigned ready:1;
    int control;
    int process;
    int query_mq_id;
    int reply_mq_id;
    int max_process;
    pthread_mutex_t mutex;
}eipc_shm_control;

typedef struct _eipc_mq
{
    long type;
    long type_reply;
    timems_t time;
    int timedout;
    int control;                
    int length;
    char to_h[128];
    char to_p[128];
    char from_h[128];
    char from_p[128];
    union{
        int info;                   // signal event
        int event;                   // signal event
    };
    char msg[MQ_MSG_LEN];
}eipc_mq;
#define MQ_FIXE_LENGTH  (sizeof(eipc_mq) - MQ_MSG_LEN - sizeof(long))

typedef struct
{
    long type;
    union flags reply;
    int reply_return;
}eipc_mq_reply;

#define MQ_REPLY_LENGTH  (sizeof(eipc_mq_reply) - sizeof(long))

typedef struct
{
    int control;
    int info;
    eipc_mq msgmq;
}eipc_tcp;

typedef struct
{
    long type;
    int control;
    int info;
}eipc_reply;



#define MQFLAG      IPC_NOWAIT

#define F_RDY       1
#define F_BUSY      2
#define F_EVENT     4
#define F_SEND      8



#define CTR_ACK_NONE            0
#define CTR_LOCAL               1
#define CTR_TCP                 2    
#define CTR_ACK_HOST            4
#define CTR_ACK_PROCESS         8
#define CTR_PROCESS_RETURN      16
#define CTR_TIMEOUT             32
#define CTR_ALIVE               64
#define CTR_EVENT               128
#define CTR_MSG                 256
#define CTR_SIGNAL              512

#define EX_COMMAND              1024
#define EX_WRT_TAG_VAL          2048

//ACK_PROCESS_RETURN
//agregar la señal para ACK_PROCESS_RETURN

#define CTR_ACK             (CTR_PROCESS_RETURN|CTR_ACK_PROCESS|CTR_ACK_HOST|CTR_TIMEOUT|CTR_ALIVE)


#define TCP_PROCESS_MQ_WAIT     1
#define TCP_PROCESS_ALIVE       2
#define TCP_ACK_HOST            CTR_ACK_HOST
#define TCP_ACK_PROCESS         CTR_ACK_PROCESS
#define TCP_ACK                 CTR_ACK



#define MODE_DAEMON     1
#define MODE_PROGRAM    2 

// uso para usuarios
#define ACK_HOST        CTR_ACK_HOST      
#define ACK_PROCESS     CTR_ACK_PROCESS
#define ACK_PROCESS_RETURN     CTR_PROCESS_RETURN
#define ACK_NONE        CTR_ACK_NONE
#define ACK_TIMEOUT     CTR_TIMEOUT
#define ACK_ALIVE       CTR_ALIVE


#define ipc_shm_lock(ctr) { pthread_mutex_lock(&(((eipc_shm_control *)ctr)->mutex));}
#define ipc_shm_unlock(ctr) { pthread_mutex_unlock(&(((eipc_shm_control *)ctr)->mutex));}

#endif

//www.rosario.gov.ar/tramitesonline/patente.do