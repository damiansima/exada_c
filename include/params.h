#ifndef __PARAMS_H
    #define __PARAMS_H

#include "ipc_types.h"


typedef struct
{
    int MAX_PROCESSES;
    int PORT_TCP;
    int SHM_SIZE;
    char SHM_NAME[64];
    char PROGRAM_NAME[64];
    char MQ_QUERY_NAME[64];   
    char MQ_REPLY_NAME[64];   
}param_struct;


/// * * * * * * * * * * * * * * * * * * * * * *
///     read_params() 
/// * * * * * * * * * * * * * * * * * * * * * *

void read_params(param_struct *params) {
    char line[64];
    char name[64];
    char value[64];

    FILE *fp;

    fp = fopen("../cfg/eipcd.cfg", "r");

    while (fgets(line, 64, fp)) {
        if (strlen(line) && ('#' == line[0]))
            ; // ignora comentarios
        else {
            sscanf(line, "%[ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghjiklmnopqrstuvwxyz]=%s\n", name, value);
            name[63] = '\0';
            value[63] = '\0';
            if (!strcmp(name, "MAX_PROCESSES"))
                params->MAX_PROCESSES = atoi(value);
            if (!strcmp(name, "SHM_SIZE"))
                params->SHM_SIZE = atoi(value);
            if (!strcmp(name, "PORT_TCP"))
                params->PORT_TCP = atoi(value);
            if (!strcmp(name, "SHM_NAME"))
                strcpy(params->SHM_NAME, value);
            if (!strcmp(name, "PROGRAM_NAME"))
                strcpy(params->PROGRAM_NAME, value);
            if (!strcmp(name, "MQ_QUERY_NAME"))
                strcpy(params->MQ_QUERY_NAME, value);
            if (!strcmp(name, "MQ_REPLY_NAME"))
                strcpy(params->MQ_REPLY_NAME, value);
            name[0] = '\0';
            value[0] = '\0';
        }
    }
    printf("MAX_PROCESSES=%i\n", params->MAX_PROCESSES);
    printf("SHM_SIZE=%i\n", params->SHM_SIZE);
    printf("PORT_TCP=%i\n", params->PORT_TCP);
    printf("SHM_NAME=%s\n", params->SHM_NAME);
    printf("PROGRAM_NAME=%s\n", params->PROGRAM_NAME);
    printf("MQ_QUERY_NAME=%s\n", params->MQ_QUERY_NAME);
    printf("MQ_REPLY_NAME=%s\n", params->MQ_REPLY_NAME);

}

#endif
