#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <signal.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>                          
#include <sys/select.h>
#include <string.h>
// #include <sys/name.h>
// #include <sys/dev.h>

 
int connect_tcp(char *host,int port);
int create_tcp_socket(char *host,int port);
char *get_ip(char *host);
char *build_get_query(char *page);

int readline( int sock, char *bufptr, int len,int timeseg  );
int readall( int sock, char *bufptr, int len,int timeseg  );
int custreadline( int sock, char *bufptr, int len,int timeseg, char* cendline );
int readhttp(int sock,char **buf);
int sendall(int sock,char *buf,int len);
 
#define HOST  "nse.siderar.ot"
#define PORT 80
#define USERAGENT "HTMLGET 1.1" 
#define BUFSIZE 20480
      
               


/**
 * int sendall(int sock,char *buf,int len)
 * 
 * Bucle para enviar, solo sale cuando envia todo o algun error.
 * @return: 0 si envia todo 
 */
int sendall(int sock,char *buf,int len)
{
    int sent,tmpres;
    sent=0;
    while(len>0)
    { 
        tmpres = send(sock, buf+sent,len,0);
        if(tmpres == -1){
            perror("Can't send query");
            return -1;
        }    
        sent+=tmpres;                  
        len -= tmpres;
    }
    return len;
}
  
/** 
 * int readhttp(int sock,char *buf)
 * 
 * @param sock: sock TCP conectado de donde lee la respuesta
 * @param buf: puntero del puntero  donde se alloca la respuesta con malloc()
 * @return: entero con la longitud leida o errores (-1 -2) 
 * @warning: es necesario realizar free(buf);
 * 
 * Ejemplo:
 *      char *buf;
 *      r=readhttp(sock,&buf)
 *      if (r>0)
 *          // hace algo // 
 *      free (buf);       
 */
int readhttp(int sock,char **buf)
{  
    int length,tmpres;
    
    *buf=malloc(1000);
    // primero lee la cabecera hasta que encuentra el \r\n    
    length=0;
    tmpres=readline(sock,*buf,1000,5);    
    while(tmpres)
    {
        // printf ("--%s",*buf);
        if ( strncmp(*buf,"Content-Length: ",strlen("Content-Length: "))==0 )
        {
            length = atoi ( *buf + strlen("Content-Length: "));
        }
        // busca la linea sola para fin de cabezera
        if(strcmp(*buf, "\r\n")==0)
            break;
        tmpres=readline(sock,*buf,1000,5);
    }
    if(length==0) 
        return -1; // no se lee la longitud
        
    // printf ("\nlength: %i ",length);
    *buf=realloc(*buf,length+1);
    if(buf==0)
        return -2; // error en malloc
        
    memset(*buf,0,length);
    tmpres=readall(sock,*buf,length,5);
    return tmpres;
}  


/**
 * int readline( int sock, char *buf, int maxlen, int timeseg  )
 * 
 * lee una linea delimitada por \n o hasta igual a maxlen;  
 * 
 * @param sock: sock TCP
 * @param buf: bufer donde guardar los datos leidos
 * @param maxlen: longitud maxima a leer
 * @param timesg: tiempo maximo en segundos para empezar a leer  
 * @return: longitud del string leido hasta un \n
 *
 */  
int readline( int sock, char *bufptr, int len,int timeseg )
{
    char *bufx = bufptr;
    char c;  
    fd_set refd;
    int n,cnt;
    struct timeval tv;
    
    tv.tv_sec = 0;
    tv.tv_usec = 1000*timeseg;
    while ( --len > 0 )
    {
        FD_ZERO (&refd);
        FD_SET (sock, &refd );
        n =  select( 1+sock, &refd, NULL, NULL, &tv );
        switch(n) 
        {
        case -1: // error   
            return -1;
        case  0: // timeout, entoces ya no hay mas nada para leer
            return bufptr - bufx;
        default:
            if(FD_ISSET(sock,&refd))
            {
                cnt = recv(sock,&c,1, 0 );
                if ( cnt < 0 )
                {
                    if ( errno == EINTR )
            		{
                        len++;		/* the while will decrement */
                        continue;
            		}
                    return -1;
        	    }
                if ( cnt == 0 )
                    return bufptr - bufx;
            }
            *bufptr++ = c;
            if ( c == '\n' )
            {
                *bufptr = '\0';
                return bufptr - bufx;
            }
            tv.tv_sec = 1;
            tv.tv_usec = 0;
        }
    }
    return bufptr - bufx;
}
  
   
/**
 * int readall( int sock, char *buf, int len,int timeseg )
 * 
 * Lee hasta la longitud len, solo termina cuando lee todo o si se cumple el 
 * tiempo de timedout  
 *  
 * @param sock: socket TCP
 * @param buf: buffer donde guardar los datos leidos
 * @param len: longitud a leer
 * @param timeseg: timedout en segundos para empezar a leer     
 * @return: retorna len si pudo leer completamente 
 *          0 si sale por timedout 
 *          -1 si hay algun error   
 */   
int readall( int sock, char *buf, int len,int timeseg )
{
    fd_set refd;
    int n,tmpres,ibuf;
    struct timeval tv;
    memset(buf, 0, len);
    ibuf=0;  
    tv.tv_sec = 0;
    tv.tv_usec = 1000*timeseg;
    while(len>0)
    {
        FD_ZERO (&refd);
        FD_SET (sock, &refd );
         
        n =  select( 1+sock, &refd, NULL, NULL, &tv );
        switch(n) 
        {
            case -1: // error    
                return -1;
            case  0: // timeout, entoces ya no hay mas nada para leer
                return 0;
            default:
                if(FD_ISSET(sock,&refd))
                {
                    tmpres=recv(sock,buf+ibuf,len,0);
                    if(tmpres < 0)
                    {
                        perror("\nError receiving data");
                        return -1;
                    }
                    ibuf+=tmpres;
                    len-=tmpres;
                }
                break;
        }
//        tv.tv_sec = 1;
//        tv.tv_usec = 0;
    }
    return ibuf;
}

  
int bind_tcp(int port,int liste)
{
    int sock;
    struct sockaddr_in serv_addr;
    int optval=1;
    
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("Can't create TCP socket");
        return (-1);
    }            
    memset(&serv_addr,0,sizeof(struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("0.0.0.0");
    serv_addr.sin_port = htons(port);
    
    setsockopt(sock,SOL_SOCKET, SO_REUSEADDR,&optval, sizeof(optval));
    setsockopt(sock,SOL_SOCKET, SO_REUSEPORT,&optval, sizeof(optval));
    
    if (bind(sock, (struct sockaddr *) &serv_addr, (socklen_t)sizeof(struct sockaddr)) == -1) 
    {
        perror( "Can't binding TCP socket");
        return (-1);
    }
    if (listen(sock, liste) == -1) 
    {
        perror("Can't listen TCP socket");
        return (-1);
    }
    return sock;
}

   
int connect_tcp(char *host,int port)
{
    int sock;
    struct sockaddr_in serv_addr;
    char *ip;
    int optval=1;
    
    if(atoi(host)==0 && host[0]!='0')
        ip = get_ip(host);
    else
        ip = strdup(host);
    
    if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        perror("Can't create TCP socket");
        return (-1);
    }
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);; 
    serv_addr.sin_port = htons(port);
    free(ip);
    
    setsockopt(sock,SOL_SOCKET, SO_REUSEADDR,&optval, sizeof(optval));
    setsockopt(sock,SOL_SOCKET, SO_REUSEPORT,&optval, sizeof(optval));
    
    if(connect(sock,(struct sockaddr *)&serv_addr,(socklen_t)sizeof(struct sockaddr)) < 0){
        perror("Could not connect");
        close (sock);
        return (-2);
    }    
    return sock;
}

int create_tcp_socket(char *ip,int port)
{
    int sock;
    struct sockaddr_in serv_addr;
    
    if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        perror("Can't create TCP socket");
        return (-1);
    }
    
    fprintf(stderr, "IP is %s\n", ip); 
        
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);; 
    serv_addr.sin_port = htons(port);
    
    if(connect(sock,(struct sockaddr *)&serv_addr,(socklen_t)sizeof(struct sockaddr)) < 0){
        perror("Could not connect");
        close (sock);
        return (-2);
    }
    
    return sock;
}
 
 
int hostname_to_ip(char *hostname , char *ip)
{
    struct addrinfo hints, *servinfo, *p;
    struct sockaddr_in *h;
    int rv;
 
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // use AF_INET6 to force IPv6
    hints.ai_socktype = SOCK_STREAM;
 
    if ( (rv = getaddrinfo( hostname , "http" , &hints , &servinfo)) != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }
 
    // loop through all the results and connect to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next)
    {
        h = (struct sockaddr_in *) p->ai_addr;
        strcpy(ip , inet_ntoa( h->sin_addr ) );
    }
     
    freeaddrinfo(servinfo); // all done with this structure
    return 0;
}

char *get_ip(char *host)
{
    struct hostent *hent;
    int iplen = 15; //XXX.XXX.XXX.XXX
    char *ip = (char *)malloc(iplen+1);
    memset(ip, 0, iplen+1);
    
    // setear una ip manual para ver la consulta realizada
    // strcpy(ip,"172.17.19.219");
    // return ip;  
  
    if((hent = gethostbyname(host)) == NULL)
    {
        herror("Can't get IP");
        return(char *) -1;
    }
    strcpy(ip,inet_ntoa(*((struct in_addr *)hent->h_addr)));
    printf("Direccion IP es: %s\n",ip);
             
    return ip;
}
 
 
char *build_get_query(char *param)
{    
    char *staticparam="TpoTeoricoQNX=1566,timeT=1285804177,PesoL2=93500,PesoL1=93500,";
    // static char *pages="webservice/WSModelo.asmx/GetSalidaModelo?strXml=<string><EXECSERVICE><NAME>WebServices</NAME><TITLE>Modelos</TITLE><CLIENTE>MULTICLIENTE</CLIENTE><NIVELSUPERIOR>17</NIVELSUPERIOR><CODMODELO>11742</CODMODELO><PARAMETROS>";
    static char *pages="webservice/WSModelo.asmx/GetSalidaModelo?strXml=<string><EXECSERVICE><NAME>WebServices</NAME><TITLE>Modelos</TITLE><CLIENTE>MULTICLIENTE</CLIENTE><NIVELSUPERIOR>17</NIVELSUPERIOR><CODMODELO>11855</CODMODELO><PARAMETROS>";
    static char *pagee="</PARAMETROS></EXECSERVICE></string>";
    char *query;
    static char *tpl = "GET /%s%s%s%s HTTP/1.1\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n ";
    query = (char *)malloc(strlen(HOST)+strlen(param)+strlen(pages)+strlen(pagee)+
                            strlen(staticparam)+strlen(USERAGENT)+strlen(tpl));
    sprintf(query, tpl, pages,staticparam,param,pagee, HOST, USERAGENT);
    return query;
}


/**
 * int custreadline( int sock, char *buf, int maxlen, int timeseg ,char* cendline)
 * 
 * lee una linea delimitada por \n o hasta igual a maxlen;  
 * 
 * @param sock: sock TCP
 * @param buf: bufer donde guardar los datos leidos
 * @param maxlen: longitud maxima a leer
 * @param timesg: tiempo maximo en segundos para empezar a leer
 * @param cendline: fin de linea personalizado   
 * @return: longitud del string leido hasta un \n
 *
 */  
int custreadline( int sock, char *bufptr, int len,int timeseg, char* cendline )
{
    char *bufx = bufptr;
    char c;  
    fd_set refd;
    int n,cnt;
    int le;
    struct timeval tv;
    
    tv.tv_sec = timeseg;
    tv.tv_usec = 0;
    
    le=strlen(cendline);
    
    while ( --len > 0 )
    {    
        FD_ZERO (&refd);
        FD_SET (sock, &refd );
        n =  select( 1+sock, &refd, NULL, NULL, &tv );
        switch(n) 
        {
        case -1: // error   
            return -1;
        case  0: // timeout, entoces ya no hay mas nada para leer
            return bufptr - bufx;
        default:
            if(FD_ISSET(sock,&refd))
            {
                // WARNING: ver que si se usa un socket o un file descriptor no se comporta
                // igual usando read que rcev
                //cnt = read(sock,&c,1);
                cnt = recv(sock,&c,1,0);
                if ( cnt < 0 )
                {
                    if ( errno == EINTR )
            		{
                        len++;		/* the while will decrement */
                        continue;
            		}
                    return -1;
        	    }
                if ( cnt == 0 )
                    return bufptr - bufx;
            }
            *bufptr++ = c;
            if ( !strcmp(bufptr-le,cendline) )
            {
                *bufptr = '\0';
                return bufptr - bufx;
            }
            tv.tv_sec = 1;
            tv.tv_usec = 0;
        }
    }
    return bufptr - bufx;
}


int create_sock_udp_broadcast(int broadcast)
{
    int sock;
    int optval=1;
    
    sock=socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) 
    {
        perror("Opening socket");
        return sock;
    }    
    if(broadcast)
    {
        setsockopt(sock,SOL_SOCKET, SO_BROADCAST,&optval, sizeof(optval));
    }	
    setsockopt(sock,SOL_SOCKET, SO_REUSEADDR,&optval, sizeof(optval));
    setsockopt(sock,SOL_SOCKET, SO_REUSEPORT,&optval, sizeof(optval));
        
    return sock;
}
