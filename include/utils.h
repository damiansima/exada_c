#ifndef __UTILS_H
#define __UTILS_H

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <string.h>
#include <sys/types.h>
#include <jmorecfg.h>

#include <sys/mman.h>
#include <sys/msg.h>
#include <fcntl.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <sys/time.h>

#include <stdarg.h>   
#include "../library/logger/include/logger.h"

//#define FILE_LOG   "/tmp/log.log"

#define DEBUG 

#define EXADA_TMP  "/tmp"

#ifndef msleep
    #define msleep(x) usleep(1000*(x))
#endif

typedef long long timems_t;

#define TEST(A) {printf("%-40s",#A);printf("%3i\n",A);}
#define TESTS(A,fmt) {printf("%-40s",#A);printf(fmt,A);printf("\n");}
#define TESTP(A) {printf("%-40s",#A);}
#define TESTR(fmt,arg) {printf(fmt,arg);printf("\n");}

//#define assert_if(A) if(!(A)){log_error("%s:%u: e_asert(%s) fail !",__FILE__, __LINE__, #A);}
//#define eassert(A) ((A)? TRUE : FALSE)


// fixme: cambiar todo a logger
#ifndef assert_sql
    #define assert_sql(r) {if(r!=SQLITE_OK){log_error("%s:%u: Failed database %s",__FILE__, __LINE__,sqlite3_errstr(r));sqlite3_close(db);return r;}}
#endif

#ifndef assert_msg_sql
    #define assert_msg_sql(r) {if(r!=SQLITE_OK){log_error("%s:%u: Failed database %s",__FILE__, __LINE__,sqlite3_errstr(r));sqlite3_close(db);}}
#endif

#ifndef assert_errno
    #define assert_errno(A) {if(!(A)){log_error("%s:%u: %s\n",__FILE__, __LINE__, strerror(errno));return errno;}}
#endif

#ifndef assert_msg_errno
    #define assert_msg_errno(A,msg) {if(!(A)){log_error("%s:%u: (%s) %s\n",__FILE__, __LINE__, msg, strerror(errno));return errno;}}
#endif

#ifndef assert_return
    #define assert_return(A) {if(!(A)){fprintf(stderr,"%s:%u: assert_return(%s) fail !\n",__FILE__, __LINE__, #A);return -1;}}
#endif
#ifndef assert_msg
    #define assert_msg(A) ((A)? 0 : fprintf(stderr,"%s:%u: assert_msg(%s) fail !\n",__FILE__, __LINE__, #A))
#endif

// proyecto de 
#define dumpme(x, fmt) printf("%s:%u: %s=" fmt, __FILE__, __LINE__, #x, x)
//int alguna_funcion() {
//    int foo;
//    /* [aquí va un montón de código complicado] */
//    dumpme(foo, "%d");
//    /* [y aquí más código complicado] */
//}



timems_t timems() {
    struct timeval te; 
    gettimeofday(&te, NULL);
    timems_t milliseconds = te.tv_sec*1000LL;
    milliseconds += te.tv_usec/1000LL;
//    printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}

#define PROGRESS_CHARS 50

void print_progress_bar(int percent)
{
	int i;
	int p;
	if (percent > 100)
		percent = 100;
	putc('\r', stderr);
	putc('[', stderr);
	p = PROGRESS_CHARS * percent / 100;
	for (i=0; i < p; i++)
		putc('-', stderr);

	for (i=p; i < PROGRESS_CHARS; i++)
		putc(' ', stderr);
	fprintf(stderr," %3i%%]",percent);
		
	fflush(stderr);
}

void say(const char *pName, const char *pMessage) 
{
    time_t the_time;
    struct tm *the_localtime;
    char timestamp[256];
    
    the_time = time(NULL);
    
    the_localtime = localtime(&the_time);
    
    strftime(timestamp, 255, "%H:%M:%S", the_localtime);
    
    printf("%s @ %s: %s\n", timestamp, pName, pMessage);
    
}



int release_semaphore(const char *pName, sem_t *pSemaphore, int live_dangerously) {
    int rc = 0;
    char s[1024];
    
    say(pName, "Releasing the semaphore.");
    
    if (!live_dangerously) {
        rc = sem_post(pSemaphore);
        if (rc) {
            sprintf(s, "Releasing the semaphore failed; errno is %d\n", errno);
            say(pName, s);
        }
    }
    
    return rc;
}


int acquire_semaphore(const char *pName, sem_t *pSemaphore, int live_dangerously) {
    int rc = 0;
    char s[1024];

    say(pName, "Waiting to acquire the semaphore.");

    if (!live_dangerously) {
        rc = sem_wait(pSemaphore);
        if (rc) {
            sprintf(s, "Acquiring the semaphore failed; errno is %d\n", errno);
            say(pName, s);
        }
    }

    return rc;
}


/*      void logger(char *fmt,...)
*       
*  Se puede pasar muchos argumentos como printf
*  se configura el archivo de log:         
*       #define FILE_LOG   "/tmp/logger.log"
*              
*  Permite hacer logger y printf si esta definido 
*       #define DEBUG
*/
//void logger(const char *fmt,...)
//{
//    FILE *Fd = NULL;
//    char bufti[100];
//    time_t Time;
//    
//    va_list arglist;
//    
//    Fd = fopen( FILE_LOG, "a+" );
//    if( Fd == NULL )
//    {
//        perror( "logger() fopen" );
//    }
//    else
//    {
//        Time = time(NULL);                         
//        strftime(bufti,80,"%d/%m/%Y %H:%M:%S", localtime(&Time));
//        fprintf( Fd, "%s:: ",bufti );
//        va_start( arglist, fmt );
//        vfprintf( Fd, fmt, arglist );
//        va_end( arglist );
//        fprintf( Fd, "\r\n" );
//        fflush(Fd);
//        fclose(Fd);        
//#ifdef DEBUG
//        printf("%s:: ",bufti );
//        va_start( arglist, fmt );
//        vprintf(fmt,arglist);
//        va_end( arglist );
//        printf("\r\n");
//#endif
//    }
//    
//}


int* map_file(char *file,char *mode,int len)
{
	int fd;
	int *tmap;
	
	if(mode[0]=='w' || mode[1]=='w' )
    {
		fd = open(file, O_RDWR | O_CREAT, (mode_t)0600);
        if (ftruncate(fd, len)) {
            perror("Resizing the file failed");
        }
    }
	else
		fd = open(file, O_RDONLY,(mode_t)0600);
	
    if (fd == -1) 
	{
		perror("Error opening file for writing");
		return (int *)-1;
    }
	
	if(mode[0]=='w' || mode[1]=='w' )
		tmap = (int *) mmap(0, len, PROT_READ | PROT_WRITE ,  MAP_SHARED, fd, 0);
	else
		tmap = (int *) mmap(0, len, PROT_READ , MAP_SHARED, fd, 0);
		
	
	close(fd);	
	
    if (tmap == MAP_FAILED) 
    {
		close(fd);
		perror("Error mmapping the file");
		return (int *)-1;
    }
    
    return tmap;	
}


void unmap_file(void *tmap,int len)
{
	if (munmap(tmap, len) == -1) 
	{
		perror("Error un-mmapping the file");
	}
	return;
}


// int* map_shm(const char *file,const char *mode,int len)
// {
	// int fd;
	// int *tmap;
	// 
	// if(mode[0]=='w' || mode[1]=='w' )
	// {
		// fd = shm_open(file, O_RDWR | O_CREAT, (mode_t)0600);
		// if (ftruncate(fd,(off_t) len)) {
            // perror("Resizing the shared memory failed");
        // }
	// }
	// else
		// fd = shm_open(file, O_RDONLY,(mode_t)0600);
		// 
	// if (fd == -1) 
	// {
		// perror("Error opening file for writing");
		// return (int *)-1;
    // }
	// 
	// if(mode[0]=='w' || mode[1]=='w' )
		// tmap = (int *) mmap(0, len, PROT_READ | PROT_WRITE ,  MAP_SHARED, fd, 0);
	// else
		// tmap = (int *) mmap(0, len, PROT_READ , MAP_SHARED, fd, 0);
		// 
	// 
	// close(fd);	
	// 
    // if (tmap == MAP_FAILED) 
    // {
		// close(fd);
		// perror("Error mmapping the file");
		// return (int *)-1;
    // }
    // 
    // return tmap;	
// }
// 
// 
// void unmap_shm(char *name, void *tmap,int len)
// {
	// if (munmap(tmap, len) == -1) 
	// {
		// perror("Error un-mmapping the file");
	// }
    // shm_unlink(name);
	// return;
// }

void delete_shm(char *name)
{
    key_t key;
    int shmid;
    char msg[100];
    char pathfile[200];	
    sprintf(pathfile,"%s/%s",EXADA_TMP,name);
    key = ftok(pathfile,0);
    shmid =shmget(key,0,0);
    sprintf(msg,"delete_shm(): remove shm '%s', shmid:%i\n",name,shmid);
    write(2,msg,strlen(msg));
    shmctl(shmid, IPC_RMID, 0); 
}

static void *map_shm(char *name,const char *mode,int len)
{
    int shmid;    
    void *shared_memory;
    key_t key;
    size_t perm=0400; // read only
    FILE *fd;
    char pathfile[200];
	
    sprintf(pathfile,"%s/%s",EXADA_TMP,name);
    log_info("map_shm:%s",pathfile);    
    key = ftok(pathfile,0);
    if(key<0)
    {
        fd = fopen(pathfile,"w");
        fprintf(fd,"shm key=");
        key = ftok(pathfile,0);
        fprintf(fd,"%i",(int)key);
        fclose(fd);  
        log_msg("key file: %s: %i",pathfile, (int)key);
    }
    if(key<0)
        return (void *)-1;
    
    if(mode[0]=='w' || mode[1]=='w' )
        perm+=0200;
        
    shmid =shmget(key,len,perm | IPC_CREAT);
    
    if (shmid == -1)
    {
        perror("shmget failed");
        if(mode[1]=='t' || mode[2]=='t' )
        {
            delete_shm(name);
            shmid =shmget(key,len,perm | IPC_CREAT);
        }
        if (shmid == -1)
            return (void *)-1;
    }
    log_info("shmid: %i",shmid);
    
    shared_memory = shmat(shmid,(void *)0, 0);
	
    if ((int)shared_memory == -1) 
    {
		perror("Error shmat ");
		return (void *)-1;
    }
    
    return (void *) shared_memory;	
}


void unmap_shm(char *name, void *tmap)
{
    key_t key;
    int shmid;
    char pathfile[200];
    sprintf(pathfile,"%s/%s",EXADA_TMP,name);
    key = ftok(pathfile,0);
    shmid =shmget(key,0,0);
    shmdt(tmap);
    shmctl(shmid, IPC_RMID, 0); 
}



int mq_delete(char *name)
{
    key_t key;
    int mqid;
    char pathfile[200];
    sprintf(pathfile,"%s/%s",EXADA_TMP,name);
    key = ftok(pathfile,0);
    mqid = msgget( key ,0);
    
    if (msgctl(mqid, IPC_RMID, 0) == -1)
    {
        perror("failed to delete");
        return -1;
    }
    return 0;
}

int mq_open(char *name,char *mode)
{
    size_t perm=0400; // read only
    key_t key;
    int mqid;
    
    FILE *fd;
    char pathfile[200];
	
    sprintf(pathfile,"%s/%s",EXADA_TMP,name);
    key = ftok(pathfile,0);
    if(key<0)
    {
        fd = fopen(pathfile,"w");
        fprintf(fd,"msg key=");
        key = ftok(pathfile,0);
        fprintf(fd,"%i",(int)key);
        fclose(fd);  
    }
    if(key<0)
        return -1;
    
    log_info("file:%s key:%i",pathfile, (int)key);
    
    if(mode[0]=='w' || mode[1]=='w' )
        perm+=0200;
    mqid = msgget( key ,perm | IPC_CREAT);
    log_info("mqid: %i",(int)mqid);
    return mqid;
}


#endif

