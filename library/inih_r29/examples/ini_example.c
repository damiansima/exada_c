/* Example: parse a simple configuration file */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../ini.c"

typedef struct
{
    int version;
    const char* name;
    const char* email;
    char active;
} configuration;

static int handler(void* user, const char* section, const char* name, const char* value)
{
    #define MATCH(s, n) (strcmp(section, s) == 0 && strcmp(name, n) == 0)
    configuration* pconfig = (configuration*)user;

    
    if (MATCH("protocol", "version")) {
        pconfig->version = atoi(value);
    } else if (MATCH("user", "name")) {
        pconfig->name = strdup(value);
    } else if (MATCH("user", "email")) {
        pconfig->email = strdup(value);
    } else if (MATCH("user", "active")) {
        pconfig->active = atoi(value);
    } else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}

int main(int argc, char* argv[])
{
    configuration config;

    if (ini_parse("test.ini", handler, &config) < 0) {
        printf("Can't load 'test.ini'\n");
        return 1;
    }
    printf("Config loaded from 'test.ini': version=%d, name=%s, email=%s active=%i\n",
        config.version, config.name, config.email, config.active);
    return 0;
}
