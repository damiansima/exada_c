/* Example: parse a simple configuration file */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../ini.c"

typedef struct
{
    char *section;
    int version;
    const char* name;
    const char* email;
    char active;
} configuration;

int ini_index=0;
char *pc_section = NULL;
configuration Config[3];
configuration *pconfig;

static int handler(void* user, const char* section, const char* name, const char* value)
{
    #define CMPN(n) (strcmp(name, n) == 0)
    
    if( pc_section == NULL ){
        Config[ini_index].section = strdup(section);
        pc_section = Config[ini_index].section;
        
    }
    if ( strcmp(section, pc_section) )
    {
        ini_index += 1;
        Config[ini_index].section = strdup(section);
        pc_section = Config[ini_index].section;
    }
    
    if CMPN("version"){
        Config[ini_index].version = atoi(value);
    }else if CMPN("name"){
        Config[ini_index].name = strdup(value); ;
    }else if CMPN("email"){
        Config[ini_index].email = strdup(value); ;
    }else if CMPN("active"){
        Config[ini_index].active = atoi(value); ;
    }else {
        return 0;
    }
    return 1;
}

int main(int argc, char* argv[])
{
    int i;

    if (ini_parse("test2.ini", handler, NULL) < 0) {
        printf("Can't load 'test.ini'\n");
        return 1;
    }
    
    for (i=0; i<=ini_index; i++)
    {
        pconfig=&Config[i];
        printf("section=%s:: version=%d, name=%s, email=%s active=%i\n",
            pconfig->section, pconfig->version, pconfig->name, pconfig->email, pconfig->active);
    }   
//    printf("Config loaded from 'test.ini': version=%d, name=%s, email=%s active=%i\n",
//        config.version, config.name, config.email, config.active);
    return 0;
}
