#include <stdio.h>
#include <stdlib.h>

#include "../include/logger.h"

int main( void )
{
    // elige el archivo de log
    log_set_file("/tmp/borrar.log");
    
    // selecciona las opciones de debug a imprimir en pantalla
    log_debug_level(LOG_ALERT|LOG_ERROR|LOG_MSG|LOG_INFO|LOG_WARNING);
    //log_debug_level(LOG_ALERT|LOG_ERROR|LOG_WARNING);
    
    // solo guardo error y alert en archivo 
    log_logger_level(LOG_ERROR|LOG_ALERT); 
    
    // ejemplo de uso 
    log_msg("%s %d %s", "Failed", 1010, "times" );
    log_error("desconocida el tag");
    log_info("Alta informacion %i",15);
    log_alert("valro x > %d",45);        
    log_warning("valro x > %d",45);
    
    return EXIT_SUCCESS;
}
  