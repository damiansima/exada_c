#ifndef __LOGGER_H
        #define __LOGGER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>   
#include <sys/time.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>


#define _LOG_ERROR       "\033[01;31m"
#define _LOG_ALERT       "\033[01;31m"
#define _LOG_WARNING     "\033[01;33m"
#define _LOG_MSG         "\033[01;34m"
#define _LOG_INFO        "\033[01;32m"
#define _LOG_DEBUG       "\033[01;44m"
#define _LOG_RESET       "\033[00m"

//#define _LOG_ERROR       "\033@40\033<"
//#define _LOG_ALERT       "\033@40\033<"
//#define _LOG_WARNING     "\033@60\033<"
//#define _LOG_MSG         "\033@70\033<"
//#define _LOG_INFO        "\033@20\033<"
//#define _LOG_RESET       "\033R"


#define __LOG_ALERT     1
#define __LOG_ERROR     1<<1
#define __LOG_INFO      1<<2
#define __LOG_MSG       1<<3
#define __LOG_WARNING   1<<4
#define __LOG_DEBUG     1<<5


// variables privadas globales
static char __FILE_LOG[200]="/tmp/logger.log";
static int __LOG_PRINT=__LOG_ALERT|__LOG_ERROR;
static int __LOGGER=255;
static FILE *Fd = NULL;


void log_set_file(char *log)
{
    strcpy(__FILE_LOG,log);
    printf(_LOG_INFO"- INFO: "_LOG_RESET);
    printf("FILE_LOG: %s\n",__FILE_LOG);
}              

void log_logger_level(int lev)
{
    __LOGGER=lev;
}

void log_debug_level(int lev)
{
    __LOG_PRINT=lev;
}


void _debug(int mode, const char *fmt, va_list arglist)
{    
    switch(mode)
    {
        case __LOG_ALERT: printf(_LOG_ALERT"- ALERT: "_LOG_RESET);break;
        case __LOG_ERROR: printf(_LOG_ERROR"- ERROR: "_LOG_RESET);break;
        case __LOG_INFO: printf(_LOG_INFO"- INFO: "_LOG_RESET);break;
        case __LOG_DEBUG: printf(_LOG_DEBUG"- DEBUG:"_LOG_RESET" ");break;
        case __LOG_WARNING: printf(_LOG_WARNING"- WARNING: "_LOG_RESET);break;
        default: printf(_LOG_MSG"- MSG: "_LOG_RESET);break;
    }
    vprintf(fmt,arglist);
    printf("\n"); 
}
   
void _vlogger(char *mode, const char *format, va_list arglist)
{
    
    char bufti[100];
    time_t Time;
    struct timeval  tv;
    if( Fd == NULL )
    {
        Fd = fopen( __FILE_LOG, "a+" );
        if( Fd == NULL )
            perror( "_vlogger::fopen() falil !" );
    }
    if( Fd != NULL )
    {
        gettimeofday(&tv, NULL);
        Time = time(NULL);                         
        strftime(bufti,80,"%d/%m/%Y %H:%M:%S.%%03u ", localtime(&Time));
        fprintf( Fd, bufti,(tv.tv_usec/1000));
        fprintf( Fd,"%s ", mode);
        vfprintf( Fd, format, arglist );
        fprintf( Fd, "\n" );
        fflush(Fd);
//        fclose(Fd);
    }
}


/*
log_logger() generico     [OK]
log_error() error         [OK]
log_alert() alerta        [OK]
log_info()  informacion   [OK]
log_debug() depurac  n    [NO]
log_user()  usuario       [NO]
*/

void log_alert(unsigned int mod, char *fmt,...)
{
    va_list arglist;
    if(__LOGGER & __LOG_ALERT)
    {
        va_start( arglist, fmt );
        _vlogger("ALERT:", fmt, arglist );
        va_end( arglist );
    }
    if(__LOG_PRINT & __LOG_ALERT)
    {
        va_start( arglist, fmt ); 
        _debug(__LOG_ALERT,fmt,arglist);
        va_end( arglist );
    }
}

/*
 * void log_debug(char *fmt,...)
 * yea men... en la documentacion
 */
void log_debug(char *fmt,...)
{
    va_list arglist;
    
    if(__LOGGER & __LOG_DEBUG)
    {
        va_start( arglist, fmt );
        _vlogger("DEBUG:", fmt, arglist );
        va_end( arglist );
    }
    if(__LOG_PRINT & __LOG_DEBUG)
    {
        va_start( arglist, fmt ); 
        _debug(__LOG_DEBUG,fmt,arglist);
        va_end( arglist );
    }
}

void log_info( char *fmt,...)
{
    va_list arglist;
    if(__LOGGER & __LOG_INFO)
    {
        va_start( arglist, fmt );
        _vlogger("INFO:", fmt, arglist );
        va_end( arglist );
    }
    if(__LOG_PRINT & __LOG_INFO)
    {
        va_start( arglist, fmt ); 
        _debug(__LOG_INFO,fmt,arglist);
        va_end( arglist );
    }
}

void log_error(char *fmt,...)
{
    va_list arglist;
    
    if(__LOGGER & __LOG_ERROR)
    {
        va_start( arglist, fmt ); 
        _vlogger("ERROR:", fmt, arglist );
        va_end( arglist );
    }
    if(__LOG_PRINT & __LOG_ERROR)
    {
        va_start( arglist, fmt ); 
        _debug(__LOG_ERROR,fmt,arglist);
        va_end( arglist );
    }
}


void log_msg(char *fmt,...)
{
    va_list arglist;
    if(__LOGGER & __LOG_MSG)
    {
        va_start( arglist, fmt );
        _vlogger("MSG:", fmt, arglist );
        va_end( arglist );
    }
    if(__LOG_PRINT & __LOG_MSG)
    {
        va_start( arglist, fmt ); 
        _debug(__LOG_MSG,fmt,arglist);
        va_end( arglist );
    }
}

void log_warning(char *fmt,...)
{
    va_list arglist;
    if(__LOGGER & __LOG_WARNING)
    {
        va_start( arglist, fmt );
        _vlogger("WARNING:", fmt, arglist );
        va_end( arglist );
    }
    if(__LOG_PRINT & __LOG_WARNING)
    {
        va_start( arglist, fmt ); 
        _debug(__LOG_WARNING,fmt,arglist);
        va_end( arglist );
    }
}






#endif
