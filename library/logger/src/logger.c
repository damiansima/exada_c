#ifndef _LOGGER_H
        #define _LOGGER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>   
#include <sys/time.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include "../include/logger.h"





#define _LOG_ERROR       "\033@40\033<"
#define _LOG_ALERT       "\033@40\033<"
#define _LOG_WARNING     "\033@60\033<"
#define _LOG_MSG         "\033@70\033<"
#define _LOG_INFO        "\033@20\033<"
#define _LOG_RESET       "\033R"


#define __LOG_ALERT 1
#define __LOG_ERROR 1<<1
#define __LOG_INFO  1<<2
#define __LOG_MSG   1<<3
#define __LOG_WARNING 1<<4


// variables privadas globales
char __FILE_LOG[200]="/tmp/logger.log";

int __LOG_DEBUG=__LOG_ALERT|__LOG_ERROR;
int __LOGGER=255;


void log_set_file(char *log)
{
    strcpy(__FILE_LOG,log);
    printf(_LOG_INFO"- INFO: "_LOG_RESET);
    printf("FILE_LOG: %s\n",__FILE_LOG);
}              

void log_logger_level(int lev)
{
    __LOGGER=lev;
}

void log_debug_level(int lev)
{
    __LOG_DEBUG=lev;
}

/*
log_logger() generico     [OK]
log_error() error         [OK]
log_alert() alerta        [OK]
log_info()  informacion   [OK]
log_debug() depuraci�n    [NO]
log_user()  usuario       [NO]
*/

void log_alert(char *fmt,...)
{
    va_list arglist;
    
    if(__LOGGER & __LOG_ALERT)
    {
        va_start( arglist, fmt );
        logger("ALERT:", fmt, arglist );
        va_end( arglist );
    }
    if(__LOG_DEBUG & __LOG_ALERT)
    {
        va_start( arglist, fmt ); 
        _debug(__LOG_ALERT,fmt,arglist);
        va_end( arglist );
    }
}

void log_info(char *fmt,...)
{
    va_list arglist;
    
    if(__LOGGER & __LOG_INFO)
    {
        va_start( arglist, fmt );
        logger("INFO:", fmt, arglist );
        va_end( arglist );
    }
    if(__LOG_DEBUG & __LOG_INFO)
    {
        va_start( arglist, fmt ); 
        _debug(__LOG_INFO,fmt,arglist);
        va_end( arglist );
    }
}

void log_error(char *fmt,...)
{
    va_list arglist;
    
    if(__LOGGER & __LOG_ERROR)
    {
        va_start( arglist, fmt ); 
        logger("ERROR:", fmt, arglist );
        va_end( arglist );
    }
    if(__LOG_DEBUG & __LOG_ERROR)
    {
        va_start( arglist, fmt ); 
        _debug(__LOG_ERROR,fmt,arglist);
        va_end( arglist );
    }
}

void log_msg(char *fmt,...)
{
    va_list arglist;
    
    if(__LOGGER & __LOG_MSG)
    {
        va_start( arglist, fmt );
        logger("MSG:", fmt, arglist );
        va_end( arglist );
    }
    if(__LOG_DEBUG & __LOG_MSG)
    {
        va_start( arglist, fmt ); 
        _debug(__LOG_MSG,fmt,arglist);
        va_end( arglist );
    }
}

void log_warning(char *fmt,...)
{
    va_list arglist;
    
    if(__LOGGER & __LOG_WARNING)
    {
        va_start( arglist, fmt );
        logger("WARNING:", fmt, arglist );
        va_end( arglist );
    }
    if(__LOG_DEBUG & __LOG_WARNING)
    {
        va_start( arglist, fmt ); 
        _debug(__LOG_WARNING,fmt,arglist);
        va_end( arglist );
    }
}


void _debug(int mode, const char *fmt, va_list arglist)
{    
    switch(mode)
    {
        case __LOG_ALERT: printf(_LOG_ALERT"- ALERT: "_LOG_RESET);break;
        case __LOG_ERROR: printf(_LOG_ERROR"- ERROR: "_LOG_RESET);break;
        case __LOG_INFO: printf(_LOG_INFO"- INFO: "_LOG_RESET);break;
        case __LOG_WARNING: printf(_LOG_WARNING"- WARNING: "_LOG_RESET);break;
        default: printf(_LOG_MSG"- MSG: "_LOG_RESET);break;
    }
    vprintf(fmt,arglist);
    printf("\n"); 
    flushall();
}
   
void logger(char *mode, const char *format, va_list arglist)
{
    FILE *Fd = NULL;
    char bufti[100];
    time_t Time;
//    int	filedes, sizefile=0;

	//Limito el tamano del archivo de log
        // filedes = open( __FILE_LOG, O_RDONLY );
        // if( filedes != -1 )
        // {
            // sizefile = filelength( filedes );
            // close( filedes );
        // }
        // if( sizefile > 50000000 )
        // {
            // sprintf(bufti,"mv %s %s.old",__FILE_LOG,__FILE_LOG);
            // system( bufti );
        // }
    
    Fd = fopen( __FILE_LOG, "a+" );
    if( Fd == NULL )
    {
        perror( "Loglib:logger2() fopen" );
    }
    else
    {
        Time = time(NULL);                         
        strftime(bufti,80,"%d/%m/%Y %H:%M:%S", localtime(&Time));
        fprintf( Fd, "%s ",bufti );
        fprintf( Fd,"%s ", mode);
        vfprintf( Fd, format, arglist );
        fprintf( Fd, "\n" );
        fflush(Fd);
        fclose(Fd);
    }
}



#endif
