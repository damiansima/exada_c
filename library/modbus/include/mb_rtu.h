
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>   
#include <time.h>

#include <pthread.h>



typedef struct {
    int     fd;
    char*   port_name;
    int     timeout;
    int     baudrate;
    char    parity;
    int     bytesize;
    int     stopbits;
    pthread_mutex_t mtx;
}e_driver_t;

typedef union{
    ushort val;
    struct{
        unsigned char low;
        unsigned char high;
    };
}ushrthl;

typedef enum {
    NO_ERROR,
    E_PREV_OPEN,
    E_NO_OPEN,
}edrv_error; 


int fd_open(char *portname, int baudrate, char parity, int bytesize, int stopbits);
        
int e_fd_write(int fd, char *buf, int nbytes, int timeout);
int e_fd_read(int fd, char *buf, int nbytes, int saddres, int timeout);
ushort e_CRC16(char *data, ushort len);


e_driver_t* edrv_init(const char *fmt, ...);
edrv_error edrv_connect(e_driver_t* edrv);
edrv_error edrv_disconnect(e_driver_t* edrv);

// slave_addres,register_addres,number_of_register
edrv_error edrv_write (e_driver_t* edrv, int slave_addres, int register_addres, int value);
edrv_error edrv_read_block (e_driver_t* edrv, int slave_addres, int register_addres, int number_of_register, char *msg);

//edrv_error edrv_write_block(e_driver_t* edrv, const char *plc, const char *address, int nregs, int reglen, void *data);

//edrv_error edrv_read_coils (e_driver_t* edrv, const char *plc, const char *address, int nregs, int reglen, void *data, const char *options);
//edrv_error edrv_write_coil (e_driver_t* edrv, const char *plc, const char *address, int value);

