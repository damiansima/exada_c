
#include "../../../library/logger/include/logger.h"
#include "../include/mb_rtu.h"
#include "../../../include/utils.h"

#include <stdio.h>
#include <stdarg.h>



void lee_arg(const char *fmt, ...)
{
    #define CMPSTR(a) (strncasecmp(&fmt[index], a, strlen(a))==0)
    va_list ap;
    char parity;
    int index=0;
    e_driver_t *edrv=calloc(1,sizeof(e_driver_t));
    va_start( ap, fmt ); 
    
    edrv->fd = -1;
    edrv->timeout = 200;
    edrv->baudrate = 9600;
    edrv->bytesize = 8;
    edrv->stopbits = 1;
    edrv->parity = 'N';
    edrv->port_name = 0;
    index=0;
    while(fmt[index])
    {
        if CMPSTR("baudrate")
            edrv->baudrate = va_arg(ap, int);
        else if CMPSTR("parity")
        {
            parity = (char) va_arg(ap, int);
            if (parity<3)
                edrv->parity = parity ==1 ? 'O':parity ==2 ? 'P':'N';
            else
                edrv->parity = parity =='O' ? 'O':parity =='P' ? 'P':'N';
        }
        else if CMPSTR("bytesize")
            edrv->bytesize = va_arg(ap, int);
        else if CMPSTR("port_name")
            edrv->port_name = va_arg(ap, char *);
        else if CMPSTR("timeout")
            edrv->timeout = va_arg(ap, int);
        else if CMPSTR("stopbits")
            edrv->stopbits = va_arg(ap, int);
        index++;
    }
    #undef CMPSTR
    va_end( ap );
    
    TESTS(edrv->baudrate,"%i");
    TESTS(edrv->parity,"%c");
    TESTS(edrv->port_name,"%s");
    TESTS(edrv->bytesize,"%i");
    TESTS(edrv->stopbits,"%i");
    TESTS(edrv->timeout,"%i");
    sleep(1);
    free(edrv);
}
    
int main()
{
//    int numopt=0;
//    char *options[10];
//    char values[10];
    
//    lee_arg("baudrate,parity,port_name",34,'P',"/dev/ser1");
    
    ushrthl *_val;
    ushort val[128];
    char msg[256];
    int i;
    int len=22;
    
    log_set_file("/tmp/log.log");
    log_debug_level(255);
    log_logger_level(0);
    
    
    e_driver_t *edrv = edrv_init("baudrate,parity,port_name,timeout",9600,'P',"/tmp/tty1",200);
    
    edrv_error err = edrv_connect(edrv);
    
    edrv_write(edrv,43,3,3);
    
    edrv_read_block (edrv, 43, 0, len , msg);
        
    for (i=0;i<len;i++)
    {
        _val=(ushrthl *)&val[i];
        _val->high=msg[3+i*2];
        _val->low=msg[4+i*2];
        printf(" %i:%i",i,val[i]);
    }
    printf("\n");
    
    TESTS(err,"%i");
    
    sleep(1);
    free(edrv);
    return 0;
}
