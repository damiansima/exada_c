#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

#include <stdarg.h>

typedef struct _eipc_shm
{
    int pid;
    char process[128];
    int control;        
    int flags;
    int event;
    int mq_count;
    int mqid;
}eipc_shm;


#define MAX_PROCESS 20
eipc_shm SHM[MAX_PROCESS];
eipc_shm *shm;


int push_process(eipc_shm *shm, eipc_shm *process)
{
    int i;
    
    for(i=0; i<MAX_PROCESS; i++)
    {
        if( shm->pid == 0 )
        {
            shm->pid=process->pid;
            shm->mqid=process->mqid;
            strcpy(shm->process,process->process);
        }
    }
}

int main(void)
{

    char *str="hola mundo";
    
    printf("hola mundo 2\n");
    
    
    openlog("syslog-test", LOG_PID, LOG_USER);
    
    syslog(7, "log: %s", str);
    closelog();
    
    return 0;
}