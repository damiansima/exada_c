<html>
<head><title>Comp111 Assignment 1</title> </head> 
<body>
<h3> Comp111 Assignment 1: measuring time</h3> 

<h3> Overview</h3>

<p> In class we have discussed the nature of time in an operating
system, including the fundamental inaccuracies in the system clock and
the concept of operating system "overhead".  In this assignment, we
will write a program that measures and quantifies the time taken to
execute a variety of C library functions and system calls.  This
program can be run on a variety of linux hardware to measure library
function speed on each kind of hardware.

<h3>Objectives</h3>

<p> Your objective is to write a program <code>timing.c</code> 
that measures the time taken for the following function calls that 
we will use later in the course: 
<ol> 
<li> <code>pthread_mutex_lock</code> -- lock a mutex lock. 
<li> <code>sem_post</code> -- modify the state of a semaphore. 
<li> <code>open</code> -- open a file for reading. 
<li> <code>sbrk</code> -- allocate pages of memory. 
</ol> 
This program should measure and print the following averages on <code>stdout</code>: 
<ol type='a'> 
<li> The identity of the function. 
<li> User time taken for the call. 
<li> System time taken for the call. 
<li> When there is variation in the time due to circumstances, 
itemize (at least five) circumstances that affect timing,  
and report the average user and system time taken for each circumstance. 
</ol> 

<h3>Getting started</h3> 
<p> 
To get started,
<pre> 
mkdir Comp111
cd Comp111
mkdir a1
cd a1
cp /comp/111/a/a1/timing.c .
gcc -g timing.c -lrt -lm -lpthread
</pre> 
The compilation command includes three libraries: 
<ul> 
<li> -lrt: the high resolution clock. 
<li> -lm: mathematical functions. 
<li> -lpthread: thread functions including <code>pthread_mutex_lock</code>. 
</ul> 
Your objective is to modify <code>timing.c</code> so that it meets the 
requirements above. As written, it isn't very interesting! 

<h3>Hints</h3> 
This <em>seems</em> like a really simple assignment, but you will quickly discover
that it is a very subtle and difficult assignment indeed. The functions
above have been chosen to expose you to a variety of timing difficulties, to wit: 
<ol> 
<li> 
The main difficulty in this assignment is not in measuring time. The difficulty 
lies in <em>avoiding</em> measuring things <em>other</em> than the time 
of interest. 
<ol type='a'> 
<li> We know already that one has to repeat something a number of 
times in order to measure its runtime. 
<li> The problem is that the act of repeating also takes time. 
<li> Thus, the solution is to measure the act of repeating -- without 
the function call, -- and then subtract that from the time for repeating 
<em>with</em> the function call. 
</ol> 
<li> 
Another basic difficulty is that the timing of some of the 
system calls varies with their arguments and/or the context
in which they are called, e.g., <code>open</code> 
and <code>sbrk</code>. In these cases, you should attempt to quantify
the variation, which means that you need to vary the arguments yourself. 
For example, 
<ul> 
<li> The time taken by <code>open</code> may vary with file path length, 
and also, with whether the file is already open or not. 
<li> The time taken by <code>sbrk</code> may vary with how much memory
is requested. 
</ul> 
<li> A third basic difficulty is that some of the routines to be measured
cannot be called in isolation. One can call <code>pthread_mutex_lock</code>
only once before it blocks on the second call. One must call <code>pthread_mutex_unlock</code> to unlock it. Likewise -- and more subtle -- 
calling <code>open</code> more than about 40 times without closing the opened files 
will fill the file descriptor table, so that further <code>open</code>s will fail. Thus, if you simply repeat <code>open</code> 1000 times without <code>close</code>, only the first 37 or so will actually open anything (open is called three times -- for <code>stdin</code>, <code>stdout</code>, <code>stderr</code> -- before main is invoked)!
<li> A final difficulty is that some system calls and library functions 
take more time the <em>first</em> time they are called. E.g., <code>malloc</code> -- as we will study -- takes more time the first time to set up the memory desriptor table. Likewise, the first time it is called, <code>printf</code> allocates the stdio memory buffers. 

</ol> 

<h3>Survival Skills</h3> 

The typical student might be tempted to simply write the whole program
before testing anything.  This is a <em>fatal</em> strategy for this
assignment.  Instead, implement and test small parts before trying for
overarching solutions.  In dealing with a new program, you are not
just debugging the program; you are <em>debugging your understanding
of the program</em>.  Thus, it is likely that proceeding directly to
an overarching and comprehensive solution will waste a <em>lot</em> of
time.  Instead, make sure you understand what is going on with a
series of smaller "experiments", and <em>keep records as to what works
and does not work.</em>.

<p>Second, when you have a problem, <em>concentrate on understanding
what the program does, rather than what it does not do.</em> Serial
programs like this are <em>deterministic.</em> Thus whatever it does
not do is a problem with your understanding, and not a problem with
the program.  Correct the understanding and the program will be much
easier to correct in turn.

<h3> Submission checklist</h3> 
<table border='1'> 
<tr><td>&nbsp;&nbsp;&nbsp;</td><td>Submission is in <em>one C file</em> <code>timing.c</code>. </td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;</td><td>All measurement is done with that program. </td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;</td><td>User and system time are printed on <code>stdout</code> in seconds (%e format).</td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;</td><td>The function under test is clearly identified in each printout.</td></tr>

<tr><td>&nbsp;&nbsp;&nbsp;</td><td>In case of variations, (at least) five situations 
with repeatably distinct average times are described and measured in the program output.</td></tr>
</table> 

<h3> Submitting completed assignments</h3>
To submit this program, first ssh to <code>comp111-01.cs.tufts.edu</code>,
cd to the directory in which the solution resides, and then 
type:
<pre>
provide comp111 a1 timing.c
</pre>
It will be graded offline. To see grading status or comments, type
<pre>
progress comp111
</pre>
or check the "Grading" link on the course homepage. 

<h3>Grading</h3> 

When grading your solution, I will select a linux machine at random
and run your solution on it. I am not going to tell you my results
in advance.  Your grade will be based upon: 
<ul> 
<li> Obtaining measurements that are -- within reasonable error bounds -- <em>identical to mine</em>. 
<li> <em>Itemizing circumstances</em> that affect timing of the calls, 
together with timing of each circumstance. 
<li> <em>Proper experimental design</em> of your program: it must be designed to measure the appropriate things. 
<li> <em>Not exceeding</em> completion time bounds. 
</ul> 
 
<h3>Lateness</h3> 
There will be one point of ten possible points (10%) subtracted per week 
late or fraction thereof. 

<h3>Extra Credit</h3> 
For 10% extra credit, find a way to measure the execution time for the 
<code>fork</code> system call --  both for the parent and the child -- without
crashing our systems in the process! 
</body>
</html>

