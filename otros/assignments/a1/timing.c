// Examples of how to call salient timing functions 

#define _POSIX_C_SOURCE 199309 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <stdint.h> 
#include <time.h>
#include <sys/time.h> 
#include <sys/resource.h> 
#include <pthread.h> 
#include <semaphore.h> 
#include <fcntl.h> 

// not declared in standard headers
extern void *sbrk(intptr_t); 

// get the resolution of the real time clock
inline double seconds_per_tick()
{
    struct timespec res; 
    clock_getres(CLOCK_REALTIME, &res);
    double resolution = res.tv_sec + (((double)res.tv_nsec)/1.0e9);
    return resolution; 
}

int main() { 
    int success; 

    // how to invoke mutex locks 
    pthread_mutex_t onelock; 
    pthread_mutex_init(&onelock,NULL); 
    pthread_mutex_lock(&onelock); 
    pthread_mutex_unlock(&onelock); 
 
    // how to manipulate semaphores 
    sem_t onesem;  
    success = sem_init(&onesem,0,0); 
    if (success<0) perror("sem_init"); 
    success = sem_post(&onesem); 
    if (success<0) perror("sem_post"); 

    // how to open a file 
    int fd = open("/etc/motd", O_RDONLY); 
    // did it work? 
    if (fd<0) perror("open"); 
    close(fd); 
    
    // how to allocate memory
    void *p; 
    p=sbrk(8024); 
    // did it work? 
    if (p==NULL) perror("sbrk"); 

    // how to read user and system time 
    struct rusage buf; 
    getrusage(RUSAGE_SELF, &buf); 
    double u=(double)buf.ru_utime.tv_sec 
            + ((double)buf.ru_utime.tv_usec)/1e6; 
    double s=(double)buf.ru_stime.tv_sec 
            + ((double)buf.ru_stime.tv_usec)/1e6; 
    printf("user=%e system=%e\n",u,s); 
} 
