#include "/comp/111/a/a4/anthills.h" 

#define TRUE 1
#define FALSE 0

int initialized=FALSE; // semaphores and mutexes are not initialized 

// define your mutexes and semaphores here 
// they must be global variables. 

// first thread initializes mutexes 
void *aardvark(void *input) { 
    char aname = *(char *)input; 
    // first caller need to initialize the mutexes!
    if (!initialized++) { // this succeeds only for one thread
	// initialize your mutexes and semaphores here
    } 

    // now be an aardvark
    while (chow_time()) { 
        // try to slurp
	slurp(aname,0); // identify self, slurp from first anthill
    } 

    return NULL; 
} 
