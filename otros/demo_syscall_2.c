#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>
#include <stdio.h>



static void sig_handler(int signo)
{
    printf("Quiting For Signal No. %d\n", signo);
//    exit(5);
}


int
main(int argc, char *argv[])
{
    pid_t tid;

    signal(SIGHUP,sig_handler);
    
    tid = syscall(SYS_gettid);
    printf("tid: %d\n",tid);
    tid = syscall(SYS_tgkill, getpid(), tid, SIGHUP);
    printf("tid: %d\n",tid);
    printf("tid: %d pid:%d \n", tid, getpid());
    sleep(3);
}