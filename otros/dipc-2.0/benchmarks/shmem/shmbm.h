
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <linux/dipc.h>
#include <unistd.h>
#include <errno.h>
#include <sys/signal.h>
#include <sys/times.h>

#ifdef __GLIBC__
#define IPC_DIPC   00010000   /* make it distributed */
#endif

#if !defined(__GNU_LIBRARY__) || defined(_SEM_SEMUN_UNDEFINED)
union semun {
	int val;
	struct semid_ds *buf;
	unsigned short int *array;
	struct seminfo *__buf;
	void *__pad;
};
#endif

#define SEM_KEY 110
#define SEM_MODE (IPC_DIPC | IPC_EXCL | 0777)

#define MSG_RESULT_KEY 111 
#define MSG_MODE (IPC_DIPC | IPC_EXCL | 0777)
#define MSG_RESULT_SIZE 100
#define MSG_TYPE 10

#define SHM_MODE (IPC_DIPC | IPC_EXCL | 0777)
#define SHM_NUM 7

struct test_shm_t
{
 int key;
 int size;
 int id;
 char *ptr;
 double time;
}test_shm[SHM_NUM] = {
 {101, 1000, -1, NULL, 0.0}, 
 {102, 3500, -1, NULL, 0.0}, {103, 7500, -1, NULL, 0.0}, 
 {104, 11000, -1, NULL, 0.0}, {105, 25000, -1, NULL, 0.0} , 
 {106, 50000, -1, NULL, 0.0}, {107, 80000, -1, NULL, 0.0} };

struct msg_result
{
 long mtype;
 char mtext[MSG_RESULT_SIZE];
};
