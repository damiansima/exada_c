/*
 * msgbenchmark.h
 *
 * Header file for msgbench.c and msgmark.c
 *
 * By Kamran Karimi
 */
 
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/signal.h>
#include <errno.h>
#include <unistd.h>
#include <sys/times.h>

#ifdef __GLIBC__
#define IPC_DIPC   00010000   /* make it distributed */
#endif

#if !defined(__GNU_LIBRARY__) || defined(_SEM_SEMUN_UNDEFINED)
union semun {
	int val;
	struct semid_ds *buf;
	unsigned short int *array;
	struct seminfo *__buf;
	void *__pad;
};
#endif

#define SHM_KEY 20
#define SHM_MODE (IPC_DIPC | IPC_EXCL| 0777)
#define SHM_SIZE 100

#define SEM_KEY 21
#define SEM_MODE (IPC_DIPC | IPC_EXCL | 0777)

#define SEM_DONE_KEY 22

#define MSG_KEY 40
#define MSG_MODE (IPC_DIPC | IPC_EXCL | 0777)
#define MSG_TYPE 10
#define MSG_SIZE 4000

#define MSG_RESULT_KEY 41
#define MSG_RESULT_SIZE 100

struct message
{
 long mtype;
 char mtext[MSG_SIZE];
};

struct msg_result
{
 long mtype;
 char mtext[MSG_RESULT_SIZE];
};
