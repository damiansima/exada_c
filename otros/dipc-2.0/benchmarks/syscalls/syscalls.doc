
 1.0) Introduction
 This is a benchmarking program to measure the speed of executing some of
the DIPC system calls in your system (computers and networks).

 2.0) How it works 
 The program consists of two executable files: sysbench and sysmark. The 
sysbench program sets up a shared memory, a message queue, and a semaphore 
set. sysmark is then started by sysbench to make the system calls. The place 
where sysmark should be executed is determined by the contents of the 'sys_s' 
script. You could choose sysmark to be executed on the same machine as 
sysbench, or you could use the rsh command to run it in another computer. In 
this case just make sure both computers are in the same DIPC cluster.
 
 The tested system calls are: 
 semget()
 shmget()
 msgget()
 semctl() with the IPC_STAT command
 semctl() with the IPC_SET command
 msgctl() with the IPC_STAT command
 msgctl() with the IPC_SET command
 shmctl() with the IPC_STAT command
 shmctl() with the IPC_SET command
 msgsnd() with 1, 10, 100, 500, 1000, 2000 and 4000 byte messages
 msgrcv() with 1, 10, 100, 500, 1000, 2000 and 4000 byte messages
 semop()

 The first three system calls are performed only once. The rest are performed 
at least once, and if finished in less than 1 second, they are repeated 
until at least 1 second has passed. This method is used to get a more reliable 
result on fast computers and/or networks.

 The results are then sent to sysbench using a System V messages, and are 
displayed to the user.

 Note: With the exception of the first three cases, you should wait for at 
least 1 second for the results of each test to become available. Expect a
longer time if your network is not fast enough.


3.0) Installing it
 To be able to use the program on your system, do the following: 

3.1) Compile sysbench and sysmark. Do this by typing make.

3.2) Place one instance of sysmark file in other machines. They should be 
connected over a TCP/IP network to the current machine.

3.3) Modify the sys_s script and change it according to your system's
configuration and IP addresses. Make sure that rsh can find the sysmark 
program.


4.0) Running it
 You may not be able to use rsh when you are root, So you'd better not be the
root. Now just type sysbench. Note that you may have to wait a while for the 
program to do its work and finish.

