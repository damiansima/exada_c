/*
 * dipc.c
 *
 * part of dipcd source code
 *
 * Copyright (C) Kamran Karimi
 */

#include <linux/unistd.h>
#include <unistd.h>

/* this is the DIPC system call */

#ifndef __alpha__
/* this should be the same as the value defined in asm/ipc.h */
#define DIPC 25

#ifdef __GLIBC__
int dipc(int first, int cmd, void *ptr)
{
 return syscall(__NR_ipc,DIPC,first,cmd,ptr);
}
#else
 _syscall4(int,ipc,int,t,int,r,int,s,int *,u); 

int dipc(int first, int cmd, void *ptr)
{
 return ipc(DIPC, first, cmd, ptr);
} 
#endif

#else /* under alpha */
#define __NR_dipc 371

int dipc(int first, int cmd, void *ptr)
{
return syscall(__NR_dipc,first,cmd,ptr);
}
#endif
