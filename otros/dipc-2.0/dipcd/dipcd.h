/*
 * dipcd.h
 *
 * part of dipcd source code
 *
 * Copyright (C) Kamran Karimi
 */

#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pwd.h>
#include <netdb.h>
#include <sys/utsname.h>
//#include <asm/page.h>

#include "dipc.h"

#ifdef __GLIBC__
#define IPC_DIPC   00010000   /* make it distributed */
#define IPC_OWN    00020000   /* this machine is the owner */
#endif

#if !defined(__GNU_LIBRARY__) || defined(_SEM_SEMUN_UNDEFINED)
union semun {
	int val;
	struct semid_ds *buf;
	unsigned short *array;
	struct seminfo *__buf;
	void *__pad;
};
#endif

/* maximum time an employer or key manager will 
   wait for a reply, in integer seconds */ 
#define EMPLOYER_REFEREE_TIMEOUT 60
#define EMPLOYER_SHM_TIMEOUT     120
#define EMPLOYER_WORKER_TIMEOUT  300
#define SHM_AK_TIMEOUT           30
#define REFEREE_TIMEOUT          15

/* maximum time a machine can have a shm, in seconds */
#define SHM_HOLD_TIMEOUT 0.2

#define DIPC_E_TIMEOUT EIO
#define DIPC_E_FORK    EIO
#define DIPC_E_TYPE    EINVAL

typedef int BOOLEAN;
typedef int FD;

#define BAD_FD -1

#define LOC __FILE__, __LINE__

#define CONFIG_FILE "/etc/dipc.conf"
#define ALLOW_FILE  "/etc/dipc.allow"

#define DIPC_FRONT_END_PORT 6543 /* any better addresses ? */
#define DIPC_REFEREE_PORT 6544
#define UDP_SEND_SIZE 3200 /* send this size before exchanging a sync char */

#define IPC_SET  1
#define IPC_STAT 2

#define MAXDATA (4056 + 4) /* MSGMAX + 4 */

#define REQ_SHMMAN     101
#define RES_SHMMAN    1001

#define REQ_REFEREE    102
#define RES_REFEREE   1002

#define REQ_DOWORK   118
#define RES_DOWORK  1018

#define FALSE 0
#define TRUE  1

#define FAIL     0
#define SUCCESS  1

#define NETWORK_BYTEORDER

struct message {
 int arch;
 struct sockaddr_in address;
 struct sockaddr_in address2;
 int pid;
 int request; 
 int reserved;
 struct dipc_info info; 
};

struct usr_info
{
 char name[40];
};

#define SAME_MACHINE(a,b) ((a)->sin_addr.s_addr == (b).sin_addr.s_addr)

#define SAME_ADDRESS(a,b) (((a)->sin_addr.s_addr == (b).sin_addr.s_addr) &&\
((a)->sin_port == (b).sin_port))
