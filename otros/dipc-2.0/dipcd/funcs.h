/* 
 * funcs.h
 *
 * part of dipcd source code
 *
 * Copyright (C) Kamran Karimi
 */

/* from support.c */
extern void strip_space(char *buff);
extern int check_symbolic(char *buff);
extern BOOLEAN check_allow(int address);
extern BOOLEAN read_dipc_allow(void);
extern void log(char *filename, int line, char *format, ...);
extern char *work_string(int func);
extern char *request_string(int req);
extern int readn(int fd, char *prt, int nbytes);
extern int writen(int fd, char *ptr, int nbytes, struct sockaddr_in *addr);
extern int get_in_data(int in_sock, char *data, struct message *message);
extern int get_in_message(int in_sock, struct message *message);
extern int send_net_data(int in_sock, char *data, struct message *message,
                       struct sockaddr_in *addr);
extern int send_net_message(int in_sock, struct message *message,
                       struct sockaddr_in *addr);
extern FD plug(int family, struct sockaddr *my_addr, int addr_len);
extern FD ring(int family, struct sockaddr* my_addr, int addr_len, int silent);
extern BOOLEAN send_in_message(struct message *message,
                                             struct sockaddr_in addr);
extern void child_handler(int signo);
extern BOOLEAN become(char *user);

/* from worker.c */
extern void worker(int req_sock, struct message message);

/* from referee.c */
extern void referee(void);

/* from key_man.c */
extern void shm_man(struct dipc_info *di, char *data);

/* from front_end.c */
extern void front_end(void);

/* from employer.c */
extern void employer(struct dipc_info *di, char *data);
