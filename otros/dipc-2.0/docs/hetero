

1.0) Introduction

 Theoretically, you should be able to add any 32 bit machine in a DIPC
cluster. The same holds for 64 bit machines in their own cluster. This file 
describes what steps are taken to make DIPC work in a heterogeneous 
environment. 

 DIPC is not concerned with code conversions. The programmer/user should make 
sure programs are executed on suitable machines. 

 As explained below, It is rather easy to do the porting for 32 bit machines 
with similar data-alignment needs. Especially if the same compiler (e.g. GNU C)
is used in all the machines.


2.0) The design of DIPC makes its usage in a heterogeneous system rather easy.
Both messages and shared memories work on a byte basis. So they don't
have any problem: their contents are sent with no conversion. Any needed 
conversion should be done by the applications themselves, depending on the 
data semantics. If DIPC is configured to transfer whole segments, then the 
problem of different page sizes on different architectures is also solved.
The configuration should also enable sending signals to shared memory readers 
or writers, so the applications could know when to do the conversions. In any 
case, DIPC does not touch user data.

 DIPC processes only use integers to communicate among themselves. This
means that it only needs to consider the differences in the representation
of integers in little-endian and big-endian machines.


3.0) There are some considerations in making DIPC heterogeneous:

3.1) DIPC source codes should be ported. This has been done for many
architectures supported on which Linux runs.

3.2) The problem of page sizes. If DIPC is to be used on two architectures 
with different page sizes, then DIPC's page size on all the computers should 
be set to the greater of the two. This is because the machine with the 
greater page size can not supervise sections of memory smaller than this 
size. This problem is solved.

3.3) The values of some of the fields in structures used by DIPC to pass
information between machines (the message structure, see the theory file),
should be converted at run-time, to make sure they have the same meaning on
any machine. This has been handled in the file dipcd/support.c


4.0) Message conversion places
 
 There are two solutions for the conversion problem: 
 1) Conversion is done at the receiving machine. 
  
  Here each computer sends data in its own native format, and the receiving 
  side does any necessary conversions.

 2) Conversion is attempted by both the sender and the receiver (suggested
    by Michael Schmitz).

  In this scheme the network byte order is used by the sending computer. The
  receiving machine then changes the data, if necessary, to its native
  format.

 You can use MY_DIPC_ARCH to find out whether the computer where the program
was compiled is little-endian, big-endian, 32 bit or 64 bit. The special 
value: LINUX_NBOR, indicates that network byte order is being used by the 
sender. The macro MY_DIPC_CPU indicates the CPU type.

 The following information should be changed when data is coming from another 
incompatible machine:
 
4.1) The message structure. This structure holds information about the 
work needing to be done, and possibly some arguments. The message.arch field
has the architecture (MY_DIPC_ARCH of the sending computer) of the sending 
machine.
 
4.2) If the function being applied remotely needs data, that data should also 
be converted. These functions are: msgctl(), semctl(), shmctl() and semop().



5.0) Guide lines
 The following should be considered:
 
5.1) int, short, and other data types should have the same number of bits on
all the compilers on different machines. Their sign conventions should also
be the same.

5.2) Kernel structures should be identical, both in the order of their fields
and also in the field types and sizes, otherwise, some conversions should
be attemtped.

5.3) Compilers should not reorder the fields, or do it the same on all
architectures.

5.4) Compilers should not add pad space in structures, or do it the same on
all architectures.

5.5) The maximum message size should be the same.

 These conditions seems to hold for 32 bit machines (for example i386 and 
68020). In this case, the conversion amounts to changing integers from 
little endian to big endian and vice versa.

