
 History of DIPC releases

Version 2.0:
 * DIPC is poterd to Linux/PowerPC (32 or 64 bit)
 * DIPC is ported to Linux/MIPS (32 bit)
 * DIPC is ported to Linux/Alpha (64 bit)
 * DIPC is ported to Linux/SPARC (32 bit)
 * DIPC is now SMP-friendly.
 * The exclusion routines (dipc_excl() and dipc_wake()) have been re-written.
 * DIPC supports glibc 2.1
 * DIPC no longer swaps out pages to make them read-protected.
 * DIPC's shared memory handling has been modified to conform to Linux kernel's
   standards.
 * DIPC is now GPL code.
 * Numerous other bug fixes and modifications.

Version 1.1c:
 * DIPC would try to handle "normal" shared memory segment swappings. Fixed.
 * DIPC would not create new shared memory segments correctly. Bug introduced 
   in 1.1b. Fixed.
 * Other small modifications. 

Version 1.1b:
 * DIPC now patches 2.2.x kernels.
 * Small modifications to make the programs in the package compile under 
   newer C compilers.

Version 1.1:
* DIPC's shared memory operations would fail is a segment was locked. fixed.
* When a computer became the writer of a shared memory, DIPC would send a 
  DIPC_SIG_WRITER to processes that had attached the segment as read-only. 
  Now it sends a DIPC_SIG_READER.
* A bug prevented DIPC applications from running on the same computer when 
  dipcd was not running. fixed (this bug only affected 2.1.x kernels)
* dipcker could not find all the IPC structures in the kernel. fixed.
* dipcker would show all the IPC structures in the kernel to anyone. fixed.
* Reformatting, and re-tabbing DIPC's kernel sources, so they look more
  like other kernel code.
* Many other modifications, including removal of redundant code, renaming of
  functions and variables, etc.


Version 1.0: (DIPC is no longer considered beta)
* in the page mode, DIPC can use distributed pages that are a multiple of the 
  native virtual memory's page size, determined at dipcd start time.
* DIPC now supports glibc.
* DIPC can now use UDP as well as TCP for its network operations, resulting 
  in increased speed. There is no flow control and data fragmentation.
* dipcd cleans up the kernel data structures when it is started. killing
  and restarting dipcd will take care of many problems (no need to re-boot).
* the protection bits in the kernel IPC structures were not set correctly in 
  some cases. fixed.
* sys_dipc() (in file ipc/dipc.c) would not check to make sure that only root
  programs can have access to some functions. fixed.
* major changes done to the data conversion routines (support.c), to make them 
  better use the symmetry of little-endian and big-endian integers.
* many other small and big corrections and additions.


Version 0.9.1:
* the distributed shared memory didn't work for segments with more than
  one page (bug introduced in 0.9). fixed.
* DIPC would not leave IPC structures created with the IPC_PRIVATE key 
  alone. fixed.
* it is now possible to use DIPC in heterogeneous environments using the
  native data format to send data (using the -n option is no longer necessary).
* DIPC is now a kernel configuration option. Answer 'yes' when prompted if to
  include DIPC support (CONFIG_SYSVIPC_DIPC) !
* some other small corrections.


Version 0.9: (This is the first beta release)
* DIPC is ported to Linux version 2.0.x and 2.1.x kernels
* DIPC is ported to Linux/M68k. The x86 and 680x0 versions can talk to
  each other.
* DIPC now allows only the computers specified in the /etc/dipc.allow file
  to be considered part of a cluster (more security).
* many other small and big corrections.


Version 0.35 (This version was not released publicly)
* the SEM_UNDO flag in semop() system call is now supported.
* some other small corrections.


Version 0.3:
* added the ability to use pages as well as segments as the unit of management 
  for shared memories.
* added the option to disable sending shared memory read or write signals.
* dipcd had to read the IP address of the computer it was running on from 
  the configuration file. fixed.
* dipcd's home directory had to be /home/dipcd. fixed.
* the shared memory hold-time mechanism did not work. fixed.
* semctl() did not work in some cases. fixed.
* tasks waiting for exclusive access to distributed structures were not 
  restarted when dipcd exited. fixed.
* many other small and big corrections.
  

Version 0.15pl1:
* the implementation of the method used to provide exclusion in the kernel
  had problems. fixed.


Version 0.15:
* the added prev_page_prot field in the vm_area_struct seems redundant. 
  removed. So there is no need to modify kernel files mm.h and processor.h.  
* added the option to disable all or individual time-outs.
* added support for dipcshm program (shm_man back door).
* added support for dipcref program (referee back door).
* added support for dipcker program.
* shm_man would not check to see if a first read-candidate was the owner.
  fixed.
* shm_man would not change a previous writer to a reader, when a new read
  request arrived. fixed.
* DIPC would not work as specified if the referee did not answer in time to 
  a search query. fixed.
* the become() function in support.c did no work correctly. fixed.
* dipcd did not check to see if it is running as root. fixed.
* the return code of dipc system calls were not checked, which caused some
  problems when DIPC was not present in the kernel. fixed.
* MY_ARCH was not used in all messages.
* MY_ARCH and related definitions were not accessible to user programs.
* MY_ARCH was changed to MY_DIPC_ARCH to prevent clashes with programmer's 
  chosen identifiers. 
* RDABLE and WRABLE funcs were redundant. removed.
* the SND func was never used. removed.
* made dipcd output messages more meaningful.
* many other small and big corrections.


Version 0.1: 
 This was the original release

