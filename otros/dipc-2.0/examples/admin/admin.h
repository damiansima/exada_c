/*
 * admin.h
 *
 * headr file for admin.c and reporter.c
 *
 * By Kamran Karimi
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/utsname.h>
#include <errno.h>
#include <signal.h>
#include <linux/dipc.h>
#include <string.h>

#ifdef __GLIBC__
#define IPC_DIPC   00010000   /* make it distributed */
#endif

#define MSG_MODE (IPC_DIPC | IPC_EXCL | 0777)

#define MSG_KEY 10
#define MSG_TYPE 10
#define MSG_SIZE 1024

#define FIRST    1
#define CONTINUE 2

#define REPORTER_SLEEP_TIME 10
#define REPORTER_FILE_NAME "/tmp/reporter"

#define SPEC_SIZE 40

struct body
{
 int architecture;
 int type; /* FIRST or CONTINUE */
 int done; /* 0 or 1 */
 char spec[SPEC_SIZE];
 char mtext[MSG_SIZE];
};

struct message
{
 long mtype;
 struct body body;
};
