/*
 * game.h
 *
 * Header file for game.c and player.c
 *
 * By Kamran Karimi
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <linux/dipc.h>
#include <unistd.h>
#include <errno.h>
#include <sys/signal.h>
#include <sys/utsname.h>
#include <pwd.h>
#include <linux/dipc.h>

#ifdef __GLIBC__
#define IPC_DIPC   00010000   /* make it distributed */
#endif

#if !defined(__GNU_LIBRARY__) || defined(_SEM_SEMUN_UNDEFINED)
union semun {
	int val;
	struct semid_ds *buf;
	unsigned short int *array;
	struct seminfo *__buf;
	void *__pad;
};
#endif

#define SHM_KEY 20
#define SHM_MODE (IPC_DIPC | IPC_EXCL| 0777)

#define SEM_KEY 21
#define SEM_MODE (IPC_DIPC | IPC_EXCL | 0777)
#define SEM_NUM 2

#define MSG_KEY 22
#define MSG_MODE (IPC_DIPC | IPC_EXCL | 0777)

#define MSG_TYPE 10

#define MAX_PLAYERS 5

#define MIN_STRING  4
#define MAX_STRING 60

#define PLAYER_TIMEOUT 30

#define MAX_NAME 10
#define MAX_ADDR 40
#define MAX_SPEC 20
struct Player
{
 char name[MAX_NAME]; /* login name */
 char addr[MAX_ADDR]; /* machine address */
 char spec[MAX_SPEC]; /* machine specification */
 int pid; /* pid of the player */
};

struct message
{
 long mtype;
 int architecture;
 struct Player player;
};

struct GameBoard
{
 int architecture; /* for use in heterogeneous environments*/
 int NumPlayers;
 int turn; /* index in players to show whos turn it is to play */
 struct Player players[MAX_PLAYERS]; 
 int scores[MAX_PLAYERS];
 char orig_string[MAX_STRING]; /* we are serching for this string */
 char found_string[MAX_STRING]; /* so far we have found this */
};
