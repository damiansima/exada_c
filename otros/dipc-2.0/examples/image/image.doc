
1.0) Introduction
 This is a very simple image processing program. A picture, made of ASCII
characters /, -, |, and \ is setup in a 2 dimensional array (20x80). This
picture is in a shared memory. Now 4 other processes are started to change 
it. Each of these processes modifies (writes to) a specific part of the
picture by changing the character that is stored there. The results are 
then shown.

 This could be used as the skeleton code for programs that use a
multi-dimensional array as their main data structure. These include
mathematical problems, that manipulate matrices, or it could be a genuine
image processing program. In all these cases, first some data are put in an 
array, then other processes will each access a part of it and do some 
computations based on the initial values. The results may be placed in the 
original place, or they could be put somewhere else.
 

2.0) How it works
 This program uses a distributed shared memory and a semaphore set. The main 
part is in image.c. Image manipulations are done by worker processes (source
in worker.c). image prepares the shared memory and the semaphore, then
places the initial picture in the shared memory (treated as a 2 dimensional
array). It now forks and executes the image_s script. This one could either 
execute the worker programs locally (by just running them) or it can use rsh 
to invoke instances of worker in other machines. Any of these will modify a 
different part of the picture, as specified by the command line parameter
(see below). 

 This will show how DIPC behaves when no special arbitration between the 
writers is done. If the writes by worker processes were done at random 
positions, they would overwrite data written by others.

 If some of the workers are on different computers, then the whole shared 
memory will be transferred from machine to machine, as different processes 
want to write to it. This could hurt the performance if the shared memory 
segment was big. In these cases the programmer could use more than one shared 
memory, and give each process its own segment. This way one shared memory 
won't travel the network back and forth several times.

 All worker programs will use a semaphore to notify the image that they are
done. Now image displays the transformed picture in the shared memory.

 Worker requires 1 command line parameter to do its work. It denotes the part 
of the picture that is to be manipulated by the worker. As the picture is
divided into four equal sections, numbered 0 to 3, this argument should also
be less than 4. If two workers are given the same parameter, they will
over-write each other, but the results will be the same.
 

3.0) Installing it
 To be able to run image on your system, do the following: 

3.1) Compile image.c and worker.c. Do this by typing make in the relevant
directory. You can change the program's behavior by changing the source
files before compiling.

3.2) Place one or more instances of the worker file in other machines. They
should be connected over a TCP/IP network to the current machine (that will
run image) and be in the same DIPC cluster.

3.3) Modify the image_s script and change it according to your system's
configuration and IP addresses. Make sure that rsh can find the worker 
programs.


4.0) Running it
 You may not be able to use rsh when you are root, So you'd better not be
this user. Now just type image. Note that you may have to wait a while for
the program to do its work and finish.


