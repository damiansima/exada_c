/*
 * image.h
 *
 * Header file for image.c and worker.c
 *
 * By Kamran Karimi
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <linux/dipc.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/signal.h>

#ifdef __GLIBC__
#define IPC_DIPC   00010000   /* make it distributed */
#endif

#if !defined(__GNU_LIBRARY__) || defined(_SEM_SEMUN_UNDEFINED)
union semun {
	int val;
	struct semid_ds *buf;
	unsigned short int *array;
	struct seminfo *__buf;
	void *__pad;
};
#endif

#define NUM_WORKERS 4
#define ROW 20
#define COLUMN 80

#define SHM_KEY 30
#define SHM_MODE (IPC_DIPC | IPC_EXCL | 0777)
#define SHM_SIZE ROW * COLUMN

#define SEM_KEY 31
#define SEM_MODE (IPC_DIPC | IPC_EXCL | 0777)

struct bound
{
 int row1, row2, col1, col2;
};

static struct bound bounds[4] = 
{ 
 { 0, ROW / 2, 0, COLUMN / 2 },
 { 0, ROW / 2, COLUMN / 2, COLUMN },
 { ROW / 2, ROW, 0, COLUMN / 2 },
 { ROW / 2, ROW, COLUMN / 2, COLUMN }
};

