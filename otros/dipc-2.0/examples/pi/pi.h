/*
 * pi.h
 *
 * Header file for pi.c and integrator.c
 *
 * By Kamran Karimi
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <linux/dipc.h>
#include <string.h>
#include <math.h>

#ifdef __GLIBC__
#define IPC_DIPC   00010000   /* make it distributed */
#endif

#define MSG_KEY 50
#define MSG_MODE (IPC_DIPC | IPC_EXCL | 0777)
#define MSG_TYPE 10

#define NUM_INTEGRATORS 10
#define SECTOR_SIZE (1.0 / NUM_INTEGRATORS)

struct body
{
 int architecture;
 double result;
};

struct message
{
 long mtype;
 struct body body;
};
