
 SECTION    TITLE
      1     Installing DIPC
      2     Running a simple example
      3     Running a more complex example



1.0) Installing DIPC

 This file is for people who want to install and test DIPC, without being
forced to do a lot of reading. It describes what you should do in a step by
step manner. You can later refer to other documents to better understand 
what you were doing here.

 DIPC is of practical use when you want to use it on two or more computers,
but you can still install it to test it and get a feeling of how it works, 
even if you have only one machine available.

 1.1) If you intend to use more than one computer, make sure you have an
account with the SAME login name on all of them (passwords are not important). 
This login name could be anything, as long as it is yours on all the 
machines. 
 
 1.2) Become the root. To do so you may log in as root, or you may use the
command 'su' to become the super user. You need to know the root password to
do so. You also need to know the IP address of at least one of the 
computers you want to install DIPC on (this one computer is called the
"referee". This better be in dots-and-numbers notation (something like
192.168.0.0).

 1.3) Now run the dipc.install script. Just type its name. It will do many of
the things necessary for the installation of DIPC. But there are some other 
things that should still be done by you. 

 1.4) Type 'adduser' to create a new account for a user called dipcd. the user
id, group id and the password values are not important.

 NOTE: This account is for DIPC's private use. Do not login as this user, and 
do not put any files in this account's home directory. 

 1.5) Go to the directory where the kernel files are placed (/usr/src/linux)
type 'make config' or 'make xconfig'. Be sure to answer positively when asked 
if you want System V IPC, Distributed System V IPC, and TCP/IP Networking 
support. 

 Now type 'make dep; make clean; make zImage (or make bzImage)' to compile 
the kernel.

 1.6) Copy the new kernel (from arch/.../boot) to the proper directory 
(/ or /boot). Then install the new kernel (probably by lilo). Now the kernel 
of your computer has support for DIPC. 
 
 NOTE: It might be a good idea to save your old kernel somewhere before 
overwriting it.

 1.7) If you have multiple computers that are connected by a TCP/IP network
(This could include networks connected by Ethernet boards), Then repeat the
above steps on the other computers that you want to install DIPC on.

 1.8) Now you should select one of the computers as the 'main' one. In DIPC
terms it is called a 'referee'. If you have only one computer, then you don't 
have any difficulty selecting this referee machine :-). Otherwise you may 
want to choose the one that is faster, or has more memory, as the referee.

 1.9) In all the computers, edit the file /etc/dipc.conf. You could use the
dipcx program in the tools directory. dipc.conf is the configuration file of
DIPC. Move the cursor to line that has the phrase 'this_computer', and
substitute the IP address of that machine for the default value provided, or
you could set this entry to 'dipc_auto_find', so DIPC will try to find this 
address. In the line that starts with the 'referee' word, substitute the 
default value with the IP address of the referee computer that you chose 
before. make sure you set the referee address in ALL the computers the same. 
By this, you are placing these computers in the same DIPC 'cluster', and they 
can work together.

NOTE: Please use real addresses, not names like 'localhost' or IP addresses
like 127.0.0.1

 1.10) In the referee computer, edit a the /etc/dipc.allow file. In it, write 
the address of all the computers that can be part of the cluster, one per 
line.

 NOTE: This should also include the address of the referee computer itself.
Also, like the dipc.conf case, you should use real addresses in this file.

Example:
# start of /etc/dipc.allow
 
194.165.8.3 # the referee
rose.ipm.ac.ir

#end of file

 The above entries in the /etc/dipc.allow indicates that the two
computers with the given addresses are allowed to be in the same cluster.
the '#' can be used to start comments. 
 
 1.11) Reboot the computers. 

 If everything has gone OK, then the next time that the system comes up, 
you are able to run programs written for DIPC. You may find some examples in 
the examples directory. The rest will show you how to run a simple example.



2.0) Running a simple example

 The first example you may want to run is in the examples/hello
directory. 

 2.1) Change dir to examples/hello and type make. When this is done, two 
executable programs with the names hello1 and hello2 are produced. Make
sure you are the owner of these programs. Use "ls -l" to find out about it.
You can become the owner a of file by giving the command 
chown <your login name> <file's name>. 

 2.2) If you are using more than one computer, copy the hello2 file to your 
account on another machine, or compile it over there.

 2.3) Execute hello1. Then execute hello2 on the other computer. If you
have only one computer, you may execute hello2 from another virtual
terminal. See the results?



3.0) Running a more complex example

 Now we will see how to execute a more complex example. This is the program
in the examples/pi directory.

 3.1) Change dir to the examples/pi directory. Type make. When done, two
executable programs with the names 'pi' and 'integrator' are created. Make
yourself the owner of these files (see the last example).

 3.2) Execute the pi program. This will invoke 10 integrator processes in the
same computer that the pi program is running on, and compute the value of the 
famous PI in mathematics. Depending on the speed of your computer, this may 
take some time.

 3.3) If you have more than one computer on the network with DIPC installed, 
you may want to see the effect of computing this value in real parallelism. 
Before being able to do so, make sure that you are able to use the UNIX 'rsh' 
to invoke programs on other computers on which you have installed DIPC. You 
may need to edit the /etc/hosts.equiv file in other computers to do this.

 3.4) Now you have to edit the file 'pi_s'. Here lines beginning with a '#' are 
commented. pi_s is a script file with 2 entries for each invocation of the 
'integrator' program. One is for the execution of integrator on another 
computer by using the rsh command (remote execution), and the other is for 
local execution. Uncomment some of the lines corresponding to remote execution 
and change the IP address appearing there with the IP address of a suitable 
computer in your network. Don't forget to comment the corresponding local 
execution. Now run the pi command again. See if the execution time is reduced 
this time.


 I hope now you are able to test other examples on your own. For more
information on DIPC read the files in the docs directory, especially the
file 'theory', which explains many DIPC terms mentioned here.

 Enjoy!
