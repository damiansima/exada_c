
#include <xview/xview.h>
#include <xview/frame.h>
#include <xview/panel.h>
#include <asm/page.h>

#include <linux/dipc.h>

#ifdef LITTLE_ENDIAN
#undef LITTLE_ENDIAN
#endif
#include <arpa/inet.h>

#define FAIL 0
#define SUCCESS 1

#ifdef NULL
#undef NULL
#endif
#define NULL 0

#define PRG_NAME "DIPCX"

#define NONE   -1
#define KERNEL  0
#define REFEREE 1 

#define MAX_TIME_OUT 99999
#define MAX_PAGE_SIZE 999999
#define MAX_HOLD_TIME 999
#define MIN_HOLD_TIME 0.2


extern int dipc_config(void);
extern int get_kernel_list(void);
extern int rem_kernel_info(int num);
extern int get_kernel_info(int num);
extern int is_shm(int num);
extern int shm_key(int num);

extern int get_referee_list(void);
extern int get_referee_info(int num);
extern int rem_referee_info(int num);

extern int get_shm_list(int key);
extern int shm_remove_manager(int key);

extern void show_ok_notice(char *message);
extern void show_ok_2_notice(char *message, char *message2);
extern void show_ok_3_notice(char *message, char *message2, char *message3);
extern void show_ok_4_notice(char *mess, char *mess2, char *mess3, char *mess4);

extern int readn(int fd, char *ptr, int nbytes);
extern int writen(int fd, char *ptr, int nbytes);
extern int ring(int family, struct sockaddr *addr, int addr_len);

extern void clean_all(void);

extern int display_struct;
extern int display_config;
extern int display_shm;

extern Frame frame;
extern Panel panel, panel_list;
extern Panel config_panel;
extern Panel shm_panel;
