# client

import zmq
import time
import sys

context = zmq.Context()

socket = context.socket(zmq.QUEUE)
#socket = context.socket(zmq.REQ)    # uncomment for Req/Rep

a=socket.connect("ipc:///tmp/pepe")

print('a',a)

delay = 1

for a in range(10):
  #socket.send('')     # uncomment for Req/Rep
  message = socket.recv()
  print ("recv:", message)
  time.sleep(delay)
