#!/usr/bin/python
# -*- coding: utf-8 -*-


import struct
import sqlite3


fmt='<i'
length=struct.calcsize(fmt)


def lee_dat():
    global indiceActual
    with open("test/101Ancho.dat", 'rb') as f:
        
        for j in range(60):
            chunk = f.read(blen)            
            ancho.append([struct.unpack(fmt, chunk[i:i+4])[0] for i in range(0,blen,4)])
            chunk = f.read(blen)
            desvio.append([struct.unpack(fmt, chunk[i:i+4])[0] for i in range(0,blen,4)])
            f.read(bfill) # lee datos no importantes
        
        chunk = f.read(4)
    indiceActual = struct.unpack(fmt, chunk)[0]
    
    # correccion porque el primer valor esta horrible
    indiceActual-=1
    del(ancho[0])
    del(desvio[0])
    
    
# defaults values min and max
VMIN=0.0
VMAX=1000.0
LENTAG=96 
FILE='./init_tags.dat'        
 
f=open(FILE,'wb')

        
'''
guarda como struct data_tag
que puede ser:
    double val_min;    
	double val_max;
    int index;
	int fill;
    char tag[96];
'''        
def to_file(index,tag,v_min,v_max):
    print("to_file:",index,tag,v_min,v_max)
    fill=0
    f.write(struct.pack('<d',float(v_min)))
    f.write(struct.pack('<d',float(v_max)))
    f.write(struct.pack('<i',int(index)))
    f.write(struct.pack('<i',int(fill)))
    m=bytes(str(tag), encoding='latin1')
    f.write(m)
    f.write(b'\0'*(LENTAG-len(m)))
    
    



conn = sqlite3.connect('DBConfig.db')	
c = conn.cursor()

c.execute("""select id,tag,value_min,value_max FROM TAGS 
             order by id asc""")
res=c.fetchall()
res.append([8,"na",4,5])
i=0
for r in res:
    _id,_tag,_vmin,_vmax=r
    # print(_id,_tag,_vmin,_vmax)
    while i<_id:
        to_file(i,"",VMIN,VMAX)        
        i+=1
    if not _vmax or not _vmin:
        to_file(_id,_tag,VMIN,VMAX)        
    else:
        to_file(_id,_tag,_vmin,_vmax) 
    i+=1    
    
    
f.close()   
conn.close()


leni=struct.calcsize('<i')
lend=struct.calcsize('<d')
lens=struct.calcsize('<%ss'%LENTAG)

with open(FILE,'rb') as f:
    while True:
        try:
            r=f.read(lend)
            v_min=struct.unpack('<d',r)[0]
        except:
            break
        r=f.read(lend)
        v_max=struct.unpack('<d',r)[0]
        r=f.read(leni)
        index=struct.unpack('<i',r)[0]
        r=f.read(leni)
        fill=struct.unpack('<i',r)[0]
        r=f.read(lens)
        tag=str(struct.unpack('<%ss'%LENTAG,r)[0])
        tag=str(r)        
        print("from_file:",index,tag,v_min,v_max)
        
    # m=bytes(str(tag), encoding='latin1')
    # f.write(m)
    # for i in range(len(m),LENTAG):
        # f.write(b'\0')
    # f.write(struct.pack('<d',float(v_max)))
    # f.write(struct.pack('<d',float(v_min)))




       
