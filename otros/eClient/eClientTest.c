#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <sys/stat.h>
#include "utils.h"
// #include <iostream>


#define SHMFD "/tmp/shm_client_tags"
#define FILEFD "/tmp/shm_tags"
#define NUMTAG 10
#define NUM_TAG 20
struct shm_tag
{
    unsigned updating:1;
    unsigned alarm_n1:1;
    unsigned alarm_n2:1;
    unsigned is_str:1;
    char priority;
    double value;
    double val_min;
    double val_max;
};
#define T_LEN (NUMTAG*sizeof(struct shm_tag))

struct shm_tag *tmap;


int is_updated(int index)
{
	return ((tmap+index)->updating);
}

double get_value(int index)
{
	return ((tmap+index)->value);
}

int get_alarm_n1(int index)
{
	return ((tmap+index)->alarm_n1);
}

int get_alarm_n2(int index)
{
	return ((tmap+index)->alarm_n2);
}

// #define get_value(index) ((tmap+index)->value)

int init_shm(void)
{
	tmap = (struct shm_tag *) map_shm(SHMFD, "r", T_LEN); 
	// tmap = (struct shm_tag *) map_file(FILEFD,"rw",sizeof(struct shm_tag)); 
	
	if((int)tmap>-1)
	{
		printf("No se puede mapear la memoria\n");
		return 0;
	}
	return 1;	
}

int main(int argc, char *argv[])
{
    int i;
	double val;
	
	if(!init_shm())
	{
		logger("No se puede leer la memoria compartida");
		return 0;
	}
	
	
	logger("Value: %f",get_value(2));
	logger("Value: %i",is_updated(2));
	
	return 0;
}


