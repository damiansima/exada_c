#include <stdio.h>      
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <stdlib.h>     
#include <string.h>     
#include <unistd.h>     

#include "e_common.h"
#include "utils.h"

// #include <sys/types.h>
// #include <unistd.h>
// #include <fcntl.h>
// #include <sys/mman.h>
// #include <sys/stat.h>


int create_sock_udp_broadcast(int broadcast)
{
    int sock;
    int optval=1;
    
    sock=socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) 
    {
        perror("Opening socket");
        return sock;
    }    
	if(broadcast)
		setsockopt(sock,SOL_SOCKET, SO_BROADCAST,&optval, sizeof(optval));
		
    setsockopt(sock,SOL_SOCKET, SO_REUSEADDR,&optval, sizeof(optval));
    setsockopt(sock,SOL_SOCKET, SO_REUSEPORT,&optval, sizeof(optval));
        
    return sock;
}

void init_tags(struct SHM_TAG *tag)
{
	FILE *fd;
	struct data_tag dt;
	int len=sizeof(struct data_tag);
	
	logger("init_tags()");
	
	fd = fopen("./init_tags.dat","r");
	while(fread(&dt,len,1,fd)>0)
	{
		tag->streaky 	= 0;
		tag->alarm_n1	= 0;
		tag->alarm_n2	= 0;
		tag->hb			= 0;
		tag->index		= dt.index;
		// tag->value		= 0;
		tag->value_min	= dt.val_min;
		tag->value_max	= dt.val_max;
		strcpy( tag->tag, dt.tag);
		logger("index: %i %s [%f-%f]",dt.index,dt.tag,dt.val_min,dt.val_max);
		tag++;
	}
	fclose(fd);			
}


#define T_LEN (NUMTAG*sizeof(struct shm_tag))


int main(int argc, char *argv[])
{
    int sock,n,i;
    socklen_t fromlen;
    struct sockaddr_in addr, from;
	struct SHM_TAG *shmtags;
	struct SHM_TAG *tag;
	struct server_client rctag;
	
	logger("inicio");
	
	// hay que poner cosas de control aca...
	shmtags = (struct SHM_TAG *) map_shm(SHM_CLIENT, "rwt", EXADA_SHM_TAGS); 
	if((int)shmtags == -1)
	{
		printf("No se puede mapear la memoria\n");
	}
	
	init_tags(shmtags);
	
	sock=create_sock_udp_broadcast(0);
	
    memset(&addr, 0, sizeof(addr));  
    addr.sin_family = AF_INET;              
    addr.sin_addr.s_addr = INADDR_ANY;         
    addr.sin_port = htons(EXADA_PORT_SUS);  

    fromlen = sizeof(struct sockaddr_in);  
    
	if (bind(sock,(struct sockaddr *)&addr,fromlen)<0) 
        logger("binding fail !");
        
    while(1)
    {    
		n = recvfrom(sock,&rctag.length,sizeof(struct server_client),0,(struct sockaddr *)&from,&fromlen);
		if (n < 0) 
			logger("recvfrom() fail !");
		
		logger("len:%i from: %s::%i ",n,inet_ntoa(from.sin_addr),ntohs(from.sin_port));

		for(i=0;i<rctag.length;i++)
        {
			tag = shmtags + rctag.tag[i].index;
			memcpy(tag,&rctag.tag[i],sizeof(struct shm_tags));
        }
	}
	/*
	
	
	i=0;
	
   
	for (i=0;i<NUMTAG;i++)
	{
		(tmap+i)->updating = 1;
		(tmap+i)->alarm_n1 = 1; 
		(tmap+i)->alarm_n2 = 1;
		(tmap+i)->is_str   = 1;
		(tmap+i)->priority = (char)i%100;
		(tmap+i)->value    = (double)(rand()%101);
		(tmap+i)->val_min  = (double)i;
		(tmap+i)->val_max  = (double)i;
		
		// memcpy((char *)&(tmap+i)->value,"asdfasdf",8);
			
		logger("value: %f",(tmap+i)->value); 
	}
	(tmap+2)->updating = 0;
	logger("tamanio del double: %iB",sizeof(double));
	memcpy(dest,(char *)&(tmap)->value,8);
	for (i=0;i<8;i++)
		printf("%c ",dest[i]);
	
	unmap_shm(SHMFD,tmap);
	// unmap_file(tmap,sizeof(struct shm_tag));	
    return 0;
   */
}
