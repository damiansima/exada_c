#define UDP_MAX_TAGS        1000
#define UDPLEN              60000

// este valor es para definir variables en C
#define EXADA_MAX_TAG       2000
#define EXADA_PORT_ASI_SEND 50000
#define EXADA_PORT_ASI_RCV  50000
#define EXADA_PORT_PUB      50001
#define EXADA_PORT_SUS      50001

// server: refresco para tag rayado y envio
#define TIME_SEND       (500)   // milisegundos
#define TIME_STREAKY    (2000)  // milisegundos
#define SHM_SERVER  "/tmp/shm_server_tags"
#define SHM_CLIENT  "/shm_client_tags"

// / WARNING: para el raspi se necesita que las struct tengan sizeof
// / divisibles por 8 :(
struct SHM_TAG
{
    char     streaky;         // tag rayado 1: nivel 1 2:nivel 2:
    char     hb;              // nivel del heart beat
    unsigned alarm_n1 :1;     // alarma nivel 1
    unsigned alarm_n2 :1;     // alarma de nivel 2, por limites, o por rayado o por usuario
    unsigned fill     :14;    // relleno
    int      index;           // indices del tag, en variables ID de (db)
    double   value;           // valor del tag
    double   value_max;       // maximo (db) o seteado por applicacion
    double   value_min;       // minimo (db) o seteado por applicacion
    char     tag[96];         // nombre del tag (db)
};
#define EXADA_SHM_TAGS  (EXADA_MAX_TAG*sizeof(struct SHM_TAG))


struct shm_tags
{
    char     streaky;         // tag rayado 1: nivel 1 2:nivel 2:
    char     hb;              // nivel del heart beat
    unsigned alarm_n1 :1;     // alarma nivel 1
    unsigned alarm_n2 :1;     // alarma de nivel 2, por limites, o por rayado o por usuario
    unsigned fill     :14;    // relleno
    int      index;           // indices del tag, en variables ID de (db)
    double   value;           // valor del tag
    double   value_max;       // maximo (db) o seteado por applicacion
    double   value_min;       // minimo (db) o seteado por applicacion
};


struct shm_asi_tags
{
    char     streaky;    // tag rayado 1: nivel 1 2:nivel 2:
    unsigned alarm_n1 :1;
    unsigned alarm_n2 :1;
    unsigned fill     :22;
    int      index;      // indices del tag
    double   value;      // valor del tag
};


struct data_tag
{
    double val_min;
    double val_max;
    int    index;
    int    fill;
    char   tag[96];
};


struct shm_control
{
    unsigned sending   :1;
    unsigned writing   :1;
    unsigned recv_exit :1;
    unsigned send_exit :1;
    unsigned fill      :28;
    int      actual_len;
};


struct shm_asi
{
    int fill;
    int length;

    struct shm_asi_tags tag[UDP_MAX_TAGS];
};


struct shm_server
{
    struct shm_control control;


    struct shm_tags tags[UDP_MAX_TAGS];
};


struct server_client
{
    int fill;
    int length;

    struct shm_tags tag[UDP_MAX_TAGS];
};


//~ Formatted by Jindent --- http://www.jindent.com
