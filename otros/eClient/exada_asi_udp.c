#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket() and bind() */
#include <arpa/inet.h>  /* for sockaddr_in */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#include "e_common.h"



int main(int argc, char *argv[])
{
    int sock;                         /* Socket */
    struct sockaddr_in broadcastAddr; /* Broadcast address */
    char *broadcastIP;                /* IP broadcast address */
    unsigned short broadcastPort;     /* Server port */
    // char *sendString;                 /* String to broadcast */
    int optval;          /* Socket opt to set permission to broadcast */
    // unsigned int sendStringLen;       /* Length of string to broadcast */

    int sendlen;
    struct shm_asi TAGS;
    
    TAGS.length=3;
    TAGS.tag[0].value=41.2323;
    TAGS.tag[1].value=2.2;
    TAGS.tag[2].value=4.3;
    TAGS.tag[0].index=2;
    TAGS.tag[1].index=1;
    TAGS.tag[2].index=4;
    TAGS.tag[0].streaky=1;
    TAGS.tag[1].streaky=0;
    TAGS.tag[2].streaky=1;
    TAGS.tag[0].alarm_n1=0;
    TAGS.tag[1].alarm_n1=0;
    TAGS.tag[2].alarm_n1=1;
    
    if (argc < 3)                     /* Test for correct number of parameters */
    {
        fprintf(stderr,"Usage:  %s <IP Address> <Port> \n", argv[0]);
        exit(1);
    }

    broadcastIP = argv[1];            /* First arg:  broadcast IP address */ 
    broadcastPort = atoi(argv[2]);    /* Second arg:  broadcast port */
    // sendString = argv[3];             /* Third arg:  string to broadcast */

    /* Create socket for sending/receiving datagrams */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        perror("socket() failed");

    /* Set socket to allow broadcast */
    optval = 1;
    setsockopt(sock,SOL_SOCKET, SO_REUSEADDR,&optval, sizeof(optval));
    setsockopt(sock,SOL_SOCKET, SO_REUSEPORT,&optval, sizeof(optval));
    
    // if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &optval, sizeof(optval)) < 0)
        // perror("setsockopt() failed");

    /* Construct local address structure */
    memset(&broadcastAddr, 0, sizeof(broadcastAddr));   /* Zero out structure */
    broadcastAddr.sin_family = AF_INET;                 /* Internet address family */
    broadcastAddr.sin_addr.s_addr = inet_addr(broadcastIP);/* Broadcast IP address */
    broadcastAddr.sin_port = htons(broadcastPort);         /* Broadcast port */

    
    // sendStringLen = strlen(sendString);  /* Find length of sendString */
    sendlen = sizeof(TAGS.length) + TAGS.length*(sizeof(struct shm_asi_tags));
    if (sendto( sock, 
                &TAGS.length,
                sendlen, 
                0, 
                (struct sockaddr *) &broadcastAddr, 
                sizeof(broadcastAddr)
              ) != sendlen
       )
        perror("sendto () sent a different number of bytes than expected");

    return 0;
}
