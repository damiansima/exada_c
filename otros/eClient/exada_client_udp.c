#include <stdio.h>      
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <stdlib.h>     
#include <string.h>     
#include <unistd.h>     


#include "e_common.h"


int main(int argc, char *argv[])
{
    int sock;                         /* Socket */
    struct sockaddr_in broadcastAddr; /* Broadcast address */
    unsigned short broadcastPort;     /* Server port */
    int broadcastPermission;          /* Socket opt to set permission to broadcast */
    
    
    struct server_client shmtags;
    
    int n,i,j;
    struct sockaddr_in from;
    socklen_t fromlen;
    
    
    if (argc < 2)                     /* Test for correct number of parameters */
    {
        fprintf(stderr,"Usage:  %s <Port>\n", argv[0]);
        exit(1);
    }

    broadcastPort = atoi(argv[1]);    /* Second arg:  broadcast port */
    
    /* Create socket for sending/receiving datagrams */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        perror("socket() failed");

    /* Set socket to allow broadcast */
    broadcastPermission = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, 
          sizeof(broadcastPermission)) < 0)
        perror("setsockopt() failed");

    /* Construct local address structure */
    memset(&broadcastAddr, 0, sizeof(broadcastAddr));   /* Zero out structure */
    broadcastAddr.sin_family = AF_INET;                 /* Internet address family */
    broadcastAddr.sin_addr.s_addr = INADDR_ANY;         
    broadcastAddr.sin_port = htons(broadcastPort);         /* Broadcast port */

    fromlen = sizeof(struct sockaddr_in);  
    
    if (bind(sock,(struct sockaddr *)&broadcastAddr,fromlen)<0) 
        perror("binding");
        
       
    for (;;)
    {
        n = recvfrom(sock,&shmtags.length,sizeof(struct server_client),0,(struct sockaddr *)&from,&fromlen);
        if (n < 0) 
            perror("recvfrom()");
        
        printf("\nlen:%i\nfrom:%s::%i\n",n,inet_ntoa(from.sin_addr),ntohs(from.sin_port));        
            
        for(i=0;i<shmtags.length;i++)
        {
            for(j=0;j<shmtags.tag[i].streaky;j++)
                printf("#");
            printf("val[%i]=%f an1:%i, an2:%i\n",
                    shmtags.tag[i].index,
                    shmtags.tag[i].value,
                    (int)shmtags.tag[i].alarm_n1,
                    (int)shmtags.tag[i].alarm_n2);
        }
    }
    return 0;
}
