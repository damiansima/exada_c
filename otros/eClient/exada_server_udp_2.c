/* Creates a datagram server.  The port 
   number is passed as an argument.  This
   server runs forever */

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <arpa/inet.h>  /* for sockaddr_in */
#include  <signal.h>

// #include <iostream>

#include "utils.h"
#include "e_common.h"



int *ptags;


void error(const char *msg)
{
    perror(msg);
    // exit(0);
}

void to_exit(int sig) 
{
	free(ptags);
	logger("signal:%i exit",sig);
	exit(0);
}


int create_sock_udp_broadcast(int broadcast)
{
    int sock;
    int optval=1;
    
    sock=socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) 
    {
        perror("Opening socket");
        return sock;
    }    
	if(broadcast)
		setsockopt(sock,SOL_SOCKET, SO_BROADCAST,&optval, sizeof(optval));
		
    setsockopt(sock,SOL_SOCKET, SO_REUSEADDR,&optval, sizeof(optval));
    setsockopt(sock,SOL_SOCKET, SO_REUSEPORT,&optval, sizeof(optval));
        
    return sock;
}


void init_tag(struct shm_tags *tag,int index)
{
    FILE *fd;
    struct data_tag dt;
    int len=sizeof(struct data_tag);

    logger("init_tag()");

    fd = fopen("./init_tags.dat","r");
    while(fread(&dt,len,1,fd)>0)
    {
        if(dt.index==index)
        {
            tag->index=index;
            tag->alarm_n2=0;
            tag->value_min=dt.val_min;
            tag->value_max=dt.val_max;
            logger("index: %i %s [%f-%f]",dt.index,dt.tag,dt.val_min,dt.val_max);
        }		
    }
    fclose(fd);			
}


void send_SHM_server(struct shm_control *shmmap)
{
    int sock_pub, length, n,i;
    struct sockaddr_in to;
    struct shm_tags *shmtags=(struct shm_tags *)(shmmap+1);
    struct shm_tags *tag;

    signal(SIGINT, to_exit);
    signal(SIGQUIT, to_exit);
    signal(SIGTERM, to_exit); // esta es la del kill es -15 
    // signal(SIGKILL, to_exit); // esta no se puede capturar -9
    // signal(SIGUSR1, to_exit);
    // signal(SIGUSR2, to_exit);
	
	
    sock_pub =create_sock_udp_broadcast(1);
        
    length = sizeof(struct sockaddr_in);
    memset(&to,0,length);
    to.sin_family=AF_INET;
    // to.sin_addr.s_addr=INADDR_ANY;
    to.sin_addr.s_addr = inet_addr("255.255.255.255"); // envia en broadcast
    to.sin_port=htons(EXADA_PORT_PUB);    
    
          
    while (1) 
    {  
		
		while(shmmap->writing) // espera que se termine de escribir
			msleep(1);
			
		shmmap->sending=1;
		
		// controla los campos de los tags a enviar
		for(i=0;i<shmmap->actual_len;i++)
		{
			tag = shmtags+i;
			if( tag->value > tag->value_max || tag->value < tag->value_min )
				tag->alarm_n2=1;
				
			// printf("hb:%i\n",(int)tag->hb);
			if( tag->hb == 1 )
			{
				tag->hb=0;
				tag->streaky += 2;
			}
			else if( tag->hb > 1 )
				tag->hb--;
			
		}
		
		logger("sendto() len:%i",shmmap->actual_len);
        n = sendto( sock_pub, 
					&shmmap->actual_len, 
					sizeof(shmmap->actual_len)+shmmap->actual_len*sizeof(struct shm_tags), 
					0,
					(struct sockaddr *)&to, 
					length
				  );
					
		// shmmap->actual_len=0;
        shmmap->sending=0;
		if (n  < 0) 
		{
            logger("send_SHM_server sendto() fail !");
			shmmap->send_exit=1;
			exit(0);
		}
		if(shmmap->recv_exit)
		{
			shmmap->send_exit=1;
			exit(0);
		}
		// sleep(1);
		msleep(TIME_SEND);
    }
	
}

void recv_SHM_server(struct shm_control *shmmap)
{
	int sock_recv, length, n;
    socklen_t fromlen;
    struct sockaddr_in server;
    struct sockaddr_in from;
    struct shm_asi_tags *atag;
	struct shm_tags *shmtags=(struct shm_tags *)(shmmap+1);
	struct shm_tags *tag;
	int i;
	struct shm_asi rcv_tag;
	
	signal(SIGINT, to_exit);
	signal(SIGQUIT, to_exit);
	signal(SIGTERM, to_exit);
	
	ptags = (int *)malloc( EXADA_MAX_TAG*sizeof(int) );
	if ( ptags == (int *)NULL )
	{
		logger("recv_SHM_server() Alocate memory fail !");
		shmmap->recv_exit=1;
		free(ptags);
		exit(0);
	}		
	
	for(i=0;i<EXADA_MAX_TAG;i++)
		ptags[i]=-1;
	
    sock_recv=create_sock_udp_broadcast(0);
        
    fromlen = sizeof(struct sockaddr_in);        
    length = sizeof(struct sockaddr_in);        
    memset(&server,0,length);
    server.sin_family=AF_INET;
    server.sin_addr.s_addr=INADDR_ANY; 
    server.sin_port=htons(EXADA_PORT_ASI_RCV);    
    
    if (bind(sock_recv,(struct sockaddr *)&server,length)<0) 
    {
        logger("recv_SHM_server() bind fail !");
        shmmap->recv_exit=1;
        free(ptags);
        exit(0);
    }
        
    while (1) 
    {
        n = recvfrom(sock_recv,&rcv_tag.length,sizeof(rcv_tag),0,(struct sockaddr *)&from,&fromlen);
        if (n < 0) 
            logger("recv_SHM_server recvfrom() fail !");			
		
            // espera que se termine de enviar o escribir en shm
            while(shmmap->sending || shmmap->writing) 
                msleep(1);

            shmmap->writing=1;
            atag= (struct shm_asi_tags *)(&rcv_tag.tag);
            for(i=0;i<rcv_tag.length ;i++)
            {
                logger("value[%i]:%f index:%i",i,atag->value,atag->index);
                // chequeo el puntero para ese indice
                if( ptags[atag->index] == -1 )
                {
                    // apunta
                    ptags[atag->index] = shmmap->actual_len;
                    shmmap->actual_len +=1;
                    // carga valores iniciales
                    init_tag(shmtags + ptags[atag->index],atag->index);

                }
                logger("ptags[%i]:%i",atag->index,ptags[atag->index]);
                // init_tag(shmtags + ptags[atag->index],atag->index);
                tag = shmtags + ptags[atag->index];
                // tag->index=atag->index;
                tag->value = atag->value;
                tag->streaky = atag->streaky;
                tag->alarm_n1 = atag->alarm_n1;
                tag->hb=(char)(TIME_STREAKY/TIME_SEND)+1;
                atag++;
            }
            shmmap->writing=0;
            logger("recv_SHM_server len:%i from:%s::%i",n,inet_ntoa(from.sin_addr),ntohs(from.sin_port));
            if(shmmap->send_exit)
            {
                    shmmap->recv_exit=1;
                    free(ptags);
                    exit(0);
            }
    }
}


int main(int argc, char *argv[])
{
	int childpid;
	struct shm_control *shmmap;
	int shmlen = sizeof(struct shm_server);
	
	// delete_shm(SHM_SERVER);
    shmmap = (struct shm_control *) map_shm(SHM_SERVER, "rwt", shmlen);

	shmmap->send_exit=0;
	shmmap->recv_exit=0;
	shmmap->actual_len=0;
	shmmap->writing=0;
	shmmap->sending=0;
	 
	if((int)shmmap>-1)
	{
		logger("map_shm() no se puede mapear la memoria");
		exit(0);
	}
	
	if ( (childpid = fork()) == 0 ) 
	{
		logger("Recv_SHM_server PID:%i",getpid());
		recv_SHM_server(shmmap);
		sleep(1);
	} 
	else if ( childpid > 0 ) 
	{
		logger("Send_SHM_server PID:%i",getpid());
		send_SHM_server(shmmap);
		sleep(1);	
	}
	else
	{
		logger("error fork\n");
	}
	
	return 0;
 }



