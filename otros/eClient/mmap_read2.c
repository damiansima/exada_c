#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <string.h>

#define FILEPATH "/tmp/mmapped.txt"
#define NUMINTS  (1000)
#define FILESIZE 100

struct test
{
	int val;
	int vec[100];
	char buf[100];
};


void unmap_file(void *tmap,int len)
{
	if (munmap(tmap, len) == -1) 
	{
		perror("Error un-mmapping the file");
	}
	return;
}


int* map_file(char *file,char *mode,int len)
{
	int fd;
	int *tmap;
	
	if(mode[0]=='w' || mode[1]=='w' )
		fd = open(FILEPATH, O_RDWR | O_CREAT, (mode_t)0600);
	else
		fd = open(file, O_RDONLY,(mode_t)0600);
		
	if (fd == -1) 
	{
		perror("Error opening file for writing");
		return (void *)-1;
    }
	
	if(mode[0]=='w' || mode[1]=='w' )
		tmap = mmap(0, len, PROT_READ | PROT_WRITE ,  MAP_SHARED, fd, 0);
	else
		tmap = mmap(0, len, PROT_READ , MAP_SHARED, fd, 0);
		
	
	close(fd);	
	
    if (tmap == MAP_FAILED) 
    {
		close(fd);
		perror("Error mmapping the file");
		return (void *)-1;
    }
    
    return tmap;	
}



int main(int argc, char *argv[])
{
    int i;
	struct test *tmap;
	struct test ant_tmap;
	// int result=50;
    // int *map;
	// char buf[FILESIZE]="hola mundo";
	
	
	tmap = (struct test *) map_file(FILEPATH,"rw",sizeof(struct test)); 
	
	if(tmap==(struct test *)-1)
	{
		printf("No se puede mapear la memoria\n");
	}
	
	
	while(1)
	{
		
		if(memcmp(&ant_tmap,tmap,sizeof(struct test)))
		{
			memcpy(&ant_tmap,tmap,sizeof(struct test));
			printf("val: %i\n",tmap->val); 
			printf("vec: "); 
			for (i = 0; i <10; ++i) 
			{
				printf("%i  ",tmap->vec[i]); 
			}
			printf("\nbuf: %s\n",tmap->buf); 
			
			// tmap->buf[1]='A';
		}
		sleep(1);
	}
	
	
	unmap_file(tmap,sizeof(struct test));
	
	// write(fd,buf,100);
	// printf("write:<%s>\n",buf);
	
	// fread(fd,buf,100);
	// printf("read:<%s>\n",buf);
	
    return 0;
}
