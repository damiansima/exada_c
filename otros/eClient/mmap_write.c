#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <string.h>

#define FILEPATH "/tmp/mmapped.txt"
#define NUMINTS  (1000)
#define FILESIZE 100

struct test
{
	int val;
	int vec[10];
	char buf[100];
};

int main(int argc, char *argv[])
{
    int i;
    int fd;
	struct test *tmap;
	// int result=50;
    // int *map;
	char buf[10];
	
	
	// fd = open(FILEPATH, O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
	fd = open(FILEPATH, O_RDWR , (mode_t)0600);
	if (fd == -1) {
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
    }
	
	tmap = (struct test*) mmap(0, sizeof(struct test), PROT_READ | PROT_WRITE,MAP_SHARED, fd, 0);
	
    if (tmap == MAP_FAILED) {
		close(fd);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
    }
	
	while(1)
	{
		scanf("%s",buf);
		memcpy(tmap->buf,buf,strlen(buf)+1);		
		// printf("\nbuf: %s\n",tmap->buf); 
		
		if (!memcmp(tmap->buf, "end",3))
			break;
		
		for(i=0;i<strlen(buf);i++)
		{
			tmap->vec[i]=(int) buf[i];
		}
		
	}
	
	if (munmap(tmap, sizeof(struct test)) == -1) {
		perror("Error un-mmapping the file");
		/* Decide here whether to close(fd) and exit() or not. Depends... */
    }
	
	// write(fd,buf,100);
	// printf("write:<%s>\n",buf);
	
	// fread(fd,buf,100);
	// printf("read:<%s>\n",buf);
	close(fd);
    return 0;
}
