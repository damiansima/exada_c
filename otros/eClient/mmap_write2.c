#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <string.h>

#define FILEPATH "/tmp/mmapped.txt"
#define NUMINTS  (1000)
#define FILESIZE 100

struct test
{
	int val;
	int vec[100];
	char buf[100];
};

int main(int argc, char *argv[])
{
    int i;
    int fd;
	struct test tmap;
	// int result=50;
    // int *map;
	// char buf[10];
	
	
	fd = open(FILEPATH, O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
	// fd = open(FILEPATH, O_RDWR | O_CREAT, (mode_t)0600);
	if (fd == -1) {
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
    }	
	
	memset(&tmap,0,sizeof(tmap));
	
	// scanf("%s",buf);
	strcpy(tmap.buf,argv[1]);
	for(i=0;i<strlen(tmap.buf);i++)
	{
		tmap.vec[i]=(int) tmap.buf[i];
	}	
		
	write(fd,&tmap,sizeof(tmap));
	close(fd);
    return 0;
}
