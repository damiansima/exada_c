#include <sys/types.h> 
#include <sys/ipc.h>
#include <sys/msg.h>


void delete_shm(char *name)
{
    key_t key;
    int shmid;
    char msg[100];
    key = ftok(name,'s');
    shmid =shmget(key,0,0);
    sprintf(msg,"delete_shm(): remove shm '%s', shmid:%i\n",name,shmid);
    write(2,msg,strlen(msg));
    shmctl(shmid, IPC_RMID, 0); 
}

int msgflg; /* message flags for the operation */
struct msgbuf *msgp; /* pointer to the message buffer */
int msgsz; /* message size */
long msgtyp; /* desired message type */
int msqid /* message queue ID to be used */

int main()
{
    msgp = (struct msgbuf *)malloc((unsigned)(sizeof(struct msgbuf)
                                   - sizeof msgp->mtext + maxmsgsz));

    if (msgp == NULL) 
    {
        (void) fprintf(stderr, "msgop: %s %d byte messages.\n",
            "could not allocate message buffer for", maxmsgsz);
        exit(1);

        
        msgsz = ...
        msgflg = ...

        if (msgsnd(msqid, msgp, msgsz, msgflg) == -1)
        perror("msgop: msgsnd failed");
        
        
        msgsz = 
        msgtyp = first_on_queue;
        msgflg = 
        if (rtrn = msgrcv(msqid, msgp, msgsz, msgtyp, msgflg) == -1)
            perror("msgop: msgrcv failed");
    }
    return 0;
    
}
