# server

import zmq
import time

context = zmq.Context()
socket = context.socket(zmq.QUEUE)
#socket = context.socket(zmq.REP)   # uncomment for Req/Rep

socket.bind("ipc:///tmp/pepe")

i = 0
# time.sleep(1)   # naive wait for clients to arrive

for a in range(20):
  #msg = socket.recv()    # uncomment for Req/Rep
  msg='hola'
  socket.send(msg.encode())
  
time.sleep(1)   # naive wait for tasks to drain
