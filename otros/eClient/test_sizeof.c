#include <stdio.h>      
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <stdlib.h>     
#include <string.h>     
#include <unistd.h>     


#include "e_common.h"

#define TEST(A) {printf("%-40s%3i\n",#A,A);}
				
				
int main(int argc, char *argv[])
{
	printf("%-40s%s\n","type","size");
	printf("%-40s%s\n","----","----");
	TEST(sizeof(char));
	TEST(sizeof(short));
	TEST(sizeof(int));
	TEST(sizeof(long));
	TEST(sizeof(float));
	TEST(sizeof(double));
	TEST(sizeof(struct SHM_TAG));
	TEST(sizeof(struct shm_tags));
	TEST(sizeof(struct shm_asi_tags));
	TEST(sizeof(struct data_tag));
	TEST(sizeof(struct shm_control));
	TEST(sizeof(struct shm_asi));
	TEST(sizeof(struct shm_server));
	TEST(sizeof(struct server_client));
	return 0;
}
