#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <string.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <stdarg.h>   
#define FILE_LOG   "/tmp/logger.log"
#define DEBUG

#define msleep(x) usleep(1000*(x))


void say(const char *pName, const char *pMessage) 
{
    time_t the_time;
    struct tm *the_localtime;
    char timestamp[256];
    
    the_time = time(NULL);
    
    the_localtime = localtime(&the_time);
    
    strftime(timestamp, 255, "%H:%M:%S", the_localtime);
    
    printf("%s @ %s: %s\n", timestamp, pName, pMessage);
    
}



int release_semaphore(const char *pName, sem_t *pSemaphore, int live_dangerously) {
    int rc = 0;
    char s[1024];
    
    say(pName, "Releasing the semaphore.");
    
    if (!live_dangerously) {
        rc = sem_post(pSemaphore);
        if (rc) {
            sprintf(s, "Releasing the semaphore failed; errno is %d\n", errno);
            say(pName, s);
        }
    }
    
    return rc;
}


int acquire_semaphore(const char *pName, sem_t *pSemaphore, int live_dangerously) {
    int rc = 0;
    char s[1024];

    say(pName, "Waiting to acquire the semaphore.");

    if (!live_dangerously) {
        rc = sem_wait(pSemaphore);
        if (rc) {
            sprintf(s, "Acquiring the semaphore failed; errno is %d\n", errno);
            say(pName, s);
        }
    }

    return rc;
}


/*      void logger(char *fmt,...)
*       
*  Se puede pasar muchos argumentos como printf
*  se configura el archivo de log:         
*       #define FILE_LOG   "/tmp/logger.log"
*              
*  Permite hacer logger y printf si esta definido 
*       #define DEBUG
*/
void logger(const char *fmt,...)
{
    FILE *Fd = NULL;
	char bufti[100];
	time_t Time;
    
    va_list arglist;
    
    Fd = fopen( FILE_LOG, "a+" );
	if( Fd == NULL )
	{
		perror( "logger() fopen" );
	}
    else
    {
        Time = time(NULL);                         
        strftime(bufti,80,"%d/%m/%Y %H:%M:%S", localtime(&Time));
        fprintf( Fd, "%s:: ",bufti );
        va_start( arglist, fmt );
        vfprintf( Fd, fmt, arglist );
        va_end( arglist );
        fprintf( Fd, "\r\n" );
        fflush(Fd);
        fclose(Fd);        
#ifdef DEBUG
        printf("%s:: ",bufti );
        va_start( arglist, fmt );
        vprintf(fmt,arglist);
        va_end( arglist );
        printf("\r\n");
#endif
    }
    
}


int* map_file(char *file,char *mode,int len)
{
	int fd;
	int *tmap;
	
	if(mode[0]=='w' || mode[1]=='w' )
    {
		fd = open(file, O_RDWR | O_CREAT, (mode_t)0600);
        if (ftruncate(fd, len)) {
            perror("Resizing the file failed");
        }
    }
	else
		fd = open(file, O_RDONLY,(mode_t)0600);
	
    if (fd == -1) 
	{
		perror("Error opening file for writing");
		return (int *)-1;
    }
	
	if(mode[0]=='w' || mode[1]=='w' )
		tmap = (int *) mmap(0, len, PROT_READ | PROT_WRITE ,  MAP_SHARED, fd, 0);
	else
		tmap = (int *) mmap(0, len, PROT_READ , MAP_SHARED, fd, 0);
		
	
	close(fd);	
	
    if (tmap == MAP_FAILED) 
    {
		close(fd);
		perror("Error mmapping the file");
		return (int *)-1;
    }
    
    return tmap;	
}


void unmap_file(void *tmap,int len)
{
	if (munmap(tmap, len) == -1) 
	{
		perror("Error un-mmapping the file");
	}
	return;
}


 int* map_shm(const char *file,const char *mode,int len)
 {
	 int fd;
	 int *tmap;
	 
	 if(mode[0]=='w' || mode[1]=='w' )
	 {
		 fd = shm_open(file, O_RDWR | O_CREAT, (mode_t)0600);
		 if (ftruncate(fd,(off_t) len)) {
             perror("Resizing the shared memory failed");
         }
	 }
	 else
		 fd = shm_open(file, O_RDONLY,(mode_t)0600);
		 
	 if (fd == -1) 
	 {
		 perror("Error opening file for writing");
		 return (int *)-1;
     }
	 
	 if(mode[0]=='w' || mode[1]=='w' )
		 tmap = (int *) mmap(0, len, PROT_READ | PROT_WRITE ,  MAP_SHARED, fd, 0);
	 else
		 tmap = (int *) mmap(0, len, PROT_READ , MAP_SHARED, fd, 0);
		 
	 
	 close(fd);	
	 
     if (tmap == MAP_FAILED) 
     {
		 close(fd);
		 perror("Error mmapping the file");
		 return (int *)-1;
     }
     
     return tmap;	
 }
 
 
// void unmap_shm(char *name, void *tmap,int len)
// {
//	 if (munmap(tmap, len) == -1) 
//	 {
//		 perror("Error un-mmapping the file");
//	 }
//     shm_unlink(name);
//	 return;
// }

void delete_shm(char *name)
{
    key_t key;
    int shmid;
    char msg[100];
    key = ftok(name,'s');
    shmid =shmget(key,0,0);
    sprintf(msg,"delete_shm(): remove shm '%s', shmid:%i\n",name,shmid);
    write(2,msg,strlen(msg));
    shmctl(shmid, IPC_RMID, 0); 
}

int* map_shm2(char *name,const char *mode,int len)
{
	int shmid;    
    void *shared_memory;
    key_t key;
    size_t perm=0400; // read only
    
    if(mode[0]=='w' || mode[1]=='w' )
        perm+=0200;
        
    key = ftok(name,'s');    
    shmid =shmget(key,len,perm | IPC_CREAT);
    
    if (shmid == -1)
    {
        perror("shmget failed");
        if(mode[1]=='t' || mode[2]=='t' )
        {
            delete_shm(name);
            shmid =shmget(key,len,perm | IPC_CREAT);
        }
        if (shmid == -1)
            return (int *)-1;
    }
    printf("shmid: %i\n",shmid);
    
    shared_memory = shmat(shmid,(void *)0, 0);
	
    if ((int)shared_memory == -1) 
    {
		perror("Error shmat ");
		return (int *)-1;
    }
    
    return (int *) shared_memory;	
}


void unmap_shm(char *name, void *tmap)
{
    key_t key;
    int shmid;
    key = ftok(name,'s');
    shmid =shmget(key,0,0);
    shmdt(tmap);
    shmctl(shmid, IPC_RMID, 0); 
}




