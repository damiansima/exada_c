// Hello World server

#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>     

typedef struct _e_ipc{
    void *zmq_context;
    void *zmq_socket;
    char *name_attach;
}e_ipc;


void e_ipc_free(void *ipc)
{
    e_ipc *eipc=(e_ipc *)ipc;
    zmq_close(eipc->zmq_socket);
    zmq_ctx_destroy(eipc->zmq_context);
    free(eipc->name_attach);
    free(eipc);    
}


void *e_ipc_new(char *name)
{    
    e_ipc *ipc;
    
    ipc = (e_ipc *) malloc(sizeof(e_ipc));
    name[100]=0;
    ipc->name_attach=strdup(name);
    printf("Inicia el server");
    ipc->zmq_context = zmq_ctx_new ();
    ipc->zmq_socket = zmq_socket (ipc->zmq_context, ZMQ_REP);
    // //int rc = zmq_bind (responder, "tcp://*:5555");
    if(zmq_bind (ipc->zmq_socket, "ipc:///tmp/filename")==0)
    {
        e_ipc_free((void *)ipc);
        return (void *)-1;
    }
    return ((void *)ipc);
}




int main (void)
{
    // Socket to talk to clients
    printf("Inicia el server");
    
    void *context = zmq_ctx_new ();
    void *responder = zmq_socket (context, ZMQ_REP);

    // int rc = zmq_bind (responder, "tcp://*:5555");
    int rc = zmq_bind (responder, "ipc:///tmp/filename");
    assert (rc == 0);
    
    
    
    // while (1) 
    if (1) 
    {
        // char buffer [10];
        // zmq_recv (responder, buffer, 10, 0);
        // buffer[9]=0;
        // printf ("Received: %s\n",buffer);
        // sleep (1); // Do some 'work'
        zmq_send (responder, "World", 5, 0);
    }
    zmq_close(responder);
    zmq_ctx_destroy(context);
    return 0;
}
