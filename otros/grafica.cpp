/* * * * * * MODO DE USO * * * * * * 
 * compilar con: 
 
	g++ grafica.cpp -lcurses
 
 * 
 * initscr();                			// Inicio CURSES 
 * 
 * move(2, 8);							// Ae mueve al punto 2 (de arriba abajo) 8 (de izquierda a derecha)
 * addch('*' );							// Agrega un carcter
 * 
 * refresh();                      		// Refresca la pantalla para que se vean los cambios
 * getch();								// Espera que se presione una tecla 
 * endwin();                       		// Finaliza CURSES
 * 
 * NOTA: Las lineas de move() y addch() son las que hay que modificar para trabajar.
 * por supuesto que se puede usar un print, sería: 
 * 		printw("Actual: x %d y %d \n", ix, iy);
 * 
 * * * * * * COLORES: * * * * * * 
 *      COLOR_BLACK   0
        COLOR_RED     1
        COLOR_GREEN   2
        COLOR_YELLOW  3
        COLOR_BLUE    4
        COLOR_MAGENTA 5
        COLOR_CYAN    6
        COLOR_WHITE   7
 * init_pair(1, COLOR_BLUE, COLOR_BLUE); parametros: identificador, color de letra, color de fondo
 * attron(COLOR_PAIR(1)); usa la combinación de color del identificador
 * attroff(COLOR_PAIR(1)); usa color por defecto
 * * * * * * * * * * * * * * * * * 
 
 * 
 * 
*/


#include <ncurses.h>

int main()
{       
        initscr();                      /* Start curses mode              */
        move(3, 10);
		addch('*' );
		printw("dale a una tecla para salir");
		
		move(1, 1);
		printw("Las variables COLS y LINES estan definidas por initscr()\n");
		printw("Numero de lineas: %d y de columnas %d \n", LINES, COLS);
		
        //~ start_color();                  /* Start color                  */
        //~ init_pair(1, 1, COLOR_BLUE);
        //~ init_pair(2, 2, COLOR_BLACK);
//~ 
//~ 
        //~ attron(COLOR_PAIR(1));
        //~ printw("presione una tecla para salir");      /* Print Hello World              */
        //~ //attroff(COLOR_PAIR(1));
		//~ attron(COLOR_PAIR(2));
        //~ attroff(COLOR_PAIR(1));
        //~ move(4, 80);
		//~ addch('*' );
		//~ refresh();                      /* Print it on to the real screen */
        //~ printw("presione una tecla para salir");      /* Print Hello World              */
        //~ char ch =getch();                        /* Wait for user input */
        //~ if(ch == KEY_LEFT)
        //~ printw("Left arrow is pressed\n");
        getch();
        endwin();                       /* End curses mode                */

        return 0;
}


//~ /* * R J Botting.  CS 202  Thu Oct 27 15:34:27 PDT 1994 wallpaper.c*/
//~ /* Purpose:  	Another cursor movement program*/
//~ /* WARNING:   Must compile with options '-lm' and '-lcurses' */
//~ #include <stdio.h>
//~ #include <curses.h>



//~ int
//~ main( void )
//~ {
	//~ int	i; /*number of iterations in bugs llife cycle*/
	//~ int	ix, iy;
	//~ int	vx, vy;
	//~ int	midx,  midy;
	//~ initscr(); /*Intialize the screen... essential to make curses work*/
	//~ printw("Hello World !!!");
	//~ midx = LINES / 2; /* COLS and LINES are defined by initscr(), and not before*/
	//~ midy = COLS / 2;
	//~ cbreak();/*allow CTRL/C interupts... man cbreak*/
	//~ clear();   /*clear screen*/
	//~ ix = midx;
	//~ iy = midy;
	//~ vx = 1; 
	//~ vy = -1;
	//~ move(ix, iy);
	//~ addch('*' );
	//~ refresh();
	//~ printw("Actual: x %d y %d \n", ix, iy);
	//~ for (i = 0; i < 20; i++) {
		//~ /*move(ix, iy);
		//~ addch(' ' );
		//~ */
		//~ ix += vx;
		//~ iy += vy;
		//~ 
		//~ move(ix, iy);
		//~ addch('*' );
		//~ printw("Hello World !!!");
		//~ printw("Screen is %d rows by %d cols\n", ix, iy);
		//~ refresh();
		//~ if (ix <= 1)
			//~ vx = 1; 
		//~ else if (ix >= LINES - 2)
			//~ vx = -1;
		//~ if (iy <= 1)
			//~ vy = 1; 
		//~ else if (iy >= COLS - 2)
			//~ vy = -1;
	//~ }
	//~ getch();
	//~ endwin();
	//~ return(0);
//~ }
