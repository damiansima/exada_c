/* 
 * File:   guarda_dat.c
 * Author: sima
 *
 * Escribe registros en el fichero ALUMNOS.DAT 
 * 
 */

#include <stdio.h>
#include <stdlib.h>



#include <stdio.h>
#include <conio.h>
#include <ctype.h>

typedef struct  {
        char nombre[30];
        int edad;
        int curso;
        int nota;
       } TIPOALUMNO;



void MostrarCabeceras ()
{
  gotoxy(1, 8);
  puts("---------------------------------------------");
  puts("         DATOS ALUMNO                       ");
  puts("---------------------------------------------");
  puts("      NOMBRE :");
  puts("      EDAD   :");
  puts("      CURSO  :");
  puts("      NOTA   :");
  puts("---------------------------------------------");
}


// El parámetro reg es un parámetro de salida
// Debemos utilizar un puntero al dato que vamos
// a modificar
void LeerCampos ( TIPOALUMNO *reg)
{
  /* Borro los textos anteriores */
  gotoxy(15, 11); puts("                      ");
  gotoxy(15, 12); puts("                      ");
  gotoxy(15, 13); puts("                      ");
  gotoxy(15, 14); puts("                      ");
  /* Leo los nuevos valores */
  fflush(stdin);
  gotoxy(15, 11);  gets(reg->nombre);
  gotoxy(15, 12);  scanf("%d",&reg->edad);
  gotoxy(15, 13);  scanf("%d",&reg->curso);
  gotoxy(15, 14);  scanf("%d",&reg->nota);
  fflush(stdin);
}

main ()

{
  FILE *fsal;
  TIPOALUMNO ralumno;
  char continuar;

  // Abro para añadir
  fsal = fopen("ALUMNOS.DAT","ab");
  if ( fsal  == NULL )
   {
   perror("ALUMNOS.DAT");
   exit(1);
   }

  clrscr(); // Borro la pantalla
  MostrarCabeceras();
  continuar = 's';
  while ( toupper(continuar) == 'S' )
     {
     LeerCampos(&ralumno);
     fwrite(&ralumno,sizeof(TIPOALUMNO),1,fsal );
     gotoxy(20,16); printf( "Continuar (s/n):");
     continuar = getchar();
     gotoxy(20,16); printf( "                  ");
     }

  fclose(fsal);

}