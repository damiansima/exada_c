#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include   <stdio.h>
#include   <setjmp.h>
#include   <signal.h>
#include   <sys/types.h>
#include   <sys/ipc.h>
#include   <sys/shm.h>
#include   <string.h>

int readLine(int fd, char *str) {
    int n;

    do {
        n = read(fd, str, 1);
    } while (n > 0 && *str++ != 0);
    return (n > 0);
}

int lector(int argc, char *argv[]) {

    int fd;
    char message[100];

    puts("lector");

    //~ unlink(aPipe);
    mknod("aPipe", S_IFIFO, 0);
    chmod("aPipe", 0660);
    fd = open("aPipe", O_RDONLY);
    while (readLine(fd, message))
        printf("readLine: %s\n", message);

    close(fd);
    return 0;
}

int escritor(int argc, char *argv[]) {

    int fd, messagelen;
    char message[100];

    puts("escritor");

    sprintf(message, "Hello from PID %d", getpid());
    messagelen = strlen(message) + 1;
    do {
        fd = open("aPipe", O_WRONLY | O_NDELAY);
        if (fd == -1) sleep(1);
    } while (fd == -1);

    //~ for (i = 1; i < 4; i++) 
    //~ {
    //~ write(fd, message, messagelen);
    //~ sleep(3);
    //~ }

    while (1) {
        fgets(message,99,stdin);
        messagelen = strlen(message) + 1;
        write(fd, message, messagelen);
    }
    close(fd);
    return 0;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        puts("error en argumentos");
        puts("[read write]");
    }

    if (!strcmp("read", argv[1]))
        lector(0, NULL);

    if (!strcmp("write", argv[1]))
        escritor(0, NULL);

    return 0;

}
