 



#include <termios.h>


#include <sys/stat.h>
#include <errno.h>
#include "../../library/logger/include/logger.h"
#include "mb_rtu.h"
#include "../../include/utils.h"


e_driver_t* edrv_init(const char *fmt, ...)
{
    /*
    lanzar consolas virtuales
    socat PTY,link=/tmp/pty1 PTY,link=/tmp/tty1
    */
    #define CMPSTR(a) (strncasecmp(&fmt[index], a, strlen(a))==0)
    e_driver_t *edrv=calloc(1,sizeof(e_driver_t));
    int index;
    char parity;
    va_list ap;
    va_start( ap, fmt ); 
    
    edrv->fd = -1;
    edrv->timeout = 200;
    edrv->baudrate = 9600;
    edrv->bytesize = 8;
    edrv->stopbits = 1;
    edrv->parity = 0;
    edrv->port_name = 0;
    pthread_mutex_unlock(&edrv->mtx);
    index=0;
    while(fmt[index])
    {
        if CMPSTR("baudrate")
            edrv->baudrate = va_arg(ap, int);
        else if CMPSTR("parity")
        {
            parity = (char) va_arg(ap, int);
            if (parity<3)
                edrv->parity = parity ==1 ? 'O':parity ==2 ? 'P':'N';
            else
                edrv->parity = parity =='O' ? 'O':parity =='P' ? 'P':'N';
        }
        else if CMPSTR("bytesize")
            edrv->bytesize = va_arg(ap, int);
        else if CMPSTR("port_name")
            edrv->port_name = va_arg(ap, char *);
        else if CMPSTR("timeout")
            edrv->timeout = va_arg(ap, int);
        else if CMPSTR("stopbits")
            edrv->stopbits = va_arg(ap, int);
        index++;
    }
    #undef CMPSTR
    va_end( ap );
    
    TESTS(edrv->baudrate,"%i");
    TESTS(edrv->parity,"%c");
    TESTS(edrv->port_name,"%s");
    TESTS(edrv->bytesize,"%i");
    TESTS(edrv->stopbits,"%i");
    TESTS(edrv->timeout,"%i");
  
    return edrv;
}

edrv_error edrv_connect(e_driver_t* edrv)
{
    if( edrv->fd > 0 ) return E_PREV_OPEN;    
    errno = 0;
    
    // if( (edrv->fd = open( edrv->port_name, O_RDWR )) < 0 ) 
    edrv->fd = fd_open( edrv->port_name, edrv->baudrate, edrv->parity, edrv->bytesize, edrv->stopbits );
    if( edrv->fd <= 0 )
    {
        log_error("edrv_connect(): error to open -%s-. %s\n", edrv->port_name, strerror(errno));
        return E_NO_OPEN;
    }
    log_debug("edrv_connect() connected ! ");
    return NO_ERROR;
}



edrv_error edrv_disconnect(e_driver_t* edrv)
{
    if( edrv->fd < 0 ) 
        return NO_ERROR;  
    close(edrv->fd);
    log_debug("edrv_disconnect()");
    return NO_ERROR;
}

void _MSG(char *msg,int len) 
{
    int i;
    printf("msg: [");
    for (i=0;i<len;i++)
    {
        printf(" 0x%02X:%03i ",msg[i],msg[i]);
    }
    printf("]\n");
}

edrv_error edrv_write (e_driver_t* edrv, int slave_addres, int register_addres, int value)
{
    int res;
    ushrthl crc, reg, val;
    static char msg[20];
    assert_return( edrv->fd >0 );
    
    val.val= (unsigned short) value;
    reg.val = (unsigned short)register_addres;
    msg[0] = (char) slave_addres;
    msg[1] = (char)6;
    msg[2] = reg.high; 
    msg[3] = reg.low;
    msg[4] = val.high; 
    msg[5] = val.low; 
    crc.val = e_CRC16(msg,6);
    msg[6] = crc.low;
    msg[7] = crc.high;
    
    res = e_fd_write( edrv->fd, msg, 8, edrv->timeout );
    assert_return( res > 0 );
    
    res = e_fd_read( edrv->fd, msg, 8, slave_addres, edrv->timeout );
    assert_return( res > 0 );
//    _MSG(msg,8);
    crc.low=msg[6];
    crc.high=msg[7];
    assert_return( e_CRC16( msg, 6 ) == crc.val );
    assert_return( msg[1] == 6 );
    
    
    return NO_ERROR;
}

edrv_error edrv_read_block (e_driver_t* edrv, int slave_addres, int register_addres, int number_of_register, char *msg)
{
    int res;
    ushrthl crc, dat;
    int length = 5+2*number_of_register;
    assert_return( edrv->fd >0 );
    
    msg[0] = (char) slave_addres;
    msg[1] = (char)3;
    dat.val = (unsigned short)register_addres;
    msg[2] = dat.high; 
    msg[3] = dat.low; 
    dat.val = (unsigned short)number_of_register;
    msg[4] = dat.high; 
    msg[5] = dat.low; 
    crc.val = e_CRC16(msg,6);
    msg[6] = crc.low;
    msg[7] = crc.high;
    
    res = e_fd_write( edrv->fd, msg, 8, edrv->timeout );
    memset(msg,0,length);
    
    assert_return( res > 0 );
    
    res = e_fd_read( edrv->fd, msg, length, slave_addres, edrv->timeout );
//    _MSG(msg,lengtCh);
    assert_return( res > 0 );
    crc.low=msg[length-2];
    crc.high=msg[length-1];
    assert_return( e_CRC16( msg, length-2 ) == crc.val );
    assert_return( msg[1] == 3 );
    
    return NO_ERROR;
}

ushort e_CRC16(char *data, ushort len)
{
    ushort index;
    ushort tCRC=0xFFFF;
    ushort N,flag;

    for (index=0;index<len;index++){
        tCRC = data[index] ^ tCRC;
        for(N=0;N<8;N++){
            flag = tCRC & 0x0001;
            tCRC >>=1;
            if (flag)
                tCRC ^= 0xA001;
        }
    }
    return tCRC;
}



int e_fd_read(int fd, char *buf, int nbytes, int saddres, int timeout)
{
    	fd_set rfds;
	struct timeval tv;
        int res;
        ushort length=nbytes;
        timems_t time_end = timems() + timeout;
        char c;
        char *buf_read = buf;
        enum {wait_addres, wait_len_eof}etapa;
        etapa = wait_addres;
	
	tv.tv_sec = 0;
	tv.tv_usec = 1000 * timeout;
	
	FD_ZERO(&rfds);
	FD_SET(fd, &rfds);
        
        while( time_end > timems())
        {
            res = select(fd + 1, &rfds, NULL, NULL, &tv);
            if(assert_msg(res > 0 ))
                return -1;

            if(FD_ISSET(fd, &rfds))
            {
                switch(etapa)
                {
                    /* el primer dato de la trama tiene que ser la direcccion 
                    del master en este caso */
                    case wait_addres: 
                        read(fd, &c, 1);
                        if((int)c==saddres)
                        {
                            *buf_read=c;
                            buf_read++;
                            nbytes--;
                            etapa = wait_len_eof;
                        }
                        break;
                    case wait_len_eof:
                        res = read(fd, buf_read, nbytes);
                        if(assert_msg(res >= 0 ))
                            return -1;
                        buf_read += res;
                        nbytes -=res;
                        log_debug("buf_read len:%i :%s ",length-nbytes,buf_read);
                        if(nbytes==0) // se leen todos los bytes pedidos
                        {
                            return length;
                        }
                        break;
                }
                
            }
        
        }
	return -1;
}


int e_fd_write(int fd, char *buf, int nbytes, int timeout)
{
    	fd_set wfds;
	struct timeval tv;
	
	tv.tv_sec = timeout;
	tv.tv_usec = 0;
	
	FD_ZERO(&wfds);
	FD_SET(fd, &wfds);
	
	if(select(fd + 1, NULL, &wfds, NULL, &tv) == -1) 
            return -1;
	
	if(FD_ISSET(fd, &wfds))
            return write(fd, buf, nbytes);
	
	return -1;
}


int fd_open(char *portname, int baudrate, char parity, int bytesize, int stopbits)
{
//    int flags = O_RDWR | O_NOCTTY | O_NDELAY | O_EXCL;
    int flags = O_RDWR ;
    int fd;
    
    speed_t speed;
    struct termios tios;
    
    fd = open(portname, flags);
    assert_return( fd>0 )
    
    memset(&tios, 0, sizeof(struct termios));

    switch (baudrate) {
    case 110:
        speed = B110;
        break;
    case 300:
        speed = B300;
        break;
    case 600:
        speed = B600;
        break;
    case 1200:
        speed = B1200;
        break;
    case 2400:
        speed = B2400;
        break;
    case 4800:
        speed = B4800;
        break;
    case 9600:
        speed = B9600;
        break;
    case 19200:
        speed = B19200;
        break;
    case 38400:
        speed = B38400;
        break;
#ifdef B57600
    case 57600:
        speed = B57600;
        break;
#endif
#ifdef B115200
    case 115200:
        speed = B115200;
        break;
#endif
#ifdef B230400
    case 230400:
        speed = B230400;
        break;
#endif
#ifdef B460800
    case 460800:
        speed = B460800;
        break;
#endif
#ifdef B500000
    case 500000:
        speed = B500000;
        break;
#endif
#ifdef B576000
    case 576000:
        speed = B576000;
        break;
#endif
#ifdef B921600
    case 921600:
        speed = B921600;
        break;
#endif
#ifdef B1000000
    case 1000000:
        speed = B1000000;
        break;
#endif
#ifdef B1152000
   case 1152000:
        speed = B1152000;
        break;
#endif
#ifdef B1500000
    case 1500000:
        speed = B1500000;
        break;
#endif
#ifdef B2500000
    case 2500000:
        speed = B2500000;
        break;
#endif
#ifdef B3000000
    case 3000000:
        speed = B3000000;
        break;
#endif
#ifdef B3500000
    case 3500000:
        speed = B3500000;
        break;
#endif
#ifdef B4000000
    case 4000000:
        speed = B4000000;
        break;
#endif
    default:
        speed = B9600;
    }

    
    if ( (cfsetispeed(&tios, speed) < 0) ||
        (cfsetospeed(&tios, speed) < 0)) 
    {
        log_error("cfsetospeed(&tios, speed)");
        close(fd);
        return -1;
    }

    tios.c_cflag |= (CREAD | CLOCAL);
    /* CSIZE, HUPCL, CRTSCTS (hardware flow control) */

    /* Set data bits (5, 6, 7, 8 bits)
       CSIZE        Bit mask for data bits
    */
    tios.c_cflag &= ~CSIZE;
    switch (bytesize) {
        case 5:
            tios.c_cflag |= CS5;
            break;
        case 6:
            tios.c_cflag |= CS6;
            break;
        case 7:
            tios.c_cflag |= CS7;
            break;
        case 8:
        default:
            tios.c_cflag |= CS8;
            break;
    }

    if (stopbits == 1)
        tios.c_cflag &=~ CSTOPB;
    else
        tios.c_cflag |= CSTOPB;

    if (parity == 'N') {
        /* None */
        tios.c_cflag &=~ PARENB;
    } else if (parity == 'E') {
        /* Even */
        tios.c_cflag |= PARENB;
        tios.c_cflag &=~ PARODD;
    } else {
        /* Odd */
        tios.c_cflag |= PARENB;
        tios.c_cflag |= PARODD;
    }

    tios.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

    if (parity == 'N') {
        tios.c_iflag &= ~INPCK;
    } else {
        tios.c_iflag |= INPCK;
    }

    tios.c_iflag &= ~(IXON | IXOFF | IXANY);
    tios.c_oflag &=~ OPOST;

    tios.c_cc[VMIN] = 0;
    tios.c_cc[VTIME] = 0;

    if (tcsetattr( fd, TCSANOW, &tios) < 0) {
        close( fd );
        return -1;
    }

    return fd;
}


    