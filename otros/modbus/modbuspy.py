#!/usr/bin/python
# -*- coding: utf-8 -*-


import struct 
import serial
import time

import logging
FULLFORMAT = "%(asctime)s [%(levelname)s] %(message)s"
logger = logging.getLogger(__name__)

loglevel = logging.DEBUG
loglevel = logging.INFO
logging.basicConfig(level=loglevel,format=FULLFORMAT,datefmt='%m/%d/%Y %H:%M:%S')
# logging.basicConfig(filename='modbuspy.log',level=loglevel,format=FULLFORMAT,datefmt='%m/%d/%Y %H:%M:%S')

logger.info("Inicia modbuspy..")

FORMAT_PACK='>h'


def _XOR(integer1, integer2):
    return integer1 ^ integer2 

def _rightshift(inputInteger):
    shifted = inputInteger >> 1
    carrybit = inputInteger & 1
    return shifted, carrybit

def _to2byte(data):
	return str(struct.pack(FORMAT_PACK,data), encoding='latin1')


def _calculateCrcString(inputstring):
    """Calculate CRC-16 for Modbus.

    Args: inputstring (str):
    Returns: A two-byte CRC string
    """
    POLY = 0xA001
    # Preload a 16-bit register with ones
    register = 0xFFFF

    for character in inputstring:
        register = _XOR(register, ord(character))
        for i in range(8):
            register, carrybit = _rightshift(register)
            if carrybit:
                register = _XOR(register, POLY)

    return str(struct.pack('<H',register), encoding='latin1')



def _ver(msg):
	print('---msg---')
	for m in msg:
		print('0x{:x} \t{}'.format(ord(m),ord(m)))

EXCEPTION={
	1:"ILLEGAL FUNCTION",
	2:"ILLEGAL DATA ADDRESS",
	3:"ILLEGAL DATA VALUE",
	4:"SLAVE DEVICE FAILURE",
	5:"ACKNOWLEDGE",
	6:"SLAVE DEVICE BUSY"
	}



class MBRTU():

	def __init__(self,serial_port='/tmp/tty1',timeout=.2,baudrate=9600):
		'''
		lanzar consolas virtuales
		socat PTY,link=/tmp/pty1 PTY,link=/tmp/tty1
		'''
		self.serial_port=serial_port
		self.timeout=timeout
		self.sp=False
		self.baudrate = baudrate
		self.parity   = serial.PARITY_ODD
		self.bytesize = 8
		self.stopbits = 1

	def connect(self):
		logger.info('connecta port:%s timeout:%s',self.serial_port,self.timeout)
		self.sp = serial.Serial(self.serial_port,timeout=self.timeout,baudrate=self.baudrate,
			parity=self.parity,bytesize=self.bytesize,stopbits=self.stopbits)
		

	def close(self):
		logger.info('desconnecta port:%s ',self.serial_port)
		self.sp.close()
		self.sp=False

	def write(self,msg):
		self.sp.flushInput()
		msg = bytes(msg, encoding='latin1')
		logger.debug('msg write: %s',msg)
		try:
			self.sp.write(msg)
		except:
			logger.info('problema al escribir prot:%s',self.serial_port)
			self.close()
		self.sp.flushOutput()
		time.sleep(0.003)
		# GPIO.output(4,0)
		

	def read(self,addres,len_frame):	
		timeout=time.time()+self.timeout 
		self.sp.flushInput()
		sf = 255
		addres = ord(addres)
		# lee la direccion del esclavo
		while sf != addres:
			r = self.sp.read(1)
			try:
				sf=r[0]
			except:
				sf=255
			if time.time() > timeout:
				logger.error('timeduot de lectura prot:%s',self.serial_port)
				return False
		# lee el resto de la trama
		answer = self.sp.read(len_frame-1)
		# convierte la trama leida de byte a string		
		answer = chr(sf) + str(answer, encoding='latin1')
		if len(answer) != len_frame:
			crc=_calculateCrcString(answer)=='\x00\x00'
			if crc:
				if answer[1]=='\x83':
					logger.error('port:%s Funcion:3 Error:%s',self.serial_port,EXCEPTION[ord(answer[2])])	
				elif answer[1]=='\x86':
					logger.error('port:%s Funcion:6 Error:%s',self.serial_port,EXCEPTION[ord(answer[2])])
				else:
					logger.error('Respuesta incorrecta port:%s answer:%s',self.serial_port,bytes(answer, encoding='latin1'))
			else:
				logger.error('Respuesta incorrecta port:%s answer:%s',self.serial_port,bytes(answer, encoding='latin1'))
			return False	
		# GPIO.output(4,1)
		logger.debug('msg read: %s',bytes(answer, encoding='latin1'))
		return answer


	def read_registers(self,slave_addres,register_addres,number_of_register):
		logger.debug('read_registers %i %i %i',slave_addres,register_addres,number_of_register)
		# crea un string con los datos y adjunta el crc
		msg = chr(slave_addres) + chr(3) + _to2byte(register_addres) + _to2byte(number_of_register)
		msg = msg + _calculateCrcString(msg)
		self.write(msg)
		#msg=self.read(chr(slave_addres),5+2*number_of_register)
		msg=self.read(chr(slave_addres),5+2*number_of_register)
		if not msg:
			return False
		crc=_calculateCrcString(msg)=='\x00\x00'
		logger.debug("CRC:%s",crc)
		if not crc:
			return False
		msg=msg[3:-2]
		values = []
		try:
			for i in range(0,2*number_of_register,2):
				res2=bytes(msg[i:i+2], encoding='latin1')
				val=struct.unpack(FORMAT_PACK,res2)[0]
				values.append(val)
			logger.debug('registers: '+' '.join([ '%i:%i'%(register_addres+i,values[i]) for i in range(len(values))]))			
		except:
			return False
		return values
		



	def write_register(self,slave_addres,register_addres,wrte_data):
		logger.debug('write_register %i %i %i',slave_addres,register_addres,wrte_data)
		msg = chr(slave_addres) + chr(6) + _to2byte(register_addres) + _to2byte(wrte_data)
		msg = msg + _calculateCrcString(msg)
		self.write(msg)
		msg=self.read(chr(slave_addres),8)
		if not msg:
			return False
		crc=_calculateCrcString(msg)=='\x00\x00'
		logger.debug("CRC:%s",crc)
		if not crc:
			return False
		msg=msg[4:-2]
		res2=bytes(msg, encoding='latin1')
		val=struct.unpack(FORMAT_PACK,res2)[0]
		return True if wrte_data == val else False



"""
Ejemplo de implementacion:

	rtu=MBRTU('/tmp/tty1',.1)
	rtu.connect()

	for i in range(10):
		rtu.write_register(5,i,i)
		time.sleep(.02)
		rtu.read_registers(5,i,i)
		time.sleep(.01)

	rtu.close()

"""


rtu=MBRTU('/tmp/tty1',.05)
rtu.connect()

for i in range(2):
	rtu.write_register(1,i,i)
	# time.sleep(.05)
	rtu.read_registers(1,i,2)
	# time.sleep(.01)

rtu.close()
exit(0)

def values_to_string(values,format='<h'):
	'''
	convierte la lista numerica a string segun el formato:
	"<" indica que primero esta el menos significativo 
	"little-endian", como en el micro y la pc	
		h: con signo 16bits
		H: sin signo
		l: con signo de 32bits
		L: sin signo de 32bits
		f: float de 4 bits
		d: float de 8 bits
	'''
	return ''.join(
		str(struct.pack(format,val), encoding='latin1') 
		for val in values
		)

# msg='hola mundo como estas?'
# values=[]
# for i in range(0,len(msg)-1,2):
# 	res2=bytes(msg[i:i+2], encoding='latin1')
# 	val=struct.unpack('<h',res2)[0]
# 	values.append(val)

values=[28520, 24940, 27936, 28277, 28516, 25376, 28015, 8303, 29541, 24948, 16243]
# print(values)
# r=''
# for val in values:
# 	r+=str(struct.pack('<h',val), encoding='latin1')

r=values_to_string(values)
print('String:<<%s>>'%r)


exit(0)

res=_to2byte(257)

print('retorna:',res[0],res[1])

# lo mismo que 
fm='>h'
val=257
r=struct.pack(fm,val)
res= str(r, encoding='latin1')

print('retorna:',res[0],res[1])
print('retorna ord:',ord(res[0]),ord(res[1]))

res2=bytes(res, encoding='latin1')
val=struct.unpack(fm,res2)[0]

print('val:',val)


# montevideo 2534 entre rodrigues y pueyrredon 
# 8:0hs

