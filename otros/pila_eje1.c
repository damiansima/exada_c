#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <string.h>
#include <sys/types.h>

#include <sys/mman.h>
#include <sys/msg.h>
#include <fcntl.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <sys/time.h>

#include <stdarg.h>   

#define FALSE 0
#define TRUE  1


typedef struct{
    int key;
    int nbytes;
    void *next;
    void *prev;
    char data[];
}q_node_t; 

typedef struct{
    int qcount;
    q_node_t *start;
    q_node_t *end;
}q_list_t; 

typedef struct{
    int numero;
    int cuchara;
    int id_lugar;
}colada_t;


#define LOG_P(p) {printf("--- %-10s : %X \n",#p,(int)p);}
#define LOG_FUN() {printf("funcion: %s\n",__FUNCTION__);}
#define logger(fmt, args...) printf("[%u]: "fmt"\n",__LINE__,##args)



#define q_data_colada(node) ((colada_t *)( node ? ((q_node_t *)node)->data : NULL ))


q_list_t *q_init()
{
    q_list_t *list;
    LOG_FUN();
    list = calloc(1,sizeof(q_list_t));
    LOG_P(list);
    list->end=NULL;
    list->start=NULL;
    list->qcount=0;
    return list;
}

q_node_t *q_free(q_list_t *list)
{
    q_node_t *next;
    q_node_t *_free;
    LOG_FUN();
    next=list->start;
    while(next)
    {
        _free=next;
        next=_free->next;
        LOG_P(_free);
        free(_free);
    }
    LOG_P(list);
    free(list);
    return NULL;
}

q_node_t *q_get(q_list_t *list, int key)
{
    q_node_t *next;
    LOG_FUN();
    if(!list)return NULL;
    next=list->start;
    if(key<=0) 
        return next;
    while(next)
    {
        if(next->key==key)
            return next;
        next=next->next;
    }
    return NULL;
}


q_node_t *q_pop_node(q_list_t *list, q_node_t *node)
{
    q_node_t *next;
    q_node_t *prev;
    LOG_FUN();
    if(list && node)
    {
        list->qcount-=1;
        next=node->next;
        prev=node->prev;
        if(prev) prev->next=next;
        else if(next) list->start=next;
        
        if(next) next->prev=prev;
        else if(prev) list->end=prev;
        
        if(list->qcount==0)
            list->end=list->start=NULL;
        LOG_P(node);
        return node;
    }
    return NULL;
}

void q_delete_node(q_list_t *list, q_node_t *node) 
{
    LOG_FUN();
    if( list && node )
    {
        q_node_t *next;
        next=list->start;
        while(next)
        {
            if(next == node)
            {
                q_pop_node(list,node);
                break;
            }
            next=next->next;
        }
    }
    if(node) free(node);
}



q_node_t *q_last(q_list_t *list)
{
    LOG_FUN();
    if(list)
        return list->end;
    return NULL;
}

q_node_t *q_first(q_list_t *list)
{
    LOG_FUN();
    if(list)
        return list->start;
    return NULL;
}

q_node_t *q_next(q_node_t *node)
{
    LOG_FUN();
    if(node)
        return node->next;
    return NULL;
}

q_node_t *q_prev(q_node_t *node)
{
    LOG_FUN();
    if(node)
        return node->prev;
    return NULL;
}

q_node_t *q_pop(q_list_t *list, int key)
{
    q_node_t *node;
    LOG_FUN();
    node=q_get(list,key);
    return q_pop_node(list,node);
} 

void q_delete(q_list_t *list, int key)
{
    q_node_t *node;
    LOG_FUN();
    node=q_get(list,key);
    q_delete_node(list,node);
} 


typedef int (*q_order_hand) (void *,void *);

void q_order(q_list_t *list, q_order_hand function ) 
{
    q_node_t *node;
    q_node_t *nodenext;
    q_node_t *nodetmp;
    node=list->start;
    while( node && node->next )
    {
        nodenext=node->next;
        if( function(node->data,nodenext->data) )
        {
            if((nodetmp=node->prev))
            {
                nodetmp->next=nodenext;
                nodenext->prev=nodetmp;
            }   
            else
            {
                list->start=nodenext;
                nodenext->prev=NULL;
            }
            if((nodetmp=nodenext->next))
            {
                nodetmp->prev=node;
                node->next=nodetmp;
            }
            else
            {
                node->next=NULL;
                list->end=node;
            }
            node->prev=nodenext;
            nodenext->next=node;
            
            node=list->start;
        }
        else
            node=node->next;
    }
}

q_node_t *q_push(q_list_t *list, int key, int nbytes) 
{
    q_node_t *node;
    q_node_t *last;
    LOG_FUN();
    logger("key:%i nbytes:%i",key,nbytes);
    if(key>0)
    {
        node=q_get(list,key);
        if(node)
            return NULL; 
    }
    node = (q_node_t *)calloc(1,sizeof(q_node_t)+nbytes);
    LOG_P(node);
    if(!node)
       return NULL;     
    list->qcount+=1;
    last=list->end;
    list->end=node;
    if(last)
        last->next=node;
    else
        list->start=node;
    node->prev=last;
    node->next=NULL;
    node->nbytes=nbytes;
    node->key=key;
    return node;
}

int orden_colada(colada_t *col_prev, colada_t *col_next)
{
    return (col_prev->cuchara < col_next->cuchara);
} 


int main ()
{
    LOG_FUN();
    q_list_t *list;
    q_node_t *node;
    int i=1;
    colada_t *colada;
    
    list=q_init(); 
    while(i++<10)
    {
        node = q_push(list,0,sizeof(colada_t));
        colada = q_data_colada(node);
        colada->cuchara=random()%100;
        colada->id_lugar=1;
        
        LOG_P(colada);
//        q_push(list,3,test,strlen(test));
    }
    
    node=q_first(list);
    while(node)
    {
        colada = q_data_colada(node);
        logger("colada %i-%i",colada->cuchara,colada->id_lugar);
        node=q_next(node);
    }
    
    logger("Ordena...");
    q_order(list,orden_colada);
            
    node=q_first(list);
    while(node) 
    {
        colada = q_data_colada(node);
        logger("colada %i-%i",colada->cuchara,colada->id_lugar);
        node=q_next(node);
    }
    
//    logger("---qcount:%i",list->qcount);
//    logger("---aca");
//    node=q_last(list);
//    LOG_P(node);
//    node=q_next(node);
//    LOG_P(node);
//    
////    node=q_pop(list,0);
////    LOG_P(node);
////    free(node);
//    q_delete(list,0);
//    q_delete(list,0);
//    q_delete(list,0);
//    
//    logger("---qcount:%i",list->qcount);
//    logger("---aca2");
//    node=q_first(list);
//    LOG_P(node);
//    node=q_next(node);
//    LOG_P(node);
    
    
    q_free(list);
    return 0;
}


//static struct dipc_list *list_find_key(struct dipc_list *first, int key) {
//    struct dipc_list *dl2 = first->next;
//
//    while (dl2) {
//        if (dl2->info.key == key)
//            break;
//        dl2 = dl2->next;
//    }
//    return dl2;
//}
//
//static void list_remove(struct dipc_list **last, struct dipc_list *dl) {
//    if (dl == *last)
//        (*last) = (*last)->prev;
//    (dl->prev)->next = dl->next;
//    if (dl->next)
//        (dl->next)->prev = dl->prev;
//}
//
//static struct dipc_list *get_next_key(struct dipc_list *dl) {
//    struct dipc_list *dl2 = dl->next;
//
//    while (dl2) {
//        if (dl2->info.key == dl->info.key)
//            break;
//        dl2 = dl2->next;
//    }
//    return dl2;
//}
//