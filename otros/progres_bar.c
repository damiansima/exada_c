#include <stdio.h>
#include <stdlib.h>     /* qsort() */
#include <time.h>       /* time() - Para generar la semilla aleatoria */
#include <unistd.h>


#define PROGRESS_CHARS 50

static void print_progress_bar(int percent)
{
	int i;
	int p;

	if (percent > 100)
		percent = 100;

	/* Use stderr, so we don't capture it */
	putc('\r', stderr);
	putc('[', stderr);
	// for (i=0; i < PROGRESS_CHARS; i++)
		// putc(' ', stderr);
	// putc('|', stderr);
	// putc('\r', stderr);
	// putc('|', stderr);

	p = PROGRESS_CHARS * percent / 100;

	for (i=0; i < p; i++)
		putc('-', stderr);

	for (i=p; i < PROGRESS_CHARS; i++)
		putc(' ', stderr);
	// putc('|', stderr);
	fprintf(stderr," %3i%%]",percent);
		
	fflush(stderr);
}

int main()
{
	int i;
	
	fprintf(stderr,"%30s\n","hola");
	fprintf(stderr,"%-30s\n","hola");
	
	for(i=0;i<111;i++)
	{
		print_progress_bar(i);
		usleep(20000);
	}
	fprintf(stderr,"\nchau\n");
    return 0;
}
