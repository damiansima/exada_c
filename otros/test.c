#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sqlca.h>


#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>


#include <stdarg.h>
#include <time.h>	


void logger(const char *fmt, ...);


int main(int narg, char *args[]) 
{
    
    while(1)
    {
        logger("test");
        sleep(1);
    }
    printf("FIN\n");
    return 0;
}

void logger(const char *fmt, ...) {
    static FILE *Fd = NULL;
    static time_t nexTime = 0;
    char bufti[100];
    time_t Time;
    int x;

    va_list arglist;
    va_start(arglist, fmt);

#ifndef FILE_LOG
#define FILE_LOG "/tmp/test.log"
#endif      

    Time = time(NULL);
    if (Fd == NULL || Time >= nexTime) 
    {
        strftime(bufti, 80, FILE_LOG".%y%m%d", localtime(&Time));
        if (Fd ) 
            fclose(Fd);
        Fd = fopen(bufti, "a+");
        // cada hora intenta abrir nuevamente el archivo, por si se eliminó
        nexTime = 3600*(time_t)(Time/3600 + 1);
        printf("test file log:%s time:%i\n", bufti,nexTime);
    }

    if (Fd == NULL) {
        perror("logger() fopen");
    } else {
        strftime(bufti, 80, "%d/%m/%Y %H:%M:%S", localtime(&Time));
        fprintf(Fd, "%s ", bufti);
        vfprintf(Fd, fmt, arglist);
        fprintf(Fd, "\r\n");
        fflush(Fd);
        // fclose(Fd);        
    }
    va_end(arglist);
}
