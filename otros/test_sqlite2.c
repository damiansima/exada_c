// sqltest.c : Simple SQLite3 program in C by D. Bolton (C) 2013 http://cplus.about.com


#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <string.h>
#include <unistd.h>


char *dbname = "../cfg/DBConfig.db";
char *sql = "select id,tag from tags";

sqlite3 * db;
sqlite3_stmt *stmt;
char message[255];

int main(int argc, char* argv[])
{
    char tag[96];
    int id;
    
    /* open the database */
    int result=sqlite3_open(dbname,&db);
    if (result != SQLITE_OK) {
            printf("Failed to open database %s\n\r",sqlite3_errstr(result));
            sqlite3_close(db);
            return 1;
    }
    printf("Opened db %s OK\n\r",dbname);

    /* prepare the sql, leave stmt ready for loop */
    result = sqlite3_prepare (db, sql, strlen(sql)+1, &stmt, NULL);
    if (result != SQLITE_OK) {
            printf("Failed to prepare database %s\n\r",sqlite3_errstr(result));
            sqlite3_close(db);
            return 2;
    }

    printf("SQL prepared ok\n\r");

    /* loop reading each row until step returns anything other than SQLITE_ROW */
    do {
            result = sqlite3_step(stmt);
            if (result == SQLITE_ROW) {
                    id = sqlite3_column_int(stmt,0);
                    strcpy(tag, (char *)sqlite3_column_text(stmt,1));
//                    strcpy(venue, (char *)sqlite3_column_text(stmt,2));
//                    printf("On %d at %s for '%s' \n\r",date,venue,description);
                    printf("id:%i tag:'%s' \n\r",id,tag);
            }
    } while (result == SQLITE_ROW);

    /* finish off */
    sqlite3_close(db);
//    free(description);
//    free(venue);
    return 0;
}
