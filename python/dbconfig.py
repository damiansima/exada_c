from __future__ import print_function
import time
import Pyro4
import socket
import sqlite3
import re


class DBconfig(object):
	def __init__(self):
		self.isconect=False	
		self.conn = None	
		
	def connect(self):
		if not self.isconect: 
			self.conn = sqlite3.connect('../cfg/DBConfig.db')	
			self.c = self.conn.cursor()
			self.isconect=True
		
	def initDB(self):
		if not self.isconect: 
			self.connect()
		# Se crear una tabla de maquinas con los respectivos roles.
		# ROLES(bool): server,cliente,db,asi,historic,models
		self.c.execute('''CREATE TABLE HOSTS(
			ID integer primary key autoincrement,
			HOST varchar unique not null,
			SERVER varchar,
			DESCRIPTION varchar
			)''')
			
		self.c.execute('''CREATE TABLE DRIVERS(
			ID integer primary key autoincrement,
			DRIVER varchar unique not null
			)''')

		self.c.execute('''CREATE TABLE DEVICES(
			ID integer primary key autoincrement,
			DEVICE varchar unique not null,
			DESCRIPTION varchar,
			DRIVER varchar,
			ADDRESS int,
			REGSTART int,
			REGEND int, 
			STEP int, 
			FOREIGN KEY(DRIVER) REFERENCES DRIVERS(DRIVER)
			)''')
			
		self.c.execute('''CREATE TABLE TAGS(
			ID integer primary key autoincrement,
			TAG varchar unique not null,
			DESCRIPTION varchar,
			MODELS varchar,
			DEVICE varchar,
			REGISTER int,
			RWA varchar,
			TYPE text,
			LEN_TYEP int,
			VALUE_MIN double,
			VALUE_MAX double,
			FOREIGN KEY(MODELS) REFERENCES HOSTS(HOST),
			FOREIGN KEY(DEVICE) REFERENCES DEVICES(DEVICE)
			)''')
			
		self.c.execute('''CREATE TABLE HISTORIC(
			ID integer primary key autoincrement,
			TAG varchar unique not null,
			HOST varchar,
			FOREIGN KEY(HOST) REFERENCES HOSTS(HOST),
			FOREIGN KEY(TAG) REFERENCES TAGS(TAG)
			)''')
                        
		self.c.execute('''CREATE TABLE SCANS(
			ID integer primary key autoincrement,
			SCAN varchar,
			HOST varchar,
                        DEVICE varchar,
			FOREIGN KEY(HOST) REFERENCES HOSTS(HOST),
                        FOREIGN KEY(DEVICE) REFERENCES DEVICES(DEVICE)
			)''')
			
		self.c.execute('PRAGMA foreign_keys = ON')
		self.conn.commit()
		
	def close(self):
		if self.conn:
			# self.conn.commit()
			self.conn.close()
		self.conn = None
		self.isconect=False
		
		
	def addTag(self,tag,description,models,device,register,rwa,_type=None,len_type=None,val_min=None,val_max=None):
		r=re.compile('^[a-zA-Z_][a-zA-Z0-9_]*$')
		rr=r.search(tag)
		if not rr:
			print("El tag (%s) NO es alfanumerico. Debe pasar ^[a-zA-Z_][a-zA-Z0-9_]*$"%tag)
			return False
	
		if not self.isconect: 
			self.connect()
		try:
			self.c.execute("INSERT INTO TAGS VALUES(?,?,?,?,?,?,?,?,?,?,?)", 
				(None,tag,description,models,device,register,rwa,_type,len_type,val_min,val_max))
			self.conn.commit()
		except:
			print("no se pudo agregar el tag",(tag,device))
			
	def addDriver(self, driver):					
		if not self.isconect: 
			self.connect()
		try:
			self.c.execute('INSERT INTO DRIVERS VALUES(?,?)',(None,driver))
			self.conn.commit()
		except:
			print("NO se pudo agregar DRIVERS",driver)
                        
	def addScan(self, scan, host, device):					
		if not self.isconect: 
			self.connect()
		try:
			self.c.execute('INSERT INTO SCANS VALUES(?,?,?,?)',
                            (None,scan,host,device))
			self.conn.commit()
		except:
			print("NO se pudo agregar el SCAN",(scan,host,device))
				
	def addHosts(self,host,server,description=None):
					
		if not self.isconect: 
			self.connect()
		try:
			self.c.execute('INSERT INTO HOSTS VALUES(?,?,?,?)',
				(None,host,server,description))
			self.conn.commit()
		except:
			print("NO se pudo agregar host",host,server,description)
				
	def addDevices(self,device,description,driver,address,regstart,regend,step):			
		if not self.isconect: 
			self.connect()
		try:
			self.c.execute('INSERT INTO DEVICES VALUES(?,?,?,?,?,?,?,?)', 
				(None,device,description,driver,address,regstart,regend,step))
			self.conn.commit()
		except:
			print("no se pudo agregar ese device",(device,driver,address,
				regstart,regend,step))
			
	def getDevicesTags(self, host=None):
		if not self.isconect: 
			self.connect()
		self.c.execute("""select device,driver,address,regstart,regend,step FROM devices where host='%s'"""%host)
		devices = self.c.fetchall()
		print (devices)
		result=dict()
		for dev,dr,a,rs,re,sp in devices:
			print(dev,dr,a,rs,re,sp)
			self.c.execute("""select tag,register,rwa FROM TAGS 
				WHERE device='%s' order by register asc"""%(dev))
			res=self.c.fetchall()
			result[dev] = (dr,a,rs,re,sp,res)
		
		return(result)
		
		
	def delTag(self, val):
		if not self.isconect: 
			self.connect()
		self.c.execute('DELETE FROM TAGS WHERE tag=?', val)
		self.conn.commit()
		return True
	
	def showTag(self, val=None):
		if not self.isconect: 
			self.connect()
		self.c.execute("select * FROM TAGS WHERE tag='%s'"%val)
		values = self.c.fetchone()
		return(values)

	def getHNS(self, ip=None):
		# lee Hostname y servidor
		if not self.isconect: 
			self.connect()
		if ip:
			self.c.execute("select HOST,SERVER FROM HOSTS WHERE HOST='%s' "%ip)
		values = self.c.fetchone()
		return(values)
		
	# def cfgDevice(self, dev=None):
		# if not self.isconect: 
			# self.connect()
		# self.c.execute("select REGSTART,REGEND,STEP FROM DEVICES WHERE DEVICE='%s'"%dev)
		# return (self.c.fetchone())

	def addHistoric(self,tag,host):
		if not self.isconect: 
			self.connect()
		try:
			self.c.execute('INSERT INTO historic VALUES(?,?,?)',(None,tag,host))
			self.conn.commit()
		except:
			print("no se pudo agregar ese device",(tag,host))
				
		
	def historicTAG(self, host=None):
		if not self.isconect: 
			self.connect()
		self.c.execute("""select tag FROM historic WHERE host='%s'"""%host)
		res=self.c.fetchall()
		results={}
		for tag, in res:
			results[tag]=None
		return results
		
	def readableTAGS(self, tags={}):
		'''
		recibe el diccionario de tags y retorna un diccionario 
			retorno[server][tag]=None
		'''
		if not self.isconect: 
			self.connect()
		result={}
		for tag in tags.keys():
			print('busca %s'%tag)
			self.c.execute("""select device,models  FROM TAGS WHERE tag='%s' and rwa like '%%r%%' """%tag)
			res=self.c.fetchone()
			if res:
				device=res[0]
				if device:
					self.c.execute("select host FROM devices WHERE device='%s' "%device)
					res=self.c.fetchone()
					host=res[0]
				elif res[1]:
					host=res[1]
				else:
					return({})
				self.c.execute("select server FROM hosts WHERE host='%s' "%host)
				res=self.c.fetchone()
				server=res[0]
				if server in result:
					result[server][tag]=None
				else:
					result[server]={tag:None}
			else:
				print("parece que es de nivel 2")
		return(result)
		
	
	def writableTAG(self, tag):
		'''
		recibe un tag y retorna {tag:server}
		'''		
		if not self.isconect: 
			self.connect()
		
		if tag:
			self.c.execute("""select device,models FROM TAGS WHERE tag='%s' and rwa like '%%w%%' """%tag)
			res=self.c.fetchone()
			if res:
				device,models=res
				self.c.execute("select host FROM devices WHERE device='%s' "%device)
				res=self.c.fetchone()
				host=res[0]
				self.c.execute("select server FROM hosts WHERE host='%s' "%host)
				res=self.c.fetchone()
				server=res[0]
				level = 2 if models is not None else 1					
				print({tag:(server,host,level)})
				return({tag:(server,host,level)})
		
		print({tag:None})
		return({tag:None})

				
	def getTagN2(self, host=None):
		if not self.isconect: 
			self.connect()
		self.c.execute("""select tag,rwa FROM TAGS WHERE models='%s'"""%(host))
		res=self.c.fetchall()
		return (res)
		
	def getTagN(self, host=None,level=2):
		if not self.isconect: 
			self.connect()
		self.c.execute("""select tag,rwa FROM TAGS WHERE 
			models='%s'"""%(host,str(level)))
		res=self.c.fetchall()
		print(res)
		return(res)
		
	def getlevel(self, tag=''):
		if not self.isconect: 
			self.connect()
		self.c.execute("""select level FROM TAGS WHERE tag='%s'"""%(tag))
		return (self.c.fetchone()[0])
		

def initialize():
    db=DBconfig()
    db.connect()
    try:
        db.initDB()
    except:
        print('db.initDB() previo !')

    """
    Secuencia para crear la base de datos
    1.  host: maquinas
    2.  drivers: nombre de drivers: modbusRTU, modbusASCII
    3.  dispositivos: sensor_de_corriente, ph_metro, etc: ver en que maquina
            va a correr y en que maquina va a guardar los tags (maquina servidor)
    4.  añadir los tags vinculados con un sensor y registro o con un modelo
    5.  agregar los tags para historicos
    """
    # para standalone uso todo con la ip de la unica maquina que corre
    myip=socket.gethostbyname(socket.gethostname())
    print('aca::::',myip)

    ## host | servidor | descripcion 
    db.addHosts(myip,myip,'standalone')
    db.addHosts('192.168.0.5',myip,'yea')
    db.addHosts('192.168.0.8',myip,'yea2')
    db.addHosts('192.168.0.9',myip,'yea3')
    # db.addHosts('192.168.0.6','192.168.0.2','maquina de desarrollo Rasberry pi')

    db.addDriver('modbusRTU')
    # db.addDriver('modbusASII')
    # db.addDriver('modbusTCP')

    ## device | description | driver | address | regstart | regend | step
    db.addDevices('device_generic','Esclavo modbus generico con actuador','modbusRTU',43,0,10,10)
    db.addDevices('device_generic1','Esclavo modbus generico con actuador','modbusRTU',41,0,10,10)
    db.addDevices('device_generic2','Esclavo modbus generico con actuador','modbusRTU',42,0,10,10)

    db.addDevices('device_generic3','Esclavo modbus generico con actuador','modbusRTU',44,0,10,10)
    db.addDevices('device_generic4','Esclavo modbus generico con actuador','modbusRTU',25,10,40,10)
    # db.addDevices('temphum1','sensore de temperatura y humedad de exor','192.168.0.5','modbusRTU',2,0,10,10)
    # db.addDevices('tenscorr1','sensore de tension y corriente de exor','192.168.0.5','modbusRTU',3,0,10,5)
    # db.addDevices('ph1','sensore de ph de exor','192.168.0.5','modbusRTU',4,0,5,5)
    # db.addDevices('ph2','sensore de ph de exor','192.168.0.5',
    # 	'modbusTCP',32,0,50,20)
    # 	scan | host | device 
    db.addScan('serial1', '192.168.0.5', 'device_generic')
    
    db.addScan('serial1', myip, 'device_generic')
    db.addScan('serial1', myip, 'device_generic1')
    
    db.addScan('serial2', myip, 'device_generic1')
    db.addScan('serial2', myip, 'device_generic2')
    db.addScan('serial2', myip, 'device_generic3')
    db.addScan('serial2', myip, 'device_generic4')


    # tag | description | models | device | register | rwa | type | len_type | 
    db.addTag('Temperatura','Temperatura del esclavo generico',None,"device_generic",6,'r')

    # Pablo aca agrego el tag y en models ponogo el host donde corre el modelo y como device no va nada
    # db.addTag('Temperatura1','variable del esclavo generico',myip,None,0,'r')
    # db.addTag('Temperatura2','variable del esclavo generico',myip,None,0,'r')

    db.addTag('Presion','variable del esclavo generico',None,"device_generic",1,'r',val_min=1,val_max=23)
    db.addTag('Actuador_1','actuador1 del esclavo generico',None,"device_generic",4,'rw')
    #db.addTag('Actuador_2','actuador2 del esclavo generico',None,"device_generic",5,'rw')
    db.addTag('Contador_Infinito_1','contador infinito',None,"device_generic",5,'r')
    # db.addTag('Sapecado_Velocidad_Cinta','temp del motor secundario de tambora (abajo)',
    # 	'192.168.0.2',None,None,'rw')		

    # db.addHistoric('Sapecado_Motor1_temp','192.168.0.2')
    db.addHistoric('Temperatura',myip)
    # db.addHistoric('Temperatura1',myip)
    # db.addHistoric('Temperatura2',myip)
    db.addHistoric('Presion',myip)
    db.addHistoric('Actuador_1',myip)
    return 


def configure(myip):
	dbc=DBconfig()
	hostname,servername=dbc.getHNS(myip)
	print("hostname,servername")
	print(hostname,servername)
	
	scan=dbc.getDevicesTags(hostname)
	print('scan')
	print(scan)
	for dev,vv in scan.items():
		driver,address,regstart,rend,step,tags=vv
		print(dev)
		print("driver,address,regstart,rend,step,tags")
		print(driver,address,regstart,rend,step,tags)
		# regstart,regend,block=dbc.cfgDevice(dev)
		#for tag,sensor,reg,rwa in vv:
			#addDev(dev,sensor,tag,reg,regstart,regend,block,rwa)
			#print(dev,sensor,tag,reg,regstart,regend,block,rwa)
	dbc.close()

if __name__ == '__main__':
	try:
		initialize()
	except:
		print('\nLa DB ya esta inicializada\n')
		# time.sleep(1)
		
	db=DBconfig()
	db.connect()
	
	myip=socket.gethostbyname(socket.gethostname())
	
#	configure(myip)
        
	exit(0)


	''' 
	Con multiplex usa select, entonces todas las peticiones se hacen
	sincronizadamente en funcion de quien consulta primero
	para el caso del comandserver.py conviene usar thread
	'''
	Pyro4.config.COMPRESSION = True
	Pyro4.config.SERVERTYPE = "multiplex" 
	Pyro4.config.SOCK_REUSE = True


	

	ns= Pyro4.naming.locateNS()
	daemon= Pyro4.core.Daemon(host=myip)
	uri=daemon.register(DBconfig())
	ns.register("dbconfig",uri)
	print("object uri=",ns)
	daemon.requestLoop()
	
	
	
