
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <sys/stat.h>

// #include "utils.h"
// #include <iostream>

#include "../include/e_common.h"
#include "../include/utils.h"
#include "../library/logger/include/logger.h"

#define SHMFD "/shm_tags"
#define FILEFD "/tmp/shm_tags"
#define NUMTAG 10
#define NUM_TAG 20

// struct shm_tag
// {
// unsigned updating:1;
// unsigned alarm_n1:1;
// unsigned alarm_n2:1;
// unsigned is_str:1;
// char priority;
// double value;
// double val_min;
// double val_max;
// };
// #define T_LEN (NUMTAG*sizeof(struct shm_tag))
e_shm_tags_client * tmap;

int is_updated(int index)
{
    return ((tmap + index)->streaky.val);
}

double get_value(int index)
{
    return ((tmap + index)->value);
}

int get_alarm_n1(int index)
{
    return ((tmap + index)->alarm_n1);
}

int get_alarm_n2(int index)
{
    return ((tmap + index)->alarm_n2);
}

// #define get_value(index) ((tmap+index)->value)
int init_shm(void)
{
    tmap = (e_shm_tags_client *) map_shm(E_SHM_CLIENT_NAME, "r", E_SHM_TAGS_LEN);

    if ((int) tmap > -1)
    {
        printf("No se puede mapear la memoria\n");

        return 0;
    }

    return 1;
}

int main(int argc, char * argv[])
{
    log_set_file("/tmp/log.log");
    log_debug_level(255);
    log_logger_level(0);
    
    if (!init_shm())
    {
        log_msg(0,"No se puede leer la memoria compartida");

        return 0;
    }

    while (1)
    {
        log_msg(0,"get_value: %f", get_value(2));
        log_msg(0,"is_updated: %i", is_updated(2));
        sleep(1);
    }

    return 0;
}

