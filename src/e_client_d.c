
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "../include/e_common.h"
#include "../include/utils.h"
#include "../library/logger/include/logger.h"


// #include <sys/types.h>
// #include <unistd.h>
// #include <fcntl.h>
// #include <sys/mman.h>
// #include <sys/stat.h>


typedef struct
{
    unsigned writing:1;
    int actual_len;
    pthread_t pthN2;
}e_control_client;

e_shm_tags_client *shmtags;
e_control_client control;


int bind_udp(int broadcast)
{
    int sock;
    int optval = 1;
    struct sockaddr_in addr;
    socklen_t fromlen;

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        perror("Opening socket");

        return sock;
    }
    if (broadcast)
    {
        setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(optval));
    }

    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval));

    
    memset(&addr, 0, sizeof(addr));
    addr.sin_family      = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port        = htons(EXADA_PORT_SUS);
    fromlen              = sizeof(struct sockaddr_in);
    
    if (bind(sock, (struct sockaddr*) &addr, fromlen) < 0)
    {
        perror("binding fail !");
    }
    return sock;
}

void init_tags(e_shm_tags_client *tag)
{
    FILE * fd;
    e_dat_tag dt;
    int len = sizeof(e_dat_tag);

    log_msg(0,"init_tags()");
    
    control.actual_len=0;
    
    fd = fopen("../data/init_tags.dat", "r");
    while (fread(&dt, len, 1, fd) > 0)
    {
        tag->streaky.val = 7;
        tag->alarm_n1 = 0;
        tag->alarm_n2 = 0;
        tag->hb       = 0;
        tag->index     = dt.index;
        tag->value_min = dt.val_min;
        tag->value_max = dt.val_max;
        strcpy(tag->tag, dt.tag);
        log_msg(0,"index: %i %s [%f-%f]", dt.index, dt.tag, dt.val_min, dt.val_max);
        tag++;
        control.actual_len +=1;
    }
    fclose(fd);
}

void update_tag(void *p)
{
    int index = *((int *)p);
    log_msg(0,"update_tag()");
    
    log_msg(0,"index: %i",index);
    sleep(4);
    control.pthN2=0;
}

void loop_check_alive_tags(void) 
{
    e_shm_tags_client *tag;
    int i;
    
    
    log_msg(0,"loop_check_alive_tags()");
    
    while (1)
    {
        tag = shmtags;
        for (i=0; i<control.actual_len; i++) 
        {
            if (tag->hb_client > 0)
            {
                if ( !tag->nivel2 )
                    tag->hb_client -=10;
                else
                {
                    // aca se puede pedir que se actualizen los tags de nivel 2
                    // del correspondiente host
                    
                //    tag->hb_client -=1;
                }
            }
            else
            {
                if (!control.pthN2 )
                    pthread_create (&control.pthN2, NULL, (void *)&update_tag, (void *)&tag->index);
                tag->streaky.flag.n3 = 1;
            }
            tag++;
        }
        // puts(".");
        sleep(1);
    }
} 



int main(void)
{
    int       sock, n, i;
    socklen_t fromlen;
    struct sockaddr_in from;
    e_shm_tags_client *tag;
    rx_client_server rctag;
    pthread_t pth;
    int e;
    

    log_set_file("/tmp/eipcd.log");
    log_debug_level(255);
    log_logger_level(255);
    
    log_info(0,"inicio");
    
    control.pthN2=0;

    // hay que poner cosas de control aca...
    shmtags = (e_shm_tags_client *) map_shm(E_SHM_CLIENT_NAME, "rwt", E_SHM_TAGS_LEN);

    if ((int) shmtags == -1)
    {
        printf("No se puede mapear la memoria\n");
    }

    init_tags(shmtags);

    e = pthread_create (&pth, NULL, (void *)&loop_check_alive_tags, NULL);
    if (e)
    {
        log_error(0,"main(): error al crear thread");
    }
    
    
    sock = bind_udp(0);
    fromlen = sizeof(struct sockaddr_in);

    while (1)
    {
        n = recvfrom(sock, &rctag.length, sizeof(rx_client_server), 0, (struct sockaddr*) &from, &fromlen);

        if (n < 0)
        {
            log_msg(0,"recvfrom() fail !");
        }

        log_msg(0,"len:%i from: %s::%i ", n, inet_ntoa(from.sin_addr), ntohs(from.sin_port));

        for (i = 0; i < rctag.length; i++)
        {
            tag = shmtags + rctag.tag[i].index;
            memcpy(tag, &rctag.tag[i], sizeof(e_shm_tags_server));
            log_msg(0, "tag:%s i:%i val:%f hb:%i", tag->tag, tag->index, tag->value, tag->hb );
            
//
//            memcpy(tag, &rctag.tag[i], sizeof(e_shm_tags_server));
//            tag->hb_client = 120;
        }
    }
}
