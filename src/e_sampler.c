
#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <string.h>
#include <unistd.h>
#include <strings.h>

#include "../include/static_list.h"
#include "../include/utils.h"


typedef short e_scan_t; // registros de 16 bits
typedef struct
{
    char name[96];
    int  addres;
    int  start;
    int  end;
    int  len_block;
    e_scan_t data[1];
}SCAN_BLOCK;

typedef struct
{
    timems_t now;
    enum  {busy, ready, writing }state;
}SCAN_CTR;


typedef struct
{
    char scan_name_attach[96];
    SCAN_CTR *scan_ptr_ctr;
    void *next;
}s_SCAN;

typedef enum{type_int16, type_int32, typte_float,type_string }e_TAG_TYPE;
typedef struct
{
    char tag_name[96];
    char scan_name_attach[96];
    s_SCAN *scan;
    int id;
    int _addres;
    int _register;
    e_TAG_TYPE type;
    int len_type;
    void *ptr;
    void *next;
}s_TAG;


static int num_tags=0;
s_SCAN *scans=NULL;
s_SCAN *tags=NULL;


s_SCAN *add_scan(char *name)
{
    s_SCAN *prevs, *ps=scans;
    if( !ps )
    {
        scans=malloc(sizeof(s_SCAN));
        ps=scans;
    }
    else
    {
        do{
            if( strcmp(ps->scan_name_attach,name)==0)
                return ps;
            prevs=ps;
            ps=(s_SCAN *)prevs->next;
        }while( ps );
        
        prevs->next=(void *)malloc(sizeof(s_SCAN));
        ps=(s_SCAN *)prevs->next;
    }
    ps->next=NULL;
    ps->scan_ptr_ctr=NULL;
    strcpy(ps->scan_name_attach,name);
    ps->scan_ptr_ctr = (SCAN_CTR *)map_shm(ps->scan_name_attach, "r", 0);  
    return ps;
}


void *ptr_register(LIST_t *lshm, int addres, int reg)
{
    SCAN_BLOCK *pb;
    LIST_BLOCK *lb;

    lb = list_first_block(lshm);
    while(lb)
    {
        pb = (SCAN_BLOCK *)lb->data;

        if( addres==pb->addres && reg>=pb->start && reg<=pb->end )
            break;

        lb = list_next_block(lshm, lb);
    }
    if(lb)
    {
        printf("[%s] addres=%05i  start=%05i  end=%05i len=%05i\n", 
            pb->name, pb->addres, pb->start, pb->end, pb->len_block);
        return (void*) &(pb->data[ reg - pb->start ] );       
    }
    return (void*)NULL;
}


s_TAG *add_tag(int id, char *tag_name, char *scan_name, int addres, int reg, char *type)
{
    s_TAG *prevt, *pt=tags;
    if( !pt )
    {
        tags=malloc(sizeof(s_TAG));
        pt=tags;
    }
    else
    {
        do{
            if( strcmp(pt->tag_name,tag_name)==0)
                return pt;
            prevt=pt;
            pt=(s_TAG *)prevt->next;
        }while( pt );
        
        prevt->next=(void *)malloc(sizeof(s_TAG));
        pt=(s_SCAN *)prevt->next;
    }
    pt->next=NULL;
    pt->scan=NULL;
    strcpy(pt->tag_name,tag_name);
    strcpy(pt->scan_name_attach,scan_name);
    
    if(strncasecmp(type,"STRING",6) == 0)
    {
        pt->type=type_string;pt->len_type=atoi(type[6])/2;
    }
    else if(strncasecmp(type,"INT16",5) == 0)
    {
        pt->type=type_int16;pt->len_type=1;
    }
    else if(strncasecmp(type,"INT32",5) == 0)
    {
        pt->type=type_int32;pt->len_type=2;
    }
    
    pt->_addres=addres;
    pt->_register=reg;
    pt->scan = add_scan(pt->scan_name_attach); 
    
    if( (int)pt->scan > 0)
    {
        pt->ptr = ptr_register((LIST_t *)(pt->scan+1), addres, reg);
    }
    
    
    return pt;
}



// device | description | driver | address | regstart | regend | step
typedef struct
{
    char device[128];
    char driver[128];
    int  addres;
//    int  reg_start;
//    int  reg_end;
//    int  reg_step;
}e_scan_modbus_rtu;

typedef struct{
    char scan_name[128];
    int  num_block;
    e_scan_modbus_rtu block[256];
    void *next; 
}e_scan;

e_scan *SCANS=NULL;

#define TEST(A) {printf("%-40s",#A);printf("%3i\n",A);}
#define TESTS(A,fmt) {printf("%-40s",#A);printf(fmt,A);printf("\n");}

void print_scan(e_scan *scan)
{
    int i;
    TESTS(scan->scan_name,"%s");
    TESTS(scan->num_block,"%i");
    for(i=0; i<scan->num_block;i++)
    {
        TESTS(scan->block[i].device,"%s");
        TESTS(scan->block[i].driver,"%s");
        TEST(scan->block[i].addres);
//        TEST(scan->block[i].reg_start);
//        TEST(scan->block[i].reg_end);
//        TEST(scan->block[i].reg_step);
    }
}

void print_scans(void)
{
    e_scan *escan=SCANS;
    while(escan)
    {
        puts(" ");
        print_scan(escan);
        escan = (e_scan *)escan->next;
    }
}

void free_scans(void)
{
    e_scan *escan=SCANS, *next;
    
    while(escan)
    {
        next = (e_scan *)escan->next;
        printf("free %s\n",escan->scan_name);
        usleep(100);
        free(escan);
        escan = next;
    }
}

void scan_append(char *scan, char *device, char *driver, int addres)
{
    e_scan *pescan, *escan=SCANS;
    e_scan_modbus_rtu *block;
    if( !escan )
    {
        printf("-----primer scan a guardar\n");
        escan = SCANS = (e_scan *)malloc(sizeof(e_scan));
        strcpy(escan->scan_name, scan);
        escan->num_block=0;
    }
    else
    {
        do{
            if( strcmp(escan->scan_name, scan)==0 )
            {
                printf("-----añade block en scan\n");
                break;
            }
            pescan = escan;
            escan = (e_scan *)escan->next;
        }while( escan );
    } 
    if( !escan )
    {
        printf("-----agrrega un nuevo scan\n");
        pescan->next=(void *)malloc(sizeof(e_scan));
        escan=pescan->next;
        strcpy(escan->scan_name, scan);
        escan->num_block=0; 
        escan->next=NULL;
    }
    block = &escan->block[escan->num_block];
    strcpy(block->device,device);
    strcpy(block->driver,driver);
    block->addres=addres;
//    block->reg_start=reg_start;
//    block->reg_end=reg_end;
//    block->reg_step=reg_step;
    escan->num_block++;
//    print_scan(escan);
    return;
}

// s.scan,s.device,d.driver,d.address
static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i;
    for (i = 0; i < argc; i++)
    {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    
    scan_append(argv[0], argv[1], argv[2], atoi(argv[3]));
    printf(".....\n");
    return 0;
}

#define assert_sql(r) {if(r!=SQLITE_OK){fprintf(stderr,"Failed database %s\n\r",sqlite3_errstr(r));sqlite3_close(db);return r;}}

int main(int argc, char *argv[])
{
    sqlite3 *db;
    char *zErrMsg = 0;
    int  rc;
    char *sql;
    int i;

    rc = sqlite3_open("../cfg/DBConfig.db", &db);
    if (rc)
    {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        exit(0);
    }

    fprintf(stdout, "Opened database successfully\n");
    
    // servidor al cual publicar los datos
    sql = "select server from hosts where host = '192.168.0.5'";
    
    sql = "select s.scan,s.device,d.driver,d.address from scans as s join devices as d where d.device=s.device and s.host='192.168.0.19';";
    puts(sql);
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, ":( SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        fprintf(stdout, ":) Operation done successfully\n");
    }

    sqlite3_close(db);
    
    print_scans();
    
    log_set_file("/tmp/log.log");
    log_debug_level(255);
    log_logger_level(0);
    
    
    /*
     * capaz cambio por esto:
     * db.addTag('Actuador_1','actuador1 del esclavo generico',None,"BalanzaXX",addres,register,'rw')
     * 
     * 
     * de la tabla scan saco los scan de cada host y luego voy creando para cada scan todos los host
     * 
     */
    
    
    char scan[96];
    int n;
    char *dbname = "../cfg/DBConfig.db";
    char *sqltag;
    //char *sql = "select id,tag from tags";
    sql = "select count(*),scan from scans where host='192.168.0.19' group by scan;";
    sqltag = "select d.address,t.tag,t.register from scans as s, devices as d, tags as t"\
                "where s.scan='serial2' and d.device=s.device and d.device=t.device "\
                "and t.rwa like '%r%';";
             
    sqlite3_stmt *stmt;
    sqlite3_stmt *stmttag;
    /* open the database */
    int result=sqlite3_open(dbname,&db);
    assert_sql(result);
    
    printf("Opened db %s OK\n\r",dbname);

    /* prepare the sql, leave stmt ready for loop */
    result = sqlite3_prepare (db, sql, strlen(sql)+1, &stmt, NULL);
    assert_sql(result);
    
    printf("SQL prepared ok\n\r");

    result = sqlite3_step(stmt);
    while (result == SQLITE_ROW) {
        n = sqlite3_column_int(stmt,0);
        strcpy(scan, (char *)sqlite3_column_text(stmt,1));
        printf("scan:'%s' %i devices \n\r",scan,n);
        
        
//        result = sqlite3_prepare (db, sqltag, strlen(sqltag)+1, &stmttag, NULL);
//        assert_sql(result);
//
//        printf("SQL prepared ok\n\r");
//
//        result = sqlite3_step(stmttag);
//        while (result == SQLITE_ROW) {
//            n = sqlite3_column_int(stmttag,0);
//            strcpy(scan, (char *)sqlite3_column_text(stmttag,1));
//            printf("scan:'%s' %i devices \n\r",scan,n);
//            result = sqlite3_step(stmttag);
//        }
        
        result = sqlite3_step(stmt);
    }
    sqlite3_close(db);
         
    
    SCAN_CTR *ctr = (SCAN_CTR *)map_shm("Balanza_XX", "r", 0);  
    LIST_t *lshm = (LIST_t *)(ctr+1);
    SCAN_BLOCK *pb;
    LIST_BLOCK *lb;
    
    
    
    free_scans();
    
    return 0;
}

