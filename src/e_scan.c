/* e_scan para modbus_rtu
 * 
 * yea 
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/inicfg.h"
#include "../include/eipcc.h"
#include "../include/static_list.h"


#define FILE_CFG "../cfg/scan_1.cfg"


#define TIME_SEND       (500)   // milisegundos

typedef struct
{
    char scan_name[96];
    char driver[96];
    char device[96];
    char ip[96];
    char mode[32];
    int  port;
    int  timeout;
}Config;


#define MAX_NUM_BLOCK 256
typedef short e_scan_t; // registros de 16 bits
typedef char e_alarm_t; // alarmas x 8  
typedef struct
{
    char name[96];
    int  addres;
    int  reg_start;
    int  reg_end;
    int  reg_length;
    int  alarm_start;
    int  alarm_end;
    int  alarm_length;
    timems_t update_time;
    e_scan_t data[1];
}SCAN_BLOCK;

typedef struct
{
    timems_t now;
    enum  {busy, ready, writing }state;
}SCAN_CTR;



int b_index=0;
char prev_section[200]={"\0"};
SCAN_BLOCK block[MAX_NUM_BLOCK];
Config config;
eipc_t *eipc;

static int handler(void* user, const char* section, const char* line, const char* val)
{
    #define CMPN(n) (strcmp(line, n) == 0)    

    if ( strcmp("config", section)==0 )
    {
        if CMPN("scan_name"){
            strcpy(config.scan_name,val);
        }else if CMPN("driver"){
            strcpy(config.driver,val);
        }else if CMPN("device"){
            strcpy(config.device,val);
        }else if CMPN("ip"){
            strcpy(config.ip,val);
        }else if CMPN("port"){
            sscanf(val,"%s %i",config.mode, &config.port);
        }else if CMPN("timeout"){
            config.timeout = atoi(val);
        }else{
            return 0;
        }
    }
    else{ 
        if( prev_section[0] == '\0' ){
            strcpy(prev_section, section);
            strcpy(block[b_index].name, section);
        }
        if ( strcmp(prev_section, section) )
        {
            b_index += 1;
            strcpy(prev_section, section);
            strcpy(block[b_index].name, section);
        }

        if CMPN("addres"){
            block[b_index].addres = atoi(val);
        }else if CMPN("reg_start"){
            block[b_index].reg_start = atoi(val);
        }else if CMPN("reg_end"){
            block[b_index].reg_end = atoi(val);
            if (block[b_index].reg_end < 0 )
                block[b_index].reg_start = block[b_index].reg_end = -1;
        }else if CMPN("alarm_start"){
            block[b_index].alarm_start = atoi(val);
        }else if CMPN("alarm_end"){
            block[b_index].alarm_end = atoi(val);
            if (block[b_index].alarm_end < 0 )
                block[b_index].alarm_start = block[b_index].alarm_end = -1;
        }else {
            return 0;
        }
    }
    return 1;
}


void recev_comand(eipc_mq *msgrv) 
{
    
    log_msg(0,"\nfuncion_read() type:%i\nmsg:<<%s>>\n espero...\n",msgrv->type,msgrv->msg);
    sleep(4);
    return;
}

int mb_rtu_init(char *device);
int mb_rtu_scan(int fd, int addres, int reg_start, int reg_len, e_scan_t data[], int timedout);
int mb_rtu_write(int fd, int addres, int reg_start, int reg_count, e_scan_t data[], int timedout);
int mb_rtu_close(int fd);

#define TEST_(a) printf("%-30s: %lu\n",#a,(unsigned long)a)

int main(int argc, char* argv[])
{
    int i;
    int len;

    if (ini_parse(FILE_CFG, handler, NULL) < 0) {
        printf("Can't load '"FILE_CFG"'\n");
        return 1;
    }
    
    
    /* 
     * con estos datos se procede: 
     *          1. chequea el driver (debe existir ModubsRTU)
     *          2. atacha con el nombre de sensor (el eipc solo permite un name atach)
     *          3. abre el puerto serie
     *          4. si pudo atachar crea un shm
     *          4. abre 
     */
    
    log_set_file("/tmp/log.log");
    log_debug_level(255);
    log_logger_level(0);
    
    
    if (strcmp("ModbusRTU",config.driver))
    {
        log_error("error driver [%s] desconocido",config.driver);
        return -1;
    }
    
    printf("scan_name=%s::\n\tdriver=%s   device=%s   ip=%s   "\
            "mode:%s   port:%i   timeout:%i\n",
            config.scan_name, config.driver, config.device, config.ip,
            config.mode, config.port, config.timeout);
    
    eipc = eipc_attach(config.scan_name);
    if((int)eipc == -1)
    {
        log_error("attach() error !");
        return -1;
    }
    
    // detecta el tamanio necesario para la SHM
    b_index++;
    len = sizeof(SCAN_CTR) + sizeof(SCAN_BLOCK)*b_index 
            + sizeof(LIST_CTR) + sizeof(LIST_BLOCK)*b_index ;
    for (i=0; i<b_index; i++)
    {
        printf(" - block=%s  addres=%05i  start=%05i  end=%05i\n",block[i].name, block[i].addres, block[i].reg_start, block[i].reg_end);
        block[i].reg_length= (block[i].reg_end - block[i].reg_start);
        block[i].alarm_length= (block[i].alarm_end - block[i].alarm_start);
        len += block[i].reg_length*sizeof(e_scan_t);
        len += block[i].alarm_length*sizeof(e_alarm_t);
    }  
    
    // crea la shm y copia los datos necesarios para luego hacer el scan
    log_msg("len shm \"%s\": %i ",config.scan_name, len);
    
    SCAN_CTR *shmctr = (SCAN_CTR *)map_shm(config.scan_name, "rwt", len);  
    LIST_t *lshm = (LIST_t *)(shmctr+1);
    
    list_init(lshm, len);
    SCAN_BLOCK *pb;
    LIST_BLOCK *lb;
    TEST_(lshm);
    shmctr->state=busy;
    for (i=0; i<b_index; i++)
    {
        lb = list_add(lshm, block[i].reg_length*sizeof(e_scan_t) + sizeof(SCAN_BLOCK) , i);
//        pb = (SCAN_BLOCK *)lb->data;
//        TEST_(lb);
//        TEST_(pb);
//        TEST_(lb->nbytes);
//        TEST_(lshm->ctr.block_end);
//        
//        strcpy(pb->name, block[i].name);
//        pb->addres = block[i].addres;
//        pb->start = block[i].start;
//        pb->end = block[i].end;
//        pb->len_block = block[i].len_block ;
        memcpy(lb->data,block[i].name,7*4+96);
    }
    
    
     // hilo para recibir comandos de escritura
    pth_recv *epth = eipc_pth_msg_recv( eipc, recev_comand, 0);
    
//    // avisa al sampler que es
//    int res = eipc_send_event(eipc, "", "e_sampler", 9, 200 );
//    if(res!=1)
//        log_warning(0,"e_sampler no esta corriendo");
//    
//    eipc_send_msg(eipc, "", "e_sampler", 5, config.scan_name, strlen(config.scan_name), 0, 0);
            
    // escanea:   
    int mb = mb_rtu_init(config.device);
    timems_t t_end, t_now;
    int res;
    int t_interval = TIME_SEND;
    t_end = timems() + t_interval;
    fprintf(stderr,"scaning...\n");    
    
    while(1)
    {
        printf("\n");
        print_progress_bar(0);
        lb = list_first_block(lshm);
        i=0;
        while(lb)
        {
            pb = (SCAN_BLOCK *)lb->data;
            eipc_lock(eipc);
            shmctr->state=busy;
//            printf("[%s] addres=%05i  start=%05i  end=%05i len=%05i\n", 
//                    pb->name, pb->addres, pb->start, pb->end, pb->len_block);
        
            res = mb_rtu_scan( mb, pb->addres, pb->reg_start, pb->reg_length, pb->data, config.timeout);
            
            // actualiza la fecha solo si el scan dio correcto
            if( res )
                pb->update_time=timems();
                    
            eipc_unlock(eipc);
            shmctr->state=ready;
            print_progress_bar((100*(i+1))/b_index);
            i++;
            msleep(20);
            lb = list_next_block(lshm, lb);
        }
        t_now = timems();
        if( t_now < t_end )
            msleep( t_end-t_now );
        
        t_end = timems() + t_interval;        
        
    }
    
    pth_recv_delete(epth);
    
    eipc_deattach(eipc);
    return 0;
    
}


int mb_rtu_init(char *device)
{
    usleep(500000);
    return 0;
}

int mb_rtu_scan(int fd, int addres, int reg_start, int reg_len, e_scan_t data[], int timedout)
{
    int i;
    for(i=0; i<reg_len;i++)
    {
        data[i] = (int)random()%1000;
//        printf("%i\n",*(data+i));
    }
    msleep(10);
    return 1;
}
