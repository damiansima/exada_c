/*
 *  Creates a datagram server.  The port
 *  number is passed as an argument.  This
 *  server runs forever
 */

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <arpa/inet.h>  /* for sockaddr_in */
#include  <signal.h>

#include <pthread.h>

#include "../include/e_common.h"
#include "../include/utils.h"
#include "../library/logger/include/logger.h"

e_shm_server *SHM;
int * ptags;
pthread_mutex_t m_send_rcev=PTHREAD_MUTEX_INITIALIZER; 


void error(const char * msg)
{
    perror(msg);

    // exit(0);
}

void to_exit(int sig)
{
//    free(ptags);
    log_msg(0, "signal:%i exit", sig);
    exit(0);
}

int create_sock_udp_broadcast(int broadcast)
{
    int sock;
    int optval = 1;

    sock = socket(AF_INET, SOCK_DGRAM, 0);

    if (sock < 0)
    {
        perror("Opening socket");

        return sock;
    }

    if (broadcast)
    {
        setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(optval));
    }

    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval));

    return sock;
}

void init_tag(e_shm_tags_server *tag, int index)
{
    FILE *    fd;
    e_dat_tag dt;
    int       len = sizeof(e_dat_tag);

    log_msg(0, "init_tag()");

    fd = fopen("../data/init_tags.dat", "r");

    while (fread(&dt, len, 1, fd) > 0)
    {
        if (dt.index == index)
        {
            tag->index     = index;
            tag->alarm_n2  = 0;
            tag->value_min = dt.val_min;
            tag->value_max = dt.val_max;

            log_msg(0, "index: %i %s [%f-%f]", dt.index, dt.tag, dt.val_min, dt.val_max);
        }
    }

    fclose(fd);
}


void send_SHM_server(void)
{
    int sock_pub, length, i;
    struct sockaddr_in to;
    e_shm_tags_server *tag;

    log_msg(0,"send_SHM_server()");

    signal(SIGINT, to_exit);
    signal(SIGQUIT, to_exit);
    signal(SIGTERM, to_exit);    // esta es la del kill es -15

    // signal(SIGKILL, to_exit); // esta no se puede capturar -9
    // signal(SIGUSR1, to_exit);
    // signal(SIGUSR2, to_exit);

    sock_pub = create_sock_udp_broadcast(1);
    length   = sizeof(struct sockaddr_in);

    memset(&to, 0, length);

    to.sin_family = AF_INET;

    // to.sin_addr.s_addr=INADDR_ANY;
    to.sin_addr.s_addr = inet_addr("255.255.255.255");    // envia en broadcast
    to.sin_port        = htons(EXADA_PORT_PUB);

    while (1)
    {
        pthread_mutex_lock(&m_send_rcev);
        SHM->control.sending = 1;

        // controla los campos de los tags a enviar
        for (i = 0; i < SHM->control.actual_len; i++)
        {
            tag = SHM->tags + i;

            if ((tag->value > tag->value_max) || (tag->value < tag->value_min))
            {
                tag->alarm_n2 = 1;
            }

            // printf("hb:%i\n",(int)tag->hb);
            if (tag->hb == 1)
            {
                tag->hb = 0;
                tag->streaky.flag.n2 = 1;
            }
            else if (tag->hb > 1)
            {
                tag->hb--;
            }
        }

        log_msg(0, "sendto() len:%i", SHM->control.actual_len);

        sendto(sock_pub, &SHM->control.actual_len,
                   sizeof(SHM->control.actual_len) + SHM->control.actual_len * sizeof(e_shm_tags_server), 0,
                   (struct sockaddr*) &to, length);

        SHM->control.sending = 0;
        pthread_mutex_unlock(&m_send_rcev);
        msleep(TIME_SEND);
    }
}

void recv_SHM_server(void)
{
    int sock_recv, length, n;
    socklen_t fromlen;
    struct sockaddr_in server;
    struct sockaddr_in from;
    e_shm_tags_scan *atag;
    e_shm_tags_server *tag;
    int i;
    e_shm_scan rcv_tag;

    log_msg(0,"recv_SHM_server()");

    signal(SIGINT, to_exit);
    signal(SIGQUIT, to_exit);
    signal(SIGTERM, to_exit);

    ptags = (int *) map_shm(E_SHM_SERVER_I_NAME, "rwt", E_MAX_NUM_TAG * sizeof(int));
//    ptags = (int *) malloc(E_MAX_NUM_TAG * sizeof(int));

    if (ptags == (int *) NULL)
    {
        log_msg(0, "recv_SHM_server() Alocate memory fail !");
        SHM->control.recv_exit = 1;
//        free(ptags);
        exit(0);
    }

//    for (i = 0; i < E_MAX_NUM_TAG; i++)
//    {
//        ptags[i] = -1;
//    }

    sock_recv = create_sock_udp_broadcast(0);
    fromlen   = sizeof(struct sockaddr_in);
    length    = sizeof(struct sockaddr_in);

    memset(&server, 0, length);

    server.sin_family      = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port        = htons(EXADA_PORT_ASI_RCV);

    if (bind(sock_recv, (struct sockaddr*) &server, length) < 0)
    {
        log_msg(0, "recv_SHM_server() bind fail !");
        SHM->control.recv_exit = 1;
//        free(ptags);
        exit(0);
    }

    while (1)
    {
        n = recvfrom(sock_recv, &rcv_tag.length, sizeof(rcv_tag), 0, (struct sockaddr*) &from, &fromlen);

        if (n < 0)
        {
            log_error(0, "recv_SHM_server recvfrom() fail !");
        }

        pthread_mutex_lock(&m_send_rcev);
        SHM->control.writing = 1;
        atag = (e_shm_tags_scan*) (&rcv_tag.tag);

        for (i = 0; i < rcv_tag.length; i++)
        {
            log_msg(0, "value[%i]:%f index:%i", i, atag->value, atag->index);
            // chequeo el puntero para ese indice
            if (ptags[atag->index] == -1)
            {
                ptags[atag->index] = SHM->control.actual_len;
                SHM->control.actual_len += 1;
                // carga valores iniciales
                init_tag(SHM->tags + ptags[atag->index], atag->index);
            }
            log_msg(0, "ptags[%i]:%i", atag->index, ptags[atag->index]);

            // init_tag(SHM->tags + ptags[atag->index],atag->index);
            tag = SHM->tags + ptags[atag->index];
            
            tag->value = atag->value;
            tag->streaky.val = atag->streaky;
            tag->alarm_n1 = atag->alarm_n1;
            tag->hb = (char)(TIME_STREAKY / TIME_SEND) + 1;

            atag++;
        }

        SHM->control.writing = 0;
        pthread_mutex_unlock(&m_send_rcev);

        log_msg(0, "recv_SHM_server len:%i from:%s::%i", n, inet_ntoa(from.sin_addr), ntohs(from.sin_port));
        
    }
}

int main(int argc, char *argv[])
{
    pthread_t pth_recv;
    pthread_t pth_send;
    int e=0;
    
    log_set_file("/tmp/log.log");
    log_debug_level(255);
    log_logger_level(0);
    
    // delete_shm(SHM_SERVER);
    SHM = (e_shm_server *) map_shm(E_SHM_SERVER_NAME, "rwt", sizeof(e_shm_server));
    SHM->control.send_exit  = 0;
    SHM->control.recv_exit  = 0;
//    SHM->control.actual_len = 0;
    SHM->control.writing    = 0;
    SHM->control.sending    = 0;

    if ((int) SHM > -1)
    {
        log_msg(0, "map_shm() no se puede mapear la memoria");
        exit(0);
    }

//    e += pthread_create (&pth_recv, NULL, (void *)&recv_SHM_server, NULL);
    e += pthread_create (&pth_send, NULL, (void *)&send_SHM_server, NULL);
    if(e)
    {
//        pthread_cancel(pth_recv);
        pthread_cancel(pth_send);
        log_msg(0,"main(): error al crear thread");
    }
    
//    send_SHM_server();
    recv_SHM_server();
    
    return 0;
}


//~ Formatted by Jindent --- http://www.jindent.com
