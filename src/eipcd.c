//#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>   
#include <time.h>

#include <pthread.h>

#include "../include/ipc_types.h"
#include "../include/socketlib.h"
#include "../include/params.h"

#include <syslog.h>

#include <sys/stat.h>
#include <errno.h>
#include "../library/logger/include/logger.h"


//#define FILE_LOG "/tmp/eipcd.log"

/// lanza exepcion:
/// quiere decir que responde la query informando del evento (si pide ack).


#define LOGD(a, fmt, args...) log_debug("[%u]: "fmt,(unsigned int)a,##args)
//#define  LOGD(a,b) log_debug(#b"[%i]",a)


eipc_shm_control *SHM_CTR;
eipc_shm *SHM;
param_struct params;  
pthread_mutex_t m_mqrcv=PTHREAD_MUTEX_INITIALIZER; 
int max_num_conn=10;
int num_conn=0;
   
  

int tcp_send(int sock,char *buf,int len,int o)
{
   
    return sock;
}
int tcp_rcev(int sock,char *buf,int max,int o)
{
    return sock;
}

int chek_process_alive(int pid)
{
    struct stat sts;
    char prcs[100];
    sprintf( prcs, "/proc/%i", pid);
    if (stat( prcs, &sts) == -1 && errno == ENOENT)
        return 0;
    else
        return pid;
}

int wait_ack_process(eipc_shm *process, int timesleep)
{
    int i;
    timesleep = timesleep>10 ? timesleep/10 : 1;
    for( i=0 ; i<= timesleep ; i++ )
    {
        if(process->query_ack.flag.ack_process)
            return 1;
        usleep(10000);
    }
    return 0;
}

void mq_send(eipc_shm *process, eipc_mq *msg)
{
    // si tira error en la cola saca los mensajes que tienen menos tiempo
    int len = MQ_FIXE_LENGTH + msg->length;
    if(msgsnd(process->mqid, (void *)msg, len, MQFLAG) == -1)
    {
        perror("mq_send()::msgsnd() failed !");
    }
    return ;
}

int mq_recv(int mqid, eipc_mq *msg)
{
    if(msgrcv( mqid, msg, BUFSIZ, 0, 0 )  == -1)
    {
        fprintf(stderr, "failedto receive: \n");
        return -1;
    }
    return 0;
}


eipc_shm *get_process(char *process)
{
    int i=0;
    eipc_shm *shm = &SHM[0];    
    for(i=0; i<SHM_CTR->max_process; i++)
    {
        if(!strcmp(shm->process,process))
            return shm;
        shm++;
    }
    // si no encuentra registrado este proceso agrega donde pueda 
    shm = &SHM[0];    
    for(i=0; i<SHM_CTR->max_process; i++)
    {
        if( shm->pid == 0 )
            return shm;
        shm++;
    }
    perror("get_process() no puede añadir mas procesos");
    return (eipc_shm *)-1;
}



// hilo para chequera continuamente los procesos en ejecucion
void loop_check_pid_shm()
{
    int i;
    eipc_shm *shm;
    while(1)
    {
        shm = &SHM[0];
        for(i=0;i<SHM_CTR->max_process;i++)
        {
            if( shm->pid > 0 && chek_process_alive(shm->pid) == 0 )
            {
                LOGD(0,"shm->process [%s] killed ! ",shm->process);
                shm->pid=0;
            }
            shm++;
        }
        sleep(1);
    }
}

void send_local_mq(void *p)
{
    eipc_mq msg;
    eipc_shm *process_to; 
    eipc_shm *process_from; 
    timems_t timeout;
    char command[1000];
    
    LOGD(pthread_self(),"send_local_mq()");
         
    memcpy( &msg, p, sizeof(eipc_mq) );    
    pthread_mutex_unlock(&m_mqrcv);
    
    process_to = get_process(msg.to_p);
    process_from = get_process(msg.from_p);
    
    LOGD(pthread_self(),"------LOCK() from:%s::%s to:%s:%s tid:%u",
            msg.from_h,msg.from_p,msg.to_h,msg.to_p,
            (uint) pthread_self());
    pthread_mutex_lock(&process_to->mutex);
    LOGD(pthread_self(),"  + + + ");
    
        
    process_to->query_ack.flag.signal = 0;
    process_to->query_ack.flag.event = 0;
    process_to->query_ack.flag.msg = 0;

    if ( chek_process_alive(process_to->pid) )
    {
        LOGD(pthread_self(),"proceso vivo");
        process_from->reply_ack.flag.alive=1;
        process_from->reply_ack.flag.ack_host=1;
        if(msg.control & CTR_SIGNAL)
        {
            
            sprintf( command, "kill -%d %d &", msg.info, process_to->pid);
            LOGD(pthread_self(),"signal: %s", command);
            system(command);
            process_to->query_ack.flag.signal = 1;
        }
        else if( (msg.control & CTR_EVENT) || (msg.control &  CTR_MSG))
        {
            LOGD(pthread_self(),"event || msg");
            // guarda el evento en shm
            process_to->info = msg.info;
            if( msg.control & CTR_EVENT ) 
                process_to->query_ack.flag.event = 1;
            else
                process_to->query_ack.flag.msg = 1;
            
            process_to->query_ack.flag.ack_process=0;
            mq_send(process_to, &msg);
            process_to->query_ack.flag.ready=1;
            if( msg.control & ACK_PROCESS )
            {
                timeout = msg.time - timems(); 
                process_from->reply_ack.flag.ack_process = 0;
                LOGD(pthread_self(),"call wait_ack_process() timeout:%lld", timeout );
                if ( timeout > 0 && timeout < 120000 ) // timeout < 2 min
                {
                    process_from->reply_ack.flag.ack_process = wait_ack_process( process_to, timeout);
                }
            }
        }
        else 
        {
            LOGD(pthread_self(),"send ni idea");
        }
    }
    else
    {
        LOGD(pthread_self(),"proceso muerto");
        process_from->reply_ack.flag.alive = 0;
        // -chequeo si hay colas para eliminar
        if( msg.time > timems() && process_to->mqid > 0 )
        {
            LOGD(pthread_self(),"envia el msg");
            mq_send(process_to, &msg);
            process_from->reply_ack.flag.timeout = 1;
        }
    }
    pthread_mutex_unlock(&process_to->mutex);
    LOGD(pthread_self(),"------UNLOCK() from:%s::%s to:%s:%s tid:%u",
            msg.from_h,msg.from_p,msg.to_h,msg.to_p,
            (uint)pthread_self());
    process_from->reply_ack.flag.ready = 1;
    pthread_detach(pthread_self());
}



/*      void send_tcp(void *buf)
 * 
 * - recive el msg de la quueue para enviar por tcp
 * - lanza un hilo:
 *      - envia el mensaje
 *      - queda esperando la respuesta o timedout
 *      - responde en la queue y shm correspondiente
 *        tiene los datos en el puntero de la shm
 *
 */
void send_tcp(void *buf)
{
    int res;
    int sock;
    int timemsg;
    eipc_mq msg;
    eipc_tcp_reply msg_reply;
    eipc_shm *process_from;
    
    int length = sizeof(long) + MQ_FIXE_LENGTH + ((eipc_mq *)buf)->length;
    
    memcpy( &msg, buf, length);    
    pthread_mutex_unlock(&m_mqrcv);
    

    LOGD(0,"send_tcp()");
    
    process_from = get_process(msg.from_p);

    process_from->reply_ack.value = 0;
    process_from->reply_ack.flag.ready = 0;
    timemsg = msg.time - timems();
    if(timemsg>0 || msg.time == 0 )
    {
        sock = connect_tcp(msg.to_h,params.PORT_TCP);
        LOGD(sock, "connect_tcp(%s,%i)",msg.to_h,params.PORT_TCP);
        
        res = sendall( sock, (char *)&msg, length);
        if(res==0)
        {
            process_from->reply_ack.flag.send_tcp = 1; 
        }
        // espera la respuesta si es necesario
        if(  msg.control & CTR_ACK )
        {
            if(timemsg>0 && res == 0 )
            {
                res = readall( sock, (char *)&msg_reply, sizeof(msg_reply), timemsg +100 );
                // setea en shm el ack
                if(res>0)
                {
                    // setea en ok
                    process_from->reply_ack.value = msg_reply.reply.value;
                    process_from->reply_ack.flag.ack_host = 1;
                }
                else
                {
                    process_from->reply_ack.flag.ack_host = 0;
                    process_from->reply_ack.flag.ack_process = 0;
                    process_from->reply_ack.flag.timeout = 1;
                }
            }
            else
            {
                // respondo en shm que tuvo timedout
                process_from->reply_ack.flag.timeout = 0;
            }
        }
        close(sock);
    }
    process_from->reply_ack.flag.ready = 1;
    pthread_detach(pthread_self());
}



/*
 * - msgrcv(mq,buf,MAX,0)
 * - analiza si destino es local o tcp
 * - si local:
 *      - si el proces esta corriendo ejecuta-encola
 *      - si proceso no corre:
 *          - chequeo si hay colas con tiempo para descartar: elimina
 *          - si tiene time alive:
 *              - encola. controla si la queue esta llena:
 *                  - lanza exepcion
 *
 * - si tcp:
 *      - encola para ::send_tcp()
 *      - o lanza un hilo ::send_tcp(msg,len,p_msg)
 *
 *  WARNING: todos los encolas y chequeos van con un shm para count y mayor time alive.
 */
void loop_mq_recv(void)
{
    LOGD(pthread_self(),"loop_mq_recv() start !");
    int mqid=0;
    eipc_mq msg;
    int err;
    
    mqid = mq_open(params.MQ_NAME,"rw");
    SHM_CTR->mq_id = mqid;
    LOGD(pthread_self(),"SHM_CTR->mq_id:%i",SHM_CTR->mq_id);
    LOGD(pthread_self(),"MQ_NAME:%s",params.MQ_NAME);
    
    while(1)
    {
        err = mq_recv(mqid, &msg);
        
        if ( msg.control & CTR_LOCAL )
        {
            LOGD(pthread_self(),"call send_local_mq()");
            // send_tcp(&buf);
            pthread_t pth;
            pthread_mutex_lock(&m_mqrcv);
            err = pthread_create (&pth, NULL, (void *)&send_local_mq, (void *)&msg);
            pthread_mutex_lock(&m_mqrcv); // solo continua despues que el hilo copie el mensaje
            pthread_mutex_unlock(&m_mqrcv);
        }
        else if ( msg.control & CTR_TCP )
        {   
            LOGD(pthread_self(),"call send_tcp()");
            // send_tcp(&buf);
            pthread_t pth;
            pthread_mutex_lock(&m_mqrcv);
            err = pthread_create (&pth, NULL, (void *)&send_tcp, (void *)&msg);
            pthread_mutex_lock(&m_mqrcv); // solo continua despues que el hilo copie el mensaje
            pthread_mutex_unlock(&m_mqrcv);
        }
        err=err;
    }
    LOGD(pthread_self(),"loop_mq_recv() stop :( !");
    pthread_detach(pthread_self());
}




// void reply_tcp(char *host, char *process, int response)
// zmq responde a la ultima query
void send_mq_reply_tcp( void *p)
{
    
    int n;
    int sock=*((int *)p);
    eipc_mq msg;
    eipc_tcp_reply msg_reply;
    char command[1000];
    timems_t timeout;
    eipc_shm *process_to;
    
    LOGD(sock,"send_mq_reply_tcp()");
    
    n = readall( sock, (char *)&msg, sizeof(long) + MQ_FIXE_LENGTH, 200);
    if (n<0)
    {
        goto end_s_mq_r_t;
    }
    if(msg.length>0)
    {
        n = readall( sock, (char *)&msg.msg, msg.length, 200);
        if (n<=0)
        {
            goto end_s_mq_r_t;
        }
    }
    
    msg_reply.reply.flag.ack_host = 1;
    
    process_to = get_process(msg.to_p);
    
    LOGD(sock,"------LOCK() from:%s::%s to:%s:%s",
            msg.from_h,msg.from_p,msg.to_h,msg.to_p);
    pthread_mutex_lock(&process_to->mutex);
    
        
    process_to->query_ack.flag.signal = 0;
    process_to->query_ack.flag.event = 0;
    process_to->query_ack.flag.msg = 0;

    if ( chek_process_alive(process_to->pid) )
    {
        LOGD(sock,"proceso vivo");
        msg_reply.reply.flag.alive=1;
        msg_reply.reply.flag.ack_host=1;
        if(msg.control & CTR_SIGNAL)
        {
            
            sprintf( command, "kill -%d %d &", msg.info, process_to->pid);
            LOGD(sock,"signal: %s", command);
            system(command);
            process_to->query_ack.flag.signal = 1;
        }
        else if( (msg.control & CTR_EVENT) || (msg.control &  CTR_MSG))
        {
            LOGD(sock,"event || msg");
            // guarda el evento en shm
            process_to->info = msg.info;
            if( msg.control & CTR_EVENT ) 
                process_to->query_ack.flag.event = 1;
            else
                process_to->query_ack.flag.msg = 1;
            
            process_to->query_ack.flag.ack_process=0;
            mq_send(process_to, &msg);
            process_to->query_ack.flag.ready=1;
            if( msg.control & ACK_PROCESS )
            {
                timeout = msg.time - timems(); 
                msg_reply.reply.flag.ack_process = 0;
                LOGD(sock,"call wait_ack_process() timeout:%lld", timeout );
                if ( timeout > 0 && timeout < 120000 ) // timeout < 2 min
                {
                    msg_reply.reply.flag.ack_process = wait_ack_process( process_to, timeout);
//                    TEST(msg_reply.reply.flag.ack_process);
                }
            }
        }
        else 
        {
            LOGD(sock,"send ni idea");
        }
    }
    else
    {
        LOGD(sock,"proceso muerto");
        msg_reply.reply.flag.alive = 0;
        // -chequeo si hay colas para eliminar
        if( msg.time > timems() && process_to->mqid > 0 )
        {
            LOGD(sock,"envia el msg");
            mq_send(process_to, &msg);
            msg_reply.reply.flag.timeout = 1;
        }
    }
    pthread_mutex_unlock(&process_to->mutex);
    LOGD(sock,"------UNLOCK() from:%s::%s to:%s:%s",
            msg.from_h,msg.from_p,msg.to_h,msg.to_p);
    msg_reply.reply.flag.ready = 1;
    
    sendall( sock, (char *)&msg_reply, sizeof(eipc_tcp_reply));
//    TEST(msg_reply.reply.flag.ack_process);
//    TEST(msg_reply.reply.flag.ack_host);
//    TEST(msg_reply.reply.flag.alive);
//    TEST(msg_reply.reply.flag.busy);
//    TEST(msg_reply.reply.flag.timeout);
//    TEST(msg_reply.reply.flag.ready);
    
end_s_mq_r_t:    
    close(sock);
    num_conn--;
    pthread_detach(pthread_self());
}

/*
 * - rcev(sock,buf,max,0)
 *
 * - debe ser local:
 *      - si el proces esta corriendo ejecuta-encola
 *      - si proceso no corre:
 *          - chequeo si hay colas con tiempo para descartar: elimina
 *          - si tiene time alive: encola
 *
 *      - responde tcp informando el estado.
 *          - ack
 *          - time alive
 *          - vacio
 *      :: reply_tcp()
 *
 */
void loop_tcp_recv(void)
{
    int e=0;
    int sock,conn;
    socklen_t clen = sizeof(struct sockaddr_in);
    struct sockaddr_in client;
    pthread_t ptconn;
    
    LOGD(pthread_self(),"loop_tcp_recv() start !");
    sock = bind_tcp(params.PORT_TCP,max_num_conn);
    LOGD(sock,"bind_tcp() socket !");
    
    while(1)
    {
        // Se bloquea con las conexiones
        conn = accept(sock, (struct sockaddr *)&client, &clen);
        num_conn++;
        LOGD(sock, "Conexion desde: %s:%d, connexiones disponibles: %d/%d", 
                inet_ntoa(client.sin_addr),ntohs(client.sin_port),
                num_conn,max_num_conn );
        
        LOGD(sock, "call send_mq_reply_tcp(%i)",conn);
        e = pthread_create (&ptconn, NULL, (void *)&send_mq_reply_tcp,&conn);
        if (e)
        {
            LOGD(sock,"no se puede crar thread para atender conneccion");
            close(conn);
        }
        if(num_conn>100)
            sleep(1);
        msleep(10);
    }
    pthread_detach(pthread_self());
}



int main(int argc, char *argv[])
{
    pthread_t pth_mq_recv;
    pthread_t pth_tcp_recv;
    
    read_params(&params);
    
    log_set_file("/tmp/eipcd.log");
    log_debug_level(255);
    log_logger_level(255);
    
            
    
    LOGD(0,"Inicia %s",params.PROGRAM_NAME);
    
    SHM_CTR = (eipc_shm_control *)map_shm(EIPC_SHM_NAME, "rwt",
        params.MAX_PROCESSES*sizeof(eipc_shm) + sizeof(eipc_shm_control)); 
    SHM = (eipc_shm *)(SHM_CTR +1);   
    
    // inicializa shm
    SHM_CTR->max_process=params.MAX_PROCESSES;
    
    int e=0;
    char *ip=get_ip("192.168.0.3");
    LOGD(0,"sima-nb:<<%s>>  %i",ip,atoi(ip));
    free(ip);
    
    
    e += pthread_create (&pth_mq_recv, NULL, (void *)&loop_mq_recv, NULL);
    e += pthread_create (&pth_tcp_recv, NULL, (void *)&loop_tcp_recv, NULL);
    if(e)
    {
        pthread_cancel(pth_mq_recv);
        pthread_cancel(pth_tcp_recv);
        LOGD(0,"main(): error al crear thread");
    }
    
    loop_check_pid_shm();
//    pthread_join(pth_mq_recv,0);
    return 0;
    
}
