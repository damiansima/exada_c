
// epm: exada proces manager


#include "../include/ipc_types.h"
struct param_struct
{
    int MAX_PROCESSES;
    int PORT_TCP;
    char SHM_NAME[256];
};



/// * * * * * * * * * * * * * * * * * * * * * *
///     read_params() 
/// * * * * * * * * * * * * * * * * * * * * * *
void read_params(struct param_struct *params) 
{
    char line[256];
    char name[256];
    char value[256];
    
    FILE *fp;
    
    fp = fopen("../cfg/eipcd.cfg", "r");
    
    while (fgets(line, 256, fp)) 
    {
        if (strlen(line) && ('#' == line[0]))
            ; // ignora comentarios
        else 
        {
            sscanf(line, "%[ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghjiklmnopqrstuvwxyz]=%s\n", name, value);
            name[255] = '\0';
            value[255] = '\0';
            if (!strcmp(name, "MAX_PROCESSES"))
                params->MAX_PROCESSES = atoi(value);
            if (!strcmp(name, "PORT_TCP"))
                params->PORT_TCP = atoi(value);
            if (!strcmp(name, "SHM_NAME"))
                strcpy(params->SHM_NAME, value);
            name[0] = '\0';
            value[0] = '\0';
        }
    }
    printf("MAX_PROCESSES=%i\n",params->MAX_PROCESSES);
    printf("PORT_TCP=%i\n",params->PORT_TCP);
    printf("SHM_NAME=%s\n",params->SHM_NAME);
            
}



/// * * * * * * * * * * * * * * * * * * * * * *
///     main() 
/// * * * * * * * * * * * * * * * * * * * * * *
int main(void)
{
    // Socket to talk to clients
    printf("Inicia el server\n");
    
    struct param_struct params;
    read_params(&params);
    
    eipc_shm *shm = (eipc_shm *)map_shm(params.SHM_NAME, "rwt", 
                        sizeof(eipc_shm)*params.MAX_PROCESSES); 
    
    memset(shm,0,sizeof(eipc_shm)*params.MAX_PROCESSES);
    
    char server[256];
    sprintf(server,"tcp://*:%d",params.PORT_TCP);
    
    void *zmq_ctx = zmq_ctx_new ();
    void *zmq_sock = zmq_socket (zmq_ctx, ZMQ_REP);
    //zmq_bind (sock, "tcp://*:5555");
    if(zmq_bind(zmq_sock,server)!=0)
    {
        logger("ERROR: eipcd::main(): bind fail !");
        return -1;
    }
    
    zmq_pollitem_t items [2];
    /* First item refers to ØMQ socket 'socket' */
    items[0].socket = zmq_sock;
    items[0].events = ZMQ_POLLIN;
    /* Second item refers to standard socket 'fd' */
    items[1].socket = NULL;
    items[1].fd = 1;
    items[1].events = ZMQ_POLLIN;
    /* Poll for events indefinitely */
    int rc = zmq_poll (items, 2, -1);
    logger("rc:: polling() %d",rc);
    assert (rc >= 0); /* Returned events will be stored in items[].revents */
        
    if (items [0].revents & ZMQ_POLLIN)
        logger("rc:: polling() item 0");
    if (items [1].revents & ZMQ_POLLIN)        
        logger("rc:: polling() item 1");
        
    // while (1) 
    if (1) 
    {
        // char buffer [10];
        // zmq_recv (responder, buffer, 10, 0);
        // buffer[9]=0;
        // printf ("Received: %s\n",buffer);
        // sleep (1); // Do some 'work'
        // zmq_send (responder, "World", 5, 0);
        
    }
    zmq_close(zmq_sock);
    zmq_ctx_destroy(zmq_ctx);
    return 0;
}
