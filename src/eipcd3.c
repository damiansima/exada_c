
// epm: exada proces manager

#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>     

#include "../include/ipc_types.h"
struct param_struct
{
    int MAX_PROCESSES;
    int PORT_TCP;
    char SHM_NAME[256];
    char PROGRAM_NAME[64];
    char EIPC_MQ_SERVER[64];
};




typedef struct _hosts_conn{
    void *zmq_context;
    void *zmq_socket;
    int  state;
    char *hostname;
}hosts_conn;


void *hosts[100];
int num_hosts=0;

struct param_struct params;   
    
    
/// * * * * * * * * * * * * * * * * * * * * * *
///     read_params() 
/// * * * * * * * * * * * * * * * * * * * * * *
void read_params(struct param_struct *params) 
{
    char line[256];
    char name[256];
    char value[256];
    
    FILE *fp;
    
    fp = fopen("../cfg/eipcd.cfg", "r");
    
    while (fgets(line, 256, fp)) 
    {
        if (strlen(line) && ('#' == line[0]))
            ; // ignora comentarios
        else 
        {
            sscanf(line, "%[ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghjiklmnopqrstuvwxyz]=%s\n", name, value);
            name[255] = '\0';
            value[255] = '\0';
            if (!strcmp(name, "MAX_PROCESSES"))
                params->MAX_PROCESSES = atoi(value);
            if (!strcmp(name, "PORT_TCP"))
                params->PORT_TCP = atoi(value);
            if (!strcmp(name, "SHM_NAME"))
                strcpy(params->SHM_NAME, value);
            if (!strcmp(name, "PROGRAM_NAME"))
                strcpy(params->PROGRAM_NAME, value);
            if (!strcmp(name, "EIPC_MQ_SERVER"))
                strcpy(params->EIPC_MQ_SERVER, value);
            name[0] = '\0';
            value[0] = '\0';
        }
    }
    printf("MAX_PROCESSES=%i\n",params->MAX_PROCESSES);
    printf("PORT_TCP=%i\n",params->PORT_TCP);
    printf("SHM_NAME=%s\n",params->SHM_NAME);
    printf("PROGRAM_NAME=%s\n",params->PROGRAM_NAME);
    printf("EIPC_MQ_SERVER=%s\n",params->EIPC_MQ_SERVER);
            
}



void zmq_tcp_disconnect(void *em)
{
    hosts_conn *eipc=(hosts_conn *)em;
    logger("dettach: %s",eipc->hostname);
    zmq_close(eipc->zmq_socket);
    zmq_ctx_destroy(eipc->zmq_context);
    free(eipc->hostname);
    free(eipc);    
}

void *zmq_tcp_connect(char *hsot,int port)
{    
    hosts_conn *em;
    char _name[300];
    
    sprintf(_name,"tcp://%s:%i",hsot,port);
    em = (hosts_conn *) malloc(sizeof(hosts_conn));
    em->hostname=strdup(hsot);
    em->zmq_context = zmq_ctx_new ();
    em->zmq_socket = zmq_socket (em->zmq_context, ZMQ_REQ);
    logger("connect to [%s]",_name);
    em->state = zmq_connect(em->zmq_socket, _name);
    if (em->state!=0)
        logger("ERROR: %s zmq_tcp_connect()::zmq_connect(%s) fail !",params.PROGRAM_NAME,em->hostname);
    return ((void *)em);
}

void *find_host(char *host)
{
    int i;
    for (i=0;i<num_hosts;i++)
    {
        // logger("compara con host: %s",((hosts_conn *)hosts[i])->hostname);
        if(!strcmp(host,((hosts_conn *)hosts[i])->hostname))
        {
            return hosts[i];
        }
    }
    num_hosts++;
    hosts[i] = zmq_tcp_connect(host,params.PORT_TCP);
    return hosts[i];
}


void connect_tcp(char *host,void *msg, int len)
{
    hosts_conn *conn = (hosts_conn *)find_host(host);
    char buf[1024];
    if (conn->state==0)
    {   
        logger("[%s] send ok ",conn->hostname);
        zmq_send (conn->zmq_socket,(char *)msg, len, 0);
        
        zmq_pollitem_t items [] = { { conn->zmq_socket, 0, ZMQ_POLLIN, 0 } };
        zmq_poll (items, 1, 1000);
        if (items [0].revents & ZMQ_POLLIN)
        {
            logger("poll ok");
            zmq_recv(conn->zmq_socket, buf, 25, 0); 
        }
        logger("poll despues");
    }
    else
        logger("[%s] send fail !",conn->hostname);
    return;
}

void parser_msg(msg_t *msg, int tipe)
{
    char buf[512];
    int pid=777;
    struct msg_signal *signal = &msg->data;
    struct msg_event  *event  = &msg->data;
    struct msg_tcp_mq *tcp_mq = &msg->type;
    
    logger("msg ok: type:%i",msg->type);
    system("ls /home");
    switch(msg->type)
    {
        case MQ_SIGNAL:
            if( strcmp(signal->to_host,"") )
            {
                
            }
            else
            {
                sprintf(buf,"echo kill -%d %d",signal->signal,pid);
                logger("system(%s) [%s|%s]",buf,signal->to_host,signal->to_process);
                system(buf);
            }
            break;
        case MQ_EVENT:
            break;
        case MQ_HP:
            break;
        case MQ_ACK:
            break;
        case MQ_NOWAIT:
            break;
        case MQ_NORMAL:
            break;
        default:
            logger("MSG.type default");
    }
            
}

void bind_ipc(eipc_shm *shm)
{
    logger("Inicia bind_ipc()");
    int msgid = mq_open(params.EIPC_MQ_SERVER,"rw");
    
    msg_t MSG;
    int msgtype=0, msgflag=0;

    while(1)
    {
        if(msgrcv(msgid, &MSG, BUFSIZ, msgtype, msgflag )  == -1)
        {
            fprintf(stderr, "failedto receive: \n");
            exit(EXIT_FAILURE);
        }
        else
        { 
            parser_msg(&MSG,0);            
        }
    }
    return ;
}


void bind_tcp(eipc_shm *shm)
{
    // int command;
    logger("Inicia %s bind_tcp()",params.PROGRAM_NAME);
    
    char server[256];
    sprintf(server,"tcp://*:%d",params.PORT_TCP);
    logger("Servidor en: [%s]",server);
    
    void *zmq_ctx = zmq_ctx_new ();
    void *zmq_sock = zmq_socket (zmq_ctx, ZMQ_REP);
    //zmq_bind (sock, "tcp://*:5555");
    if(zmq_bind(zmq_sock,server)!=0)
    {
        logger("ERROR: %s bind_tcp()::bind() fail !",params.PROGRAM_NAME);
        return ;
    }
    
    char buf [256];
    int nbytes;
    while (1) 
    {
        
        nbytes = zmq_recv(zmq_sock, buf, 25, 0); 
        if(nbytes<0)
        {
            logger("nbytes: %i",nbytes);
        }
        else
        {
            buf[nbytes]=0;
            logger("nbytes: %i: buf:<<%s>>",nbytes,buf);
            sleep(0);
            zmq_send (zmq_sock, "", 0, 0);
            // zmq_recv (zmq_sock, &command,4, 0);
            // buffer[9]=0;
            // printf ("Received: %i\n",command);
        }
        msleep(10);
    }
    zmq_close(zmq_sock);
    zmq_ctx_destroy(zmq_ctx);
    return;
}

/// * * * * * * * * * * * * * * * * * * * * * *
///     main() 
/// * * * * * * * * * * * * * * * * * * * * * *
int main(int argc, char *argv[])
{
    int childpid;
    read_params(&params);
    
    logger("Inicia %s\n",params.PROGRAM_NAME);
    
    eipc_shm *shm = (eipc_shm *)map_shm(params.SHM_NAME, "rwt", 
                        sizeof(eipc_shm)*params.MAX_PROCESSES); 
                        
    memset(shm,0,sizeof(eipc_shm)*params.MAX_PROCESSES);
    
    
    
    if ( (childpid = fork()) == 0 ) 
	{
        bind_ipc(shm);
		sleep(1);
        exit(0);
	} 
	else if ( childpid > 0 ) 
	{
		bind_tcp(shm);
        sleep(1);	
        exit(0);
	}
	else
	{
        logger("ERROR: %s main()::fork() fail !",params.PROGRAM_NAME);
	}
	
    return 0;
}
