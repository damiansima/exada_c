//#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>   
#include <time.h>

#include <pthread.h>

#include "../include/ipc_types.h"
#include "../include/socketlib.h"
#include "../include/params.h"

#include <syslog.h>

#include <sys/stat.h>
#include <errno.h>

/// lanza exepcion:
/// quiere decir que responde la query informando del evento (si pide ack).





eipc_shm_control *SHM_CTR;
eipc_shm *SHM;
param_struct params;  
pthread_mutex_t m_tcpsnd=PTHREAD_MUTEX_INITIALIZER; 
int max_num_conn=10;
int num_conn=0;
   
  

int tcp_send(int sock,char *buf,int len,int o)
{
    return sock;
}
int tcp_rcev(int sock,char *buf,int max,int o)
{
    return sock;
}

int chek_process_alive(int pid)
{
    struct stat sts;
    char prcs[100];
    sprintf( prcs, "/proc/%i", pid);
    if (stat( prcs, &sts) == -1 && errno == ENOENT) {
      // process doesn't exist
       printf("El proceso NO EXISTE\n");
       return 0;
    }
    else
    {
        printf("El proceso EXISTE\n");
        return 1;
    }  
}

void mq_send(eipc_shm *process, eipc_mq *msg)
{
    // si tira error en la cola saca los mensajes que tienen menos tiempo
    int len = MQ_FIXE_LENGTH + msg->length;
    if(msgsnd(process->mqid, (void *)msg, len, MQFLAG) == -1)
    {
        logger("mq_send()::msgsnd() failed !");
    }
    else
    {
        process->mq_count++;
        process->flags |= F_RDY;
    }
    return ;
}

int mq_recv(int mqid, eipc_mq *msg)
{
    if(msgrcv( mqid, msg, BUFSIZ, 0, 0 )  == -1)
    {
        fprintf(stderr, "failedto receive: \n");
        return -1;
    }
    return 0;
}


eipc_shm *get_process(char *process)
{
    int i=0;
    eipc_shm *shm = &SHM[0];    
    for(i=0;i<SHM_CTR->num_process;i++)
    {
        if(!strcmp(shm->process,process))
            return shm;
        shm++;
    }
    memset((void *)shm,0,sizeof(eipc_shm));
    SHM_CTR->num_process++;
    // assert(SHM_CTR->num_process<NUM_PROCESS);
    return shm;
}



// hilo para chequera continuamente los procesos en ejecucion
void loop_check_pid_shm()
{
    int i;
    eipc_shm *shm;
    while(1)
    {
        shm = &SHM[0];
        printf("check running...\n");
        for(i=0;i<SHM_CTR->num_process;i++)
        {
            if( shm->pid > 0 && chek_process_alive(shm->pid) == 0 )
            {
                shm->pid=0;
                shm->mqid=0;
            }
            printf("process[%i]:%s ",i,shm->process);
            printf("pid:%i ",shm->pid);
            printf("mqid:%i ",shm->mqid);
            printf("mq_count:%i \n",shm->mq_count);
            shm++;
        }
        sleep(1);
    }
}


/*      void send_tcp(void *buf)
 * 
 * - recive el msg de la quueue para enviar por tcp
 * - lanza un hilo:
 *      - envia el mensaje
 *      - queda esperando la respuesta o timedout
 *      - responde en la queue y shm correspondiente
 *        tiene los datos en el puntero de la shm
 *
 */
void send_tcp(void *buf)
{
    int res;
    int sock;
    int timemsg;
    eipc_mq msg_tcp;
    eipc_tcp_reply msg_reply;
    eipc_shm *process;
    
    int length = sizeof(long) + MQ_FIXE_LENGTH + msg_tcp.length;
    
    memcpy( &msg_tcp, buf, length);    
    pthread_mutex_unlock(&m_tcpsnd);
    
    process = get_process(msg_tcp.to_p);
    process->flags=0; 
    
    timemsg = (int) msg_tcp.time - time(NULL);
    if(timemsg>0 || msg_tcp.time == 0 )
    {
        sock = connect_tcp(msg_tcp.to_h,params.PORT_TCP);
        res = sendall( sock, (char *)&msg_tcp, length);
        if(res==0)
        {
            process->flags |= F_SEND;
        }
        // espera la respuesta si es necesario
        if(msg_tcp.control & CTR_ACK)
        {
            process->control=0;
            if(timemsg>0 && res == 0 )
            {
                res = readall( sock, (char *)&msg_reply, sizeof(msg_reply), timemsg );
                // setea en shm el ack
                if(res>0)
                {
                    // setea en ok
                    process->control = msg_reply.control;
                }
                else
                {
                    process->control &= (~CTR_ACK);
                    process->control |= CTR_TIMEOUT;
                }
            }
            else
            {
                // respondo en shm que tuvo timedout
                process->control |= CTR_TIMEOUT;
            }
        }
    }
    res=res;
}



/*
 * - msgrcv(mq,buf,MAX,0)
 * - analiza si destino es local o tcp
 * - si local:
 *      - si el proces esta corriendo ejecuta-encola
 *      - si proceso no corre:
 *          - chequeo si hay colas con tiempo para descartar: elimina
 *          - si tiene time alive:
 *              - encola. controla si la queue esta llena:
 *                  - lanza exepcion
 *
 * - si tcp:
 *      - encola para ::send_tcp()
 *      - o lanza un hilo ::send_tcp(msg,len,p_msg)
 *
 *  WARNING: todos los encolas y chequeos van con un shm para count y mayor time alive.
 */
void loop_mq_recv(void)
{
    logger("loop_mq_recv() start !");
    int mqid=0;
    char command[1024];
    eipc_mq buf;
    int err;
    eipc_shm *process_to;
    eipc_shm *process_from;
    
    mqid = mq_open(params.MQ_NAME,"rw");
    SHM_CTR->mq_id = mqid;
    logger("SHM_CTR->mq_id:%i",SHM_CTR->mq_id);
    logger("MQ_NAME:%s",params.MQ_NAME);
    
    while(1)
    {
        err = mq_recv(mqid, &buf);
        logger("recibe msg.type:   %10i",buf.type );
        logger("recibe msg.control:%10i",buf.control );
        logger("recibe msg.time:   %10i",buf.time );
        logger("recibe msg.to_h:     %s",buf.to_h );
        logger("recibe msg.to_p:     %s",buf.to_p );
        logger("recibe msg.from_h:   %s",buf.from_h );
        logger("recibe msg.from_p:   %s",buf.from_p );
        logger("recibe msg.info:   %10i",buf.info );
        logger("recibe msg.fill:   %10i",buf.fill );
        
        
        process_to = get_process(buf.to_p);
        process_from = get_process(buf.from_p);

        if (buf.control & CTR_LOCAL)
        {   // local enqueue
            logger("local");

            process_from->event = 0;
            process_from->flags = F_BUSY;
            process_from->control = 0;

            if (chek_process_alive(process_to->pid))
            {
                logger("proceso vivo");
                if(buf.control & CTR_SIGNAL)
                {
                    logger("signal");
                    sprintf( command, "echo kill -%d %d", buf.info, process_to->pid);
                    system(command);
                }
                else if(buf.control & CTR_EVENT)
                {
                    logger("event");
                    // guarda el evento en shm
                    process_to->event = buf.info;
                    process_to->flags |= F_EVENT;
                    mq_send(process_to, &buf);
                }
                else if(buf.control &  CTR_MSG) 
                {
                    logger("mq_send");
                    mq_send(process_to, &buf);
                }
                process_from->flags |= F_SEND;
            }
            else
            {
                logger("proceso muerto");
                // -chequeo si hay colas para eliminar
                if( buf.time > time(NULL) )
                {
                    process_from->flags |= F_SEND;
                    mq_send(process_to, &buf);
                }
                else 
                {
                    // process_from->flags &= ~F_SEND;
                }
            }
        }
        else
        {   
            logger("call send_tcp()");
            // send_tcp(&buf);
            pthread_t pth;
            pthread_mutex_lock(&m_tcpsnd);
            err = pthread_create (&pth, NULL, (void *)&send_tcp, (void *)&buf);
            pthread_mutex_lock(&m_tcpsnd); // solo continua despues que el hilo copie el mensaje
            pthread_mutex_unlock(&m_tcpsnd);
        }
        err=err;
    }
}




// void reply_tcp(char *host, char *process, int response)
// zmq responde a la ultima query
void send_mq_reply_tcp( void *sock)
{
    int n;
    eipc_mq buf;
    eipc_tcp_reply buf_reply;
    char command[1000];

    n = readall((int)&sock,(char *)&buf,sizeof(eipc_mq),0);
    if (n<=0)
    {
        close((int)&sock);
        num_conn--;
    }
    eipc_shm *process_to = get_process(buf.to_h);
    process_to->flags |= F_BUSY;
    buf_reply.control = TCP_ACK_HOST;

    if (chek_process_alive(process_to->pid))
    {
        buf_reply.control |= TCP_PROCESS_ALIVE;
        if(buf.control&CTR_SIGNAL)
        {
            sprintf( command, "echo kill -%d %d", buf.info, process_to->pid);
            system( command );
            buf_reply.control |= TCP_ACK_PROCESS;
        }
        else if(buf.control&CTR_EVENT)
        {
            // guarda el evento en shm
            process_to->event = buf.info;
            process_to->flags |= F_EVENT;
            // buf_reply.control |= TCP_ACK_PROCESS;
        }
        else 
        {
            mq_send(process_to, &buf);
            buf_reply.control |= TCP_ACK_PROCESS;
        }
        buf_reply.control |= CTR_ACK_HOST;
    }
    else
    {
        // -chequeo si hay colas para eliminar
        if( buf.time > time(NULL) && (buf.control & (CTR_SIGNAL|CTR_EVENT))==0  )
        {
            buf_reply.control = TCP_PROCESS_MQ_WAIT;
            mq_send(process_to, &buf);
        }
        else 
        {
            // process_from->flags &= ~F_SEND;
        }
    }
    close((int)&sock);
    num_conn--;
}

/*
 * - rcev(sock,buf,max,0)
 *
 * - debe ser local:
 *      - si el proces esta corriendo ejecuta-encola
 *      - si proceso no corre:
 *          - chequeo si hay colas con tiempo para descartar: elimina
 *          - si tiene time alive: encola
 *
 *      - responde tcp informando el estado.
 *          - ack
 *          - time alive
 *          - vacio
 *      :: reply_tcp()
 *
 */
void loop_tcp_recv(void)
{
    int e=0;
    int sock,conn;
    socklen_t clen = sizeof(struct sockaddr_in);
    struct sockaddr_in client;
    pthread_t ptconn;
    
    sock = bind_tcp(params.PORT_TCP,max_num_conn);

    while(1)
    {
        // Se bloquea con las conexiones
        conn = accept(sock, (struct sockaddr *)&client, &clen);
        num_conn++;
        logger( "Conexion desde: %s:%d, connexiones disponibles: %d/%d", 
                inet_ntoa(client.sin_addr),ntohs(client.sin_port),
                num_conn,max_num_conn );
        
        e = pthread_create (&ptconn, NULL, (void *)&send_mq_reply_tcp,&conn);
        if (e)
        {
            logger("logger no se puede crar thread para atender conneccion");
            close(conn);
        }
        msleep(10);
    }
}



int main(int argc, char *argv[])
{
    pthread_t pth_mq_recv;
//    pthread_t pth_tcp_recv;
    
    read_params(&params);
     
    logger("Inicia %s\n",params.PROGRAM_NAME);
    
    SHM_CTR = (eipc_shm_control *)map_shm(params.SHM_NAME, "rwt",params.SHM_SIZE); 
    SHM = (eipc_shm *)(SHM_CTR +1);   
    
    // inicializa shm
    SHM_CTR->num_process=0;
    
    int e=0;
    char *ip=get_ip("192.168.0.3");
    logger("sima-nb:<<%s>>  %i",ip,atoi(ip));
    free(ip);
    
    
    e += pthread_create (&pth_mq_recv, NULL, (void *)&loop_mq_recv, NULL);
//    e += pthread_create (&pth_tcp_recv, NULL, (void *)&loop_tcp_recv, NULL);
    if(e)
    {
        pthread_cancel(pth_mq_recv);
//        pthread_cancel(pth_tcp_recv);
        logger("main(): error al crear thread");
    }
    
    loop_check_pid_shm();
//    pthread_join(pth_mq_recv,0);
    return 0;
    
}

/* 
// Funcion que se ejecuta en el thread hijo.
void *funcionThread (void *parametro)
{
    printf("entra con parametro: %i\n",*((int *)parametro));

//    pthread_mutex_unlock(&mymutex);

    while (1)
    {
//        pthread_mutex_lock(&mymutex);
        contador--;
//        pthread_mutex_unlock(&mymutex);

        printf ("Hilo  : %d\n", contador);
        usleep(100*1000);
    }
}
* */
