
// epm: exada proces manager

#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>   

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>  

#include <zmq.h>

#include "../include/ipc_types.h"
struct param_struct
{
    int MAX_PROCESSES;
    int PORT_TCP;
    char SHM_NAME[256];
    char PROGRAM_NAME[64];
};


struct param_struct params;   
    
    
/// * * * * * * * * * * * * * * * * * * * * * *
///     read_params() 
/// * * * * * * * * * * * * * * * * * * * * * *
void read_params(struct param_struct *params) 
{
    char line[256];
    char name[256];
    char value[256];
    
    FILE *fp;
    
    fp = fopen("../cfg/eipcd.cfg", "r");
    
    while (fgets(line, 256, fp)) 
    {
        if (strlen(line) && ('#' == line[0]))
            ; // ignora comentarios
        else 
        {
            sscanf(line, "%[ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghjiklmnopqrstuvwxyz]=%s\n", name, value);
            name[255] = '\0';
            value[255] = '\0';
            if (!strcmp(name, "MAX_PROCESSES"))
                params->MAX_PROCESSES = atoi(value);
            if (!strcmp(name, "PORT_TCP"))
                params->PORT_TCP = atoi(value);
            if (!strcmp(name, "SHM_NAME"))
                strcpy(params->SHM_NAME, value);
            if (!strcmp(name, "PROGRAM_NAME"))
                strcpy(params->PROGRAM_NAME, value);
            name[0] = '\0';
            value[0] = '\0';
        }
    }
    printf("MAX_PROCESSES=%i\n",params->MAX_PROCESSES);
    printf("PORT_TCP=%i\n",params->PORT_TCP);
    printf("SHM_NAME=%s\n",params->SHM_NAME);
    printf("PROGRAM_NAME=%s\n",params->PROGRAM_NAME);
            
}

typedef struct _hosts_conn{
    void *zmq_context;
    void *zmq_socket;
    int  state;
    char *hostname;
}hosts_conn;


void *hosts[100];
int num_hosts=0;

void zmq_tcp_disconnect(void *em)
{
    hosts_conn *eipc=(hosts_conn *)em;
    logger("dettach: %s",eipc->hostname);
    zmq_close(eipc->zmq_socket);
    zmq_ctx_destroy(eipc->zmq_context);
    free(eipc->hostname);
    free(eipc);    
}

void *zmq_tcp_connect(char *hsot,int port)
{    
    hosts_conn *em;
    char _name[300];
    
    sprintf(_name,"tcp://%s:%i",hsot,port);
    em = (hosts_conn *) malloc(sizeof(hosts_conn));
    em->hostname=strdup(hsot);
    em->zmq_context = zmq_ctx_new ();
    em->zmq_socket = zmq_socket (em->zmq_context, ZMQ_REQ);
    logger("connect to [%s]",_name);
    em->state = zmq_connect(em->zmq_socket, _name);
    if (em->state!=0)
        logger("ERROR: %s zmq_tcp_connect()::zmq_connect(%s) fail !",params.PROGRAM_NAME,em->hostname);
    return ((void *)em);
}

void *find_host(char *host)
{
    int i;
    for (i=0;i<num_hosts;i++)
    {
        // logger("compara con host: %s",((hosts_conn *)hosts[i])->hostname);
        if(!strcmp(host,((hosts_conn *)hosts[i])->hostname))
        {
            return hosts[i];
        }
    }
    num_hosts++;
    hosts[i] = zmq_tcp_connect(host,params.PORT_TCP);
    return hosts[i];
}

void connect_tcp(char *host,void *msg, int len)
{
    hosts_conn *conn = (hosts_conn *)find_host(host);
    char buf[1024];
    if (conn->state==0)
    {   
        logger("[%s] send ok ",conn->hostname);
        zmq_send (conn->zmq_socket,(char *)msg, len, 0);
        
        zmq_pollitem_t items [] = { { conn->zmq_socket, 0, ZMQ_POLLIN, 0 } };
        zmq_poll (items, 1, 1000);
        if (items [0].revents & ZMQ_POLLIN)
        {
            logger("poll ok");
            zmq_recv(conn->zmq_socket, buf, 25, 0); 
        }
        logger("poll despues");
    }
    else
        logger("[%s] send fail !",conn->hostname);
    return;
}

/// * * * * * * * * * * * * * * * * * * * * * *
///     main() 
/// * * * * * * * * * * * * * * * * * * * * * *
int main(int argc, char *argv[])
{
    // int childpid;
    read_params(&params);
    
    logger("Inicia %s",params.PROGRAM_NAME);
    
    eipc_shm *shm = (eipc_shm *)map_shm(params.SHM_NAME, "rwt", 
                        sizeof(eipc_shm)*params.MAX_PROCESSES); 
                        
    memset(shm,0,sizeof(eipc_shm)*params.MAX_PROCESSES);
    
    
    
    connect_tcp("sima-nb","hola mundo",10);
    connect_tcp("sima-nb","hola che todo bien",18);
        
	int i;
    for (i=0;i<num_hosts;i++)
    {
        zmq_tcp_disconnect((void *)hosts[i]);
    }
    
    return 0;
}
