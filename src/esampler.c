/*
 * Muestrea los valores escaneados para asociar cada registro con un tag
 * correspondiente. 
 */

#include "../include/utils.h"
//#include "../include/static_list.h"
#include "../include/lista_continua.h"
#include "../include/eipcc.h"
#include "../include/socketlib.h"


#include <stdlib.h>
#include <sqlite3.h>
#include <string.h>
#include <unistd.h>
#include <strings.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>


// SHM de los SCAN  (escan)
typedef short e_scan_t; // registros de 16 bits
typedef struct
{
    char name[96];
    int  addres;
    int  reg_start;
    int  reg_end;
    int  reg_length;
    int  alarm_start;
    int  alarm_end;
    int  alarm_length;
    timems_t update_time;
    e_scan_t data[1];
}SCAN_BLOCK;

typedef struct
{
    timems_t now;
    enum  {busy, ready, writing }state;
}SCAN_CTR;



// estructura a enviar por udp a ercvtags
#define TIME_SEND       (500)   // milisegundos
#define TIME_STREAKY    (5000)  // milisegundos
typedef enum{type_int16=1, type_int32, typte_float,type_string }e_TAG_TYPE;
typedef struct 
{
    int     id;              // indices del tag, en variables ID de (db)
    int     heartbeat;       // nivel del heart beat
    e_TAG_TYPE  type;
    int         len_type;
    union{
        int16_t int16;
        int32_t int32;
        float_t  float32;
        double_t float64;
        char string[8];
    }value;                 // valor del tag
}SAMPLER_BLOCK;
typedef struct
{
    int         id;
    int         _addres;
    int         _register;
    e_TAG_TYPE  type;
    int         len_type;
    timems_t    *update_time;
    void        *ptr;
}s_TAG;

LIST_BLOCK *ptr_register(LIST_t *lshm, int addres, int reg)
{
    SCAN_BLOCK *pb;
    LIST_BLOCK *lb;

    lb = list_first_block(lshm);
    while(lb)
    {
        pb = (SCAN_BLOCK *)lb->data;

        if( addres==pb->addres && reg>=pb->reg_start && reg<=pb->reg_end )
            break;

        lb = list_next_block(lshm, lb);
    }
    return lb;
}


//int f_addres;
//int handler_find(LIST_BLOCK *b)
//{
//    SCAN_BLOCK *pb = (SCAN_BLOCK *)b->data;
//    
//    if(pb->addres == f_addres )
//        return 1;
//    return 0;
//}


char scan_name[96]={"Balanza_XX"};
char name_attach[128];
int tag_length=0;
eipc_t *eipc;

#define assert_sql(r) {if(r!=SQLITE_OK){log_error("Failed database %s",sqlite3_errstr(r));sqlite3_close(db);return r;}}

//
//// s.scan,s.device,d.driver,d.address
//static int callback(void *NotUsed, int argc, char **argv, char **azColName)
//{
//    int i;
//    for (i = 0; i < argc; i++)
//    {
//        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
//    }
//    
////    scan_append(argv[0], argv[1], argv[2], atoi(argv[3]));
//    printf(".....\n");
//    return 0;
//}




int main(int argc, char* argv[])
{
    log_set_file("/tmp/esampler.log");
    log_debug_level(255);
    log_logger_level(0);
    
    
    if (argc < 2)
    {
        log_error("Usage:  %s <Scan Name> \n", argv[0]);
    }
    else
        strcpy(scan_name,argv[1]);
    
    sprintf(name_attach,"esampler_%s",scan_name);
    log_msg("Usa scan_name=%s", scan_name);
    
    eipc = eipc_attach(name_attach);
    assert_return((int)eipc != -1);
    
    
    sqlite3 *db;
    int  ret;
    sqlite3_stmt *stmt;
    char sql[1024];

    ret = sqlite3_open("../cfg/DBConfig.db", &db);
    assert_sql(ret);
    
    char hostname[128];
    char ip[128];

    gethostname(hostname,128);
    hostname_to_ip(hostname,ip);
    log_info("hostname <%s::%s>",hostname,ip);
    
    LIST_t *l_sampler = list_alloc( 32 );
    LIST_t *l_scan = list_alloc( 32 );
    LIST_BLOCK *lb_samp;
    LIST_BLOCK *lb_s;
    SAMPLER_BLOCK *sampler_tags;
    s_TAG *scan_tags;
    int id;
    int len;
  
    
    SCAN_CTR *ctr = (SCAN_CTR *)map_shm( scan_name, "r", 0);  
    LIST_t *lshm = (LIST_t *)(ctr+1);
    SCAN_BLOCK *pb;
    LIST_BLOCK *lb;
    
    sprintf(sql,"select t.id,t.tag,t.type,d.address,t.register"\
            " from scans as s join devices as d join tags as t"\
            " where d.device=s.device and t.device=s.device and"\
            " rwa like '%%r%%' and s.scan='%s' and s.host='%s';",scan_name,ip);
    
    log_msg("sql: <<%s>>",sql);
    
    ret = sqlite3_prepare (db, sql, strlen(sql)+1, &stmt, NULL);
    assert_sql(ret);
    
    char *type;
        
    ret = sqlite3_step(stmt);
    while (ret == SQLITE_ROW) 
    {
        id = sqlite3_column_int(stmt,0);
        
        log_debug("list_add_alloc(&l_scan, sizeof(s_TAG), %i)",id);
        lb_s = list_add_alloc(&l_scan, sizeof(s_TAG), id);
        scan_tags = (s_TAG *)lb_s->data;
        
        scan_tags->id = sqlite3_column_int(stmt,0);
        scan_tags->_addres = sqlite3_column_int(stmt,3);
        scan_tags->_register = sqlite3_column_int(stmt,4);
        
        type = (char *)sqlite3_column_text(stmt,2);
        if(strncasecmp(type,"STRING",6) == 0)
        {
            scan_tags->type=type_string;scan_tags->len_type=atoi(&type[6]);
        }
        else if(strncasecmp(type,"INT16",5) == 0)
        {
            scan_tags->type=type_int16;scan_tags->len_type=2;
        }
        else if(strncasecmp(type,"INT32",5) == 0)
        {
            scan_tags->type=type_int32;scan_tags->len_type=4;
        }
        else if(strncasecmp(type,"FLOAT",5) == 0)
        {
            scan_tags->type=typte_float;scan_tags->len_type=4;
        }
        
        lb = ptr_register(lshm, scan_tags->_addres, scan_tags->_register);
        if (lb)
        {
            pb = (SCAN_BLOCK *)lb->data;
            printf("[%s] addres=%05i  start=%05i  end=%05i len=%05i\n", 
                pb->name, pb->addres, pb->reg_start, pb->reg_end, pb->reg_length);
            
            scan_tags->ptr =(void *) &(pb->data[ scan_tags->_register - pb->reg_start ] ); 
            scan_tags->update_time = &pb->update_time;
        }
        else
        {
            log_warning("El registro %i para la direccion %i no se encontro",
                    scan_tags->_register, scan_tags->_addres);
            scan_tags->ptr = (void *)0; 
        }
        
        
        log_info("agregado TAG: t.id(%i),t.tag(%s),t.type(%s),d.address(%i),t.register(%i):: %i:%i",
            scan_tags->id,
            sqlite3_column_text(stmt,1),
            sqlite3_column_text(stmt,2),
            scan_tags->_addres,
            scan_tags->_register,
            scan_tags->type,scan_tags->len_type);
                
        
        len = scan_tags->len_type > 8 ? (scan_tags->len_type-8) : 0;
        log_debug("list_add_alloc(&l_sampler, %i + sizeof(SAMPLER_BLOCK) , %i);",len,id);
        lb_s = list_add_alloc(&l_sampler, len + sizeof(SAMPLER_BLOCK) , id);
        sampler_tags = (SAMPLER_BLOCK *)lb_s->data;
        
        sampler_tags->id=id;
        sampler_tags->type=scan_tags->type;
        sampler_tags->len_type=scan_tags->len_type;
        
        ret = sqlite3_step(stmt);
    }
    sqlite3_close(db);
    
//    free(l_sampler);
//    free(l_scan);
//    eipc_deattach(eipc);
//    //eipc_deattach(eipc_scan);
//    return ;
    
    
    timems_t update_time;
    
    while(1)
    {
        lb_samp = list_first_block(l_sampler);
        lb_s = list_first_block(l_scan);

        update_time = timems();
        
        log_info("---- Ahora Esacane ----");
        while(lb_s)
        {
            sampler_tags = (SAMPLER_BLOCK *)lb_samp->data;
            scan_tags = (s_TAG *)lb_s->data;

            while (ctr->state != ready)
                msleep(TIME_SEND/10);
                    
            if( (int)scan_tags->ptr != 0 )
            {   
                memcpy( sampler_tags->value.string, scan_tags->ptr, sampler_tags->len_type );
                if ( update_time < (*scan_tags->update_time + TIME_SEND) )
                    sampler_tags->heartbeat=(TIME_STREAKY/TIME_SEND)+1;
//                else
//                {
//                    if (sampler_tags->heartbeat > 0 )
//                        sampler_tags->heartbeat-=1;
//                }
                log_msg("sampler_tags:: id(%i)  type(%i %i) heartbeat(%i)",
                    sampler_tags->id,
                    sampler_tags->type,sampler_tags->len_type,
                    sampler_tags->heartbeat);
                
            }
            lb_s = list_next_block(l_scan, lb_s);
            lb_samp = list_next_block(l_sampler, lb_samp);
        }
        log_info(" - - - - - - - - - ");
        msleep(TIME_SEND);
    }
    
    free(l_sampler);
    free(l_scan);
    log_msg("chau ----");
    eipc_deattach(eipc);
    return 1;
    
}
