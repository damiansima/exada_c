
#ifndef __EX_CLIENT_LIB_
    #define __EX_CLIENT_LIB_

#include "../include/utils.h"
#include "../include/lista_continua.h"
#include "../include/socketlib.h"
#include "../include/e_common.h"
#include "../include/eipcc.h"

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <strings.h>


eipc_t *eipc;
static LIST_t *l_shm_tags;
static char *tag_find=NULL;

int handler_find_tag(LIST_BLOCK *lb)
{
    SHM_TAGS *tag = (SHM_TAGS *)lb->data;
    
    if(strcmp(tag->tag,tag_find)==0)
        return 1;
    return 0;
}


// agrego esto para probar despues

typedef SHM_TAGS*  TAG;

#define get_ID(data)        ( data ? (((SHM_TAGS *)data)->id) : -1 )
#define get_VALUE(data)     ( data ? (((SHM_TAGS *)data)->value) : -9999.0 ) 
#define get_NAME(data)      ( data ? (((SHM_TAGS *)data)->tag) : NULL )
#define get_STRING(data)    ( data ? (((SHM_TAGS *)data)->string) : NULL )
#define get_MAX(data)       ( data ? (((SHM_TAGS *)data)->value_max ) : -9999.0 )
#define get_MIN(data)       ( data ? (((SHM_TAGS *)data)->value_min ) : -9999.0 )
#define get_STREAKY(data)   ( data ? (((SHM_TAGS *)data)->streaky.n1 || \
                                      ((SHM_TAGS *)data)->streaky.n2 || \
                                      ((SHM_TAGS *)data)->streaky.n3) : -1 )

SHM_TAGS * gettag(char *tag)
{
    LIST_BLOCK *lb;
    tag_find=tag;
    lb = list_generic_find(l_shm_tags,handler_find_tag);
    if(lb==NULL)
        return NULL;
    return (SHM_TAGS *)lb->data;
}

int getid_tag(char *tag)
{
    LIST_BLOCK *lb;
//    SHM_TAGS *ptag;
    tag_find=tag;
    lb = list_generic_find(l_shm_tags,handler_find_tag);
    if(lb==NULL)
        return -1;
//    ptag = (SHM_TAGS *)lb->data;
//    return ptag->id;
    return get_ID(lb->data);
//    return ((SHM_TAGS *)lb->data)->id;
}

double getvalue_tag(char *tag)
{
    LIST_BLOCK *lb;
    tag_find=tag;
    lb = list_generic_find(l_shm_tags,handler_find_tag);
    if(lb==NULL)
    {
        log_error("No se encuentra el tag <%s> en SHM",tag);
        return -1;
    }
    return ((SHM_TAGS *)lb->data)->value;
}

double getvalue_id(int id)
{
    LIST_BLOCK *lb;
    lb = list_get_block(l_shm_tags,id);
    if(lb==NULL)
    {
        log_error("No se encuentra el tag id <%i> en SHM",id);
        return -1;
    }
    return ((SHM_TAGS *)lb->data)->value;
}

char *getstring_tag(char *tag)
{
    LIST_BLOCK *lb;
    tag_find=tag;
    lb = list_generic_find(l_shm_tags,handler_find_tag);
    if(lb==NULL)
    {
        log_error("No se encuentra el tag <%s> en SHM",tag);
        return NULL;
    }
    return ((SHM_TAGS *)lb->data)->string;
}

char *getstring_id(int id)
{
    LIST_BLOCK *lb;
    lb = list_get_block(l_shm_tags,id);
    if(lb==NULL)
    {
        log_error("No se encuentra el tag id<%i> en SHM",id);
        return NULL;
    }
    return ((SHM_TAGS *)lb->data)->string;
}

int getstreaky_tag(char *tag)
{
    LIST_BLOCK *lb;
    SHM_TAGS *ptag;
    tag_find=tag;
    lb = list_generic_find(l_shm_tags,handler_find_tag);
    if(lb==NULL)
        return -1;
    ptag = (SHM_TAGS *)lb->data;
    return (ptag->streaky.n1 || ptag->streaky.n2 );
}

void init_shm(void)
{
    log_set_file("/tmp/ex_client_lib.log");
    log_debug_level(255);
    log_logger_level(0);
    l_shm_tags = map_shm(E_SHM_CLIENT_NAME, "rw", 0);
}

//
//void tt()
//{
//    sqlite3 *db;
//    int  ret;
//    sqlite3_stmt *stmt;
//    
//    sprintf(sql,"select t.id,t.tag,t.rwa,t.type,t.value_max,t.value_min from tags as t;");
//    
//    log_msg("sql: <<%s>>",sql);
//    
//    ret = sqlite3_prepare (db, sql, strlen(sql)+1, &stmt, NULL);
//    assert_sql(ret);
//    
//    ret = sqlite3_step(stmt);
//    if (ret == SQLITE_ROW) 
//    {
//        id = sqlite3_column_int(stmt,0);
//        ret = sqlite3_step(stmt);
//    }
//    sqlite3_close(db);
//
//}

//int write_tag(eipc_t *eipc, char *tag, double value)
//{
//    write_tag_t wt;
//    SHM_TAGS *t;
//    if( (t=gettag(tag))!=NULL )
//    {
//        wt.id = t->id;
//        strcpy(wt.tag,tag);
//        wt.value_min = t->value_min;
//        wt.value_max = t->value_max;
//        wt.value = value;
//        return ex_write_tag( eipc, tag, 0, value, 1000 );
//    }    
//    return -1;
//}


#endif

