#include "ex_client_lib.c"
#include "../include/e_common.h"

int main(int argc, char *argv[])
{
//    LIST_BLOCK *ltag;
//    SHM_TAGS *tag;

    init_shm();
    
    log_info("---- Ahora Recorre ----");

//    ltag = list_first_block(l_shm_tags);
//    while(ltag)
//    { 
//        tag = (SHM_TAGS *)ltag->data;
//
//        if(tag->type==type_string)
//            log_debug("xxxx   TAG: id:%04i tag:%-30s rwa:%i type:%i:%i hb:%04i hbs:%04i streaky:%i value:%s",
//                tag->id,
//                tag->tag,
//                tag->rwa,
//                tag->type,
//                tag->len_type,
//                tag->heartbeat,
//                tag->heartbeat_sampler,
//                tag->streaky.val,
//                tag->string);
//        else
//            log_debug("xxxx   TAG: id:%04i tag:%-30s rwa:%i type:%i:%i hb:%04i hbs:%04i streaky:%i value:%6.2f",
//                tag->id,
//                tag->tag,
//                tag->rwa,
//                tag->type,
//                tag->len_type,
//                tag->heartbeat,
//                tag->heartbeat_sampler,
//                tag->streaky.val,
//                tag->value);
//        ltag = list_next_block(l_shm_tags, ltag);
//    }
    
    double val;
    
    eipc_t *eipc = eipc_attach("ex_client_test");
    
    val = getvalue_tag("Contador_MyIp");
    log_info("Contador_MyIp: %f",val);
    log_info("getstreaky_tag Presion3: %i",getstreaky_tag("Presion3"));
    val = getvalue_tag("Presion3");
    log_info("Presion3: %f",val);
    
    val = getvalue_id(2);
    log_info("Presion3: %f",val);
    
    int ret;
    ret = write_tag(eipc, "Presion3", 3.4 );
    log_msg("write_tag Presion3: %i",ret);
    
    ret = write_tag(eipc, "Contador_MyIp", 3.4 );
    log_msg("write_tag Contador_MyIp: %i",ret);
    
    TAG c = gettag("Presion3");
    int i=0;
    
    for (i=0;i<300;i++)
    {
        log_msg("%s:%i %f:%i [%f-%f]",get_NAME(c), get_ID(c), get_VALUE(c), 
                                      get_STREAKY(c), get_MAX(c), get_MIN(c) );
        msleep(1000);
    }
    
    log_msg("fin");
    eipc_deattach(eipc);
    
    return 0;
}