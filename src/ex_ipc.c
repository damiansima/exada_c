//#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>   
#include <time.h>

#include <pthread.h>

#include "../include/socketlib.h"
#include "../include/params.h"
#include "../include/ipc_common.h"
#include "../include/ipc_types.h"

#include "../include/e_common.h"
#include <sqlite3.h>

#include <syslog.h>

#include <sys/stat.h>
#include <errno.h>
#include "../library/logger/include/logger.h"


int query_mq_id=-1;
int reply_mq_id=-1;

char myhostname[128];
char myip[128];

pthread_mutex_t reply_mutex;
pthread_mutex_t sql_mutex;


//#define FILE_LOG "/tmp/eipcd.log"

/// lanza exepcion:
/// quiere decir que responde la query informando del evento (si pide ack).


#define LOGD(a, fmt, args...) log_debug("[%u]: "fmt,(unsigned int)a,##args)
//#define  LOGD(a,b) log_debug(#b"[%i]",a)

typedef struct {
    int mqid; 
    void *msg; 
    long type;
}param;


eipc_shm_control *SHM_CTR;
eipc_shm *SHM;
param_struct params;  
pthread_mutex_t m_mqrcv=PTHREAD_MUTEX_INITIALIZER; 
int max_num_conn=10;
int num_conn=0;
   
  
int tcp_send(int sock,char *buf,int len,int o)
{
   
    return sock;
}
int tcp_rcev(int sock,char *buf,int max,int o)
{
    return sock;
}


int wait_ack_process(eipc_shm *process, int timesleep)
{
    int i;
    timesleep = timesleep>10 ? timesleep/10 : 1;
    for( i=0 ; i<= timesleep ; i++ )
    {
        if(process->query_ack.flag.ack_process)
            return 1;
        usleep(10000);
    }
    return 0;
}

int wait_ack_process_return(eipc_shm *process, int timesleep)
{
    int i;
    timesleep = timesleep>10 ? timesleep/10 : 1;
    process->query_ack.flag.timeout=1;
    for( i=0 ; i<= timesleep ; i++ )
    {
        if(process->query_ack.flag.ack_process_return )
        {
            process->query_ack.flag.timeout=0;
//            return process->query_ack.flag.process_return_true;
            return process->query_return;
        }
        usleep(10000);
    }
//    return process->query_ack.flag.process_return_true;
    return process->query_return;
}

int wait_mq_void(eipc_shm *process, int timesleep)
{
    int i;
    struct msqid_ds buf;
    timesleep = timesleep>10 ? timesleep/10 : 1;
    for( i=0 ; i<= timesleep ; i++ )
    {
        msgctl(process->mqid , IPC_STAT, &buf);
        if(buf.msg_qnum==0 && process->query_ack.flag.busy==0 )
            return 1;
        usleep(10000);
    }
    return 0;
}

void mq_send(eipc_shm *process, eipc_mq *msg)
{
    // si tira error en la cola saca los mensajes que tienen menos tiempo
    int len = MQ_FIXE_LENGTH + msg->length;
    if(msgsnd(process->mqid, (void *)msg, len, MQFLAG) == -1)
    {
        perror("mq_send()::msgsnd() failed !");
    }
    return ;
}



int mq_recv_p(void *d)
{
    param *parm=d;
    int r;
    pthread_mutex_unlock( &reply_mutex );
    if( (r=msgrcv( parm->mqid , parm->msg, BUFSIZ, parm->type, 0 ))  == -1)
    {
        fprintf(stderr, "failed to receive: \n");
        return -1;
    }
    return r;
}

int mq_recv(int mqid, eipc_mq *msg, long type)
{
    int r;
    if( (r=msgrcv( mqid , msg, sizeof(eipc_mq)-sizeof(long), type, 0 ))  == -1)
    {
        fprintf(stderr, "failed to receive: \n");
        return -1;
    }
    return r;
}


// hilo para chequera continuamente los procesos en ejecucion
void loop_check_pid_shm()
{
    int i;
    struct stat sts;
    char prcs[100];
    eipc_shm *shm;
    while(1)
    {
        shm = &SHM[0];
        ipc_shm_lock(SHM_CTR);
        for(i=0;i<SHM_CTR->max_process;i++)
        {
            
            sprintf( prcs, "/proc/%i", shm->pid);
            if( shm->pid > 0 && stat( prcs, &sts) == -1 && errno == ENOENT)
            {
                LOGD(0,"shm->process [%s] killed ! ",shm->process);
                shm->pid=0;
            }
            shm++;
        }
        ipc_shm_unlock(SHM_CTR);
        sleep(1);
    }
}


void parser_msg(eipc_mq *msg, eipc_mq_reply *msg_reply)
{
    eipc_mq_reply mq_reply;
    long int type_reply = abs((long)pthread_self()/2);
    char command[1000];
    timems_t timeout;
    eipc_shm *process_to;
    
    process_to = get_process(msg->to_p, SHM_CTR);
    
    log_info("------parser_msg() from:%s:%s to:%s:%s",
            msg->from_h,msg->from_p,msg->to_h,msg->to_p);
    
    msg->time = msg->timedout>0 ? timems() + msg->timedout : 0;
    timeout = msg->timedout; 
    
    msg_reply->reply.value=0;
    msg_reply->reply.flag.alive=0;
    msg_reply->reply.flag.timeout=1;
    msg_reply->reply.flag.ack_host=1;
    msg_reply->reply.flag.ack_process=0;
    msg_reply->reply.flag.process_return_true=0;
    msg_reply->reply_return=-9999;
    if ( chek_process_alive(process_to->pid) )
    {
        log_debug("parser_msg() proceso vivo");
        msg_reply->reply.flag.alive=1;
        if(msg->control & CTR_SIGNAL)
        {
            msg_reply->reply.flag.ack_process=1;
            sprintf( command, "kill -%d %d &", msg->info, process_to->pid);
            log_debug("signal: %s", command);
            system(command);
        }
        else if( msg->control & (CTR_EVENT|CTR_MSG|EX_WRT_TAG_VAL) )
        {
            log_debug("parser_msg() event || msg");
            msg->type_reply = type_reply;
            
            if( timeout < 0 || timeout > 120000 )
            {
                log_error("timedout error !");
            }
            else if( msg->control & (ACK_PROCESS_RETURN|ACK_PROCESS) ) 
            {
                mq_send(process_to, msg);
                // necesito algo mas para ack_process y estar seguro que recibio
                msg_reply->reply.flag.ack_process = 1;
                param p;
                p.mqid = reply_mq_id;
                p.msg = &mq_reply;
                p.type = type_reply;
                pthread_mutex_lock( &reply_mutex );
                int res = thread_create_timeout( mq_recv_p, (int)timeout, &p );
                if( res != -1 )
                {
                    LOGD(pthread_self(),"reply ok %lld ", timeout );
                    msg_reply->reply.value = mq_reply.reply.value;
                    msg_reply->reply_return = mq_reply.reply_return;
                    LOGD(pthread_self(),"msg_reply->reply_return: %i ", msg_reply->reply_return );
                }
                else
                {
                    LOGD(pthread_self(),"timedout: %lld", timeout );
                }                
            }
            else
            {
                 mq_send(process_to, msg);
            }
        }
    }
    else
    {
        log_debug("parser_msg(): proceso muerto");
        if( msg->control & ACK_TIMEOUT )
        {
            // -chequeo si hay colas para eliminar
            if( timeout < 0 || timeout > 120000 )
            {
                log_warning("timedout error !");
            }
            else if( process_to->mqid > 0 )
            {
                log_debug("parser_msg(): envia el msg");
                mq_send(process_to, msg);
                msg_reply->reply.flag.timeout = 0;
            }
        }
    }
    msg_reply->reply.flag.ready = 1;
}


void send_local_mq(void *p)
{
    eipc_mq *msg=(eipc_mq *)p;
    eipc_mq_reply msg_reply;
//    eipc_shm *process_to; 
    eipc_shm *process_from; 
    LOGD(pthread_self(),"xx send_local_mq()");
         
//    memcpy( &msg, p, sizeof(eipc_mq) );    
//    pthread_mutex_unlock(&m_mqrcv);
    
//    process_to = get_process(msg.to_p, SHM_CTR);
    process_from = get_process(msg->from_p, SHM_CTR);
    pthread_mutex_lock(&process_from->mutex_send);
    
    parser_msg( msg, &msg_reply );
    free(msg);
    
    process_from->reply_ack.value = msg_reply.reply.value;
    process_from->reply_return = msg_reply.reply_return;
    process_from->reply_ack.flag.ready = 1;
    pthread_mutex_unlock(&process_from->mutex_send);
    pthread_detach(pthread_self());
}



/*      void send_tcp(void *buf)
 * 
 * - recive el msg de la quueue para enviar por tcp
 * - lanza un hilo:
 *      - envia el mensaje
 *      - queda esperando la respuesta o timedout
 *      - responde en la queue y shm correspondiente
 *        tiene los datos en el puntero de la shm
 *
 */
void send_tcp(void *buf)
{
    int res;
    int sock;
    int timemsg;
    eipc_mq *msg=buf;
    eipc_mq_reply msg_reply;
    eipc_shm *process_from;
    
    int length = sizeof(long) + MQ_FIXE_LENGTH + msg->length;
    
//    pthread_mutex_unlock(&m_mqrcv);
    
    LOGD(0,"send_tcp()");
    process_from = get_process(msg->from_p, SHM_CTR);
    pthread_mutex_lock(&process_from->mutex_send);
    
    strcpy(msg->from_h,myhostname);
    log_info("send_tcp() from:%s:%s to:%s:%s",
            msg->from_h,msg->from_p,msg->to_h,msg->to_p);
    
    process_from->reply_ack.value = 0;
    process_from->reply_ack.flag.ready = 0;
    msg->time = msg->timedout>0 ? timems() + msg->timedout : 0;
    timemsg = msg->timedout;
    
    if(timemsg>0 || msg->time == 0 )
    {
        sock = connect_tcp(msg->to_h,params.PORT_TCP);
        LOGD(sock, "connect_tcp(%s,%i)",msg->to_h,params.PORT_TCP);
        
        res = sendall( sock, (char *)msg, length);
        if(res==0)
        {
            process_from->reply_ack.flag.send_tcp = 1; 
        }
        // espera la respuesta si es necesario
        if(  msg->control & CTR_ACK )
        {
            if( timemsg>0 && res == 0 )
            {
                res = readall( sock, (char *)&msg_reply, sizeof(msg_reply), timemsg );
                // setea en shm el ack
                if(res>0)
                {
                    // setea en ok
                    process_from->reply_ack.value = msg_reply.reply.value;
                    process_from->reply_return = msg_reply.reply_return;
                    log_debug("process_from->reply_return: %i",process_from->reply_return);
                    process_from->reply_ack.flag.ack_host = 1;
                }
                else
                {
                    log_warning("timemsg res: %i",res);
                    process_from->reply_return = -9999;
                    process_from->reply_ack.flag.ack_host = 0;
                    process_from->reply_ack.flag.ack_process = 0;
                    process_from->reply_ack.flag.ack_process_return = 0;
                    process_from->reply_ack.flag.timeout = 1;
                }
            }
            else
            {
                // respondo en shm que tuvo timedout
                process_from->reply_ack.flag.timeout = 0;
            }
        }
        close(sock);
    }
    free(msg);
    process_from->reply_ack.flag.ready = 1;
    pthread_mutex_unlock(&process_from->mutex_send);
    pthread_detach(pthread_self());
}


void parser_command(void *buf)
{
    int res;
    int sock;
    int timemsg;
    eipc_mq *msg=buf;
    eipc_mq_reply msg_reply;
    eipc_shm *process_from;
    write_tag_t *wt = (write_tag_t *)msg->msg;
    
    
    sqlite3 *db;
    char sql[1024];
    int  ret;
    sqlite3_stmt *stmt;
    
    LOGD(0,"parser_command()");
    process_from = get_process(msg->from_p, SHM_CTR);
    pthread_mutex_lock(&process_from->mutex_send);
    
//    pthread_mutex_unlock(&sql_mutex);
    ret = sqlite3_open("../cfg/DBConfig.db", &db);
    assert_msg(ret==SQLITE_OK);
    if(wt->id!=0)
    {
        sprintf(sql,"select d.address,t.register,t.rwa,s.scan,s.host "\
                "from tags as t join scans as s join devices as d "\
                "where s.device=t.device and d.device=t.device and "
                "t.id=%i;",wt->id);
    }
    else if(strlen(wt->tag)>1)
    {
        sprintf(sql,"select d.address,t.register,t.rwa,s.scan,s.host "\
                "from tags as t join scans as s join devices as d "\
                "where s.device=t.device and d.device=t.device and "
                "t.tag='%s';",wt->tag);
    }
    ret = sqlite3_prepare (db, sql, strlen(sql)+1, &stmt, NULL);
    assert_msg(ret==SQLITE_OK);
    
    ret = sqlite3_step(stmt);
    if (ret == SQLITE_ROW) 
    {
        wt->_addres=sqlite3_column_int(stmt,0);
        wt->_register=sqlite3_column_int(stmt,1);
        sprintf(wt->_rwa,"%s",sqlite3_column_text(stmt,2));
        sprintf(msg->to_p,"scan_%s",sqlite3_column_text(stmt,3));
        sprintf(msg->to_h,"%s",sqlite3_column_text(stmt,4));
        sqlite3_close(db);
    }
    else
    {
        sqlite3_close(db);
        log_debug("parser_command() sql fail ! ");
        process_from->reply_return = -9998;
        process_from->reply_ack.flag.ack_host = 0;
        process_from->reply_ack.flag.ack_process = 0;
        process_from->reply_ack.flag.ack_process_return = 0;
        process_from->reply_ack.flag.timeout = 0;
        free(msg);
        process_from->reply_ack.flag.ready = 1;
        pthread_mutex_unlock(&process_from->mutex_send);
        pthread_detach(pthread_self());
        return;
    }
    
    log_info("Tag:%s ID:%i addres:%i register:%i value:%f",
            wt->tag,wt->id,wt->_addres,wt->_register,wt->value);
    
    int length = sizeof(long) + MQ_FIXE_LENGTH + msg->length;
    
    strcpy(msg->from_h,myhostname);
    log_info("parser_command() from:%s:%s to:%s:%s",
            msg->from_h,msg->from_p,msg->to_h,msg->to_p);
    
    process_from->reply_ack.value = 0;
    process_from->reply_ack.flag.ready = 0;
    msg->time = msg->timedout>0 ? timems() + msg->timedout : 0;
    timemsg = msg->timedout;
    
    if(timemsg>0 || msg->time == 0 )
    {
        sock = connect_tcp(msg->to_h,params.PORT_TCP);
        LOGD(sock, "connect_tcp(%s,%i)",msg->to_h,params.PORT_TCP);
        
        res = sendall( sock, (char *)msg, length);
        if(res==0)
        {
            process_from->reply_ack.flag.send_tcp = 1; 
        }
        // espera la respuesta si es necesario
        if(  msg->control & CTR_ACK )
        {
            if( timemsg>0 && res == 0 )
            {
                res = readall( sock, (char *)&msg_reply, sizeof(msg_reply), timemsg );
                // setea en shm el ack
                if(res>0)
                {
                    // setea en ok
                    process_from->reply_ack.value = msg_reply.reply.value;
                    process_from->reply_return = msg_reply.reply_return;
                    process_from->reply_ack.flag.ack_host = 1;
                    log_info("Tag:%s ID:%i return:%i",
                        wt->tag,wt->id,process_from->reply_return);
                }
                else
                {
                    log_warning("timemsg res: %i",res);
                    process_from->reply_return = -9999;
                    process_from->reply_ack.flag.ack_host = 0;
                    process_from->reply_ack.flag.ack_process = 0;
                    process_from->reply_ack.flag.ack_process_return = 0;
                    process_from->reply_ack.flag.timeout = 1;
                }
            }
            else
            {
                // respondo en shm que tuvo timedout
                process_from->reply_ack.flag.timeout = 0;
            }
        }
        close(sock);
    }
    free(msg);
    process_from->reply_ack.flag.ready = 1;
    pthread_mutex_unlock(&process_from->mutex_send);
    pthread_detach(pthread_self());
}



/*
 * - msgrcv(mq,buf,MAX,0)
 * - analiza si destino es local o tcp
 * - si local:
 *      - si el proces esta corriendo ejecuta-encola
 *      - si proceso no corre:
 *          - chequeo si hay colas con tiempo para descartar: elimina
 *          - si tiene time alive:
 *              - encola. controla si la queue esta llena:
 *                  - lanza exepcion
 *
 * - si tcp:
 *      - encola para ::send_tcp()
 *      - o lanza un hilo ::send_tcp(msg,len,p_msg)
 *
 *  WARNING: todos los encolas y chequeos van con un shm para count y mayor time alive.
 */
void loop_mq_recv(void)
{
    LOGD(pthread_self(),"loop_mq_recv() start !");
    eipc_mq msg;
    eipc_mq *m;
    int err;
    
    query_mq_id = mq_open(params.MQ_QUERY_NAME,"rw");
    SHM_CTR->query_mq_id = query_mq_id;
    LOGD(pthread_self(),"SHM_CTR->query_mq_id:%i",SHM_CTR->query_mq_id);
    LOGD(pthread_self(),"MQ_QUERY_NAME:%s",params.MQ_QUERY_NAME );
    
    while(1)
    {
        err = mq_recv(query_mq_id, &msg, 0);
        
        if ( msg.control & EX_WRT_TAG_VAL )
        {
            LOGD(pthread_self(),"call EX_WRT_TAG_VAL parser_command()");
            // send_tcp(&buf);
            pthread_t pth;
            m = malloc(sizeof(eipc_mq));
            memcpy(m,&msg,sizeof(eipc_mq));
            err = pthread_create (&pth, NULL, (void *)&parser_command, m);
            assert_msg(err==0);
        }
        else if ( msg.control & CTR_LOCAL )
        {
            LOGD(pthread_self(),"call send_local_mq()");
            // send_tcp(&buf);
            pthread_t pth;
            m = malloc(sizeof(eipc_mq));
            memcpy(m,&msg,sizeof(eipc_mq));
            err = pthread_create (&pth, NULL, (void *)&send_local_mq, m);
            assert_msg(err==0);
        }
        else if ( msg.control & CTR_TCP )
        {   
            LOGD(pthread_self(),"call send_tcp()");
            // send_tcp(&buf);
            pthread_t pth;
            m = malloc(sizeof(eipc_mq));
            memcpy(m,&msg,sizeof(eipc_mq));
            err = pthread_create (&pth, NULL, (void *)&send_tcp, m);
            assert_msg(err==0);
        }
        err=err;
    }
    LOGD(pthread_self(),"loop_mq_recv() stop :( !");
    pthread_detach(pthread_self());
}

void loop_mq_recv_reply(void)
{
    LOGD(pthread_self(),"loop_mq_recv_reply() start !");
    eipc_mq msg;
    
    reply_mq_id = mq_open(params.MQ_REPLY_NAME,"rw");
    SHM_CTR->reply_mq_id = reply_mq_id;
    LOGD(pthread_self(),"SHM_CTR->reply_mq_id:%i",SHM_CTR->reply_mq_id);
    
    while(1)
    {
        // bucle para leer reply_mq_id perdidas y eliminar de la queue
        pthread_mutex_lock( &reply_mutex );
        msleep(10);
        msgrcv( reply_mq_id , &msg, BUFSIZ, 0, IPC_NOWAIT );
        pthread_mutex_unlock( &reply_mutex );
        sleep(1);
    }
    LOGD(pthread_self(),"loop_mq_recv_reply() stop :( !");
    pthread_detach(pthread_self());
}




// void reply_tcp(char *host, char *process, int response)
// zmq responde a la ultima query
void send_mq_reply_tcp( void *p)
{
    
    int n;
    int sock=*((int *)p);
    eipc_mq msg;
    eipc_mq_reply msg_reply;
    
    
    LOGD(sock,"send_mq_reply_tcp()");
    
    n = readall( sock, (char *)&msg, sizeof(long) + MQ_FIXE_LENGTH, 200);
    if (n<0)
    {
        goto end_s_mq_r_t;
    }
    if(msg.length>0)
    {
        n = readall( sock, (char *)&msg.msg, msg.length, 200);
        if (n<=0)
        {
            goto end_s_mq_r_t;
        }
    }
    parser_msg( &msg, &msg_reply );
    
    sendall( sock, (char *)&msg_reply, sizeof(eipc_mq_reply));
    
end_s_mq_r_t:    
    close(sock);
    num_conn--;
    pthread_detach(pthread_self());
}

/*
 * - rcev(sock,buf,max,0)
 *
 * - debe ser local:
 *      - si el proces esta corriendo ejecuta-encola
 *      - si proceso no corre:
 *          - chequeo si hay colas con tiempo para descartar: elimina
 *          - si tiene time alive: encola
 *
 *      - responde tcp informando el estado.
 *          - ack
 *          - time alive
 *          - vacio
 *      :: reply_tcp()
 *
 */
void loop_tcp_recv(void)
{
    int e=0;
    int sock,conn;
    socklen_t clen = sizeof(struct sockaddr_in);
    struct sockaddr_in client;
    pthread_t ptconn;
    
    LOGD(pthread_self(),"loop_tcp_recv() start !");
    sock = bind_tcp(params.PORT_TCP,max_num_conn);
    LOGD(sock,"bind_tcp() socket !");
    
    while(1)
    {
        // Se bloquea con las conexiones
        conn = accept(sock, (struct sockaddr *)&client, &clen);
        num_conn++;
        LOGD(sock, "Conexion desde: %s:%d, connexiones disponibles: %d/%d", 
                inet_ntoa(client.sin_addr),ntohs(client.sin_port),
                num_conn,max_num_conn );
        
        LOGD(sock, "call send_mq_reply_tcp(%i)",conn);
        e = pthread_create (&ptconn, NULL, (void *)&send_mq_reply_tcp,&conn);
        if (e)
        {
            LOGD(sock,"no se puede crar thread para atender conneccion");
            close(conn);
        }
    }
    pthread_detach(pthread_self());
}



int main(int argc, char *argv[])
{
    pthread_t pth_mq_recv;
    pthread_t pth_tcp_recv;
    pthread_t pth_mq_recv_reply;
    
    read_params(&params);
    
    log_set_file("/tmp/eipcd.log");
    log_debug_level(255);
    log_logger_level(255);
    
            
    
    LOGD(0,"Inicia %s",params.PROGRAM_NAME);
    
    SHM_CTR = (eipc_shm_control *)map_shm(EIPC_SHM_NAME, "rwt",
        params.MAX_PROCESSES*sizeof(eipc_shm) + sizeof(eipc_shm_control)); 
    SHM = (eipc_shm *)(SHM_CTR +1);   
    
    // inicializa shm
    SHM_CTR->max_process=params.MAX_PROCESSES;
    ipc_shm_unlock(SHM_CTR);
    
    int e=0;
    
    gethostname(myhostname,128);
    hostname_to_ip(myhostname,myip);
    log_info("hostname ip<%s::%s>",myhostname,myip);
    
    
    e += pthread_create (&pth_mq_recv, NULL, (void *)&loop_mq_recv, NULL);
    e += pthread_create (&pth_tcp_recv, NULL, (void *)&loop_tcp_recv, NULL);
    e += pthread_create (&pth_mq_recv_reply, NULL, (void *)&loop_mq_recv_reply, NULL);
    if(e)
    {
        pthread_cancel(pth_mq_recv);
        pthread_cancel(pth_tcp_recv);
        pthread_cancel(pth_mq_recv_reply);
        LOGD(0,"main(): error al crear thread");
    }
    
    loop_check_pid_shm();
//    pthread_join(pth_mq_recv,0);
    return 0;
    
}
