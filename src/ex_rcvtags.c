#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <strings.h>
#include <sqlite3.h>

#include "../include/utils.h"
#include "../include/lista_continua.h"
#include "../include/eipcc.h"
#include "../include/socketlib.h"






eipc_t *eipc;
LIST_t *l_shm_tags;
LIST_BLOCK *l_tags;
SHM_TAGS_CTR *ctr;
SHM_TAGS *ptag;


int init_list_tags()
{

    sqlite3 *db;
    int  ret;
    sqlite3_stmt *stmt;
    char sql[1024];
    char *type;
    char *rwa;
    int id;
    

    ret = sqlite3_open("../cfg/DBConfig.db", &db);
    
    sprintf(sql,"select t.id,t.tag,t.rwa,t.type,t.value_max,t.value_min "\
            "from tags as t;");
    
    log_msg("sql: <<%s>>",sql);
    
    
    l_shm_tags = list_alloc( UDP_MAX_BYTES_LENGTH );
    list_add_alloc(&l_shm_tags,sizeof(SHM_TAGS_CTR) , -1); // agrego un lista para SHM_TAGS_CTR

    ret = sqlite3_prepare (db, sql, strlen(sql)+1, &stmt, NULL);
    assert_sql(ret);
    
    int len;
    SHM_TAGS tag;
    ptag = &tag;
    ret = sqlite3_step(stmt);
    while (ret == SQLITE_ROW) 
    {
        id = sqlite3_column_int(stmt,0);
        
//        log_debug("list_add_alloc(&l_shm_tags, sizeof(SHM_TAGS), %i)",id);
//        l_tags = list_add_alloc(&l_shm_tags,sizeof(SHM_TAGS) , id);
//        ptag = (SHM_TAGS *)l_tags->data;
        
        ptag->id = sqlite3_column_int(stmt,0);
        strcpy(ptag->tag,(char *)sqlite3_column_text(stmt,1));
        ptag->rwa = 0;
        rwa = (char *)sqlite3_column_text(stmt,2);
        for( ; *rwa!=0;rwa++)
        {
            if ('r'==*rwa )
                ptag->rwa +=4;  
            if ('w'==*rwa )
                ptag->rwa +=2;  
            if ('a'==*rwa )
                ptag->rwa +=1;  
        }
        type = (char *)sqlite3_column_text(stmt,3);
        if(strncasecmp(type,"STRING",6) == 0)
        {
            ptag->type=type_string;ptag->len_type=atoi(&type[6]);
        }
        else if(strncasecmp(type,"INT16",5) == 0)
        {
            ptag->type=type_int16;ptag->len_type=2;
        }
        else if(strncasecmp(type,"INT32",5) == 0)
        {
            ptag->type=type_int32;ptag->len_type=4;
        }
        else if(strncasecmp(type,"FLOAT",5) == 0)
        {
            ptag->type=typte_float;ptag->len_type=4;
            
        }
        else if(strncasecmp(type,"DOUBLE",5) == 0)
        {
            ptag->type=typte_double;ptag->len_type=4;
        }
        ptag->value_max = (double)sqlite3_column_double(stmt,4);
        ptag->value_min = (double)sqlite3_column_double(stmt,5);
        
        ptag->heartbeat = 0;
        ptag->heartbeat_sampler = 0;
        ptag->streaky.val = 0;
        
        log_info("TAG: id:%03i tag:%40s rwa:%i type:%i:%i",
            ptag->id,
            ptag->tag,
            ptag->rwa,
            ptag->type,
            ptag->len_type);
        
        len = ptag->len_type > 8 ? (ptag->len_type-8) : 0;
        log_debug("list_add_alloc(&l_shm_tags, sizeof(SHM_TAGS), %i)",id);
        l_tags = list_add_alloc(&l_shm_tags, len + sizeof(SHM_TAGS) , id);
        memcpy( l_tags->data, ptag, sizeof(SHM_TAGS) );
        
        ret = sqlite3_step(stmt);
    }
    sqlite3_close(db);
    
    
    // mueve los datos a la memoria compartida creada
    void *pshm = map_shm(E_SHM_CLIENT_NAME, "rwt", length_memory(l_shm_tags));
    assert_return(pshm>0);
    memcpy(pshm,l_shm_tags,length_memory(l_shm_tags));
    free(l_shm_tags);
    l_shm_tags = pshm;
    
    return SQLITE_OK; // SQLITE_OK==0
}



void loop_check_tags(void) 
{
    LIST_BLOCK *ltag;
    SHM_TAGS *tag;
    log_msg("loop_check_tags()");
    
    ltag = list_first_block(l_shm_tags); // la primer lista es de control
    ctr = (SHM_TAGS_CTR *)ltag->data;
    ctr->num_tags = l_shm_tags->ctr.count_list-1;
        
    while (1)
    {
        ltag = list_first_block(l_shm_tags); // salteo el ctr
        ltag = list_next_block(l_shm_tags, ltag);
        while(ltag)
        { 
            tag = (SHM_TAGS *)ltag->data;
            
            tag->streaky.n1 = tag->heartbeat ? 0 : 1;
            tag->streaky.n2 = tag->heartbeat_sampler ? 0 : 1;
            tag->streaky.n3 = tag->value_max < tag->value || \
                                   tag->value_min > tag->value ? 1 : 0;
            
            if (tag->heartbeat_sampler > 0 ) tag->heartbeat_sampler-=1;
            
            if(tag->type==type_string)
                log_debug("xxxx   TAG: id:%04i tag:%-30s rwa:%i type:%i:%i hb:%04i hbs:%04i streaky:%i value:%s",
                    tag->id,
                    tag->tag,
                    tag->rwa,
                    tag->type,
                    tag->len_type,
                    tag->heartbeat,
                    tag->heartbeat_sampler,
                    tag->streaky.val,
                    tag->string);
            else
                log_debug("xxxx   TAG: id:%04i tag:%-30s rwa:%i type:%i:%i hb:%04i hbs:%04i streaky:%i value:%6.2f",
                    tag->id,
                    tag->tag,
                    tag->rwa,
                    tag->type,
                    tag->len_type,
                    tag->heartbeat,
                    tag->heartbeat_sampler,
                    tag->streaky.val,
                    tag->value);
            ltag = list_next_block(l_shm_tags, ltag);
        }
        log_debug(" - - - - ");
        ctr->time_sampler = timems();
        log_info("ultima actualizacion %lld   cantidad de tags:%i",ctr->time_sampler,ctr->num_tags);
        msleep(TIME_SEND);
    }
} 

int main(int argc, char* argv[])
{
    int ret;
    
    log_set_file("/tmp/ex_rcvtags.log");
    log_debug_level(255);
    log_logger_level(0);
    
    eipc = eipc_attach("ex_rcvtags");
    assert_return((int)eipc != -1);
    
    init_list_tags();
    
    l_tags = list_first_block(l_shm_tags);
    log_info("---- Ahora Recorre ----");
    while(l_tags)
    { 
        ptag = (SHM_TAGS *)l_tags->data;
        log_info("TAG: id:%i tag:%s rwa:%i type:%i:%i ctr.length:%i",
            ptag->id,
            ptag->tag,
            ptag->rwa,
            ptag->type,
            ptag->len_type,
            length_memory(l_shm_tags));
        l_tags = list_next_block(l_shm_tags, l_tags);
    }
    
    log_info("---- CHAU ----");
    
    
    
    int sock_sub,n;
    struct sockaddr_in from;
    struct sockaddr_in addr;
    socklen_t len;
    
    sock_sub = create_sock_udp_broadcast(0);
    
    memset(&addr, 0, sizeof(addr));
    addr.sin_family      = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port        = htons(EXADA_PORT_SUS);
    len                  = sizeof(struct sockaddr_in);
    
    ret = bind(sock_sub, (struct sockaddr*) &addr, len);
            
    assert_errno( ret == 0);
    
    LIST_t *l_sampler = list_alloc( UDP_MAX_BYTES_LENGTH );
    LIST_BLOCK *lb;
    SAMPLER_TAGS *sampler_tags;
    SAMPLER_CTR *sampler_ctr;
    
    pthread_t pth;
    int e = pthread_create (&pth, NULL, (void *)&loop_check_tags, NULL);
    if (e)
    {
        log_error("thread loop_check_tags fail !");
    }
    
    
    while (1)
    {
        n = recvfrom( sock_sub, l_sampler, UDP_MAX_BYTES_LENGTH, 0, (struct sockaddr*) &from, &len );

        if (n < 0)
        {
            log_error("recvfrom() fail !");
        }
        else
        {
            log_debug("---> recvfrom(): %s::%i len:%i", inet_ntoa(from.sin_addr), ntohs(from.sin_port), n);

            lb = list_first_block(l_sampler);
            // primero esta el SAMPLER_CTR despues los SAMPLER_TAGS
            sampler_ctr = (SAMPLER_CTR *)lb->data;
            
            switch(sampler_ctr->type)
            {
                case update_tags:
                    lb = list_next_block(l_sampler, lb);
                    while(lb)
                    {
                        sampler_tags = (SAMPLER_TAGS *)lb->data;
                        
                        l_tags = list_get_block(l_shm_tags,sampler_tags->id);
                        ptag = (SHM_TAGS *)l_tags->data;
                        if(ptag->type==type_string)
                            memcpy( ptag->string, sampler_tags->string, ptag->len_type );
                        else
                            ptag->value = sampler_tags->value;
                        ptag->heartbeat = sampler_tags->heartbeat;
                        ptag->heartbeat_sampler = sampler_ctr->heartbeat;
                        
//                        log_debug("TAG: id:%03i tag:%-30s rwa:%i type:%i:%i hb:%03i hbs:%03i value:%f",
//                            ptag->id,
//                            ptag->tag,
//                            ptag->rwa,
//                            ptag->type,
//                            ptag->len_type,
//                            ptag->heartbeat,
//                            ptag->heartbeat_sampler,
//                            ptag->value.float64);
//                        log_msg("time: %lld sampler_tags:: id(%03i)  type(%i %i) heartbeat(%03i)",
//                            sampler_ctr->time_sampler,
//                            sampler_tags->id,
//                            sampler_tags->type,sampler_tags->len_type,
//                            sampler_tags->heartbeat);

                        lb = list_next_block(l_sampler, lb);
                    }
                    break;
                    
                default:
                    log_msg("case defautl sampler_ctr->type:%i",sampler_ctr->type);
                    break;
            }
        }
    } 
    return 0;
    
}
