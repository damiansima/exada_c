/*
 * 
 */

#include "../include/utils.h"
#include "../include/lista_continua.h"
#include "../include/eipcc.h"
#include "../include/socketlib.h"

#include <stdlib.h>
#include <sqlite3.h>
#include <string.h>
#include <unistd.h>
#include <strings.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>

char scan_name[96]={"Balanza_XX"};
char name_attach[128];
int tag_length=0;
eipc_t *eipc;

LIST_BLOCK *ptr_register(LIST_t *lshm, int addres, int reg)
{
    SCAN_BLOCK *pb;
    LIST_BLOCK *lb;

    lb = list_first_block(lshm);
    while(lb)
    {
        pb = (SCAN_BLOCK *)lb->data;

        if( addres==pb->addres && reg>=pb->reg_start && reg<=pb->reg_end )
            break;

        lb = list_next_block(lshm, lb);
    }
    return lb;
}

int main(int argc, char* argv[])
{
    log_set_file("/tmp/esampler.log");
    log_debug_level(255);
    log_logger_level(0);
    
    
    if (argc < 2)
    {
        log_error("Usage:  %s <Scan Name> \n", argv[0]);
    }
    else
        strcpy(scan_name,argv[1]);
    
    sprintf( name_attach, SAMPLER_NAME_FMT, scan_name );
    log_msg("Usa scan_name=%s", scan_name);
    
    eipc = eipc_attach(name_attach);
    assert_return((int)eipc != -1);
    
    
    sqlite3 *db;
    int  ret;
    sqlite3_stmt *stmt;
    char sql[1024];

    ret = sqlite3_open("../cfg/DBConfig.db", &db);
    assert_sql(ret);
    
    char hostname[128];
    char ip[128];

    gethostname(hostname,128);
    hostname_to_ip(hostname,ip);
    log_info("hostname ip<%s::%s>",hostname,ip);
    
    LIST_t *l_sampler = list_alloc( UDP_MIN_BYTES_LENGTH );
    LIST_t *l_scan = list_alloc( 64 );
    LIST_BLOCK *lb_samp;
    LIST_BLOCK *lb_s;
    SAMPLER_TAGS *sampler_tags;
    s_TAG *scan_tags;
    int id;
    int len;
  
    
    SCAN_CTR *ctr = (SCAN_CTR *)map_shm( scan_name, "r", 0);  
    LIST_t *lshm = (LIST_t *)(ctr+1);
    SCAN_BLOCK *pb;
    LIST_BLOCK *lb;
    
    sprintf(sql,"select t.id,t.tag,t.type,d.address,t.register"\
            " from scans as s join devices as d join tags as t"\
            " where d.device=s.device and t.device=s.device and"\
            " rwa like '%%r%%' and s.scan='%s' and s.host='%s' order by t.id;",scan_name,ip);
    
    log_msg("sql: <<%s>>",sql);
    
    ret = sqlite3_prepare (db, sql, strlen(sql)+1, &stmt, NULL);
    assert_sql(ret);
    
    char *type;
        
    // el sampler empieza con el control con id=-1
    lb_samp = list_add_alloc(&l_sampler,sizeof(SAMPLER_CTR) , -1);
    SAMPLER_CTR *sampler_ctr = (SAMPLER_CTR *)lb_samp->data;
    
    ret = sqlite3_step(stmt);
    while (ret == SQLITE_ROW) 
    {
        id = sqlite3_column_int(stmt,0);
        
        log_debug("list_add_alloc(&l_scan, sizeof(s_TAG), %i)",id);
        lb_s = list_add_alloc(&l_scan, sizeof(s_TAG), id);
        scan_tags = (s_TAG *)lb_s->data;
        
        scan_tags->id = sqlite3_column_int(stmt,0);
        scan_tags->_addres = sqlite3_column_int(stmt,3);
        scan_tags->_register = sqlite3_column_int(stmt,4);
        
        type = (char *)sqlite3_column_text(stmt,2);
        if(strncasecmp(type,"STRING",6) == 0)
        {
            scan_tags->type=type_string;scan_tags->len_type=atoi(&type[6]);
        }
        else if(strncasecmp(type,"INT16",5) == 0)
        {
            scan_tags->type=type_int16;scan_tags->len_type=2;
        }
        else if(strncasecmp(type,"INT32",5) == 0)
        {
            scan_tags->type=type_int32;scan_tags->len_type=4;
        }
        else if(strncasecmp(type,"FLOAT",5) == 0)
        {
            scan_tags->type=typte_float;scan_tags->len_type=4;
        }
        
        else if(strncasecmp(type,"DOUBLE",5) == 0)
        {
            scan_tags->type=typte_double;scan_tags->len_type=8;
        }
        
        lb = ptr_register(lshm, scan_tags->_addres, scan_tags->_register);
        if (lb)
        {
            pb = (SCAN_BLOCK *)lb->data;
            printf("[%s] addres=%05i  start=%05i  end=%05i len=%05i\n", 
                pb->name, pb->addres, pb->reg_start, pb->reg_end, pb->reg_length);
            
            scan_tags->ptr =(void *) &(pb->data[ scan_tags->_register - pb->reg_start ] ); 
            scan_tags->update_time = &pb->update_time;
            
            log_info("agregado TAG: t.id(%i),t.tag(%s),t.type(%s),d.address(%i),t.register(%i):: %i:%i",
            scan_tags->id,
            sqlite3_column_text(stmt,1),
            sqlite3_column_text(stmt,2),
            scan_tags->_addres,
            scan_tags->_register,
            scan_tags->type,scan_tags->len_type);
        
            len = scan_tags->len_type > 8 ? (scan_tags->len_type-8) : 0;
            log_debug("list_add_alloc(&l_sampler, %i + sizeof(SAMPLER_TAGS) , %i);",len,id);
            lb_samp = list_add_alloc(&l_sampler, len + sizeof(SAMPLER_TAGS) , id);
            sampler_tags = (SAMPLER_TAGS *)lb_samp->data;

            sampler_tags->id=id;
            sampler_tags->type=scan_tags->type;
            sampler_tags->len_type=scan_tags->len_type;
            sampler_tags->heartbeat=0;
            
        }
        else
        {
            log_warning("El registro %i para la direccion %i no se encontro",
                    scan_tags->_register, scan_tags->_addres);
//            scan_tags->ptr = (void *)0; 
//            scan_tags->update_time = (void *)0; 
            list_pop(l_scan);
        }
        
        ret = sqlite3_step(stmt);
    }
    sqlite3_close(db);
    
    // fixme: eliminar esto de la lista_continua
    sampler_ctr->length = length_memory(l_sampler); 
    sampler_ctr->heartbeat = (TIME_STREAKY/TIME_SEND)+1;
    sampler_ctr->type = update_tags;
    
    int sock_pub,n;
    struct sockaddr_in to;
    
    sock_pub = create_sock_udp_broadcast(1);
    len = sizeof(struct sockaddr_in);
    memset(&to,0,len);
    to.sin_family=AF_INET;
    // to.sin_addr.s_addr=INADDR_ANY;
    to.sin_addr.s_addr = inet_addr("255.255.255.255"); // envia en broadcast
    to.sin_port=htons(EXADA_PORT_PUB);    
    
    log_info("sampler count %i",l_sampler->ctr.count_list);
    log_info("scan count %i",l_scan ->ctr.count_list);
    
    timems_t update_time;
    int16_t vi16;
    int32_t vi32;
    float_t vf32;
    double_t vf64;
    
    while(1)
    {
        lb_samp = list_first_block(l_sampler);
        // se saltea sampler_ctr
        lb_samp = list_next_block(l_sampler, lb_samp);
        
        lb_s = list_first_block(l_scan);

        update_time = timems();
        
        log_info("---- Ahora Muestrea ----");
        while(lb_s)
        {
            sampler_tags = (SAMPLER_TAGS *)lb_samp->data;
            scan_tags = (s_TAG *)lb_s->data;

            if (ctr->state != ready )
                msleep(TIME_SEND/5);
         
            switch(scan_tags->type)
            {
                case type_string:
                    memcpy( sampler_tags->string, scan_tags->ptr, sampler_tags->len_type );
                    break;
                case type_int16:
                    vi16=*((int16_t*)scan_tags->ptr);
                    log_debug("int16_t %i",vi16);
                    sampler_tags->value = (double) vi16 ;
                    break;
                case type_int32:
                    vi32=*((int32_t*)scan_tags->ptr);
                    log_debug("int32_t %i",vi32);
                    sampler_tags->value = (double)vi32;
                    break;
                case typte_float:
                    vf32=*((float*)scan_tags->ptr);
                    log_debug("float_t %f",vf32);
                    sampler_tags->value = (double)vf32;
                    break;
                case typte_double:
                    vf64=*((double*)scan_tags->ptr);
                    log_debug("double_t %i",vf64);
                    sampler_tags->value = (double)vf64;
                    break;
            }
            
            if ( update_time < (*scan_tags->update_time + TIME_SEND) )
                sampler_tags->heartbeat=(TIME_STREAKY/TIME_SEND)+1;
            else
            {
                if (sampler_tags->heartbeat > 0 )
                    sampler_tags->heartbeat-=1;
            }
            
            log_msg("sampler_tags:: id(%i)  type(%i %i) heartbeat(%i) uptime scan %lld Value:%f",
                sampler_tags->id,
                sampler_tags->type,sampler_tags->len_type,
                sampler_tags->heartbeat,
                *scan_tags->update_time,
                sampler_tags->value);
               
            lb_s = list_next_block(l_scan, lb_s);
            lb_samp = list_next_block(l_sampler, lb_samp);
        }
        log_info(" - - - - - - - - - ");
        
        sampler_ctr->time_sampler=timems();
        
        n = sendto( sock_pub,l_sampler, sampler_ctr->length,0,(struct sockaddr *)&to, len );
        log_info("n:%i",n);
        
        msleep(TIME_SEND);
    }
    
    free(l_sampler);
    free(l_scan);
    log_msg("chau ----");
    eipc_deattach(eipc);
    return 1;
    
}
