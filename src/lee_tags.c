/* Example: parse a simple configuration file */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/inicfg.h"

typedef struct
{
    char tag_name[96];
    int index;
    double val_min;
    double val_max;
    unsigned active:1;
} configuration;

int ini_index=0;
char *pc_section = NULL;
configuration Config[3888];
configuration *pconfig;

static int handler(void* user, const char* tag, const char* line, const char* val)
{
    #define CMPN(n) (strcmp(line, n) == 0)
    
//    if( tag[0]=='#' || tag[0]==';') 
//        return 0;
    
    if( pc_section == NULL ){
        strcpy(Config[ini_index].tag_name, tag);
        pc_section = Config[ini_index].tag_name;
        Config[ini_index].index = ini_index;
    }
    
    if ( strcmp(tag, pc_section) )
    {
        ini_index += 1;
        strcpy(Config[ini_index].tag_name, tag);
        Config[ini_index].index = ini_index;
        pc_section = Config[ini_index].tag_name;
    }
    
    if CMPN("val_min"){
        Config[ini_index].val_min = atof(val);
    }else if CMPN("val_max"){
        Config[ini_index].val_max = atof(val); ;
    }else if CMPN("active"){
        Config[ini_index].active = atoi(val); ;
    }else {
        return 0;
    }
    return 1;
}

int main(int argc, char* argv[])
{
    int i;

    if (ini_parse("../cfg/tags.cfg", handler, NULL) < 0) {
        printf("Can't load '../cfg/tags.cfg'\n");
        return 1;
    }
    
    for (i=0; i<=ini_index; i++)
    {
        pconfig=&Config[i];
        printf("tag=%s::\n\tindex=%06i   val_min=%011.4f   val_max=%011.4f   active=%i\n",
            pconfig->tag_name, pconfig->index, pconfig->val_min, pconfig->val_max, pconfig->active);
    }   
//    printf("Config loaded from 'test.ini': version=%d, name=%s, email=%s active=%i\n",
//        config.version, config.name, config.email, config.active);
    return 0;
}
