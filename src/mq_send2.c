#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>
#define  MAX_TEXT 33


#include "../include/ipc_types.h"



struct my_msg_st
{
    long type;
    struct msg_signal signal;
};

int main()
{
    int running = 1;
    struct my_msg_st msg;
    int msgid;
    char buffer[BUFSIZ];
    
    msgid = mq_open("eipc_mq_server","rw");
    
    while(running)
    {
        printf("Enter Some Text: ");
        fgets(buffer, BUFSIZ, stdin);
        msg.type = MQ_SIGNAL;
        msg.signal.signal  = atoi(buffer);
        msg.signal.control = 1;
        strcpy(msg.signal.to_host,""); 
        strcpy(msg.signal.to_process,"test.py"); 

        if(msgsnd(msgid, (void *)&msg, sizeof(struct msg_signal), 0) == -1)
        {
            fprintf(stderr, "msgsnd failed\n");
            exit(EXIT_FAILURE);
        }
    }
    exit(EXIT_SUCCESS);
}
