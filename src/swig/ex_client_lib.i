 /* eClientlib.i */
 %module ex_client_lib
 %{
    #include "struct.h"
    extern void init_shm(void);
    extern int getid_tag(char *tag);
    extern double getvalue_tag(char *tag);
    extern double getvalue_id(int id);
    extern char *getstring_id(int id);
    extern char *getstring_tag(char *tag);
    extern SHM_TAGS * gettag(char *tag);
    
    extern eipc_t *eipc_attach(char *name);
    extern eipc_t *eipc_locate(char *name);
    extern int eipc_send_msg(eipc_t *eipc, char *to_host, char *process, int type, char *msgsd, int len, int timedout, int ack );
    extern int eipc_deattach(eipc_t *eipc);
 %} 
%include "struct.h"
extern void init_shm(void);
extern int getid_tag(char *tag);
extern double getvalue_tag(char *tag);
extern double getvalue_id(int id);
extern char *getstring_id(int id);
extern char *getstring_tag(char *tag);
extern SHM_TAGS * gettag(char *tag); 

extern eipc_t *eipc_attach(char *name);
extern eipc_t *eipc_locate(char *name);
extern int eipc_send_msg(eipc_t *eipc, char *to_host, char *process, int type, char *msgsd, int len, int timedout, int ack );
extern int eipc_deattach(eipc_t *eipc);
