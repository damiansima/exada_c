
typedef enum{type_int16=1, type_int32, typte_float, typte_double,type_string }e_TAG_TYPE;

typedef struct
{
    int id; // indices del tag, en variables ID de (db)
    union {
        struct {
            unsigned n1:1;
            unsigned n2:1;
            unsigned n3:1;
            unsigned fill:5;
        };
        char val;         
    }streaky;                 // tag rayado 1: nivel 1 2:nivel 2:
    char        heartbeat;       // nivel del heart beat
    char        heartbeat_sampler;
    char        rwa;
    char        tag[96];         // nombre del tag (db)
    e_TAG_TYPE  type;
    int         len_type;
    double      value_max;       // maximo (db) o seteado por applicacion
    double      value_min;       // minimo (db) o seteado por applicacio
    union{
        double  value;
        char string[8]; // el string puede ser mayor para eso se usa la libreria lista_contina.h
    }; 
}SHM_TAGS;


typedef void * eipc_t;
    