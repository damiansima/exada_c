
#include "../../include/utils.h"
#include "../../include/static_list.h"

#include <stdio.h>
#include <stdarg.h>

typedef short e_scan_t; // registros de 16 bits
typedef struct
{
    char name[96];
    int  addres;
    int  start;
    int  end;
    int  len_block;
    e_scan_t data[1];
}SCAN_BLOCK;

typedef struct
{
    timems_t now;
    enum  {busy, ready, writing }state;
}SCAN_CTR;

int f_addres;
int handler_find(LIST_BLOCK *b)
{
    SCAN_BLOCK *pb = (SCAN_BLOCK *)b->data;
    
    if(pb->addres == f_addres )
        return 1;
    return 0;
}


int main()
{
    log_set_file("/tmp/log.log");
    log_debug_level(255);
    log_logger_level(0);
    
    SCAN_CTR *ctr = (SCAN_CTR *)map_shm("Balanza_XX", "r", 0);  
    LIST_t *lshm = (LIST_t *)(ctr+1);
    SCAN_BLOCK *pb;
    LIST_BLOCK *lb;
    int i;
    
    
    f_addres=41;
    printf("find LSHM:%lu\n",(unsigned long)lshm);
    
    lb = list_generic_find(lshm,handler_find);
    if (lb==NULL)
        return 0;
    pb = (SCAN_BLOCK *)lb->data;
    printf("[%s] addres=%05i  start=%05i  end=%05i len=%05i\n", 
            pb->name, pb->addres, pb->start, pb->end, pb->len_block);

    printf("[ ");
    for(i=0; i<pb->len_block;i++)
    {
        printf("%04i ",pb->data[i]);
    }
    printf("]\n");

    lb = list_next_block(lshm, lb);
//    return 0;
    while(1)
    {
        lb = list_first_block(lshm);
        while(lb)
        {
            pb = (SCAN_BLOCK *)lb->data;

            printf("[%s] addres=%05i  start=%05i  end=%05i len=%05i\n", 
                    pb->name, pb->addres, pb->start, pb->end, pb->len_block);

            printf("[ ");
            for(i=0; i<pb->len_block;i++)
            {
                printf("%04i ",pb->data[i]);
            }
            printf("]\n");

            lb = list_next_block(lshm, lb);
        }
        printf("-----------\n");
        msleep(500);
    }
    
}
